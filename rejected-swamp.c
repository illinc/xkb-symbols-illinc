// ****************************************************************************
// Неактуальное и вряд ли нужное для справки — но пока оставлено 
// ****************************************************************************

// Алфавитный блок ЛАТ + ISO-запятая + общие для всех клавиатур определения
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat_iso_common" {    
    key.type[group1]="EIGHT_LEVEL";   
    
    include "illinc(lat_alph_legacy)"  // алфавитный блок ЛАТ
    include "illinc(common_iso_inner_comma_period)" // запятая и точка внутри алфавитного блока (ISO)     
    
    // слева
    //include "illinc(common_all_mod_lwin_rus)"
    include "illinc(common_all_mod_lwin_rus_CapsLk)"
    include "illinc(common_all_mod_caps_level3)"

    // справа
    // по умолчанию ralt = Alt
    //include "illinc(common_all_mod_ralt_temp_lat)"
    include "illinc(common_all_mod_ralt_tilde)"
    //include "illinc(common_all_mod_esc_temp_lat)"
    
    //include "illinc(common_all_mod_rwin_lat)"    
    //include "illinc(common_all_mod_menu_level5)"       
    include "illinc(common_all_mod_rwin_level5)"
    include "illinc(common_all_mod_menu_lat)"
    
    //key.type[group1]="EIGHT_LEVEL"; // CapsLock не должен оказывать эффект
    // Верхняя линия с оригинальными символами; восемь уровней
    // key <ESC>  { type="EIGHT_LEVEL", symbols[Group1]= [ Escape,	Escape,		Escape,	Escape		]	}; Esc более не трогаем, его на первом уровне не хватает
    // /usr/share/X11/xkb/types/pc ← там описан type "CTRL+ALT": 1 уровень без ничего, 2 Shift, 3 LevelThree, 4 LevelThree+Shift, 5 Control+Alt и названия клавиш
    // srvr_ctrl — оригинальные F1-F12; 
    // XF86_Switch_VT_x — переключение на виртуальную консоль x. А их всего 7 (добавить можно, но мне и этого пока много)!
    key <FK01> { type="CTRL+ALT", 	symbols[Group1]= [ F1, F1,  	F1, F1, 	XF86_Switch_VT_1 	] }; 
    key <FK02> { type="CTRL+ALT", 	symbols[Group1]= [ F2, F2,		F2, F2, 	XF86_Switch_VT_2 	] }; 
    key <FK03> { type="CTRL+ALT", 	symbols[Group1]= [ F3, F3,		F3, F3, 	XF86_Switch_VT_3 	] }; 
    key <FK04> { type="CTRL+ALT", 	symbols[Group1]= [ F4, F4,		F4, F4, 	XF86_Switch_VT_4 	] }; 

    key <FK05> { type="CTRL+ALT", 	symbols[Group1]= [ F5, F5,		F5, F5, 	XF86_Switch_VT_5 	] }; 
    key <FK06> { type="CTRL+ALT", 	symbols[Group1]= [ F6, F6,		F6, F6, 	XF86_Switch_VT_6 	] }; 
    key <FK07> { type="CTRL+ALT", 	symbols[Group1]= [ F7, F7,		F7, F7, 	XF86_Switch_VT_7 	] }; 
    key <FK08> {type="EIGHT_LEVEL",	symbols[Group1]= [ F8, F8,		F8, F8, 	VoidSymbol 			] }; 
    
    key <FK09> { type="EIGHT_LEVEL", symbols[Group1]= [ F9, F9, 	F9, F9, 	VoidSymbol 			] }; 
    key <FK10> { type="EIGHT_LEVEL", symbols[Group1]= [ F10, F10,	F10, F10, 	VoidSymbol 			] }; 
    key <FK11> { type="EIGHT_LEVEL", symbols[Group1]= [ F11, F11,	F11, F11, 	VoidSymbol 			] }; 
    key <FK12> { type="EIGHT_LEVEL", symbols[Group1]= [ F12, F12, 	F12, F12, 	VoidSymbol 			] }; 
    
    key <PRSC> { type="EIGHT_LEVEL", symbols[Group1]= [ Print, Sys_Req,				Print, 		Sys_Req, 		VoidSymbol		] }; // 
    key <SCLK> { type="EIGHT_LEVEL", symbols[Group1]= [ Scroll_Lock,Scroll_Lock,	Scroll_Lock,Scroll_Lock, 	VoidSymbol		] }; // 
    key <PAUS> { type="EIGHT_LEVEL", symbols[Group1]= [ Pause, 		Break, 	 		Pause, 		Break, 			VoidSymbol		] }; //  
    
    // Цифровой блок с цифрами; восемь уровней; NumLock как Level3
    include "illinc(common_all_mod_num_level3)"
   
    key.type[group1]="EIGHT_LEVEL";    
    key <KPDV> {[ KP_Divide, KP_Divide,	KP_Divide,	KP_Divide,		U2298,		U2298,		VoidSymbol,	VoidSymbol	]}; // // // ⊘⊘
    key <KPMU> {[ asterisk,	asterisk,	asterisk,	asterisk,		U2299,		U2297,		VoidSymbol,	VoidSymbol	]}; // ** ** ⊙⊗
    key <KPSU> {[ minus,	minus,		minus,	minus,				U2296,		U2296,		VoidSymbol,	VoidSymbol	]}; // -- -- ⊖⊖
    key <KPAD> {[ plus,		plus,		plus,	plus,				U2295,		U2295,		VoidSymbol,	VoidSymbol	]}; // ++ ++ ⊕⊕ верхняя высокая справа
    
    key  <KP7> {[  7,	7,				7,	VoidSymbol,			7,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP8> {[  8,	8,				8,	VoidSymbol,			8,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP9> {[  9,	9,				9,	VoidSymbol,			9,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};       

    key  <KP4> {[  4,	4,				4,	VoidSymbol,			4,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP5> {[  5,	5,				5,	VoidSymbol,			5,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP6> {[  6,	6,				6,	VoidSymbol,			6,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};

    key  <KP1> {[  1,	1,				1,	VoidSymbol,			1,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP2> {[  2,	2,				2,	VoidSymbol,			2,	VoidSymbol,			VoidSymbol,	VoidSymbol	]};
    key  <KP3> {[  3,	3,				3,	VoidSymbol,			3,	VoidSymbol,			VoidSymbol,	VoidSymbol	]}; 
        
    key <KPEN> {[ KP_Enter, KP_Enter, 	KP_Enter, KP_Enter,		KP_Enter,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // нижняя высокая справа

    key  <KP0> {[ 0,	0,				0,	VoidSymbol,			0,	VoidSymbol,			VoidSymbol,	VoidSymbol	]}; // горизонтальная широкая в нижнем ряду 
    key <KPDL> {[ period, period, 		period, period,			period,	period,			VoidSymbol,	VoidSymbol	]}; // одноместная в нижнем ряду 
     
    // Прячем Insert на 3 уровень, ибо он случайно нажимается чаще, чем по делу
    // а) на Ins тире и в 88, и в 104; б) Shift+Ins всё равно обрабатывается как вставка
    key <INS> {[ emdash, Insert, Insert, Insert]	};         
   
    // Пробел
    //			без ничего	Shift		3			3+Shift			5			5+Shift		5+3				5+3+Shift
    key <SPCE> {[	 space,	asciitilde,	asciitilde,	nobreakspace,	asciitilde,	asciitilde,	signifblank,	nobreakspace		]}; //   ~ ~ nbsp    ~ ~ ␣ nbsp

    
    // Минус и равенство
    key <AE11> {[	minus,	underscore,	emdash,			endash,			U2212,		figdash,	plusminus,	U2213		]}; // - _ —(длинное тире) –(EN DASH)    −(минус ширины +) ‒ (цифровое тире: выше короткого, но ниже минуса) ± ∓
    key	<AE12> {[	equal,	plus,		approxeq,		notequal,		plus,   	U2295,		identical,	notidentical]};	// = + ≈ ≠    + ⊕ ≡ ≢    
};
// ****************************************************************************
// Можно на F1-F12 в Chromium:  _:; ? %&«» /"' + `(grave) <>{}[]| ~ ,. × 0123456789
// нельзя на F1-F12 в Chromium: ^ ! —(тире) „” * @# −(минус ширины +) №§ ←→ ()
// так что tw-вариант с 0123456789"/ нормально работает
// в других приложениях вроде вообще всё работает, в основном блоке в Chromium — тоже



// ****************************************************************************


partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat_iso_2021_base_digitsline" {    
    include "illinc(lat_iso_common)"    
    key.type="EIGHT_LEVEL";    
    // Цифры — вариант 2021: нижние два уровня цифр соответствуют en; знаки препинания с ru-winkeys не учитываются 
    key <TLDE> {[     0,	grave,		grave,			infinity,		emptyset,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // 0 ` ` ∞    ∅
    key <AE01> {[	  1,	exclam,		exclam,			exclam,			notsign,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // 1 ! ! !    ¬ 
    key <AE02> {[	  2,	at,			at,				U22C4,			U27E1,		VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // 2 @ @ ⋄    ⟡ 
    key <AE03> {[	  3,	numbersign,	numbersign,		numerosign,		VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	]};	// 3 # # №
    key <AE04> {[	  4,	dollar,		dollar,			section,		VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // 4 $ $ §    
    key <AE05> {[	  5,	percent,	percent,		percent,		logicalor,	union,		VoidSymbol,	VoidSymbol	]}; // 5 % % %    ∨ ∪
    key <AE06> {[	  6,	asciicircum,asciicircum,	degree,			U2295,		U2295,		VoidSymbol,	VoidSymbol	]}; // 6 ^ ^ °    ⊕ ⊕ 
    key <AE07> {[	  7,	ampersand,	ampersand,		ampersand,		logicaland,	intersection,VoidSymbol,VoidSymbol	]}; // 7 & & &    ∧ ∩
    key <AE08> {[	  8,	asterisk,	asterisk, 	enfilledcircbullet,	multiply,	U22C5,		VoidSymbol,	VoidSymbol	]}; // 8 * * •    × ⋅
    key <AE09> {[	  9,	parenleft,  guillemotleft, 	doublelowquotemark,	U2286,	U2288,		includedin,	U2284		]}; // 9 ( « „    ⊆ ⊈ ⊂ ⊄
    key <AE10> {[	  0,	parenright, guillemotright,	rightdoublequotemark,U2287,	U2289,		includes,	U2285		]}; // 0 ) » ”    ⊇ ⊉ ⊃ ⊅
};


// ****************************************************************************

// ****************************************************************************
// Варианты раскладки (XKBVARIANT) полные *************************************
// то, что надо подключать в /etc/default/keyboard 
// ****************************************************************************

// ****************************************************************************
// Полная раскладка для 88, цифры как в us (но буквы — нет!)
//$ setxkbmap "illinc(lat88us),illinc(rus88us)"
partial alphanumeric_keys function_keys modifier_keys xkb_symbols "lat88us" {
    include "illinc(lat_iso_2021_base_digitsline)"    
    include "illinc(common_88_sysbrk_latexcommand_latexmath_level3)" // \Лат, правый L3 и $Лат — в верхнем правом углу (SysRq, ScrollLock, Break)   
};
partial alphanumeric_keys modifier_keys xkb_symbols "rus88us" { include "illinc(lat88us)" include "illinc(rus_alph)" };

// ****************************************************************************

// ****************************************************************************
// Полная раскладка для 105-104, цифры как в us
// Вариант для невыдвигающейся полки (знаки препинания на цифровом блоке, F1-F12 можно оставить как есть или переопределить скобками)
// для кафедры, где до верхней линии не дотянуться

//$ setxkbmap "illinc(lat105gap),illinc(rus105gap)"
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat105gap" {
    include "illinc(lat_iso_2021_base_digitsline)"     // определение в том числе верхних уровней линии цифр (совместимых с надписями 2021)
    
    // Скобки на первом уровне F1-F12 — для 105 и 104, где основные знаки препинания на цифровом блоке
    key <FK01> {[ grave,		grave		]};	
    key <FK02> {[ underscore,	overline	]};
    key <FK03> {[ at,			at			]};	
    key <FK04> {[ numbersign,	numbersign	]};
   
    key <FK05> {[ parenleft,	parenleft	]};
    key <FK06> {[ parenright,	parenright	]};        
    key <FK07> {[ less,			less		]};
    key <FK08> {[ greater,		greater		]};
    
    key <FK09> {[ braceleft,	braceleft	]};
    key <FK10> {[ braceright,	braceright	]};
    key <FK11> {[ bracketleft,	bracketleft	]};
    key <FK12> {[ bracketright,	bracketright]}; 
    
    key <PRSC> {[ bar,			bar			] };
    // Кавычки-ёлочки над Home и PageUp (на ScrollLock и Break, но их в полке уже не видно)
    key <SCLK> {[ guillemotleft,	doublelowquotemark,		Scroll_Lock,	Scroll_Lock	] }; 	// « 
    key <PAUS> {[ guillemotright,	rightdoublequotemark,	Pause, 			Break		] }; 	// »     
    
    // Для ISO-раскладки (точка и запятая в основном блоке) с цифровым блоком — вариант как в заказе 2021
    
    key  <KP7> {[  asciicircum,	parenleft		]};
    key  <KP8> {[  percent,		parenright		]};
    key  <KP9> {[  quotedbl,	quotedbl		]};       

    key  <KP4> {[  question,	braceleft		]};
    key  <KP5> {[  exclam,		braceright		]};
    key  <KP6> {[  apostrophe,	bar				]};

    key  <KP1> {[  semicolon,	bracketleft		]};
    key  <KP2> {[  colon,		bracketright	]};
    key  <KP3> {[  ampersand,	apostrophe		]}; // & ' 
    
    // $Лат на  KPEN  как напечатано — на втором уровне тоже $        
    key <KPEN> {
     type[Group1]="EIGHT_LEVEL",
        symbols[Group1] = [ dollar, dollar, 	KP_Enter, KP_Enter, 	dollar, dollar, 	dollar, dollar	 	],
        actions[Group1]=[ ],
        actions[Group2]=[ LockGroup(group=-1) ],
        actions[Group3]=[ LockGroup(group=-2) ],
        actions[Group4]=[ LockGroup(group=-3) ]
    };    
   
    key  <KP0> {[  underscore,	    less		]}; 
    // \Лат на  KPDL, на втором уровне greater
    key <KPDL> {
     type[Group1]="EIGHT_LEVEL",
        symbols[Group1] = [ backslash, greater, 	period, period, 		backslash, backslash,	backslash, backslash],
        actions[Group1]=[ ],
        actions[Group2]=[ LockGroup(group=-1) ],
        actions[Group3]=[ LockGroup(group=-2) ],
        actions[Group4]=[ LockGroup(group=-3) ]
    };      
    
};
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "rus105gap" { include "illinc(lat105gap)" include "illinc(rus_alph)" };

// 105-104 Upd. Полку удалось расклинить и вынимать клавиатуру нормально
// знаки препинания на самой верхней линии — всё равно оказалось неудобно
// пробуем знаки препинания на линии цифр, как на пишмашинке (а цифры на самой верхней линии)

// *****************************************************************************


// Полная раскладка для 88 текущая 
//$ setxkbmap "illinc(lat88),illinc(rus88)"
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat88" { include "illinc(lat88tw)"    }; 
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "rus88" { include "illinc(lat88)" include "illinc(rus_alph)" };

// Полная раскладка для 105 текущая 
//$ setxkbmap "illinc(lat105),illinc(rus105)"
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat105" { include "illinc(lat105tw)"    }; 
partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "rus105" { include "illinc(lat105)" include "illinc(rus_alph)" };



// Here's a page with unicode symbol equivalents for all of the printable key symbols. 
// http://cs.gmu.edu/~sean/stuff/n800/keyboard/keysymdef.html
↑ недоступна




// ****************************************************************************

partial function_keys xkb_symbols "common_88_F_testing_patch" {
    //replace key <FK04> {        type[Group1]="EIGHT_LEVEL_ALPHABETIC",        symbols[Group1] = [ F4 ]        }; // Alt+F4 работает
    //replace key <FK04> {        type[Group1]="EIGHT_LEVEL_ALPHABETIC",        symbols[Group1] = [ semicolon, 		semicolon, F4 ]        }; а так нет
    key <FK04> {	type="CTRL+ALT_1",	symbols[Group1]= [ semicolon, 	semicolon, F4, F4, XF86_Switch_VT_4 ]    }; // Alt+F4 не работает, но Ctrl+F4 = F4
    
    // key <PRSC> {[ question	]};	// ?  ← получается ?лат, то есть действия сохраняются
   
};


// NumLock как ЛатВрем (пока нажат) + на третьем уровне оригинальный NumLock
partial modifier_keys xkb_symbols "common_all_mod_num_temp_lat" {
    replace key <NMLK> {
        type[Group1]="FOUR_LEVEL",
        symbols[Group1] = [ VoidSymbol, VoidSymbol, Num_Lock, Num_Lock  ],
        actions[Group1]=[ ],
        actions[Group2]=[ SetGroup(group=-1) ],
        actions[Group3]=[ SetGroup(group=-2) ],
        actions[Group4]=[ SetGroup(group=-3) ]
    };
};
↑ не работает


// Menu как включатель латиницы и Shift+Menu как Compose (Compose = Multi_key)
// см. ~/.XCompose
partial modifier_keys xkb_symbols "common_all_mod_menu_lat_multi" {
    key <MENU> {    type[Group1]="TWO_LEVEL",    symbols[Group1] = [ ISO_First_Group, Multi_key] };
};

// partial modifier_keys xkb_symbols "common_all_mod_menu_lat_lattmp" {
//     key <MENU> {    type[Group1]="EIGHT_LEVEL",    
//         symbols[Group1] = [ ISO_First_Group, VoidSymbol, VoidSymbol], 
//         actions[Group1]=[ ],
//         actions[Group2]=[ LockGroup(group=-1), SetGroup(group=0), SetGroup(group=0) ],
//         actions[Group3]=[ LockGroup(group=-2), SetGroup(group=0), SetGroup(group=0) ],
//         actions[Group4]=[ LockGroup(group=-3), SetGroup(group=0), SetGroup(group=0) ]
//         
//     };
// };

// partial modifier_keys xkb_symbols "common_all_mod_menu_lat_lattmp" {
//     key <MENU> {    type[Group1]="EIGHT_LEVEL",    
//         symbols[Group1] = [ ISO_First_Group, VoidSymbol, VoidSymbol], 
//         actions[Group1]=[ ],
//         actions[Group2]=[ SetGroup(group=0), SetGroup(group=-1) ],
//         actions[Group3]=[ SetGroup(group=0), SetGroup(group=-1) ],
//         actions[Group4]=[ SetGroup(group=0), SetGroup(group=-1) ]
//         
//     };
// };

// partial modifier_keys xkb_symbols "common_all_mod_menu_lat_NumLk" {
//     key <MENU> {    type[Group1]="FOUR_LEVEL",    symbols[Group1] = [ ISO_First_Group, ISO_First_Group, Num_Lock] };    
// };


// не работает: не включает русский
// partial modifier_keys xkb_symbols "common_all_mod_lwin_rus__temp_lat" {
//      replace key <LWIN> {
//         type[Group1]="ONE_LEVEL",
//         symbols[Group1] = [ ISO_Last_Group ],
//         actions[Group1]=[ ],
//         actions[Group2]=[ SetGroup(group=-1) ],
//         actions[Group3]=[ SetGroup(group=-2) ],
//         actions[Group4]=[ SetGroup(group=-3) ]
//     };
// };
partial modifier_keys xkb_symbols "common_all_mod_menu_temp_lat" {
     replace key <MENU> {
        type[Group1]="ONE_LEVEL",
        symbols[Group1] = [ VoidSymbol ],
        actions[Group1]=[ ],
        actions[Group2]=[ SetGroup(group=-1) ],
        actions[Group3]=[ SetGroup(group=-2) ],
        actions[Group4]=[ SetGroup(group=-3) ]
    };
};

// RAlt как тильда отдельно и как временный включатель латиницы пока нажат
// как ни странно, работает — при наборе буквы с ЛатВрем тильда не воспроизводится 
// ↑↑ upd: не работает, тильда всё-таки есть

// Проверим, не будет ли Escape более безобидным символом
// в основном окне ЛатВрем работает нормально, но диалоговое окно (сохранение файла) закрывается
// в консоли в сочетании с некоторыми символами Escape работает как esc-символ
partial modifier_keys xkb_symbols "common_all_mod_esc_temp_lat" {
     replace key <ESC> {
        type[Group1]="TWO_LEVEL",		symbols[Group1] = [ Escape, Escape ],
        actions[Group1]=[ ],
        actions[Group2]=[ SetGroup(group=-1) ],
        actions[Group3]=[ SetGroup(group=-2) ],
        actions[Group4]=[ SetGroup(group=-3) ]
    };
};




partial function_keys xkb_symbols "common_all_Fline_digits" {    
    include "illinc(common_all_Fline_digits_1_24)"
};

// (-)Консоли переключаются не по Ctrl+Alt, а по Level5 — м. б. поставить везде EIGHT_LEVEL?
// после удаления многократного переопределения F1-F12 переключаются по Ctrl+Alt — где-то было ошибочное указание типа
partial function_keys xkb_symbols "common_all_Fline_digits_1_12" {    
    key.type="CTRL+ALT";    
    key <FK01> { type="CTRL+ALT", 	symbols[Group1]= [ 0, 0, 		F1, F1, 	XF86_Switch_VT_1 	] }; 
    key <FK02> { type="CTRL+ALT", 	symbols[Group1]= [ 1, 1,		F2, F2, 	XF86_Switch_VT_2 	] }; 
    key <FK03> { type="CTRL+ALT", 	symbols[Group1]= [ 2, 2,		F3, F3, 	XF86_Switch_VT_3 	] }; 
    key <FK04> { type="CTRL+ALT", 	symbols[Group1]= [ 3, 3,		F4, F4, 	XF86_Switch_VT_4 	] }; 

    key <FK05> { type="CTRL+ALT", 	symbols[Group1]= [ 4, 4,		F5, F5, 	XF86_Switch_VT_5 	] }; 
    key <FK06> { type="CTRL+ALT", 	symbols[Group1]= [ 5, 5,		F6, F6, 	XF86_Switch_VT_6 	] }; 
    key <FK07> { type="CTRL+ALT", 	symbols[Group1]= [ 6, 6,		F7, F7, 	XF86_Switch_VT_7 	] }; 
    key <FK08> {type="EIGHT_LEVEL",	symbols[Group1]= [ 7, 7,		F8, F8, 	U22C5 				] }; 
    
    key <FK09> { type="EIGHT_LEVEL", symbols[Group1]= [ 8, 8, 		F9, F9, 	VoidSymbol 			] }; 
    key <FK10> { type="EIGHT_LEVEL", symbols[Group1]= [ 9, 9,		F10, F10, 	VoidSymbol 			] }; 
    key <FK11> { type="EIGHT_LEVEL", symbols[Group1]= [ quotedbl,	quotedbl,		F11, F11, 	VoidSymbol 			] }; 
    key <FK12> { type="EIGHT_LEVEL", symbols[Group1]= [ ampersand, 	ampersand, 		F12, F12, 	VoidSymbol 			] }; 
};

partial function_keys xkb_symbols "common_all_Fline_digits_1_24" {    
    key.type="CTRL+ALT";    
    key <FK13> { type="CTRL+ALT", symbols[Group1]= [ F1, F1, F1, F1, XF86_Switch_VT_1 ] };
    key <FK14> { type="CTRL+ALT", symbols[Group1]= [ F2, F2, F2, F2, XF86_Switch_VT_2 ] };
    key <FK15> { type="CTRL+ALT", symbols[Group1]= [ F3, F3, F3, F3, XF86_Switch_VT_3 ] };
    replace key <FK16> { type="CTRL+ALT", symbols[Group1]= [ F4, F4, F4, F4, XF86_Switch_VT_4 ] };
    key <FK17> { type="CTRL+ALT", symbols[Group1]= [ F5, F5, F5, F5, XF86_Switch_VT_5 ] };
    key <FK18> { type="CTRL+ALT", symbols[Group1]= [ F6, F6, F6, F6, XF86_Switch_VT_6 ] };
    key <FK19> { type="CTRL+ALT", symbols[Group1]= [ F7, F7, F7, F7, XF86_Switch_VT_7 ] };
    key <FK20> { type="CTRL+ALT", symbols[Group1]= [ F8, F8, F8, F8, XF86_Switch_VT_8 ] };
    key <FK21> { type="CTRL+ALT", symbols[Group1]= [ F9, F9, F9, F9, XF86_Switch_VT_9 ] };
    key <FK22> { type="CTRL+ALT", symbols[Group1]= [ F10, F10, F10, F10, XF86_Switch_VT_10 ] };
    key <FK23> { type="CTRL+ALT", symbols[Group1]= [ F11, F11, F11, F11, XF86_Switch_VT_11 ] };
    key <FK24> { type="CTRL+ALT", symbols[Group1]= [ F12, F12, F12, F12, XF86_Switch_VT_12 ] };

    //replace key <FK16> { type="CTRL+ALT", symbols[Group1]= [ j, e, F4, F4, XF86_Switch_VT_4 ] };
    // ↑ работает только на русском языке (см. Sleep на кафедре и ноутбук)
    
    key <FK01> { type="CTRL+ALT", 	symbols[Group1]= [ 0, 0,		F1, F1, 	XF86_Switch_VT_1 	] }; 
    key <FK02> { type="CTRL+ALT", 	symbols[Group1]= [ 1, 1,		F2, F2, 	XF86_Switch_VT_2 	] }; 
    key <FK03> { type="CTRL+ALT", 	symbols[Group1]= [ 2, 2,		F3, F3, 	XF86_Switch_VT_3 	] }; 
    key <FK04> { type="CTRL+ALT", 	symbols[Group1]= [ 3, 3,		F4, F4, 	XF86_Switch_VT_4 	],	
        actions[Group1]= [      NoAction(),      NoAction(),   RedirectKey(keycode=<FK16>, clearmods=levelThree),   RedirectKey(keycode=<FK16>, clearmods=levelThree) ] 
        
    }; 
    // ↑ и Alt+F4 всё равно не закрывает окно

    key <FK05> { type="CTRL+ALT", 	symbols[Group1]= [ 4, 4,		F5, F5, 	XF86_Switch_VT_5 	] }; 
    key <FK06> { type="CTRL+ALT", 	symbols[Group1]= [ 5, 5,		F6, F6, 	XF86_Switch_VT_6 	] }; 
    key <FK07> { type="CTRL+ALT", 	symbols[Group1]= [ 6, 6,		F7, F7, 	XF86_Switch_VT_7 	] }; 
    key <FK08> {type="EIGHT_LEVEL",	symbols[Group1]= [ 7, 7,		F8, F8, 	U22C5 				] }; 
    
    key <FK09> { type="EIGHT_LEVEL", symbols[Group1]= [ 8, 8, 		F9, F9, 	VoidSymbol 			] }; 
    key <FK10> { type="EIGHT_LEVEL", symbols[Group1]= [ 9, 9,		F10, F10, 	VoidSymbol 			] }; 
    key <FK11> { type="EIGHT_LEVEL", symbols[Group1]= [ quotedbl,	quotedbl,		F11, F11, 	VoidSymbol 			] }; 
    key <FK12> { type="EIGHT_LEVEL", symbols[Group1]= [ ampersand, 	ampersand, 		F12, F12, 	VoidSymbol 			] }; 
    

    
};

// ****************************************************************************
// Варианты линии цифр послойно
// ****************************************************************************

// Цифр на линии цифр нет!
// 3: то, что в us было на втором (кроме ~, которая на пробеле, и (), которые остались на втором); еще \ и „”
// 4: ∞'"№§ в начале линии цифр — так как слева проще нажать Level3+Shift, чем один Level5
// 5: ⋅⊆⊇ в конце линии цифр — так как справа, наоборот, проще тянуться от Level5 
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_nodigits" {    
    include "illinc(common_digitsline_layers_base)" 
    key.type="EIGHT_LEVEL"; 

    include "illinc(common_digitsline_layers_56_ru_math_symbols)"  // 5: ∞'"№§‰⋅⊆⊇;    6: °′″‴‱⋅⊈⊉ 
    // символы ∞'"№§‰ в левой части 5 дублируем или переносим на 4
    key <TLDE> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	infinity						]}; // ∞ и на 5 уровне остаётся
    key <AE01> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	apostrophe,			VoidSymbol	]}; // ' 
    key <AE02> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	quotedbl,			VoidSymbol	]}; // "     
    key <AE03> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	numerosign,			VoidSymbol	]}; // № и на 5 дырка, ибо это символ не математический    
    key <AE04> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	section,			VoidSymbol	]}; // § и на 5 дырка, аналогично 
    key <AE05> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	permille						]}; // ‰ 
    
    key <AE08> {[ VoidSymbol,VoidSymbol,		VoidSymbol, asterisk	]}; // *       
   
    include "illinc(common_digitsline_layers_3_us_goosefeet_symbols)" 
    include "illinc(common_digitsline_layers_12_punctuation_2022isokb)" 
};

// Цифры на 3 уровне
// символы третьего уровня nodigits ⇒ на 4 без изменения порядка; правая часть четвёртого и левая часть пятого nodigits ⇒ на 5
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_digits_in_3" {    
    include "illinc(common_digitsline_layers_base)" 
    include "illinc(common_digitsline_layers_56_ru_math_symbols)"  // 5: ∞'"№§‰⋅⊆⊇;    6: °′″‴‱⋅⊈⊉ 
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ VoidSymbol,VoidSymbol,		0,	backslash				]}; // 0 эскейп-слеш, который даже в комментарии работает
    key <AE01> {[ VoidSymbol,VoidSymbol,		1,	exclam					]}; // 1 !
    key <AE02> {[ VoidSymbol,VoidSymbol,		2,	at						]}; // 2 @
    key <AE03> {[ VoidSymbol,VoidSymbol,		3,	numbersign				]}; // 3 #
    key <AE04> {[ VoidSymbol,VoidSymbol,		4,	dollar					]}; // 4 $
    key <AE05> {[ VoidSymbol,VoidSymbol,		5,	percent					]}; // 5 %
    key <AE06> {[ VoidSymbol,VoidSymbol,		6,	asciicircum				]}; // 6 ^
    key <AE07> {[ VoidSymbol,VoidSymbol,		7,	ampersand				]}; // 7 &
    key <AE08> {[ VoidSymbol,VoidSymbol,		8,	asterisk 				]}; // 8 *
    key <AE09> {[ VoidSymbol,VoidSymbol,		9,	doublelowquotemark		]}; // 9 „
    key <AE10> {[ VoidSymbol,VoidSymbol,		0,	rightdoublequotemark	]}; // 0 ”   
    include "illinc(common_digitsline_layers_12_punctuation_2022isokb)" 
};

// Цифры на 4 уровне
// символы третьего уровня nodigits остаются на 3; правая часть четвёртого и левая часть пятого nodigits ⇒ на 5, как в digits_in_3
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_digits_in_4" {    
    include "illinc(common_digitsline_layers_base)" 
    include "illinc(common_digitsline_layers_56_ru_math_symbols)"  // 5: ∞'"№§‰⋅⊆⊇;    6: °′″‴‱⋅⊈⊉  
    // 4: цифры
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	0	]}; // 0 
    key <AE01> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	1	]}; // 1 
    key <AE02> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	2	]}; // 2 
    key <AE03> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	3	]}; // 3 
    key <AE04> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	4	]}; // 4
    key <AE05> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	5	]}; // 5 
    key <AE06> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	6	]}; // 6 
    key <AE07> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	7	]}; // 7
    key <AE08> {[ VoidSymbol,VoidSymbol,		VoidSymbol, 8	]}; // 8 
    key <AE09> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	9	]}; // 9
    key <AE10> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	0	]}; // 0        
    include "illinc(common_digitsline_layers_3_us_goosefeet_symbols)"  // 3: backslash, us-символы и „” (третий уровень nodigits)
    include "illinc(common_digitsline_layers_12_punctuation_2022isokb)" 
};
<<<<<<< HEAD

// Цифры на 5 и 6 уровнях (дублируются)
// символы третьего уровня nodigits остаются на 3; правая часть четвёртого и левая часть пятого nodigits ⇒ на 4
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_digits_in_5" {    
    include "illinc(common_digitsline_layers_base)" 
    key.type="EIGHT_LEVEL"; 
    key <TLDE> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	infinity,					0,	0				]}; // ∞    0 0 
    key <AE01> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	apostrophe,					1,	1				]}; // '    1 1 
    key <AE02> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	quotedbl,					2,	2				]}; // "    2 2 
    key <AE03> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	numerosign,					3,	3				]}; // №    3 3 
    key <AE04> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	section,					4,	4				]}; // §    4 4
    key <AE05> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	permille,					5,	5				]}; // ‰    5 5 
    key <AE06> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	degree,						6,	6				]}; // °    6 6 
    key <AE07> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	ampersand,					7,	7				]}; // &    7 7
    key <AE08> {[ VoidSymbol,VoidSymbol,		VoidSymbol, U22C5,						8,	8				]}; // ⋅    8 8 
    key <AE09> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	doublelowquotemark,			9,	9				]}; // „    9 9
    key <AE10> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	rightdoublequotemark,		0,	0				]}; // ”    0 0    
    include "illinc(common_digitsline_layers_3_us_goosefeet_symbols)" 
    include "illinc(common_digitsline_layers_12_punctuation_2022isokb)" 
};



// Знаки препинания (без точки и запятой) на 1-2 уровнях
partial alphanumeric_keys xkb_symbols "common_digitsline_layers_12_punctuation_2022isokb" {     
    
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ asciicircum,		grave			]}; // ^ ` 
    key <AE01> {[ percent,			bar				]}; // % | 
    key <AE02> {[ slash,			less			]}; // / < 
    key <AE03> {[ underscore,		greater			]};	// _ > 
    key <AE04> {[ colon,			bracketleft		]}; // : [ 
    key <AE05> {[ semicolon, 		bracketright	]}; // ; ] 
    key <AE06> {[ question,			braceleft		]}; // ? { 
    key <AE07> {[ exclam,			braceright		]}; // ! } 
    key <AE08> {[ asterisk,			asterisk		]}; // * * 
    key <AE09> {[ guillemotleft,	parenleft		]}; // « ( 
    key <AE10> {[ guillemotright,	parenright		]}; // » ) 
};

// Символы второго уровня us-раскладки, кроме скобок; кавычки-лапки; обратный слеш \, чтобы был где-то даже при отсутствии \Лат
// на 3 уровне
partial alphanumeric_keys xkb_symbols "common_digitsline_layers_3_us_goosefeet_symbols" {     
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ VoidSymbol,VoidSymbol,	backslash			]}; // эскейп-слеш, который даже в комментарии работает
    key <AE01> {[ VoidSymbol,VoidSymbol,	exclam				]}; // !
    key <AE02> {[ VoidSymbol,VoidSymbol,	at					]}; // @
    key <AE03> {[ VoidSymbol,VoidSymbol,	numbersign			]}; // #
    key <AE04> {[ VoidSymbol,VoidSymbol,	dollar				]}; // $
    key <AE05> {[ VoidSymbol,VoidSymbol,	percent				]}; // %
    key <AE06> {[ VoidSymbol,VoidSymbol,	asciicircum			]}; // ^
    key <AE07> {[ VoidSymbol,VoidSymbol,	ampersand			]}; // &
    key <AE08> {[ VoidSymbol,VoidSymbol,	asterisk 			]}; // *
    key <AE09> {[ VoidSymbol,VoidSymbol,	doublelowquotemark	]}; // „
    key <AE10> {[ VoidSymbol,VoidSymbol,	rightdoublequotemark]}; // ”   
};

=======

// Цифры на 5 и 6 уровнях (дублируются)
// символы третьего уровня nodigits остаются на 3; правая часть четвёртого и левая часть пятого nodigits ⇒ на 4
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_digits_in_5" {    
    include "illinc(common_digitsline_layers_base)" 
    key.type="EIGHT_LEVEL"; 
    key <TLDE> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	infinity,					0,	0				]}; // ∞    0 0 
    key <AE01> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	apostrophe,					1,	1				]}; // '    1 1 
    key <AE02> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	quotedbl,					2,	2				]}; // "    2 2 
    key <AE03> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	numerosign,					3,	3				]}; // №    3 3 
    key <AE04> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	section,					4,	4				]}; // §    4 4
    key <AE05> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	permille,					5,	5				]}; // ‰    5 5 
    key <AE06> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	degree,						6,	6				]}; // °    6 6 
    key <AE07> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	ampersand,					7,	7				]}; // &    7 7
    key <AE08> {[ VoidSymbol,VoidSymbol,		VoidSymbol, U22C5,						8,	8				]}; // ⋅    8 8 
    key <AE09> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	doublelowquotemark,			9,	9				]}; // „    9 9
    key <AE10> {[ VoidSymbol,VoidSymbol,		VoidSymbol,	rightdoublequotemark,		0,	0				]}; // ”    0 0    
    include "illinc(common_digitsline_layers_3_us_goosefeet_symbols)" 
    include "illinc(common_digitsline_layers_12_punctuation_2022isokb)" 
};



// Знаки препинания (без точки и запятой) на 1-2 уровнях
partial alphanumeric_keys xkb_symbols "common_digitsline_layers_12_punctuation_2022isokb" {     
    
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ asciicircum,		grave			]}; // ^ ` 
    key <AE01> {[ percent,			bar				]}; // % | 
    key <AE02> {[ slash,			less			]}; // / < 
    key <AE03> {[ underscore,		greater			]};	// _ > 
    key <AE04> {[ colon,			bracketleft		]}; // : [ 
    key <AE05> {[ semicolon, 		bracketright	]}; // ; ] 
    key <AE06> {[ question,			braceleft		]}; // ? { 
    key <AE07> {[ exclam,			braceright		]}; // ! } 
    key <AE08> {[ asterisk,			asterisk		]}; // * * 
    key <AE09> {[ guillemotleft,	parenleft		]}; // « ( 
    key <AE10> {[ guillemotright,	parenright		]}; // » ) 
};

// Символы второго уровня us-раскладки, кроме скобок; кавычки-лапки; обратный слеш \, чтобы был где-то даже при отсутствии \Лат
// на 3 уровне
partial alphanumeric_keys xkb_symbols "common_digitsline_layers_3_us_goosefeet_symbols" {     
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ VoidSymbol,VoidSymbol,	backslash			]}; // эскейп-слеш, который даже в комментарии работает
    key <AE01> {[ VoidSymbol,VoidSymbol,	exclam				]}; // !
    key <AE02> {[ VoidSymbol,VoidSymbol,	at					]}; // @
    key <AE03> {[ VoidSymbol,VoidSymbol,	numbersign			]}; // #
    key <AE04> {[ VoidSymbol,VoidSymbol,	dollar				]}; // $
    key <AE05> {[ VoidSymbol,VoidSymbol,	percent				]}; // %
    key <AE06> {[ VoidSymbol,VoidSymbol,	asciicircum			]}; // ^
    key <AE07> {[ VoidSymbol,VoidSymbol,	ampersand			]}; // &
    key <AE08> {[ VoidSymbol,VoidSymbol,	asterisk 			]}; // *
    key <AE09> {[ VoidSymbol,VoidSymbol,	doublelowquotemark	]}; // „
    key <AE10> {[ VoidSymbol,VoidSymbol,	rightdoublequotemark]}; // ”   
};

>>>>>>> ba97fbec236da87e064daecfc2481d2e78314fc8
// Смесь из текстовых символов (которые должны быть на 3-4 уровнях, но не влезли) и математических
// все на 5-6 уровнях
partial alphanumeric_keys xkb_symbols "common_digitsline_layers_56_ru_math_symbols" {    
    key.type="EIGHT_LEVEL";    
    key <TLDE> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		infinity,	degree		]}; // ∞ °
    key <AE01> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		apostrophe,	minutes		]}; // ' ′
    key <AE02> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		quotedbl,	seconds		]}; // " ″
    key <AE03> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		numerosign,	U2034		]}; // № ‴
    key <AE04> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		section,	VoidSymbol	]}; // § 
    key <AE05> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		permille,	U2031		]}; // ‰ ‱
    key <AE06> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // 
    key <AE07> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		VoidSymbol,	VoidSymbol	]}; //  
    key <AE08> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		U22C5,		multiply	]}; // ⋅ ×
    key <AE09> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		U2286,		U2288		]}; // ⊆ ⊈
    key <AE10> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,		U2287,		U2289		]}; // ⊇ ⊉       
};  


// ****************************************************************************
// Знаки препинания на линии цифр (без точки и запятой)
// а также -_ и =+      (-) две правые клавиши — минус и равенство — описаны не здесь!
// ****************************************************************************

// На случай аварийного включения us нужно отобразить и цифры
// можно в слое fallback, но тогда все символы клавиши могут не поместиться
// теперь на 7-8 слоях цифры-индексы; можно их

// Upd к заказу наклеек: 
// так как символ градуса ни разу не понадобился, не изображаем его на третьем уровне AE06 (хотя символ пока остаётся) → изображаем на седьмом TLDE → на 5 
// возможно, ‰ туда же — не востребован, да и не во всех шрифтах имеется; ‱ — тем более

// ****************************************************************************
// Варианты линии цифр: везде на первом и втором уровнях знаки препинания и скобки
// ****************************************************************************
partial alphanumeric_keys xkb_symbols "common_iso_digitsline_innerpunctuation" {    
    //include "illinc(common_iso_digitsline_nodigits)"    // нет цифр: самый удобный вариант, если цифры есть на первом уровне F1-F12
    //include "illinc(common_iso_digitsline_digits_in_3)" // цифры на 3 уровне, us-символы на 4; прочие спрессованы на 5
    //include "illinc(common_iso_digitsline_digits_in_4)" // цифры на 4 уровне, us-символы на 3; прочие спрессованы на 5 (симметрично digits_in_3)
    //include "illinc(common_iso_digitsline_digits_in_5)" // цифры на 5-6 уровнях (недоступны в настоящей консоли!), us-символы на 3, прочие спрессованы на 4
    include "illinc(common_all_digitsline_twcomma)" // цифры на 3 уровне, us-символы на 4; прочие спрессованы на 5 !!! и другой 1 уровень !!!
};
  

// §№-/":,._?%!    Ятрань
// |№-/":,._?%!;   ru(typewriter)
// 01234567890-=   

//заглавные греческие совпадают с латинскими: 
//υΥ, κΚ, εΕ, νΝ, ζΖ, 
//αΑ,
//μΜ, ιΙ, τΤ

<<<<<<< HEAD

=======
>>>>>>> ba97fbec236da87e064daecfc2481d2e78314fc8

// ****************************************************************************
// Точка и запятая
// ****************************************************************************

// Для ISO-раскладки: 
// точка и запятая в основном блоке рядом с алфавитным: , на LSGT, . на BKSL
// L3+точка=запятая: на случай внезапной смены физической клавиатуры на ANSI, чтобы запятая всё же где-нибудь была в варианте РУС    
hidden partial alphanumeric_keys xkb_symbols "common_iso_inner_comma_period" {   
    key <LSGT> { type="EIGHT_LEVEL", [ comma,	comma,		comma,	comma,		comma,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // , , , ,     
//    key <BKSL> { type="EIGHT_LEVEL", [ period,	period,	comma,	comma,		U22C5,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // . . , ,   ⋅(умножение)
    key <BKSL> { type="EIGHT_LEVEL", [ period,	period,		comma,	period,		period,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // . . , .   .(знак препинания)   
};

// Для ANSI-раскладок:

// Наиболее очевидный вариант, к которому надо привыкать отдельно — запятая на BKSL (справа!!!), точка — рядом с ?! 
// L3+запятая=точка: для симметрии
partial alphanumeric_keys xkb_symbols "common_ansi_rightcomma_patch" {
    key <BKSL> { type="EIGHT_LEVEL", [ comma,	comma,	period,	period,		comma	]}; // , , . .   , 
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка на линии цифр (после ?!, под *)
};
// ↑ запятая на BKSL — очень неудобно после точки на BKSL (клавиша примерно на том же месте)

// Нестандартная ANSI, где LSGT нет, но Enter большой (BKSL расположен около Enter, но не на его месте)
partial alphanumeric_keys xkb_symbols "common_ansi_upcomma_patch" {    
    key <AE02> { type="EIGHT_LEVEL", [ comma	]};     // запятая на линии цифр (между % и _); точка остаётся на BKSL
};
// Стандартная ANSI с маленьким Enter: BKSL на месте Enter и постоянно нажимается по ошибке
partial alphanumeric_keys function_keys xkb_symbols "common_ansi_modern_patch" {    
    include "illinc(common_ansi_upcomma_patch)"
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка тоже на линии цифр (после ?!, под *)
    // На BKSL — модификатор 5 уровня: одиночное нажатие не даёт эффекта, нужен редко, но пусть будет (а RWin на новых клавиатурах обычно отсутствует)
    replace key <BKSL> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };     
};
// Нестандартная ANSI Genius: LSGT нет, Enter большой, BKSL в самом хвосте линии цифр, а RWin (Level5) имеется
partial alphanumeric_keys function_keys xkb_symbols "common_ansi_genius_patch" {    
    include "illinc(common_ansi_upcomma_patch)"
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка тоже на линии цифр (после ?!, под *)
};

// ****************************************************************************
// Точка и запятая
// ****************************************************************************

// Для ISO-раскладки: 
// точка и запятая в основном блоке рядом с алфавитным: , на LSGT, . на BKSL
// L3+точка=запятая: на случай внезапной смены физической клавиатуры на ANSI, чтобы запятая всё же где-нибудь была в варианте РУС    
hidden partial alphanumeric_keys xkb_symbols "common_iso_inner_comma_period" {   
    key <LSGT> { type="EIGHT_LEVEL", [ comma,	comma,		comma,	comma,		comma,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // , , , ,     
//    key <BKSL> { type="EIGHT_LEVEL", [ period,	period,	comma,	comma,		U22C5,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // . . , ,   ⋅(умножение)
    key <BKSL> { type="EIGHT_LEVEL", [ period,	period,		comma,	period,		period,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // . . , .   .(знак препинания)   
};

// Для ANSI-раскладок:

// Наиболее очевидный вариант, к которому надо привыкать отдельно — запятая на BKSL (справа!!!), точка — рядом с ?! 
// L3+запятая=точка: для симметрии
partial alphanumeric_keys xkb_symbols "common_ansi_rightcomma_patch" {
    key <BKSL> { type="EIGHT_LEVEL", [ comma,	comma,	period,	period,		comma	]}; // , , . .   , 
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка на линии цифр (после ?!, под *)
};
// ↑ запятая на BKSL — очень неудобно после точки на BKSL (клавиша примерно на том же месте)

// Нестандартная ANSI, где LSGT нет, но Enter большой (BKSL расположен около Enter, но не на его месте)
partial alphanumeric_keys xkb_symbols "common_ansi_upcomma_patch" {    
    key <AE02> { type="EIGHT_LEVEL", [ comma	]};     // запятая на линии цифр (между % и _); точка остаётся на BKSL
};
// Стандартная ANSI с маленьким Enter: BKSL на месте Enter и постоянно нажимается по ошибке
partial alphanumeric_keys function_keys xkb_symbols "common_ansi_modern_patch" {    
    include "illinc(common_ansi_upcomma_patch)"
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка тоже на линии цифр (после ?!, под *)
    // На BKSL — модификатор 5 уровня: одиночное нажатие не даёт эффекта, нужен редко, но пусть будет (а RWin на новых клавиатурах обычно отсутствует)
    replace key <BKSL> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };     
};
// Нестандартная ANSI Genius: LSGT нет, Enter большой, BKSL в самом хвосте линии цифр, а RWin (Level5) имеется
partial alphanumeric_keys function_keys xkb_symbols "common_ansi_genius_patch" {    
    include "illinc(common_ansi_upcomma_patch)"
    key <AE08> { type="EIGHT_LEVEL", [ period	]}; // точка тоже на линии цифр (после ?!, под *)
};




// ****************************************************************************
// Знаки препинания на линии цифр (включая точку и запятую), на втором уровне — скобки
// а также -_ и =+      (две правые клавиши — минус и равенство)
// ****************************************************************************
partial alphanumeric_keys xkb_symbols "common_all_space_digitsline_innerpunctuation" {    
    key.type="EIGHT_LEVEL";    

    // Цифры в нижнем индексе на 7 уровне us-цифр, в верхнем — на 8
    key <TLDE> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	zerosubscript,	zerosuperior	]}; // ₀⁰
    key <AE01> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	onesubscript,	onesuperior		]}; // ₁¹
    key <AE02> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	twosubscript,	twosuperior		]}; // ₂²
    key <AE03> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	threesubscript,	threesuperior	]};	// ₃³
    key <AE04> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	foursubscript,	foursuperior	]}; // ₄⁴
    key <AE05> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	fivesubscript,	fivesuperior	]}; // ₅⁵
    key <AE06> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	sixsubscript,	sixsuperior		]}; // ₆⁶
    key <AE07> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	sevensubscript,	sevensuperior	]}; // ₇⁷
    key <AE08> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol, 	eightsubscript,	eightsuperior	]}; // ₈⁸
    key <AE09> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	ninesubscript,	ninesuperior	]}; // ₉⁹
    key <AE10> {[ VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,VoidSymbol,	zerosubscript,	zerosuperior	]}; // ₀⁰
    
    // Минус и прочие чёрточки:	-(дефисоминус)	_		—(русское тире)	–(английское)	−(минус ширины +) 	‒(цифровое тире: выше короткого, но ниже минуса) ± ∓
    key <AE11> {type="EIGHT_LEVEL", [	minus,	underscore,	emdash,		endash,			U2212,		figdash,	plusminus,	U2213		]}; 
    // Равенство						= 		+			≈ 			≠    			+ 			⊕ 			≡ 			≢    
    key	<AE12> {type="EIGHT_LEVEL", [	equal,	plus,		approxeq,	notequal,		plus,   	U2295,		identical,	notidentical]}; 
      
    // Пробел
    key <SPCE> {type="EIGHT_LEVEL", [	 space,	asciitilde,	asciitilde,	nobreakspace,		asciitilde,	U223E,	signifblank,	nobreakspace]}; //   ~ ~ nbsp    ~ ~ ␣ nbsp
    // чтобы при зажатом Shift набирать текст с пробелами (тильда остаётся на третьем и пятом уровнях, а также на RAlt)
    //key <SPCE> {type="EIGHT_LEVEL", [	 space,	space,	asciitilde,	nobreakspace,		asciitilde,	U223E,	signifblank,	nobreakspace]}; // ~ nbsp    ~ ~ ␣ nbsp
    // ↑ в аббревиатурах лучше ~, а для длинных снова есть режим CapsLock

    // Переопределяем нмжние шесть уровней цифр (они чаще меняются)
    key.type="EIGHT_LEVEL";    
    // ↓ точка и запятая — как на первом, так и на втором уровнях
    //			знаки препинания	скобки и *			цифры		us-2			математика и редкие символы
    key <TLDE> {[ asciicircum,		less,				0,			grave,			infinity,				degree		]}; //  ^ < 0 \   ∞ °
    key <AE01> {[ percent,			greater,			1,			bar,			exclam,					minutes		]}; //  % > 1 |   ! ′
    key <AE02> {[ guillemotleft,	bracketleft,		2,			at,				doublelowquotemark,		seconds		]}; //  « [ 2 @   „ ″
    key <AE03> {[ guillemotright,	bracketright,		3,			numbersign,		rightdoublequotemark,	U2034		]}; //  » ] 3 #   ” ‴
    key <AE04> {[ colon,			braceleft,			4,			dollar,			section,				VoidSymbol	]}; //  : { 4 $   § 
    key <AE05> {[ semicolon, 		braceright,			5,			percent,		permille,				U2031		]}; //  ; } 5 %   ‰ ‱
    key <AE06> {[ comma,			comma,				6,			asciicircum,	VoidSymbol,				VoidSymbol	]}; //  , , 6 ^    
    key <AE07> {[ period,			period,				7,			ampersand,		VoidSymbol,				VoidSymbol	]}; //  . . 7 &    
    key <AE08> {[ underscore,		asterisk,			8,			asterisk,		U22C5,					multiply	]}; //  _ * 8 *   ⋅ ×
    key <AE09> {[ question,			parenleft,			9,			quotedbl,		U2286,					U2288		]}; //  ? ( 9 "   ⊆ ⊈
    key <AE10> {[ exclam,			parenright,			0,			apostrophe,		U2287,					U2289		]}; //  ! ) 0 '   ⊇ ⊉     

    //key <BKSL> {[ slash,			bar,				backslash,	slash,			VoidSymbol,				VoidSymbol	]}; // 
    key <BKSL> {[ slash,			slash,				backslash,	bar,			VoidSymbol,				VoidSymbol	]}; // / / \ | 
    	
    //key <LSGT> {[ asterisk,			asterisk,			comma,		asterisk,		comma,					comma	]};
    key <LSGT> {type="ONE_LEVEL",	[ asterisk	]};
    
    // Проверяем, не оставить ли старые скобки (над ,. {}) — и вроде пока нормально
    key <TLDE> {[ asciicircum,		grave,				0,			asciitilde			]}; // ^ ` 0 ~
    key <AE01> {[ percent,			bar,				1,			exclam				]}; // % | | ! 
    key <AE02> {[ guillemotleft,	less			]}; // « < 
    key <AE03> {[ guillemotright,	greater			]};	// » > 
    key <AE04> {[ colon,			bracketleft		]}; // : [ 
    key <AE05> {[ semicolon, 		bracketright	]}; // ; ] 
    key <AE06> {[ comma,			braceleft		]}; // , { 
    key <AE07> {[ period,			braceright		]}; // . } 
    key <AE08> {[ underscore,		asterisk		]}; // _ * 
    key <AE09> {[ question,			parenleft		]}; // ? ( 
    key <AE10> {[ exclam,			parenright		]}; // ! ) 
    
   
};  














// ****************************************************************************
// Модификаторы
// ****************************************************************************


// Включатель русской раскладки слева
// LWin как включатель второй раскладки (русской)
partial modifier_keys xkb_symbols "common_all_mod_lwin_rus" {
    key <LWIN> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Last_Group] };
};

// Включатель латиницы справа
// RWin как включатель первой раскладки (латиницы)
partial modifier_keys xkb_symbols "common_all_mod_rwin_lat" {
    key <RWIN> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_First_Group] };
};

// RAlt как, собственно, правый Alt
// оригинал, как ни странно, в кхмерской раскладке kh — больше grep'ом ничего такого не находится
partial modifier_keys xkb_symbols "common_all_mod_ralt_alt" {
    replace key <RALT> { [ Alt_R     ] };
};




// NumLock как модификатор третьего уровня (пока нажат)
partial modifier_keys xkb_symbols "common_all_mod_num_level3" {
   key <NMLK> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level3_Shift ] };
    modifier_map Mod5   { ISO_Level3_Shift };
};



// Menu как модификатор пятого уровня (пока нажат)
partial modifier_keys xkb_symbols "common_all_mod_menu_level5" {
  replace key <MENU> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level5_Shift ]  };
  modifier_map Mod3 { ISO_Level5_Shift };  
};






// ****************************************************************************
// Ноутбук Samsung (ANSI)

// SCLK на samsung'е нет; верхний угол: PRSC Pause/Break INS DEL
// и этот блок дальше от алфавитного, чем цифровой (и клавиши меньше по размеру).
// При этом клавиша, на которой написано Pause/Break — не PAUS!!! 
// Это key <I210>   {      [ XF86Launch3           ]       }; 
// но здесь переопределить I210 не получается — переопределяется только для русской раскладки

// нужен отдельный запуск
//  xmodmap -e "keycode 210 = ampersand"

// Цифрового блока тоже нет (не верь глазам своим);
// клавиши, похожие на цифровой блок — это не KP0..KP9, KPEN, KPDL и т. п., 
// а аппаратные дубликаты HOME, RTRN, INS, DELE и т. п.;
// то есть переопределение INS влияет и на Insert «цифрового блока»!
// А вот NumLock переопределить можно

//$ setxkbmap "illinc(lat_samsung),illinc(rus_samsung)"
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "lat_samsung" {    
    include "illinc(lat_iso_mainblock)" // алфавитный блок и модификаторы  
    include "illinc(common_all_f1_f12_digits)" // цифры на самой верхней линии
    include "illinc(common_all_insert_emdash)"      
        
    key <FK12> {
     type="FOUR_LEVEL",
        symbols[Group1] = [ backslash, bar ],
        actions[Group1]=[ ],
        actions[Group2]=[ LockGroup(group=-1) ],
        actions[Group3]=[ LockGroup(group=-2) ],
        actions[Group4]=[ LockGroup(group=-3) ]
    };
        
    key <PRSC> {
     type="FOUR_LEVEL",
        symbols[Group1] = [ dollar, apostrophe, dollar, apostrophe ], // символы на четырёх уровнях, а действие (переключение раскладки) только на первом
        actions[Group1]=[ ],
        actions[Group2]=[ LockGroup(group=-1) ],
        actions[Group3]=[ LockGroup(group=-2) ],
        actions[Group4]=[ LockGroup(group=-3) ]
    };
    
    include "illinc(common_all_mod_num_level3)"        
    //include "illinc(common_all_mod_ralt_level5)"   
    key <MENU> {    type="FOUR_LEVEL",    symbols[Group1] = [ ISO_First_Group, ISO_First_Group, Menu, Num_Lock] };        
};
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "rus_samsung" {
    include "illinc(lat_samsung)"
    include "illinc(rus_alph)"
};

// ↑↑↑ с учётом всего лучше оставить обычный lat, не переопределяя цифровой блок; возможно, с common_ansi_mod_usmodern_enterswap_patch






    replace key <NMLK> 	{type="FOUR_LEVEL",	[ apostrophe,		VoidSymbol,		Num_Lock,	Num_Lock	], actions=[NoAction()]}; // 
    // https://unix.stackexchange.com/questions/602864/using-the-numlock-key-as-an-equals-key-with-xkb
    // чтобы NumLock на третьем уровне корректно работал, 
    // надо назначить этот символ на первый уровень другой (возможно, отсутствующей на клавиатуре) клавиши
    // /usr/share/X11/xkb/symbols/inet:       key <I227>   {      [ XF86Finance           ]       };
    replace key <I227> {	type="ONE_LEVEL",	symbols = [ Num_Lock 		]	};
    modifier_map Mod2   { Num_Lock };

↑↑↑ не работает!!!
Без назначения Num_Lock'ом другой клавиши NumLock на третьем уровне криво, но работал, а после — вообще никакой реакции на Level3+NumLock

    // Апостроф (одинарная кавычка) на NumLock
    // replace key <NMLK> 	{type="FOUR_LEVEL",	[ apostrophe,	VoidSymbol,		Num_Lock,	Num_Lock	]}; // 
    // ↑↑↑ переключение скобки → цифры по Level3+NumLock,
    // а вот обратное переключение (цифры → скобки) происходит и при нажатии Level3+NumLock, и при наборе апострофа клавишей NumLock
    // если набрать апостроф NumLock'ом в режиме скобок, режим не переключается (остаются скобки)
  
    // если ^ нет на первом уровне линии цифр, пусть будет здесь
    //replace key <NMLK> 	{type="FOUR_LEVEL",	[ asciicircum,	apostrophe,		Num_Lock,	Num_Lock	]}; // 
    

    // replace key <KPEN> {
    //     symbols[Group1] = [ VoidSymbol, KP_Enter ],
    //     actions[Group1]=[ ],
    //     actions[Group2]=[ SetGroup(group=-1) ],
    //     actions[Group3]=[ SetGroup(group=-2) ],
    //     actions[Group4]=[ SetGroup(group=-3) ]
    // };
    // ↑ строчную латинскую букву ввести можно, а с Shift срабатывает Enter
        
    // на третьем уровне KP0 «вкл цифры»
    //key  <KP0> {type="FOUR_LEVEL_KEYPAD", [  less,	0	], actions=[ NoAction(), NoAction(), LockMods(modifiers=NumLock) ]}; 
    // ↑ не проверено


partial keypad_keys xkb_symbols "common_all_keypad_braces" {
    // Любое нажатие на клавишу NumLock, кроме 4 уровня (то есть кроме символa Num_Lock) — сбрасывает режим NumLock (цифровой блок = скобки)
    // 3 уровень только устанавливает режим скобок, 4 — циклический NumLock
    replace key <NMLK> 	{type="FOUR_LEVEL",	[ asciicircum,	apostrophe,		VoidSymbol,	Num_Lock	]}; 
    // если назначить символ Num_Lock на первый уровень другой (возможно, отсутствующей на клавиатуре) клавиши, 
    // то тут не будет работать ни циклическое переключение по 4 уровню, ни нециклическое включение по 1-3 уровням
    
    // 3 уровень клавиши KPDV (рядом с NumLock) выставляет режим цифр
    key <KPDV> {type="EIGHT_LEVEL", [ slash, 		slash,			VoidSymbol,		VoidSymbol,			U2298,	U2298	], 
                            actions=[ NoAction(),	NoAction(), 	LockMods(modifiers=NumLock) ]		}; // // // ⊘⊘ 
   
    // Верхняя линия цифрового блока без изменений, только на пятом уровне операторы в круге
    // и символы верхней линии не зависят от режима NumLock
    //key <KPDV> {type="EIGHT_LEVEL", [ slash, 	slash,		slash,	slash,		U2298,	U2298,		VoidSymbol,	VoidSymbol	]}; // // // ⊘⊘ 
    key <KPMU> {type="EIGHT_LEVEL", [ asterisk,	asterisk,	asterisk,asterisk,	U229B,	U229B,		VoidSymbol,	VoidSymbol	]}; // ** ** ⊛⊛
    //key <KPSU> {type="EIGHT_LEVEL", [ minus,	minus,		minus,	minus,		U2296,	U2296,		VoidSymbol,	VoidSymbol	]}; // -- -- ⊖⊖
    //key <KPAD> {type="EIGHT_LEVEL", [ plus,		plus,		plus,	plus,		U2295,	U2295,		VoidSymbol,	VoidSymbol	]}; // ++ ++ ⊕⊕ верхняя высокая справа
             
    // KPEN также всегда ЛатВрем и не зависит режима NumLock 
    // на третьем уровне оригинальный KP_Enter ⇒ не ONE_LEVEL ⇒ временное переключение на латиницу на втором уровне описываем отдельно
    replace key <KPEN> {type="EIGHT_LEVEL", 
        symbols[Group1] = [ VoidSymbol, VoidSymbol, KP_Enter ],
        actions[Group1]=[ ],
        actions[Group2]=[ SetGroup(group=-1), SetGroup(group=-1) ],
        actions[Group3]=[ SetGroup(group=-2), SetGroup(group=-2) ],
        actions[Group4]=[ SetGroup(group=-3), SetGroup(group=-3) ]
    };  
    
    // 3 уровень клавиши KP7 (под NumLock) выставляет режим цифр
    // (хотя по документации LockMods должен переключать режим циклически, как и символ Num_Lock)
    //key  <KP7> {type="FOUR_LEVEL_KEYPAD", [parenleft, 7, VoidSymbol], actions=[ NoAction(), NoAction(), LockMods(modifiers=NumLock) ]};

    key.type="KEYPAD"; // два уровня: норма и после переключения NumLk  
    
    //key  <KP7> {[  parenleft,	7		]};
    //key  <KP8> {[  parenright,	8		]};
    //key  <KP9> {[  numbersign,	9		]};       

    key  <KP4> {[  braceleft,	4		]};
    key  <KP5> {[  braceright,	5		]};
    key  <KP6> {[  bar,			6		]};

    key  <KP1> {[  bracketleft,	1		]};
    key  <KP2> {[  bracketright,2		]};
    key  <KP3> {[  at,			3		]}; 
         
    //key  <KP0> {[  less,		0		]}; 
    //key  <KPDL>{[  greater,	    period	]}; 
    
    key <KPSU> {type="EIGHT_LEVEL", [ minus,	minus,		U208B,	U207B,		U2296,	U2296,		VoidSymbol,	VoidSymbol	]}; // -- ₋⁻ ⊖⊖
    key <KPAD> {type="EIGHT_LEVEL", [ plus,		plus,		U208A,	U207A,		U2295,	U2295,		VoidSymbol,	VoidSymbol	]}; // ++ ₊⁺ ⊕⊕ верхняя высокая справа
   
    key  <KP7> {type="FOUR_LEVEL_KEYPAD", [  parenleft,		7,		U208D,	U207D		]};	// (7 ₍⁽ 
    key  <KP8> {type="FOUR_LEVEL_KEYPAD", [  parenright,	8,		U208E,	U207E		]};	// )8 ₎⁾ 
    key  <KP9> {type="FOUR_LEVEL_KEYPAD", [  numbersign,	9,		U208C,	U207C		]};	// #9 ₌⁼     
   
    
    // дублирование перечёркнутых прямых галочек ≮≯ на 3 и 4 уровне 
    // перечёркивание логичнее на 4, но что тогда на 3? И далековато для двух модификаторов
    key  <KP0>  {type="FOUR_LEVEL_KEYPAD", [  less,		0,			U226E,	U226E	]}; 
    key  <KPDL> {type="FOUR_LEVEL_KEYPAD", [  greater,	period,		U226F,	U226F	]}; 
    // логически на 3 должны быть правильные текстовые угловые скобки — но которые из них правильные?!
    // в википедии использованы математические ⟨⟩, даже в тексте — а это уже 5 уровень, а не 3
    // стандартного EIGHT_LEVEL_KEYPAD не существует; ⟨⟩ уже есть на 5 уровне F9-F10

};


// Плюс на линии знаков препинания (под плюсом), % на Insert
// если цифры на F1-F12, то + цифрового блока от них слишком далеко
partial alphanumeric_keys keypad_keys modifier_keys xkb_symbols "common_105_104_plusdigit_percent_patch" {    
    key	<AE12> 			{type="EIGHT_LEVEL", 		[ plus	]		}; 		// ++ и т. д.   (не replace, сохраняем верхние уровни)
    replace key <INS>	{type="LOCAL_EIGHT_LEVEL", 	[ percent, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert	]	};            
};

// Перезапись цифрового блока без порчи прочих, 2022-v2
// дублирование &, нет плюса → желателен common_105_plusdigit_percent_patch (меняет линию препинания/us-цифр и Insert)


    // NumLock без символа, только для включения скобок (сброса NumLock)
    // replace key <NMLK> 	{type="FOUR_LEVEL",	[ VoidSymbol,	VoidSymbol,		Num_Lock,	Num_Lock	]}; 
    // если назначить символ Num_Lock на первый уровень другой (возможно, отсутствующей на клавиатуре) клавиши, 
    // то тут не будет работать ни циклическое переключение по 4 уровню, ни нециклическое включение по 1-3 уровням
    
    // // А вдруг? Вкл скобки по совместительству ЛатВрем
    // replace key <NMLK> 	{type="FOUR_LEVEL",	[ VoidSymbol,	VoidSymbol,		Num_Lock,	Num_Lock	],
    //     actions[Group1]=[ ],
    //     actions[Group2]=[ SetGroup(group=-1), SetGroup(group=-1) ],
    //     actions[Group3]=[ SetGroup(group=-2), SetGroup(group=-2) ],
    //     actions[Group4]=[ SetGroup(group=-3), SetGroup(group=-3) ]
    // };
    // ↑ ЛатВрем работает, а режим NumLock не переключается: ни этой клавишей нециклически или циклически, ни Level3+KPDV
    // несмотря на то, что NumLock тут и только тут
    
    // 3 уровень клавиши KPDV (рядом с NumLock) выставляет режим цифр
    // (хотя по документации LockMods должен переключать режим циклически, как и символ Num_Lock)
    //key <KPDV> {type="EIGHT_LEVEL", [ slash, 		slash,			VoidSymbol,		VoidSymbol,			U2298,	U2298	], 
    //                        actions=[ NoAction(),	NoAction(), 	LockMods(modifiers=NumLock) ]		}; // // // ⊘⊘ 
    // ↑ Upd: и переключает циклически — то ли это был случайный сбой, то ли баг, устранённый обновлением
                            

//     key.type="FOUR_LEVEL_KEYPAD";    // 1-2: норма и после включения NumLock, 3-4 с Level3 общие
//          
//     key <KPSU> {[ section,		minus	]}; 
//     key <KPAD> {[ asciicircum,	plus	]}; 
//      
//     key  <KP7> {[  parenleft,	7		]};	// (7  без индексов! Когда понадобятся, сделаю заплатку
//     key  <KP8> {[  parenright,	8		]};	// )8  
//     key  <KP9> {[  grave,		9		]};	// `9      
//     //key  <KP9> {[  asciicircum,	9		]};    
// 
//     key  <KP4> {[  braceleft,	4		]};
//     key  <KP5> {[  braceright,	5		]};
//     key  <KP6> {[  bar,			6		]};
// 
//     key  <KP1> {[  bracketleft,	1		]};
//     key  <KP2> {[  bracketright,2		]};
//     key  <KP3> {[  apostrophe,	3		]}; 
//     
//     // дублирование перечёркнутых прямых галочек ≮≯ на 3 и 4 уровне 
//     key  <KP0>  {[  less,		0,			U226E,	U226E	]}; 
//     key  <KPDL> {[  greater,	period,		U226F,	U226F	]}; 
    
‰



    // Мягкий перенос U00AD [shy, видно только в LO] #define XK_hyphen                        0x00ad  /* U+00AD SOFT HYPHEN */
    // ‐ Дефис U2010 [на вид ничем не отличается от дефисоминуса] 
    // ‑ Неразрывный дефис U2011 [на вид тоже не отличается, но в LO подсвечивается и не переносится]
    // на пробеле неразрывный пробел на 4 уровне; пусть и неразрывный дефис будет на 4

    // Пробел
    //key <SPCE> {type="EIGHT_LEVEL", [	 space,	asciitilde,	asciitilde,	nobreakspace,		asciitilde,	U223E,	signifblank,	nobreakspace]}; //   ~ ~ nbsp    ~ ~ ␣ nbsp
    // ↓ чтобы при зажатом Shift набирать текст с пробелами (тильда остаётся на третьем и пятом уровнях, а также на RAlt)
    //key <SPCE> {type="EIGHT_LEVEL", [	 space,	space,	asciitilde,	nobreakspace,		asciitilde,	U223E,	signifblank,	nobreakspace]}; // ~ nbsp    ~ ~ ␣ nbsp
    // ↑ в аббревиатурах лучше ~, а для длинных снова есть режим CapsLock
    //key <SPCE> {type="EIGHT_LEVEL", [	 space,	asciitilde,	asciitilde,	nobreakspace,		asciitilde,	U223E,	signifblank,	U2009]}; //   ~ ~ nbsp    ~ ~ ␣ тонкая шпация
    //↑ ни в одном редакторе не тонкая, а в LaTeX есть \,
    // полукруглая шпация en space U2002 отображается шире nobreakspace; а тонкая шпация U2009 — такой же ширины, как nobreakspace
    
    // волосяная шпация U200A единственная \'{у}же nobreakspace в LibreOffice 
    // на пятом уровне фиурная тильда, ибо перепутать Level5 с Level3 или Shift сложно
    // на восьмом оставляем nobreakspace для совместимости с печатью на клавишах; но нажимать три модификатора с пробелом неудобно
    //key <SPCE> {type="EIGHT_LEVEL", [	 space,	asciitilde,	asciitilde,	nobreakspace,		U223E,	U200A,		signifblank,	nobreakspace]}; //   ~ ~ nbsp    ∾ « » ␣ nbsp
    // ↑ см. выше; с тремя модификаторами ∾, как самый экзотический
    key <SPCE> {[ space,	asciitilde,	asciitilde,	nobreakspace,	digitspace,		hairspace,		signifblank,	U223E	]}; //  ~ ~ nbsp  figspace волосяная ␣ ∾

    
    // Что было на F11: 
    // slash,		U2298       /⊘  (/ как часть /*, ⊘ как «вариант в круге»)
    // использовалось одноразово:
    // U26D4 ⛔ символ «Стоп», по идее красный и круглый; но он то кривой, то пробел после себя ест, то высоту строки портит
  
    // ⊗ на иксе, ⊙⊛⊖⊕ на us-цифрах
    // Чего нет, но при необходимости можно добавить заплатками:
    // ⊚ Кольцевой оператор в круге U229A
    // ⊜ Равно в круге  U229C (логично на 6 уровне =, вместо notidentical)  → на F11, но при необходимости перезаписать + дублирование ₌⁼ на F11
    // ⊝ Дефис в круге U229D
    // ⊞ Плюс в квадрате U229E ⊟ Минус в квадрате U229F ⊠ Произведение в квадрате U22A0 ⊡ Оператор точка в квадрате U22A1
    
    // треугольники
    // ◁ U25C1 ▷ U25B7
    // ⊲ ⋪ ⊴  U22B2,		U22EA,		U22B4
    // ⊳ ⋫ ⊵  U22B3,		U22EB,		U22B5

    key <FK11> {[ quotedbl,		numbersign,	F11,	F11, 			U2213,		U229C,				U208C,			U207C		]}; // "#     ∓⊜ ₌⁼        
    key <FK12> {[ equal,		notequal,	F12,	F12, 			identical,	notidentical,		U208C,			U207C		]}; // =≠     ≡≢ ₌⁼


    //key <AD01> {[	  j,		J,		VoidSymbol,		VoidSymbol,		VoidSymbol,		VoidSymbol,		VoidSymbol,	U02B2		]}; // j J             ʲ
    //key <AD01> {[	  j,		J,		backslash,		U2023,			U2216,			U2216,			VoidSymbol,	U02B2		]}; // j J \ ‣   ∖ ∖   ʲ
    //key <AD01> {[	  j,		J,		apostrophe,		apostrophe,		VoidSymbol,		VoidSymbol,		U2023,		U02B2		]}; // j J ' '       ‣ ʲ
    
    //key <AD02> {[	  c,		C,		uparrow,		U2909,			U2102,			VoidSymbol,		U21D1,		VoidSymbol	]}; // c C ↑ ⤉   ℂ   ⇑ 
    
    //key <AD06> {[	  n,		N,		Greek_nu,		Greek_NU,		U2115,			notsign,		VoidSymbol,	VoidSymbol	]}; // n N ν Ν   ℕ ¬

    // ↑ 8 уровень занят; причём xⁿ понадобится вероятнее, чем греческая Ν ⇒ если понадобится вернуть № на 3 уровень, убираем греческую Ν вообще
    // Заглавная греческая ню νΝ выглядит как заглавная латинская nN ⇒ в формулах не используется ⇒ на 7 уровень (и 8, чтобы рифмовались с заглавным омикроном)
    // key <AD06> {[	  n,		N,		Greek_nu,		numerosign,		U2115,			notsign,		U2099,		Greek_NU	]}; // n N ν №   ℕ ¬ ₙ Ν  а есть и ⁿ = U207F
    //key <AD06> {[	  n,		N,		numerosign,		Greek_nu,		U2115,			notsign,		Greek_NU,	Greek_NU	]}; // n N № ν   ℕ ¬ Ν Ν

    
    //key <AD08> {[	  h,		H,		U274C,			U274C,			U210D,			VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // h H ❌ ❌   ℍ
    //key <AD09> {[	  v,		V,		checkmark,		checkmark,		emptyset,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // v V ✓ ✓   ∅
    //key <AD08> {[	  h,		H,		section,		section,		U210D,			VoidSymbol,		U2095,		U274C		]}; // h H § §   ℍ   ₕ ❌
    //key <AD09> {[	  v,		V,		period,			period,			emptyset,		U27E1,			checkmark,	VoidSymbol	]}; // v V . .   ∅ ⟡ ✓
    // ↑ ∅ = void set; ⟡ имеет официальное значение «никогда», хотя и используется как «например»
    
    //key <AD10> {[	  z,		Z,		Greek_zeta,		Greek_ZETA,		U2124,			VoidSymbol,		U208A,		U207A		]}; // z Z ζ Ζ   ℤ   ₊ ⁺
    //key <AD11> {[ bracketleft,	braceleft,	Greek_chi,	Greek_CHI,		elementof,		notelementof,	VoidSymbol,	VoidSymbol	]}; // [ { χ Χ   ∈ ∉ 
    //key <AD12> {[ bracketright,	braceright,	Greek_psi,	Greek_PSI,		containsas,		U220C,			VoidSymbol,	VoidSymbol	]}; // ] } ψ Ψ   ∋ ∌ 
    //key <AD11> {[ bracketleft,	braceleft,	Greek_chi,	Greek_CHI,		elementof,		notelementof,	U2286,		U2288	]}; // [ { χ Χ   ∈ ∉ ⊆ ⊈
    //key <AD12> {[ bracketright,	braceright,	Greek_psi,	Greek_PSI,		containsas,		U220C,			U2287,		U2289	]}; // ] } ψ Ψ   ∋ ∌ ⊇ ⊉
    // на ЖЭ неудобно набирать 7-8 уровни, а на 90 они уже заняты
    //key <AD11> {[ bracketleft,	braceleft,	Greek_chi,	Greek_CHI,		elementof,		notelementof,	includedin,	U2284	]}; // [ { χ Χ   ∈ ∉ ⊂ ⊄ 
    //key <AD12> {[ bracketright,	braceright,	Greek_psi,	Greek_PSI,		containsas,		U220C,			includes,	U2285	]}; // ] } ψ Ψ   ∋ ∌ ⊃ ⊅ 
    // ? переместить сюда и ⊆⊇, а ∈∋, наоборот, на ()?

    //key <AC02> {[	  y,		Y,		leftarrow,	U21D0,			VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // y Y ← ⇐
    //key <AC03> {[	  w,		W,		rightarrow,	implies,		VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // w W → ⇒  
    //key <AC02> {[	  y,		Y,		leftarrow,	U219A,			U2194,		U21AE,		U21D0,		U21CD		]}; // y Y ← ↚    ↔ ↮ ⇐ ⇍
    //key <AC03> {[	  w,		W,		rightarrow,	U219B,			ifonlyif,	U21CE,		implies,	U21CF		]}; // w W → ↛    ⇔ ⇎ ⇒ ⇏
    //key <AC02> {[	  y,		Y,		leftarrow,	U21D0,			U2194,		U21AE,		U219A,		U21CD		]}; // y Y ← ⇐    ↔ ↮ ↚ ⇍
    //key <AC03> {[	  w,		W,		rightarrow,	implies,		ifonlyif,	U21CE,		U219B,		U21CF		]}; // w W → ⇒    ⇔ ⇎ ↛ ⇏
    
    //key <AC04> {[	  a,		A,		Greek_alpha,at,				U2200,		VoidSymbol,	Greek_ALPHA,Greek_ALPHA	]}; // a A α @    ∀   Α Α
    
    //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,semicolon,	    colon,	VoidSymbol,	VoidSymbol	]}; // ; : θ Θ     ; :
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	apostrophe,	 quotedbl,	VoidSymbol,	VoidSymbol	]};	// ' " η Η     ' "
    //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,quotedbl,	VoidSymbol,	VoidSymbol,	VoidSymbol]}; // ; : θ Θ     "  
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	apostrophe,	quotedbl,	VoidSymbol,	VoidSymbol]}; // ' " η Η     ' " 
    //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol]}; // ; : θ Θ     
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol]}; // ' " η Η    

        //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,elementof,notelementof,		U208D,	U207D	]}; // ; : θ Θ     ∈ ∉ ₍ ⁽    
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	containsas,	U220C,			U208E,	U207E	]}; // ' " η Η     ∋ ∌ ₎ ⁾

    //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,U25C1,		VoidSymbol,	VoidSymbol,	VoidSymbol]}; // ; : θ Θ  ◁   
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	U25B7,		VoidSymbol,	VoidSymbol,	VoidSymbol]}; // ' " η Η  ▷ 
    //key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,U22B2,		U22EA,		U22B4,	VoidSymbol]}; // ; : θ Θ  ⊲ ⋪ ⊴  
    //key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	U22B3,		U22EB,		U22B5,	VoidSymbol]}; // ' " η Η  ⊳ ⋫ ⊵
      
    //						Shift	3			3+Shift			5			5+Shift		5+3			5+3+Shift
    //key <AB01> {[	  slash, question,slash,	slash,			question,	question,	VoidSymbol,	VoidSymbol	]}; // / ? / /    ? ?  
    //key <AB01> {[	  slash, question,section,	slash,			question,	question,	backslash,	bar			]}; // / ? § /    ? ? \ | 
    // обязательно / на 4 уровне, чтобы набирать /**/ на одном модификаторе; \ в пределах 1-4 — чтобы где-то был даже без \Лат
    //key <AB01> {[	  slash, question,backslash,	slash,		question,	question,	VoidSymbol,	VoidSymbol	]}; // / ? \ /    ? ?  
    //↑ неудобно на 4 уровне, да ещё с таким расстоянием между Я и us-8; на LSGT и Я можно, но тоже неудобно → вернула /* на пятый F11-F12
    // а в тексте чаще /, чем \ → смена 3 и 4 уровней Я местами (кроме того, PrintScreen заменить на \Лат можно почти везде, а BKSL на ANSI мб модификатором L3 или L5)
    // key <AB01> {[	  slash, question,	slash,	backslash,		question,	question,	VoidSymbol,	VoidSymbol	]}; // / ? / \    ? ?  
    // ↓ \ на другую клавишу (на j). Ибо запомнить, /\ или \/ — не получается! То есть возврат к первоначальному варианту для Я
    //key <AB01> {[	  slash, question,	slash,	slash,			question,	question,	VoidSymbol,	VoidSymbol	]}; // / ? / /    ? ?  
    // Если раскладка используется, то всегда есть ? на первом уровне линии us-цифр. Ставим на 5-6 уровни /⊘. 
    // Или даже ⊘⊘, чтобы не рисовать на визуализации лишнего?
    //key <AB01> {[	  slash, question,	slash,	enfilledcircbullet,			U2298,		U2298,		VoidSymbol,	VoidSymbol	]}; // / ? / •    ⊘ ⊘

    //key <AB07> {[     x,	X,		Greek_xi,	Greek_XI,		multiply,	U22C5,		VoidSymbol,	VoidSymbol	]}; // x X ξ Ξ    × ⋅
   
    //key <AB02> {[	  q,	Q, 		downarrow,	U2908,			U211A,		U220E,		U21D3,		VoidSymbol	]}; // q Q ↓ ⤈    ℚ ∎ ⇓   
    
    

// ****************************************************************************
// Insert как длинное тире  → %
// ****************************************************************************
// partial alphanumeric_keys modifier_keys xkb_symbols "common_all_insert_emdash" {    
//     // Прячем Insert на 3 уровень, ибо он случайно нажимается чаще, чем по делу
//     // на втором уровне тоже Insert, ибо Shift+Ins всё равно обрабатывается как вставка [но не во всех программах!], что бы там ни было
//     //replace key <INS> {type="FOUR_LEVEL", [ emdash, Insert, Insert, Insert]	};   
//     // в kate работает копирование по Ctrl+Level3+Ins и вставка по Shift+Level3+Ins, а просто Shift+Ins переключает режим
//     // ↑ в Firefox работает копирование по Ctrl+Level3+Ins и вставка по Shift+Ins. Л.Ф. ругается, что вставляется не то, что она копирует
//         
//     // LOCAL_EIGHT_LEVEL в /usr/share/X11/xkb/types/pc — Control как модификатор пятого уровня
//     replace key <INS> {type="LOCAL_EIGHT_LEVEL", [ emdash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};         
//     // ↑ в kate работает копирование по Ctrl+Ins, но вставка по Shift+Level3+Ins
//     // Shift+Ins переключает режим вставки/замены и ничего не вставляет в kate, но в Firefox, как ни странно, работает вставкой
//     // но и с FOUR_LEVEL в kate так же
// };


    

partial keypad_keys xkb_symbols "common_all_bracepad_with_ops" {        
    // https://unix.stackexchange.com/questions/602864/using-the-numlock-key-as-an-equals-key-with-xkb
    // чтобы символ на клавише NumLock печатался без дополнительных эффектов, надо назначить Num_Lock на первый уровень другой (возможно, отсутствующей на клавиатуре) клавиши
    // /usr/share/X11/xkb/symbols/inet:       key <I227>   {      [ XF86Finance           ]       };
    replace key <I227>	{type="ONE_LEVEL",	[ Num_Lock 		]	};
    modifier_map Mod2   { Num_Lock };    
    // Num_Lock удалён, цифровой блок однорежимный
    key.type="EIGHT_LEVEL";    
    // <> над {}
    replace key <NMLK>	{[ less,		U226E	]	}; 
    key <KPDV> {[ greater,		U226F,		slash, 		slash	]}; // 
    // поэтому /* смещаем
    key <KPMU> {[ slash, 		slash,		asterisk,	asterisk,		U2298,	U2298	]}; // //
    key <KPSU> {[ asterisk,		asterisk,		minus,		minus,		U229B,	U229B	]}; // **
    
    key <KPAD> {[ plus,			plus,		plus,		plus,			U2295,	U2295	]}; // %% ++ ⊕⊕    
    key <KPEN> {[ minus,		minus,		KP_Enter,	KP_Enter,		U2296,	U2296	]}; // --    ⊖⊖
    
    // скобки: круглые по центру блока!
     
    key  <KP7> {[ braceleft,	braceleft,		7		]};
    key  <KP8> {[ braceright,	braceright,		8		]};
    //key  <KP9> {[ asciicircum,	asciicircum,	9		]}; // & или ^ — смотря чего нет в основном блоке

    key  <KP4> {[ parenleft,	parenleft,		4		]};
    key  <KP5> {[ parenright,	parenright,		5		]};
    //key  <KP6> {[ bar,			bar,			6		]};
    
    
    // за {} и () — обязательно | и ещё & или ^ — смотря чего нет в основном блоке
    key  <KP9> {[ bar,			bar,			9		]}; 
    key  <KP6> {[ ampersand,	ampersand,		6		]};
    

    key  <KP1> {[ bracketleft,	bracketleft,	1		]};
    key  <KP2> {[ bracketright,bracketright,	2		]};
    key  <KP3> {[ apostrophe,	apostrophe,		3		]}; 
    
    key  <KP0>  {[ equal,		notequal,		0		]}; 
    key  <KPDL> {[ at,			at,				period	]};      
};

// ****************************************************************************
// Поблочное разбиение — удалено!
// позже восстановить, но для опытов удобнее единый блок неалфавитных клавиш

// ****************************************************************************


// ****************************************************************************
// F1-F12 как цифры
// ****************************************************************************
partial function_keys xkb_symbols "common_all_f1_f12_digits" {    
};

// Блок справа вверху — latexcommand, Level3 и latexmath на верхней линии справа (SysRq, ScrollLock, Break)
// символы на четырёх/восьми уровнях, а действие (переключение раскладки) только на первом
// на третьем уровне PRSC и PAUS — PrintScreen и Pause, а не \ и $ — для единообразия с F1-F12, Ins, NumLock 
hidden partial modifier_keys xkb_symbols "common_all_sysbrk_latexcommand_latexmath_level3" {   
}; 

// ****************************************************************************
// Пробел, линия цифр (включая две правые клавиши — минус и равенство) и клавиши слева и справа от алфавитного блока:
// знаки препинания на линии цифр (включая точку и запятую), на втором уровне — скобки
// ****************************************************************************
partial alphanumeric_keys xkb_symbols "common_all_space_digitsline_innerpunctuation" {    
    key.type="EIGHT_LEVEL";    
    // Цифры в нижнем индексе на 7 уровне us-цифр, в верхнем — на 8, нормальные — на 3 уровне; на 4 от TLDE до us-8 символы 1-2 уровней американской раскладки
}; 
    
    
    
    // промилле ‰ и просантимилле ‱ — не парные символы, как «»„”, а последовательность, как ′″ ⇒ переносим на 89
    // на 7 и 8 уровнях «» — индексы-скобки; кратные штрихи ‴ и ⁗ пусть пока будут как продолжение последовательности ′″, но при необходимости перезаписать
    
    
    
// Символы, которые напечатаны на клавишах, но уже понятно, что надо переделать
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact_legacy_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AD03> {[	  u,		U,		uparrow,		U2909,			union,			logicalor,		U21D1,		VoidSymbol	]}; // u U ↑ ⤉   ∪ ∨ ⇑          
};

// Совместимость с надписями 2021
partial alphanumeric_keys xkb_symbols "lat_alph_legacy_patch" {
    include "illinc(lat_alph_jcuken_compact_legacy_patch)"   
};

// partial modifier_keys xkb_symbols "common_all_mod_menu_lat_tmp5" {
//     // key <MENU> {	type="EIGHT_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu,	VoidSymbol,		VoidSymbol],
//     //           actions[Group1] = [ NoAction(),	NoAction(),			NoAction(),	NoAction(),	RedirectKey(key=<TLDE>,clearMods=LevelFive) ]
//     // }; // ↑ тест: получается №, всё правильно; но Лат на 1 уровне не работает
//     
//     // key <MENU> {	type="EIGHT_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu,	VoidSymbol,		VoidSymbol],
//     //     actions[Group1]=[ NoAction(),			NoAction(),				NoAction(),	NoAction(),	RedirectKey(key=<TLDE>,clearMods=LevelFive)],
//     //     actions[Group2]=[ LockGroup(group=-1),	LockGroup(group=-1),	NoAction(),	NoAction(),	RedirectKey(key=<TLDE>,clearMods=LevelFive)],
//     //     actions[Group3]=[ LockGroup(group=-2),	LockGroup(group=-2),	NoAction(),	NoAction(),	RedirectKey(key=<TLDE>,clearMods=LevelFive)],
//     //     actions[Group4]=[ LockGroup(group=-3),	LockGroup(group=-3),	NoAction(),	NoAction(),	RedirectKey(key=<TLDE>,clearMods=LevelFive)]
//     // };
//     // // ↑ так работает    
//     
//      replace key <I128> {
//         type="ONE_LEVEL",
//         symbols = [ VoidSymbol ],
//         actions[Group1]=[ ],
//         actions[Group2]=[ SetGroup(group=-1) ],
//         actions[Group3]=[ SetGroup(group=-2) ],
//         actions[Group4]=[ SetGroup(group=-3) ]
//     };    
//     
//     key <MENU> {	type="EIGHT_LEVEL",	[ ISO_First_Group,ISO_First_Group,	Menu,	VoidSymbol,		VoidSymbol],
//         actions[Group1]=[ NoAction(),			NoAction(),				NoAction(),	NoAction(),	RedirectKey(key=<I128>,clearMods=all)],
//         actions[Group2]=[ LockGroup(group=-1),	LockGroup(group=-1),	NoAction(),	NoAction(),	RedirectKey(key=<I128>,clearMods=all)],
//         actions[Group3]=[ LockGroup(group=-2),	LockGroup(group=-2),	NoAction(),	NoAction(),	RedirectKey(key=<I128>,clearMods=all)],
//         actions[Group4]=[ LockGroup(group=-3),	LockGroup(group=-3),	NoAction(),	NoAction(),	RedirectKey(key=<I128>,clearMods=all)]
//     };
//     // ↑ всё равно Menu+Level5+символ получается Level5+символ, так что какой там язык — совершенно неясно
//     
// };

// partial modifier_keys xkb_symbols "common_all_mod_menu_lat_unlock" {
//     key <MENU> {	type="EIGHT_LEVEL",	
//         [ ISO_First_Group,	ISO_First_Group,		Menu,	VoidSymbol,		VoidSymbol], 
//         actions=[ NoAction(),	NoAction(),			NoAction(),	NoAction(),	LockMods(modifiers=None) ] };
//         // ↑ попытка добавить на Level5 + Лат «сброс всего»
//         // и CapsLock не сбрасывает, и Лат перестал включать
// };





// Знаки препинания [{]};:'" на ХЪЖЭ соответствуют qwerty, чтобы не загромождать клавиши надписями 
// (а набираются всё равно на линии Backspace [' на BKSL/цифровом блоке], так что вариант на ХЪЖЭЯЮЁ — аварийный)
// ,<.> смещены на ЮЁ (чтобы были рядом), /? — на Я
// 3-8 уровни с птичками ≤≰≺⊀≾≮ ≥≱≻⊁≿≯ тоже смещены на ЮЁ (ибо на Б ещё и β)

// По пожеланию Л.Ф. восстановлены греческие буквы, не используемые в~формулах → на свободные места 3-4 уровня
// Йота и ипсилон обе читаются как и/й:
// — йота ιΙ на И (чаще в греческом языке, и формой больше похожа на I латинскую, чем на J);
// — ипсилон υΥ на У (Greek_upsilon и иногда читается как У);
// — омикрон — на О, на 7-8 уровни (чтобы нельзя было сказать, что омикрона в раскладке нет). На 3-4 остаётся омега ωΩ.
// В формулах всё-таки используется не омикрон, а o малое, а настоящим грекам моя раскладка не нужна.
// Если омикрон οΟ мне хоть раз понадобится — подумаю о переносе.

// Upd3. В ru(winkeys) есть знак рубля U20BD на третьем уровне
// Раз декларируется, что ничего не пропало, пусть и тут будет — на 7 (как омикрон) уровне буквы R

partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact" {

    name[Group1]= "illinc — latin (jcuken compact)";
    key.type="EIGHT_LEVEL_ALPHABETIC";
    
    //							Shift	3				3+Shift			5				5+Shift			5+3			5+3+Shift
    key <AD01> {[	  j,		J,		VoidSymbol,		VoidSymbol,		VoidSymbol,		VoidSymbol,		U2023,		U02B2		]}; // j J           ‣ ʲ
    key <AD02> {[	  c,		C,		uparrow,		U21D1,			U2102,			VoidSymbol,		U2909,		VoidSymbol	]}; // c C ↑ ⇑   ℂ   ⤉
    key <AD03> {[	  u,		U,		Greek_upsilon,	Greek_UPSILON,	union,			logicalor,		VoidSymbol,	VoidSymbol	]}; // u U υ Υ   ∪ ∨ 
    key <AD04> {[	  k,		K,		Greek_kappa,	Greek_KAPPA,	VoidSymbol,		VoidSymbol,		U2096,		U1D4F		]}; // k K κ Κ       ₖ ᵏ
    key <AD05> {[	  e,		E,		Greek_epsilon,	Greek_EPSILON,	U2203,			U2204,			VoidSymbol,	VoidSymbol	]}; // e E ε Ε   ∃ ∄
    key <AD06> {[	  n,		N,		Greek_nu,		Greek_NU,		U2115,			notsign,		U2099,		U207F		]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	  g,		G,		Greek_gamma,	Greek_GAMMA,	degree,			VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // g G γ Γ   °
    // ШЩ при переопределённых F1-F12 — ❌✓ на 3 уровне, верхние пусты (кавычки на F11‒F12)
    // ❌ отображается не везде; а здесь — с неадекватной шириной; — но ничего лучше не находится
    //key <AD08> {[	  h,		H,		U274C,			VoidSymbol,		U210D,			VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // h H ❌     ℍ   
    //key <AD09> {[	  v,		V,		checkmark,		VoidSymbol,		emptyset,		U27E1,			VoidSymbol,	VoidSymbol	]}; // v V ✓     ∅ ⟡     
    // Если F1-F12 оригинальные — на ШЩ кавычки ⇒ ❌✓ надо продублировать на 7 уровне. На 8 английские лапки “” — ёлочки у них те же «»
    //key <AD08> {[	  h,		H,		U274C,		VoidSymbol,		U210D,		VoidSymbol,		U274C,		leftdoublequotemark	]}; // h H ❌     ℍ  ❌ “
    //key <AD09> {[	  v,		V,		checkmark,	VoidSymbol,		emptyset,	U27E1,			checkmark,	rightdoublequotemark]}; // v V ✓     ∅ ⟡ ✓ ” 
    // может, узаконить и «»„“ на 3-4, чтобы не переучиваться? ↓ не ошибка, русская правая ≡ английская левая лапка
    key <AD08> {[	  h,H,		guillemotleft,	doublelowquotemark,		U210D,	VoidSymbol,		U274C,		leftdoublequotemark	]}; // h H « „   ℍ   ❌ “
    key <AD09> {[	  v,V,		guillemotright,	leftdoublequotemark,	emptyset,	U27E1,		checkmark,	rightdoublequotemark]}; // v V » “   ∅ ⟡ ✓ ”
    // ↑ если хоть раз помешает — вернуть ❌✓ на 3 уровень
    key <AD10> {[	  z,		Z,		Greek_zeta,		Greek_ZETA,		U2124,			VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // z Z ζ Ζ   ℤ
    key <AD11> {[ bracketleft,	braceleft,	Greek_chi,	Greek_CHI,		U2286,			U2288,			includedin,	U2284		]}; // [ { χ Χ   ⊆ ⊈ ⊂ ⊄ 
    key <AD12> {[ bracketright,	braceright,	Greek_psi,	Greek_PSI,		U2287,			U2289,			includes,	U2285		]}; // ] } ψ Ψ   ⊇ ⊉ ⊃ ⊅ 
  
    key <AC01> {[	  f,		F,		Greek_phi,	Greek_PHI,	VoidSymbol,VoidSymbol,enfilledcircbullet,VoidSymbol	]}; // f F φ Φ    •
    key <AC02> {[	  y,		Y,		leftarrow,	U21D0,			U21A4,		U21A9,		U219A,		U21CD		]}; // y Y ← ⇐    ↤ ↩ ↚ ⇍
    key <AC03> {[	  w,		W,		rightarrow,	implies,		U21A6,		U21AA,		U219B,		U21CF		]}; // w W → ⇒    ↦ ↪ ↛ ⇏
    key <AC04> {[	  a,		A,		Greek_alpha,Greek_ALPHA,	U2200,		VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // a A α Α    ∀
    key <AC05> {[	  p,		P,		Greek_pi,	Greek_PI,		U220F,		U220F,		U209A,		U1D56		]}; // p P π Π    ∏ ∏ ₚ ᵖ
    key <AC06> {[	  r,		R,		Greek_rho,	Greek_RHO,		U211D,		VoidSymbol,	U20BD,		VoidSymbol	]}; // r R ρ Ρ    ℝ   ₽
    key <AC07> {[	  o,		O,		Greek_omega,Greek_OMEGA,	U1D546,	VoidSymbol,	Greek_omicron,Greek_OMICRON	]}; // o O ω Ω    𝕆   ο Ο
    key <AC08> {[	  l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // l L λ Λ    ℓ   
    key <AC09> {[	  d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,	partdifferential,	nabla	]}; // d D δ Δ    ∆ ⏨ ∂ ∇   5 ∆=приращение
    key <AC10> {[ semicolon,	    colon,	Greek_theta,Greek_THETA,elementof,notelementof,	VoidSymbol,	VoidSymbol	]}; // ; : θ Θ    ∈ ∉     
    key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,	containsas,	U220C,		VoidSymbol,	VoidSymbol	]}; // ' " η Η    ∋ ∌

    key <AB01> {[	  slash, question,	U2194,	ifonlyif,		VoidSymbol,	VoidSymbol,	U21AE,		U21CE		]}; // / ? ↔ ⇔        ↮ ⇎
    key <AB02> {[	  q,	Q, 		downarrow,	U21D3,			U211A,		U220E,		U2908,		VoidSymbol	]}; // q Q ↓ ⇓    ℚ ∎ ⤈   
    key <AB03> {[	  s,	S,		Greek_sigma,Greek_SIGMA,	U2211,		U2211,		U209B,		VoidSymbol	]}; // s S σ Σ    ∑ ∑ ₛ
    key <AB04> {[	  m,	M,		Greek_mu,	Greek_MU,		U25B3,		U2295,		U2098,		U1D50		]}; // m M μ Μ    △ ⊕ ₘ ᵐ   5 △=симм.разность мн-в
    key <AB05> {[	  i,	I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,	U1D62,		U2071		]}; // i I ι Ι    ∩ ∧ ᵢ ⁱ
    key <AB06> {[	  t,	T,		Greek_tau,	Greek_TAU,		U1D40,		U1D40,		U209C,		U1D57		]}; // t T τ Τ    ᵀ ᵀ ₜ ᵗ   5 занят условно, ᵀ логичнее на 6    
    key <AB07> {[     x,	X,		Greek_xi,	Greek_XI,		multiply,	U2297,		VoidSymbol,	VoidSymbol	]}; // x X ξ Ξ    × ⊗
    key <AB08> {[	  b,	B,		Greek_beta,	Greek_BETA,		infinity,	VoidSymbol,	VoidSymbol,	VoidSymbol	]};	// b B β Β    ∞
    // На 3 уровне ≤≥ с равенством, на 5 уровне ≺≻ птички ⇒ на 4 и 6 перечёркивание 3 и 5 (Математические операторы); 
    // на 7 помесь 3 и 5 ≾≿ (Математические операторы), но перечёркнутых ≾≿ для 8 уровня нет в этом разделе unicode (и вообще не нашлось)
    // на 8 (последнем) русские ⩽⩾ из раздела Дополнительные математические операторы (перечёркнутых ни ≾≿, ни ⩽⩾ нет и там — либо не отображаются)
    key <AB09> {[     comma,	less,	lessthanequal,	    U2270,		U227A,	U2280,	U227E,	U2A7D	]}; // , < ≤ ≰     ≺ ⊀ ≾ ⩽ 
    key <AB10> {[    period,	greater,greaterthanequal,   U2271,		U227B,	U2281,	U227F,	U2A7E	]}; // . > ≥ ≰     ≻ ⊀ ≿ ⩾ 
    // поменять местами 7 и 8 уровни или вообще перенести ⩽⩾ на 5 уровень :,[]? Если ⩽⩾ понадобятся ещё раз — подумаю 
    // (не во всех шрифтах ⩽⩾ отображаются красивее ≤≥)
        
};



// Полной читабельности мешают:
// — V вместо Щ: однбуквенного соответствия Щ, даже приблизительного, не существует;
// — Z вместо Я: если хочу совместимое Ctrl+Z, иначе не получится
// — ,. вместо ЮЁ, хотя для Ю более точным будет U (a для Ё нет однобуквенного соответствия: O непонятно, Е неправильно): можно попробовать поменять
// Или вернуть знаки qwerty на ХЪЖЭ?..
// для cCцЦ: ← нет ни перечёркнутой ⇑, ни нижнего индекса c. Поэтому ⤉ на 7, но ᶜ на 8
    //key <AD08> {[	  h,H,		guillemotleft,	doublelowquotemark,		U210D,	VoidSymbol,		U274C,		leftdoublequotemark	]}; // h H « „   ℍ   ❌ “
    //key <AD09> {[	  v,V,		guillemotright,	leftdoublequotemark,	emptyset,	U27E1,		checkmark,	rightdoublequotemark]}; // v V » “   ∅ ⟡ ✓ ”
// а вот для hH vV есть оба индекса! так ли нужны английкие кавычки “”?
// У dD есть только верхний индекс; у qQ — ни одного нет


— английские одинарные ‘марровские’ (американские внутренние) U2018/U2019 leftsinglequotemark/rightsinglequotemark → временно на 6 уровне «»
    key <FK11> {		[ guillemotleft,	doublelowquotemark,		F11,F11, 	U2034,leftsinglequotemark,	U208D,U207D	]}; // « „   ‴ ‘ ₍⁽   
    key <FK12> {		[ guillemotright,	leftdoublequotemark,	F12,F12, 	U2057,rightsinglequotemark,	U208E,U207E	]}; // » “   ⁗ ’ ₎⁾


    //key <AD01> {[	j,		J,	VoidSymbol,		VoidSymbol,		VoidSymbol,	VoidSymbol,		U2C7C,		U02B2	]}; // j J           ⱼ ʲ
    //key <AD12> {[apostrophe,apostrophe,	Greek_psi,Greek_PSI,	U2287,		U2289,			includes,	U2285	]}; // ' ' ψ Ψ   ⊇ ⊉ ⊃ ⊅ 
    


// ****************************************************************************
// Алфавитный блок (русские буквы / латинские буквы )
// (-) и знаки препинания ЛАТ ← знаки препинания выше, на линии Backspace ⇒ здесь не обязательны
// ****************************************************************************
// Расположение букв латиницы, кроме Q, H, V, соответствует оригинальной БК Электроника
// Буквы латиницы расположены сплошным блоком, без дыр:
// — Q на Ч, ибо иметь Ctrl+Q вместо Ctrl+Z неудобно: не сам, так кто-то другой нажмёт, а подтверждение запрашивается не всегда;
// — H (аш) на Ш; 
// — V на Щ: а) это последняя дырка в середине блока; б) Ctrl+V — ой, не то — Ctrl+Z набирается одной распальцовкой
// и вообще так можно кое-как дотянуться от левого Ctrl 
// upd 22.12.2022: в) как выяснилось, в белорусской раскладке на этом месте Ў (у краткое) — и там получается почти фонетическое соответствие
// для совсем полного надо W на Щ/Ў, V на В — но W чаще и в C++ (while), и в английском, и в немецком. И в БК на В именно оно

// Не заплатка, а новая сборка: h (аш) дублируем на Ш (удобное место) и на Х (ха, фонетическое)
// → раз на ХЪ не парные скобки ⇒ ставим на Ъ фонетический '
// Tак как Л. Ф. всё время спотыкается о несоответствие Ctrl+Z, а /? на яЯ я не пользуюсь → продублировать и Z
// Upd. Может быть, раз продублированы hH и zZ — поставить и вторые копии gG и eE на Ж и Э? 
// Чтобы несменённая ЛАТ→РУС раскладка была ближе к читабельному транслиту.
// ↓↓↓ в алфавитном блоке на 1-2 уровнях только буквы (и апостроф ')



// ↓↓↓ возможно, ❌✓ и «» перестанет лихорадить, и заплатки можно будет удалить

// (?)Основной вариант — с ❌✓ на 3 уровне основного блока и кавычками на первом уровне верхней линии
// определим заплатку для помещения «»„“ в основной блок (на случай, если F1-F12 не переопределяются)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact_quotemarks_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AD08> {[	  h,		H,		guillemotleft,	doublelowquotemark		]}; // h H « „   
    key <AD09> {[	  v,		V,		guillemotright,	leftdoublequotemark		]}; // v V » “ ← не ошибка, русская правая ≡ английская левая лапка
};

// Если основной вариант ШЩ, наоборот, будет с кавычками на 3-4 уровне — определим и заплатку для ❌✓ на 3 уровне
// (если есть «»„“ на 1-2 уровнях F1-F12 — 3 уровень ШЩ можно считать свободным)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact_stop_ok_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AD08> {[	  h,		H,		U274C		]}; // h H ❌  
    key <AD09> {[	  v,		V,		checkmark	]}; // v V ✓
};

// Оригинальное размещение букв латиницы с БК Электроники: H (аш) на Х (ха), V на Ж, Q на Я
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_bk_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";    
    key <AD08> {[ bracketleft,	braceleft,	U274C,		VoidSymbol,		U2286,		U2288,			includedin,	U2284		]}; // [ { ❌     ⊆ ⊈ ⊂ ⊄  Ш  
    key <AD09> {[ bracketright,	braceright,	checkmark,	VoidSymbol,		U2287,		U2289,			includes,	U2285		]}; // ] } ✓     ⊇ ⊉ ⊃ ⊅  Щ  
    key <AD11> {[	  h,		H,			Greek_chi,	Greek_CHI,		U210D,		VoidSymbol,		VoidSymbol,	VoidSymbol	]}; // h H χ Χ   ℍ        Х
    key <AD12> {[ semicolon,	    colon,	Greek_psi,	Greek_PSI,		elementof,notelementof,		VoidSymbol,	VoidSymbol	]}; // ; : ψ Ψ   ∈ ∉      Ъ

    key <AC10> {[ 	  v,		V,			Greek_theta,Greek_THETA,	emptyset,	U27E1,			VoidSymbol,	VoidSymbol	]}; // v V θ Θ   ∅ ⟡      Ж
    key <AC11> {[ apostrophe,	 quotedbl,	Greek_eta,	Greek_ETA,		containsas,	U220C,			VoidSymbol,	VoidSymbol	]}; // ' " η Η   ∋ ∌      Э
    
    key <AB01> {[	  q,		Q, 			U2194,	ifonlyif,			U211A,		VoidSymbol,		U21AE,		U21CE		]}; // q Q ↔ ⇔   ℚ ↮      Я
    key <AB02> {[	  slash, question,		downarrow,	U21D3,			VoidSymbol,	VoidSymbol,		U2908,		VoidSymbol	]}; // / ? ↓ ⇓     ⤈      Ч
    
};
// ↑↑↑ если сюда наложить lat_alph_jcuken_compact_quotemarks_patch/lat_alph_jcuken_compact_stop_ok_patch, символы []{} на 1-2 уровнях заменятся на HV 
// но основное расположение []{} всё равно на линии Backspace (us-цифр) ⇒ отдельный *_quotemarks_patch не делаю 
// Может быть, вообще продублировать HVQ по умолчанию (не в compact, а только здесь)?..
// В compact знаки препинания справа дублируют qwerty (меньше надписей в слое fallback qwerty), а / на Я — чтобы Ctrl+Q не было на месте Ctrl+Z ⇒ нужны
// А вот здесь — можно было и VoidSymbol'ы ставить на 1-2 уровни ШЩЪЭЧ




// Символы |& — под ∨∧ на 3 уровень (вместо υι)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact_and_or_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    // на 3 уровне |&, на 4 строчные υι (заглавная греческая Ι совпадает по начертанию с латинской I, заглавный ипсилон Υ — с латинским игреком ⇒ опускаем)
    // также ипсилон на 7-8 уровни, как омикрон, пока там не занято
    key <AD03> {[	  u,	U,		bar,		Greek_upsilon,	union,			logicalor,		Greek_upsilon,	Greek_UPSILON	]}; // u U | υ   ∪ ∨ υ Υ
    key <AB05> {[	  i,	I,		ampersand,	Greek_iota,		intersection,	logicaland,		U1D62,			U2071			]}; // i I & ι   ∩ ∧ ᵢ ⁱ
};


// Символы |& — под ∨∧ на 3 уровень (вместо υι)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_compact_and_or_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    // на 3 уровне |&, на 4 строчные υι (заглавная греческая Ι совпадает по начертанию с латинской I, заглавный ипсилон Υ — с латинским игреком ⇒ опускаем)
    // 5-8 уровни не переписываем
    key <AD03> {[	  u,	U,		bar,		Greek_upsilon	]}; // u U | υ   
    key <AB05> {[	  i,	I,		ampersand,	Greek_iota		]}; // i I & ι   
};
// ↑↑↑ а точно надо? &| теперь есть на линии Backspace

    // При переопределённом как угодно BKSL, если при этом на F1-F12 оригинальные F1-F12 — теряется апостроф ' и иногда |
    // include "illinc(common_ansi_apostrophe_patch)" // '| на 3-4 уровнях ;\ (крайней правой клавиши перед Backspace)

// (-) Для 61-клавишной ANSI или если одновременно используются оригинальные F1-F12 и переопределяется BKSL — нет апострофа '
// также может не быть | (вместе с $/Break)
// Определяем '| на крайней правой клавише перед Backspace
partial alphanumeric_keys xkb_symbols "common_ansi_apostrophe_patch" {    
    key	<AE12> {type="EIGHT_LEVEL", [ semicolon,	backslash,	apostrophe,	bar]}; // ;\ '| и т. д
};
// ↑↑↑ апостроф ' и вертикальная палка | есть! 
// Теперь апостроф ' на Ъ, | есть на 4 уровне ?( — последнее пустое место 1-4 уровней основного блока
// кроме того, теперь | на 2 уровне не $/Break, а \/SysRq, которая чаще есть.
// то есть ;\ можно не портить



// ****************************************************************************
// Связки (варианты алфавитного блока) РУС/ЛАТ и отдельно общая часть *********
// ****************************************************************************

// Алфавитный блок РУС
partial alphanumeric_keys xkb_symbols "rus_alph" {
    //include "illinc(rus_alph_winkeys)"
    include "illinc(rus_alph_patch)"   
};

// Алфавитный блок ЛАТ (без совместимости с устаревшими надписями на клавиатуре)
partial alphanumeric_keys xkb_symbols "lat_alph" {
    include "illinc(lat_alph_jcuken)"
};    


//*****************************************************************************
// // Полная раскладка для 105 текущая — не нужна! Или общий lat, или illinc_local(lat)
// //$ setxkbmap "illinc(lat105),illinc(rus105)"
// partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "lat105" { include "illinc(lat)"  include "illinc(common_keypad_braces)" }; 
// partial alphanumeric_keys modifier_keys keypad_keys xkb_symbols "rus105" { include "illinc(lat105)" include "illinc(rus_alph_patch)" };


//*****************************************************************************
// Полная раскладка для текущей клавиатуры с латиницей-qwerty (если какому-то студенту понадобится надолго отдать управление)
// вариант РУС для lat_qwerty — это обычный rus
//$ setxkbmap "illinc(lat_qwerty),illinc(rus)"
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "lat_qwerty" {    
    include "illinc(lat)"
    include "illinc(lat_alph_qwerty_patch)"
};

    //include "illinc(lat_alph_jcuken_bk_patch)" // оригинальное размещение латиницы с БК Электроники: H (аш) на Х (ха), V на Ж, Q на Я

    
    // По умолчанию F1-F12 — цифры и «»: если на клавиатуре F1-F12 доступны только через аппаратный модификатор Fn — бессмысленно
    //include "illinc(lat_88_originalfkeys)" // восстановление оригинальных F1-F12;             символы «»„” → 3-4 уровни ШЩ 
    // ↑↑↑ не патч, а полный 88-клавишный блок (tenkeyless) ⇒ lat_88_default можно удалить;     ralt не переопределяется

//     replace key <PRSC> {	[backslash,	ampersand, 		Print,	Sys_Req,	logicaland,	logicaland,	VoidSymbol,	VoidSymbol	], // \& PrScr SysRq   ∧∧ 
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};        
//     replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
//     replace key <PAUS> {	[ dollar,	bar, 			Pause,	Break,		logicalor,	logicalor,	U2225,		U2226		], // $| Pause Break   ∨∨  ∥∦
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  
       
//     replace key <PRSC> {	[backslash,	bar, 		Print,	Sys_Req,	logicalor,	logicalor,	U2225,		U2226		], // \| PrScr SysRq   ∨∨ ∥∦
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};        
//     replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
//     replace key <PAUS> {	[ dollar,	ampersand, 	Pause,	Break,		logicaland,	logicaland,	VoidSymbol,	VoidSymbol	], // $& Pause Break   ∧∧ 
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  

    // надо ли дублировать ∨∧, которые есть хоть и на 6 уровне, но в алфавитном блоке?
//     replace key <PRSC> {	[backslash,	bar, 		Print,	Sys_Req,	VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	], // \| PrScr SysRq   
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};        
//     replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
//     replace key <PAUS> {	[ dollar,	ampersand, 	Pause,	Break,		VoidSymbol,	VoidSymbol,	VoidSymbol,	VoidSymbol	], // $& Pause Break   
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  
//     // ↑↑↑ если приживётся — возможно, апостроф перенести обратно на $Лат, а & — на 9, поближе к ()?
    // ↓↓↓ сразу пробуем!       
    //key <FK10> {									[9, apostrophe,	F10,F10, 	seconds,	U2031,		U208A,U207A	]}; // 9 '   ″ ‱ ₊⁺

    //replace key <INS> {type="LOCAL_EIGHT_LEVEL", [ percent, percent, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  
    // ↑↑↑ а так вставка по Shift+Insert без Level3 не работает там, где выше — работала (Shift+Insert ≡ %)

    //replace key <RALT> { type="TWO_LEVEL",		symbols[Group1] = [ asciitilde, nobreakspace ] };
    //replace key <RALT> { type="EIGHT_LEVEL",	[ asciitilde, nobreakspace, nobreakspace, nobreakspace, 	U2241 ] }; // ~ nbsp nbsp nbsp  ≁ ← всё равно не ≁

    //key <AD01> {[	j,		J,	Greek_psi,		Greek_PSI,		VoidSymbol,	VoidSymbol,		U2C7C,		U02B2	]}; // j J ψ Ψ       ⱼ ʲ
    //key <AC10> {[	g,		G,		Greek_theta,Greek_THETA,	elementof,notelementof,	VoidSymbol,	VoidSymbol	]}; // g G θ Θ   ∈ ∉ 



// ****************************************************************************
// 88-клавишный блок в целом: алфавитный блок + модификаторы (кроме RAlt) + пробел и линия Backspace + верхняя линия
// ****************************************************************************
partial alphanumeric_keys function_keys modifier_keys xkb_symbols "lat_88" {    
    include "illinc(lat_alph)"                      // алфавитный блок ЛАТ 
    include "illinc(lat_alph_jcuken_combining_acute_dot_patch)"  // комбинируемые ударение (Ю) и точка (Ё) на 8 уровне + перезапись 1-7 уровней ЮЁ как в lat_alph
    //include "illinc(lat_alph_jcuken_makeatletter_patch)" // @ в алфавитном блоке (@ на Э)
    //include "illinc(lat_alph_jcuken_legacyCtrlZ_patch)"  // на Я вместо второй А — вторая Z (остаток qwerty)
    //include "illinc(lat_alph_jcuken_make_c_address_op_letter_patch)"  // на Я &    
    include "illinc(lat_alph_jcuken_at_octave_parenthesis_patch)"  // @=()[] на 1 уровне в алфавитном блоке (@ на Я)
 
    // слева
    include "illinc(common_all_mod_lwin_rus_CapsLk)"
    include "illinc(common_all_mod_caps_level3)"

    // справа
    include "illinc(common_all_mod_rwin_level5)"
    include "illinc(common_all_mod_menu_lat)"
    
    key.type="EIGHT_LEVEL"; 
    // линия Backspace (us-цифр), клавиши в основном блоке и пробел ***********
    key <TLDE> {[ quotedbl,		section,		0,	grave,			U2213,		VoidSymbol,	zerosubscript,	zerosuperior	]}; // "§ 0`  ∓  ₀⁰
    //key <TLDE> {[ quotedbl,	section,	grave,	asciitilde,		U2213,		VoidSymbol,	zerosubscript,	zerosuperior	]}; // "§ `~  ∓  ₀⁰   
    key <AE01> {[ plus,			numerosign,		1,	exclam,			plusminus,	U2295,		onesubscript,	onesuperior		]}; // +№ 1!  ±⊕ ₁¹
    key <AE02> {[ minus,		at,				2,	U2011,			U2212,		U2296,		twosubscript,	twosuperior		]}; // -@ 2@  −⊖ ₂²    
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		threesubscript,	threesuperior	]}; // /< 3#  ≮⊘ ₃³
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		foursubscript,	foursuperior	]}; // *> 4$  ≯⊛ ₄⁴
    key <AE05> {[ colon, 		bracketleft,	5,	percent,		U2A7D,		U22EE,		fivesubscript,	fivesuperior	]}; // :[ 5%  ⩽⋮ ₅⁵
    key <AE06> {[ comma,		bracketright,	6,	asciicircum,	U2A7E,		U27A4,		sixsubscript,	sixsuperior		]}; // ,] 6^  ⩾➤ ₆⁶
    key <AE07> {[ period,		braceleft,		7,	ampersand,		U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .{ 7&  ⋅⊙ ₇⁷
    key <AE08> {[ underscore,	braceright,		8,	asterisk,		multiply,	U2297,		eightsubscript,	eightsuperior	]}; // _} 8*  ×⊗ ₈⁸
    key <AE09> {[ question,		parenleft,		9,	bar,			VoidSymbol,	VoidSymbol,	ninesubscript,	ninesuperior	]}; // ?( 9|     ₉⁹
    key <AE10> {[ emdash,		parenright,		0,	endash,			figdash,	U22EF,		zerosubscript,	zerosuperior	]}; // —) 0–  ‒⋯ ₀⁰            
    key <AE11> {[ exclam,		equal,		minus,	underscore,		notequal, 	U229C,		identical,		notidentical	]}; // != -_  ≠⊜ ≡≢
    key	<AE12> {[ semicolon,	backslash,	equal,	plus,			approxeq,	U2249,		hyphen,			VoidSymbol		]}; // ;\ =+  ≈≉ shy(мягкий перенос)
    
   
            
    // так как LSGT скорее левее BKSL, чем ниже — перечислим слева направо
    key <LSGT> {[ numbersign,	grave					]}; // #`
    key <BKSL> {[ asciicircum,	asciicircum,	backslash,	bar	]}; // ^^ \|
    
    key <SPCE> {[ space,		asciitilde,		asciitilde,	nobreakspace,	U2241,hairspace,	signifblank,digitspace		]}; //  ~ ~ nbsp  ≁ волосяная   ␣ figspace
       
    // Insert (LOCAL_EIGHT_LEVEL для Ctrl+Insert без Level3; при этом Shift+Insert без Level3 работает часто, но не везде)
    replace key <INS> {type="LOCAL_EIGHT_LEVEL", [ percent, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  
    // ↑↑↑ В режиме CapsLock получается Insert на первом уровне! Но режим редкий...
    
    
    // верхняя линия: F1-F12, SysRq, ScrollLock, Break ************************
    
    key <FK01> { type="CTRL+ALT", 	symbols[Group1]=[0, grave,		F1, F1, 	XF86_Switch_VT_1 	]}; // 0 `
    key <FK02> { type="CTRL+ALT", 	symbols[Group1]=[1, numbersign,	F2, F2, 	XF86_Switch_VT_2 	]}; // 1 #
    key <FK03> { type="CTRL+ALT", 	symbols[Group1]=[2, U27E8,		F3, F3, 	XF86_Switch_VT_3 	]}; // 2 ⟨
    key <FK04> { type="CTRL+ALT", 	symbols[Group1]=[3, U27E9,		F4, F4, 	XF86_Switch_VT_4 	]}; // 3 ⟩
                                                                                                             
    key <FK05> { type="CTRL+ALT", 	symbols[Group1]=[4, U230A,		F5, F5, 	XF86_Switch_VT_5 	]}; // 4 ⌊
    key <FK06> { type="CTRL+ALT", 	symbols[Group1]=[5, U230B,		F6, F6, 	XF86_Switch_VT_6 	]}; // 5 ⌋
    key <FK07> { type="CTRL+ALT", 	symbols[Group1]=[6, U2308,		F7, F7, 	XF86_Switch_VT_7 	]}; // 6 ⌈    
    //key <FK08> {									[7, U2309,		F8, F8, 	degree, 	U27E1,		U208C,U207C	]}; // 7 ⌉   ° ⟡ ₌⁼
    //key <FK09> {									[8, asciicircum,F9, F9,  	minutes,	permille,	U208B,U207B	]}; // 8 ^   ′ ‰ ₋⁻
    //key <FK10> {									[9, ampersand,	F10,F10, 	seconds,	U2031,		U208A,U207A	]}; // 9 &   ″ ‱ ₊⁺
    //key <FK11> {		[ guillemotleft,	doublelowquotemark,		F11,F11, 	U2034,		U25C1,		U208D,U207D	]}; // « „   ‴ ◁ ₍⁽   
    //key <FK12> {		[ guillemotright,	leftdoublequotemark,	F12,F12, 	U2057,		U25B7,		U208E,U207E	]}; // » “   ⁗ ▷ ₎⁾
    
    key <FK08> {									[7, U2309,		F8, F8, 	degree, 	U27E1,		VoidSymbol,	VoidSymbol	]}; // 7 ⌉   ° ⟡ 
    key <FK09> {									[8, asciicircum,F9, F9,  	minutes,	permille,	VoidSymbol,	VoidSymbol	]}; // 8 ^   ′ ‰ 
    key <FK10> {									[9, ampersand,	F10,F10, 	seconds,	U2031,		VoidSymbol,	VoidSymbol	]}; // 9 &   ″ ‱ 
    key <FK11> {		[ guillemotleft,	doublelowquotemark,		F11,F11, 	U2286,		U2288,		includedin,	U2284		]}; // « „   ⊆ ⊈ ⊂ ⊄   
    key <FK12> {		[ guillemotright,	leftdoublequotemark,	F12,F12, 	U2287,		U2289,		includes,	U2285		]}; // » “   ⊇ ⊉ ⊃ ⊅
    
    include "illinc(lat_alphanumeric_punctuation_f)"  

    // ScrollLock как правый Level3, PRSC как \Лат с Print, Sys_Req на 3-4 уровнях, PAUS как $Лат
    replace key <PRSC> {	[backslash,	bar, 		Print,Sys_Req,		U2216,U29B8,		  U2225,U2226	], // \| PrScr SysRq   ∖⦸ ∥∦
       actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};        

    replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
    replace key <PAUS> {	[ dollar,	apostrophe,	Pause,Break,		VoidSymbol,VoidSymbol,	VoidSymbol,VoidSymbol	], // $' Pause Break   
       actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  
        
    modifier_map Mod5   { ISO_Level3_Shift };
    
};
    //key <AB09> {[bracketleft,bracketleft,	lessthanequal,	 U2270,	U227A,	U2280,			U227E,		U0303	]}; // [ [ ≤ ≰   ≺ ⊀ ≾ ̃x   8 ̃=комб.волна
    //key <AB10> {[bracketright,bracketright,	greaterthanequal,U2271,	U227B,	U2281,			U227F,		U0307	]}; // ] ] ≥ ≰   ≻ ⊁ ≿ ẋ   8 ̇=комб.производная по t
    //key <FK09> {									[8, asciicircum,F9, F9, 	minutes,permille,	U2023,	U0307	]}; // 8 ^  ′ ‰  ‣ ẋ  8 ̇=комб.производная по t
    //key <FK10> {									[9, ampersand,	F10,F10,	seconds,U2031,		U27A4,	U0308	]}; // 9 &  ″ ‱  ➤ ̈x  8 ̈=комб.«вторая» ⟨умляут⟩











// ****************************************************************************
// Алфавитный блок: {русские буквы} / {латинские буквы + апостроф'}
// ****************************************************************************

// Латиница+апостроф и верхние (общие у латиницы и кириллицы) 3-8 уровни
// Расположение букв латиницы, кроме Q и V, соответствует оригинальной БК Электроника (H есть и на БК-шном месте тоже).
// Буквы латиницы расположены сплошным блоком, без дыр:
// — Q на Ч, ибо иметь Ctrl+Q вместо Ctrl+Z неудобно: не сам, так кто-то другой нажмёт, а подтверждение запрашивается не всегда;
// — H (аш) на Ш + дубль на Ха; 
// — V на Щ: а) это последняя дырка в середине блока; б) Ctrl+V — ой, не то — Ctrl+Z набирается одной распальцовкой
// и вообще так можно кое-как дотянуться от левого Ctrl 
// upd 22.12.2022: в) как выяснилось, в белорусской раскладке на этом месте Ў (у краткое) — и там получается почти фонетическое соответствие.
//
// На Ъ фонетически соответствующий ему апостроф ' — не только на 1-2 уровнях (в латинице), но и на 3-4 (сохраняется везде).
//
// Часть букв латиницы продублированы, чтобы каждой букве кириллицы, кроме Ъ, соответствовала латинская буква:
// — h (аш) дублируем на Ш (удобное место) и на Х (ха, фонетическое);
// — вторая G на Ж, вторая E на Э, вторые U и O на Ю и О; вторая A на Я (ниже заплатки, замещающие дубли букв символами).

partial alphanumeric_keys xkb_symbols "lat_alph" {

    name[Group1]= "illinc — latin (jcuken)";
    key.type="EIGHT_LEVEL_ALPHABETIC";
        
    //							Shift	3				3+Shift			5			5+Shift			5+3			5+3+Shift
    key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	VoidSymbol,	VoidSymbol,		U2C7C,		U02B2	]}; // j J θ Θ       ⱼ ʲ
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		U207A,			U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ ⁺ ⤉ ᶜ   6 ⁺=верхний индекс (заряд)
    key <AD03> {[	u,		U,	Greek_upsilon,	Greek_UPSILON,	union,		logicalor,		U1D64,		U1D58	]}; // u U υ Υ   ∪ ∨ ᵤ ᵘ
    key <AD04> {[	k,		K,	Greek_kappa,	Greek_KAPPA,	VoidSymbol,	U207A,			U2096,		U1D4F	]}; // k K κ Κ     ⁺ ₖ ᵏ   6 ⁺ условно!
    key <AD05> {[	e,		E,	Greek_epsilon,	Greek_EPSILON,	U2203,		U2204,			U2091,		U1D49	]}; // e E ε Ε   ∃ ∄ ₑ ᵉ
    key <AD06> {[	n,		N,	Greek_nu,		Greek_NU,		U2115,		notsign,		U2099,		U207F	]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	degree,		VoidSymbol,		VoidSymbol,	U1D4D	]}; // g G γ Γ   °     ᵍ
    key <AD08> {[	h,		H,	guillemotleft,doublelowquotemark,	U210D,	U274C,			U2095,		U02B0	]}; // h H « „   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	guillemotright,leftdoublequotemark,	emptyset,checkmark,		U1D65,		U1D5B	]}; // v V » “   ∅ ✓ ᵥ ᵛ   4 “ рус.правая=англ.левая
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		VoidSymbol,		VoidSymbol,	U1DBB	]}; // z Z ζ Ζ   ℤ     ᶻ
    key <AD11> {[	h,		H,	Greek_chi,		Greek_CHI,		U2286,		U2288,			includedin,	U2284	]}; // h H χ Χ   ⊆ ⊈ ⊂ ⊄ 
    key <AD12> {[apostrophe,apostrophe,	apostrophe,apostrophe,	U2287,		U2289,			includes,	U2285	]}; // ' ' ' '   ⊇ ⊉ ⊃ ⊅
  
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		VoidSymbol,	VoidSymbol,		VoidSymbol,	U1DA0	]}; // f F φ Φ         ᶠ
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U21A4,		U21A9,			U219A,		U21CD	]}; // y Y ← ⇐   ↤ ↩ ↚ ⇍
    key <AC03> {[	w,		W,		rightarrow,	implies,		U21A6,		U21AA,			U219B,		U21CF	]}; // w W → ⇒   ↦ ↪ ↛ ⇏
    key <AC04> {[	a,		A,		Greek_alpha,Greek_ALPHA,	U2200,		U207B,			U2090,		U1D43	]}; // a A α Α   ∀ ⁻ ₐ ᵃ   6 ⁻=верхний индекс (заряд)
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		hyphen,			U209A,		U1D56	]}; // p P π Π   ∏ - ₚ ᵖ   6 -=мягкий перенос (shy)
    key <AC06> {[	r,		R,		Greek_rho,	Greek_RHO,		U211D,		U20BD,			U1D63,		U02B3	]}; // r R ρ Ρ   ℝ ₽ ᵣ ʳ
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,	enfilledcircbullet,	U2092,		U1D52	]}; // o O ω Ω   𝕆 • ₒ ᵒ
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U2023,			U2097,		U02E1	]}; // l L λ Λ   ℓ ‣ ₗ ˡ
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,	partdifferential,	nabla	]}; // d D δ Δ   ∆ ⏨ ∂ ∇   5 ∆=приращение
    key <AC10> {[	g,		G,		Greek_psi,	Greek_PSI,		elementof,notelementof,	VoidSymbol,	VoidSymbol	]}; // g G ψ Ψ   ∈ ∉ 
    key <AC11> {[	e,		E,		Greek_eta,	Greek_ETA,		containsas,	U220C,		VoidSymbol,	VoidSymbol	]}; // e E η Η   ∋ ∌ 

    key <AB01> {[	a,		A,		U2194,		ifonlyif,		VoidSymbol,	VoidSymbol,		U21AE,		U21CE	]}; // a A ↔ ⇔       ↮ ⇎
    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,	VoidSymbol	]}; // q Q ↓ ⇓   ℚ ∎ ⤈   
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		U2211,			U209B,		U02E2	]}; // s S σ Σ   ∑ ∑ ₛ ˢ
    key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		U25B3,		U2295,			U2098,		U1D50	]}; // m M μ Μ   △ ⊕ ₘ ᵐ   5 △=симм.разность мн-в
    key <AB05> {[	i,		I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,		U1D62,		U2071	]}; // i I ι Ι   ∩ ∧ ᵢ ⁱ
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U1D40,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ᵀ ₜ ᵗ
    key <AB07> {[   x,		X,		Greek_xi,	Greek_XI,		multiply,	U2297,			U2093,		U02E3	]}; // x X ξ Ξ   × ⊗ ₓ ˣ
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,	VoidSymbol,		VoidSymbol,	U1D47	]};	// b B β Β   ∞     ᵇ
    key <AB09> {[	u,		U,		lessthanequal,	 U2270,		U227A,		U2280,			U227E,	VoidSymbol	]}; // u U ≤ ≰   ≺ ⊀ ≾ 
    key <AB10> {[	o,		O,		greaterthanequal,U2271,		U227B,		U2281,			U227F,	VoidSymbol	]}; // o O ≥ ≰   ≻ ⊁ ≿  
};

// Л.Ф. ходатайствовала за омикрон, но по общему правилу на oO индексы → омикрон на 7-8 уровне оО заплаткой
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_omicron_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,enfilledcircbullet,	Greek_omicron,Greek_OMICRON	]}; // o O ω Ω    𝕆 • ο Ο
};

// Комбинируемые ударение и точка на 8 уровне Ю и Ё: на одном уровне; точка над Ё
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_combining_acute_dot_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AB09> {[	u,		U,		lessthanequal,	 U2270,		U227A,		U2280,			U227E,	U0301	]}; // u U ≤ ≰   ≺ ⊀ ≾ а́   8 ́=комб.ударение
    key <AB10> {[	o,		O,		greaterthanequal,U2271,		U227B,		U2281,			U227F,	U0307	]}; // o O ≥ ≰   ≻ ⊁ ≿ ẋ   8 ̇=комб.производная по t
};

// Заплатки на первый уровень латиницы ****************************************
// На Я вместо второй А — вторая Z (остаток qwerty), так как Л.Ф. всё время спотыкается о несоответствие Ctrl+Z.
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_legacyCtrlZ_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AB01> {[	z,		Z	]}; // z Z 
};

// Для ввода email — @ на первый уровень в алфавитный блок ЛАТ (1-2 уровни на случай режима CapsLock)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_makeatletter_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AC11> {[	at,		at	]}; // @ @ — на Э (эт), ибо на Ж совсем уж нелогично
};

// Для C/C++ — оператор получения адреса & на первый уровень в алфавитный блок ЛАТ
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_make_c_address_op_letter_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AB01> {[	ampersand,ampersand	]}; // & & — на Я (примерно под разыменованием * и немного похож на Я по форме)
};

// Для Octave — символы =[] в алфавитном блоке ЛАТ — увы, безо всякой связи с русскими буквами
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_octave_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AC10> {[	equal,			equal			]}; // = = — на Ж
    key <AB09> {[	bracketleft,	bracketleft		]}; // [ [ — на Ю
    key <AB10> {[	bracketright,	bracketright	]}; // ] ] — на Ё
};


// Комплексная заплатка первого уровня ЛАТ для:
// — ввода email (набирается строго латиницей и @ — единственный символ email, который набирается с Shift ⇒ дублируем на первом уровне как латинскую букву)
// — ввода простых скриптов Octave (задание массива [], вызов функции (), знак равенства =, остальное и так на первом уровне)
// в простых командах Octave [] чаще (), но ненамного; в других ЯП чаще () ⇒ круглые скобки () на центральную линию ЖЭ ⇒ @ не может быть на Э
// @ первого уровня под @ второго уровня (влево на Я); = первого уровня под = второго уровня (на единственное непарное место — Х/h/χ)
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_at_octave_parenthesis_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AB01> {[	at,				at				]}; // @ @ — на Я
    key <AD11> {[	equal,			equal			]}; // = = — на Х
    key <AC10> {[	parenleft,		parenleft,		Greek_psi,	Greek_PSI,		elementof,notelementof,	U208D,U207D	]}; // ( ( ψ Ψ   ∈ ∉ ₍ ⁽  — на Ж 
    key <AC11> {[	parenright,		parenright,		Greek_eta,	Greek_ETA,		containsas,	U220C,		U208E,U207E	]}; // ) ) η Η   ∋ ∌ ₎ ⁾  — на Э
    key <AB09> {[	bracketleft,	bracketleft		]}; // [ [ — на Ю
    key <AB10> {[	bracketright,	bracketright	]}; // ] ] — на Ё
};

// Комплексная заплатка первого уровня ЛАТ для ввода программ на языке C/C++
// https://stackoverflow.com/questions/3421532/frequency-of-symbols-in-programming-languages
// символы, которые в языконезависимой части не на 1 уровне (включая мерцающие #^$~), по убыванию частоты: )(=>{&}#[]<'|@$^~
// непарный > включать не хочется (или тогда надо -> сразу рядом), получается ()={}&; и оставляем ' на Ъ
partial alphanumeric_keys xkb_symbols "lat_alph_jcuken_cpp_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AB01> {[	ampersand,	ampersand		]}; // & & — на Я (примерно под разыменованием * и немного похож на Я по форме)
    key <AD11> {[	equal,		equal			]}; // = = — на Х
    key <AC10> {[	parenleft,	parenleft,		Greek_psi,	Greek_PSI,		elementof,notelementof,	U208D,U207D	]}; // ( ( ψ Ψ   ∈ ∉ ₍ ⁽  — на Ж 
    key <AC11> {[	parenright,	parenright,		Greek_eta,	Greek_ETA,		containsas,	U220C,		U208E,U207E	]}; // ) ) η Η   ∋ ∌ ₎ ⁾  — на Э
    key <AB09> {[	braceleft,	braceleft		]}; // { { — на Ю
    key <AB10> {[	braceright,	braceright		]}; // } } — на Ё
};



partial keypad_keys xkb_symbols "common_keypad_braces" {    
    include "illinc(common_keypad_numlock_equal)" // = Num_Lock

    key.type="FOUR_LEVEL_MIXED_KEYPAD";    
    
    // Скобки и цифры по умолчанию ********************************************
    // на 1-2 уровнях — скобки и цифры; на третьем — дубли управляющих клавиш (/usr/share/X11/xkb/symbols/keypad, x11)
    //key <KPDV> {[ less,		slash,		slash, 		slash		]}; // </// 
    //key <KPMU> {[ greater,	asterisk,	asterisk,	asterisk	]}; // >***    
    //key <KPSU> {type="FOUR_LEVEL",	[ minus,	minus,		minus,		minus		]}; // ----
    //key <KPAD> {type="FOUR_LEVEL",	[ plus,		plus,		plus,		plus		]}; // ++++      
    //key <KPEN> {type="FOUR_LEVEL",	[ KP_Enter,	KP_Enter,	KP_Enter,	KP_Enter	]}; // Enter
    //replace key <KPEN>	{type="ONE_LEVEL",	[ KP_Enter 		]	};        
    
    // В keysymdef есть две константы Prior и Page_Up с одним значением (Prior≡Page_Up), 
    // аналогично Next≡Page_Down, KP_Prior≡KP_Page_Up и KP_Next≡KP_Page_Down 
    // но в pc и keypad почему-то используются Prior/Next и KP_Prior/KP_Next → тут так же
    
    key  <KP7> {[ braceleft,	7,		KP_Home,	KP_Home		]};
    key  <KP8> {[ braceright,	8,		KP_Up,		KP_Up		]};
    //key  <KP9> {[ bar,			9,		KP_Prior,	KP_Prior	]}; 

    key  <KP4> {[ parenleft,	4,		KP_Left,	KP_Left		]};
    key  <KP5> {[ parenright,	5,		KP_Begin,	KP_Begin	]};
    key  <KP6> {[ ampersand,	6,		KP_Right,	KP_Right	]};

    key  <KP1> {[ bracketleft,	1,		KP_End,		KP_End		]};
    key  <KP2> {[ bracketright,	2,		KP_Down,	KP_Down		]};
    //key  <KP3> {[ apostrophe,	3,		KP_Next,	KP_Next		]}; 
    
    key  <KP0>  {[ space,		0,		KP_Insert,	KP_Insert	]};   
    //key  <KPDL> {[ at,			period,	KP_Delete,	KP_Delete	]}; // period, а не KP_Decimal — чтобы было предсказуемо! 
    key  <KPDL> {[ numbersign,	period,	KP_Delete,	KP_Delete	]}; // period, а не KP_Decimal — чтобы было предсказуемо! 
    
    // ↑↑↑ почему-то хочется апостроф ' в верхне-правом углу. Но совсем в угол на место ` не стоит (мб перезаписано, и вообще далеко).
    // Поменять ' и | местами?
    key  <KP9> {[ apostrophe,	9,		KP_Prior,	KP_Prior	]}; 
    key  <KP3> {[ bar,			3,		KP_Next,	KP_Next		]}; 
    
    // Символ № на первом уровне
    
    key <KPEN> {type="FOUR_LEVEL",	[ numerosign,	numerosign,		KP_Enter,	KP_Enter	]}; // №№ Enter
        
    // Заплатки для ввода баллов ОРИОКС: нужна литера «н»; навигация Tab и Shift+Tab; на Enter никакой реакции     
    
    // ОРИОКС на Tab и ISO_Left_Tab реагирует одинаково (оба «вперёд», а Shift+Tab и Shift+ISO_Left_Tab оба «назад»);
    // поэтому надо либо обходиться без табуляции, либо отдать под неё ОБА нижних уровня клавиши (совместить с <> не получится)
    
    //key <KPDV> {[ less,		minus,				slash, 		slash		]}; // <-  // 
    //key <KPMU> {[ greater,	plus,				asterisk,	asterisk	]}; // >+  **
    
    //key <KPSU> {					[ grave,	Cyrillic_en,		minus,		minus		]}; // `н  --
    key <KPAD> {type="FOUR_LEVEL",	[ Tab,		ISO_Left_Tab,		plus,		plus		]}; // Tab ++  
    
    // восстанавливаем минус на типичном месте, раз уж он есть (н — над <; в не-ОРИОКСовой версии поставить туда запятую)
    key <KPDV> {[ less,		Cyrillic_en,		slash, 		slash		]}; // <н  // 
    key <KPMU> {[ greater,	plus,				asterisk,	asterisk	]}; // >+  **    
    key <KPSU> {[ grave,	minus,				minus,		minus		]}; // `-  --
    
};

// Вариант цифрового блока базовый: скобки по умолчанию; ввод чисел в режиме NumLock
partial keypad_keys xkb_symbols "common_keypad_braces_digits_with_tab" {    
    include "illinc(common_keypad_numlock_equal)" // = Num_Lock    
    key.type="FOUR_LEVEL_MIXED_KEYPAD";    
    // Так как десятичный разделитель (точка/запятая) на практике зависит не от языка ввода, а от приложения —  
    // в режиме цифр нужны оба (period/comma), а не меняющийся KP_Decimal
    // запятая — за NumLock (KPDV) здесь ↓↓↓, точка на типичном месте (KPDL) внизу
    key <KPDV> {[ less,		comma,				slash, 		slash		]}; // <,  // 
    // ↓↓↓ плюс переносим, так как его типичное место будет занято табуляцией
    key <KPMU> {[ greater,	plus,				asterisk,	asterisk	]}; // >+  ** 
    // ↓↓↓ минус — на типичном месте и не переключается по NumLock: 
    // а) привычно; б) в режиме скобок получается -> на первом уровне и соседних клавишах (правда, не в том порядке)
    key <KPSU> {type="ONE_LEVEL",	[ minus	]}; // -
    
    // Для навигации по таблицам недостаточно Enter, нужны Tab и Shift+Tab
    // ОРИОКС на Tab и ISO_Left_Tab реагирует одинаково (оба «вперёд», а Shift+Tab и Shift+ISO_Left_Tab оба «назад»); возможно, не только он;
    // поэтому надо либо обходиться без табуляции, либо отдать под неё ОБА нижних уровня клавиши (совместить с <> не получится)
    replace key <KPAD> {type="FOUR_LEVEL",	[ Tab,		ISO_Left_Tab,		plus,		plus		]}; // Tab ++  
    
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 		]	};        

    key  <KP7> {[ braceleft,	7,		KP_Home,	KP_Home		]};
    key  <KP8> {[ braceright,	8,		KP_Up,		KP_Up		]};
    key  <KP9> {[ apostrophe,	9,		KP_Prior,	KP_Prior	]}; 
    
    key  <KP4> {[ parenleft,	4,		KP_Left,	KP_Left		]};
    key  <KP5> {[ parenright,	5,		KP_Begin,	KP_Begin	]};
    key  <KP6> {[ ampersand,	6,		KP_Right,	KP_Right	]};
   
    key  <KP1> {[ bracketleft,	1,		KP_End,		KP_End		]};
    key  <KP2> {[ bracketright,	2,		KP_Down,	KP_Down		]};
    key  <KP3> {[ bar,			3,		KP_Next,	KP_Next		]}; 

    key  <KP0>  {[ space,		0,		KP_Insert,	KP_Insert	]};   
    key  <KPDL> {[ grave,		period,	KP_Delete,	KP_Delete	]}; // `. Del
 
};
    
// Вариант цифрового блока специально для ввода баллов ОРИОКС: 
// — запятая не используется + нужна литера «н»  ⇒  «н» на место запятой; 
// — навигация Tab и Shift+Tab; (-) на Enter никакой реакции  ⇒  занимаем место Εnter, чтобы не было соблазна ⇒ символ № на первом уровне
// (-) — раз на 1 уровне № ⇒ ставим и § 
partial keypad_keys xkb_symbols "common_keypad_braces_digits_orioks" {   
    include "illinc(common_keypad_braces_digits_with_tab)"     
    key <KPDV> {type="FOUR_LEVEL_MIXED_KEYPAD",	[ less,			Cyrillic_en,	slash, 		slash		]}; // <н  //    
    // key <KPEN> {type="FOUR_LEVEL",				[ numerosign,	numerosign,		KP_Enter,	KP_Enter	]}; // №№ Enter
};

partial modifier_keys xkb_symbols "common_mod_lwin_rus_lattmp" {
    replace key <LWIN> {	type="PC_RALT_LEVEL2",	[ ISO_Last_Group,	VoidSymbol]],
        actions[Group1]=[ ],
        actions[Group2]=[ NoAction(),SetGroup(group=-1) ],
        actions[Group3]=[ NoAction(),SetGroup(group=-2) ],
        actions[Group4]=[ NoAction(),SetGroup(group=-3) ]
    };    
};
↑↑↑ не работает


    //key <BKSL> {[ asciicircum,	asciicircum,	backslash,	bar	]}; // ^^ \|    


    //key <FK08> {									[7, U2309,		F8, F8, 	U27E1,	U0338,	enfilledcircbullet,U0306	]}; // 7 ⌉   ⟡ ̸ • x̆  6 и 8 — комб.
    //key <FK09> {									[8, asciicircum,F9, F9, 	VoidSymbol,VoidSymbol,		U2023,	U0302	]}; // 8 ^       ‣ x̂  8 ̂=комб.циркумфлекс
    //key <FK10> {									[9, ampersand,	F10,F10,	VoidSymbol,VoidSymbol,		U27A4,	U20D7 	]}; // 9 &       ➤ x⃗  8 ⃗=комб.вектор

    //key <FK02> { type="CTRL+ALT", 	symbols[Group1]=[1, numbersign,	F2, F2, 	XF86_Switch_VT_2 	]}; // 1 #        
    //key <AE02> {[ minus,		at,				2,	U2011,			U2212,		U2296,		twosubscript,	twosuperior		]}; // -@ 2‑  −⊖ ₂²  
     //key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	U2213,		U1F12F,			U2C7C,		U02B2	]}; // j J θ Θ   ∓ 🄯 ⱼ ʲ   6 🄯=копилефт
    //key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		VoidSymbol,	VoidSymbol,		U2098,		U1D50	]}; // m M μ Μ       ₘ ᵐ   
   

partial alphanumeric_keys function_keys modifier_keys xkb_symbols "common_88hex_patch" {
    replace key <ESC>  {	type="FOUR_LEVEL",	[ F,	Escape,		Escape,	Escape		]	};
    key.type="EIGHT_LEVEL";     
     		key <FK11> {	[ A,	guillemotleft	]};  
     		key <FK12> {	[ B,	guillemotright	]};
    replace key <PRSC> {	[ C,	numbersign,		Print,	Sys_Req	]	}; 
    replace key <SCLK> {	[ D,	asciicircum,	Scroll_Lock		]	};  
    replace key <PAUS> {	[ E,	dollar,			Pause,	Break	]	};    
    
    //key <PRSC> {[C,	numbersign,		Print,	Sys_Req	],  // \# PrScr SysRq  ∖⦸ 
    //   actions[Group1]=[NoAction()],  actions[Group2]=[NoAction()],  actions[Group3]=[NoAction()],  actions[Group4]=[NoAction()]	}; 
    
};



    key <FK01> { type="CTRL+ALT", 	[0, grave,		F1, F1, 	XF86_Switch_VT_1 	]	}; // 0 `
    key <FK02> { type="CTRL+ALT", 	[1, U2212,		F2, F2, 	XF86_Switch_VT_2 	]	}; // 1 −                                    	
    key <FK03> { type="CTRL+ALT", 	[2, U27E8,		F3, F3, 	XF86_Switch_VT_3 	]	}; // 2 ⟨
    key <FK04> { type="CTRL+ALT", 	[3, U27E9,		F4, F4, 	XF86_Switch_VT_4 	]	}; // 3 ⟩
                                                                                     
    key <FK05> { type="CTRL+ALT", 	[4, U230A,		F5, F5, 	XF86_Switch_VT_5 	]	}; // 4 ⌊
    key <FK06> { type="CTRL+ALT", 	[5, U230B,		F6, F6, 	XF86_Switch_VT_6 	]	}; // 5 ⌋
    key <FK07> { type="CTRL+ALT", 	[6, U2308,		F7, F7, 	XF86_Switch_VT_7 	]	}; // 6 ⌈ 
    
    key <FK08> {					[7, U2309,		F8, F8, 	notsign,	U0338,			enfilledcircbullet,	VoidSymbol	]	}; // 7⌉  ¬  •  6̸= комб    
    key <FK09> {					[8, ampersand,	F9, F9, 	logicaland,	VoidSymbol,		U2023,				VoidSymbol	]	}; // 8&  ∧  ‣ 
    key <FK10> {					[9, bar,		F10,F10,	logicalor,	VoidSymbol,		U27A4,				VoidSymbol	]	}; // 9|  ∨  ➤ 
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	U228A,leftsinglequotemark,	includedin,	U2284]}; // «„  ⊊‘ ⊂⊄   
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	U228B,rightsinglequotemark,	includes,	U2285]}; // »“  ⊋’ ⊃⊅

        
    //include "srvr_ctrl(fkey2vt)"     // восстановление оригинальных F1-F12 вместо цифр и кавычек
    //include "illinc(common_mod_bksl_level5)"    // ANSI: BKSL=Level5 (так как на месте Enter — длинный BKSL)

    
    //key <FK08> {					[7, U2309,		F8, F8, 	notsign,	U0338,			enfilledcircbullet,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬  •  6̸= комб   

    
    // replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ percent, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  
   
    //key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		permille,		U2031,		U1DBB	]}; // z Z ζ Ζ   ℤ ‰ ‱ ᶻ   
    
//     // ScrollLock как правый Level3, PRSC как \Лат с Print, Sys_Req на 3-4 уровнях, PAUS как $Лат
//     //replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
//     //modifier_map Mod5   { ISO_Level3_Shift };    
//         
//     //replace key <PRSC> {	[backslash,	bar, 		Print,Sys_Req,		U2216,U29B8,		  U2225,U2226	], // \| PrScr SysRq   ∖⦸ ∥∦
//     //   actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};       
//     
//     replace key <PRSC> {[backslash,	numbersign,		Print,	Sys_Req,		U2216,U29B8,			VoidSymbol,VoidSymbol	],  // \# PrScr SysRq  ∖⦸ 
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)],	overlay1=<I249>	}; 
//        
//     replace key <SCLK> {[asciicircum,asciicircum,	Scroll_Lock,VoidSymbol,	U2295,VoidSymbol, 		VoidSymbol,U0302		],	overlay1=<I250>	}; // ^^ ScrollLock   ⊕  x̂  8 ̂=комб.
// 
//     replace key <PAUS> {[dollar,	apostrophe,		Pause,	Break,			VoidSymbol,VoidSymbol,	VoidSymbol,VoidSymbol	],  // $' Pause Break   
//        actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)],	overlay1=<I251>		};  

    //replace key <PRSC> {[backslash,	numbersign,		Print,	Sys_Req,		U2216,U29B8,		VoidSymbol,VoidSymbol	]	}; // \# PrScr SysRq  ∖⦸  

    //include "illinc(common_mod_ralt_temp_lat)"    // ralt = ЛатВрем

//$ setxkbmap "illinc(flat),illinc(frus)"
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "flat" { include "illinc(lat)"  include "srvr_ctrl(fkey2vt)" };
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "frus" { include "illinc(flat)" include "illinc(rus_alph_patch)" };
    

######  Знаки препинания — на us-цифрах, уровень 1

######  Скобки — на us-цифрах, уровень 2

######  Цифры — на us-цифрах, уровень 3

Далее блоки могут определяться, чтобы вытащить часть символов на первый уровень или, наоборот, скрыть ошибочно нажимаемые на третий.


### Справа сверху (на PrintScreen/SysRq, ScrollLock и Pause/Break) — %, ^, $

### На Insert — обратная (экранирующая) косая черта \
 

### На F1-F12 (слева сверху) — цифры

### На цифровом блоке — скобки

Если F1-F12 не переопределяются (если активно будут использоваться клавиатурные сокращения *Alt+Fx, Ctrl+Fx*), то и цифровой блок переопределять не стоит.

********************************************************************************

Базовый вариант — 88-клавишная ISO без цифрового блока: 
* основной блок + 
* \+ переопределённая линия F1-F12 + 
* \+ переопределённый Insert + 
* \+ блок \Лат, правый Level3, $Лат справа вверху.

Цифровой блок по умолчанию также переопределяется, и если физически присутствует — скобки доступны на 1 уровне.
    

### Спецсимволы 1 уровня us-раскладки, перенесённые на 3 или 4 уровень

ибо ошибочно нажимаются чаще, чем целенаправленно:
* Insert — для переключения режима надо нажимать *Level3+Insert;*
* Fx ∈ [F1, F12] — *Level3+Fx;*
* CapsLock — *Level3+Shift+LWin* (фактически *старый CapsLock + Shift + РУС*);


## Заплатки

Визуализация — файлы vis-diff-*.
    
    

// Level5=RWin — по умолчанию: если есть, то используется как Level5; если отсутствует (на этом месте сейчас чаще Fn) — переопределение не мешает
partial modifier_keys xkb_symbols "common_mod_rwin_level5" {
    replace key <RWIN> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };  
};
// Level5=RAlt — можно подключить, если RWin на клавиатуре отсутствует
partial modifier_keys xkb_symbols "common_mod_ralt_level5" {
    replace key <RALT> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };  
};
// Level5=BKSL — в основном для ANSI-клавиатур, где BKSL имеет размер модификатора — не совсем внизу, но справа
partial modifier_keys xkb_symbols "common_mod_bksl_level5" {
    replace key <BKSL> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };     
};


partial keypad_keys xkb_symbols "common_keypad_braces_digits_with_tab_tilde_patch" {   
    key  <KP0>  {type="FOUR_LEVEL_MIXED_KEYPAD",	[ asciitilde ]};   
    key <SPCE> { type="EIGHT_LEVEL",		[ space,		space		]}; //  пробел и на 2 уровне пробела, ибо тильда теперь есть на 1 уровне другой клавиши
};

partial keypad_keys xkb_symbols "common_mod_keypad_braces_digits_ansi_patch" {   
    key  <KP0>  {type="FOUR_LEVEL_MIXED_KEYPAD",	[ numbersign	]}; // #  
    key  <KPDL> {type="FOUR_LEVEL_MIXED_KEYPAD",	[ asciicircum	]}; // ^
    include "illinc(common_mod_bksl_level5)"    // ANSI: BKSL=Level5 (так как на месте Enter — длинный BKSL)   
};

    //include "illinc(common_mod_rwin_level5)"
    
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	U228A,leftsinglequotemark,	includedin,			U2284	],	overlay2=<I221>	}; // «„  ⊊‘ ⊂⊄   
    key <FK12> {[guillemotright,leftdoublequotemark,F12,Overlay2_Enable,	U228B,rightsinglequotemark,	includes,U2285	],	overlay2=<I222>	}; // »“  ⊋’ ⊃⊅
    

// Англокавычки на 5 и 6 уровнях над русскими (американский вариант: “” внешние ⇒ с одним модификатором, ‘’ внутренние ⇒ с двумя), 7-8 уровни не меняем
partial function_keys xkb_symbols "lat_alphanumeric_punctuation_en_us_quotes_patch" {
    key.type="EIGHT_LEVEL";     
    key <FK11> {[ guillemotleft,	doublelowquotemark,		F11,F11,	leftdoublequotemark,	leftsinglequotemark	]}; // « „   “ ‘     
    key <FK12> {[ guillemotright,	leftdoublequotemark,	F12,F12,	rightdoublequotemark,	rightsinglequotemark]}; // » “   ” ’ 
};
    
    //key <AB09> {[bracketleft,bracketleft,	lessthanequal,	 U2270,	U227A,	U2280,			U227E,		U0307	]}; // [ [ ≤ ≰   ≺ ⊀ ≾ ẋ   8 ̇=комб.производная по t
    //key <AB10> {[bracketright,bracketright,	greaterthanequal,U2271,	U227B,	U2281,			U227F,		U0308	]}; // ] ] ≥ ≰   ≻ ⊁ ≿ ẍ   8 ̈=комб.«вторая» ⟨умляут⟩
    
    key <FK08> {					[7, U2309,		F8, F8, 	notsign,	VoidSymbol,		enfilledcircbullet,	U2753	],	overlay2=<I178>	}; // 7⌉  ¬  •❓      
    key <FK09> {					[8, ampersand,	F9, F9, 	logicaland,	VoidSymbol,		U2023,				U2049	],	overlay2=<I183>	}; // 8&  ∧  ‣⁉
    key <FK10> {					[9, bar,		F10,F10,	logicalor,	VoidSymbol,		U27A4,				U2757	],	overlay2=<I184>	}; // 9|  ∨  ➤❗
    
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	leftdoublequotemark,leftsinglequotemark,	includedin,	U2284	],	overlay2=<I221>	}; // «„  “‘ ⊂⊄   
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	rightdoublequotemark,rightsinglequotemark,	includes,	U2285	],	overlay2=<I222>	}; // »“  ”’ ⊃⊅
    //key <AB11> { type="CTRL+ALT", 	[F1, F1,	F1, Overlay2_Enable, 	XF86_Switch_VT_1 	]};
    
    

   
partial keypad_keys xkb_symbols "common_keypad_to_altmodes_bracesmode" {   
    
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    replace key <NMLK> 	{type="TWO_LEVEL",	[ percent,	VoidSymbol	]};     
    modifier_map Mod2   { Num_Lock };
    
    key.type="EIGHT_LEVEL";     
    
    replace key <KPDV> {[ figdash,		endash,		slash, 		slash, 			VoidSymbol,		VoidSymbol		]}; // ‒–  //
    replace key <KPMU> {[ U22C5,		VoidSymbol,	asterisk,	asterisk,		VoidSymbol,		VoidSymbol		]}; // ⋅   **   
    replace key <KPSU> {[ U2212,		plusminus,	minus,		minus,			U208B,			U207B			]}; // −±  -- ₋⁻  1=минус ширины плюса
    
    replace key <KPAD> {[ Tab,		ISO_Left_Tab,	plus,		plus,			U208A,			U207A			]}; // Tab ++ ₊⁺
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    replace key  <KP7> {[ less,			sevensubscript,		KP_Home, 	KP_Home, 		sevensuperior,	sevensuperior	]};
    replace key  <KP8> {[ greater,		eightsubscript,		KP_Up, 		KP_Up, 			eightsuperior,	eightsuperior	]};
    replace key  <KP9> {[ minus,		ninesubscript,		KP_Prior, 	KP_Prior, 		ninesuperior,	ninesuperior	]}; 

    replace key  <KP4> {[ bracketleft,	foursubscript,		KP_Left, 	KP_Left, 		foursuperior,	foursuperior	]};
    replace key  <KP5> {[ bracketright,	fivesubscript,		KP_Begin, 	KP_Begin, 		fivesuperior,	fivesuperior	]};
    replace key  <KP6> {[ parenright,	sixsubscript,		KP_Right, 	KP_Right, 		sixsuperior,	sixsuperior		]};

    replace key  <KP1> {[ ampersand,	onesubscript,		KP_End, 	KP_End, 		onesuperior,	onesuperior		]};
    replace key  <KP2> {[ bar,			twosubscript,		KP_Down, 	KP_Down, 		twosuperior,	twosuperior		]};
    replace key  <KP3> {[ U2286,		threesubscript,		KP_Next, 	KP_Next, 		threesuperior,	threesuperior	]}; 

    replace key  <KP0> {[ space,		zerosubscript,		KP_Insert, 	KP_Insert, 		zerosuperior,	zerosuperior	]};   
    replace key <KPDL> {[ elementof,	VoidSymbol,			KP_Delete,	KP_Delete,		VoidSymbol,		VoidSymbol		]}; // ∈  Del    
     
};   


// Вариант цифрового блока базовый: скобки по умолчанию; ввод чисел в режиме NumLock
partial keypad_keys xkb_symbols "common_keypad_braces_digits_with_tab" {    
    include "illinc(common_keypad_numlock_equal)" // = Num_Lock    
    key.type="FOUR_LEVEL_MIXED_KEYPAD";    
    // Так как десятичный разделитель (точка/запятая) на практике зависит не от языка ввода, а от приложения —  
    // в режиме цифр нужны оба (period/comma), а не меняющийся KP_Decimal
    // запятая — за NumLock (KPDV) здесь ↓↓↓, точка на типичном месте (KPDL) внизу
    key <KPDV> {[ less,		comma,				slash, 		slash		]}; // <,  // 
    // ↓↓↓ плюс переносим, так как его типичное место будет занято табуляцией
    key <KPMU> {[ greater,	plus,				asterisk,	asterisk	]}; // >+  ** 
    // ↓↓↓ минус — на типичном месте и не переключается по NumLock: 
    // а) привычно; б) в режиме скобок получается -> на первом уровне и соседних клавишах (правда, не в том порядке)
    key <KPSU> {type="ONE_LEVEL",	[ minus	]}; // -
    
    // Для навигации по таблицам недостаточно Enter, нужны Tab и Shift+Tab
    // ОРИОКС на Tab и ISO_Left_Tab реагирует одинаково (оба «вперёд», а Shift+Tab и Shift+ISO_Left_Tab оба «назад»); возможно, не только он;
    // поэтому надо либо обходиться без табуляции, либо отдать под неё ОБА нижних уровня клавиши (совместить с <> не получится)
    replace key <KPAD> {type="FOUR_LEVEL",	[ Tab,		ISO_Left_Tab,		plus,		plus		]}; // Tab ++  
    
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 		]	};  
    
    //key  <KP7> {[ braceleft,	7,		KP_Home,	KP_Home		]};
    //key  <KP8> {[ braceright,	8,		KP_Up,		KP_Up		]};
    key  <KP7> {[ bracketleft,	7,		KP_Home,	KP_Home		]};
    key  <KP8> {[ bracketright,	8,		KP_Up,		KP_Up		]};
    key  <KP9> {[ apostrophe,	9,		KP_Prior,	KP_Prior	]}; 
    
    key  <KP4> {[ parenleft,	4,		KP_Left,	KP_Left		]};
    key  <KP5> {[ parenright,	5,		KP_Begin,	KP_Begin	]};
    key  <KP6> {[ ampersand,	6,		KP_Right,	KP_Right	]};
   
    //key  <KP1> {[ bracketleft,	1,		KP_End,		KP_End		]};
    //key  <KP2> {[ bracketright,	2,		KP_Down,	KP_Down		]};
    key  <KP1> {[ braceleft,	1,		KP_End,		KP_End		]};
    key  <KP2> {[ braceright,	2,		KP_Down,	KP_Down		]};
    key  <KP3> {[ bar,			3,		KP_Next,	KP_Next		]}; 

    key <KP0>  {[ space,		0,		KP_Insert,	KP_Insert	]};   
    key <KPDL> {[ grave,		period,	KP_Delete,	KP_Delete	]}; // `. Del
};

partial keypad_keys xkb_symbols "common_keypad_braces_digits_with_tab_numerosection_patch" {   
//     //key  <KP1> {[ numerosign,	1,		KP_End,		KP_End		]};
//     //key  <KP2> {[ section,		2,		KP_Down,	KP_Down		]};
//     
//     
//     key <KPSU> {type="ONE_LEVEL",	[ U2212	]}; // − ширины плюса
//     
// //     key  <KP1> {[ bar,			1,		KP_End,		KP_End		]}; // |
// //     key  <KP2> {[ numerosign,	2,		KP_Down,	KP_Down		]}; // №
//     
//     key  <KP1> {[ numerosign,	1,		KP_End,		KP_End		]}; // №
//     key  <KP2> {[ bar,			2,		KP_Down,	KP_Down		]}; // |
//     key  <KP3> {[ grave,		3,		KP_Next,	KP_Next		]}; // `
// 
//     key <KPDL> {[ section,		period,	KP_Delete,	KP_Delete	]}; // §. Del

    
    
    
    //replace key <KPDV> {[ less,		comma,				slash, 		U02D2	]}; // <,  /˒
    
    
    
//     replace key <KPDV> {[ less,		comma,				slash, 		VoidSymbol	]}; // <,  /
//     // ↓↓↓ плюс переносим, так как его типичное место будет занято табуляцией
//     replace key <KPMU> {[ greater,	plus,				asterisk,	U207A		]}; // >+  *⁺
//     replace key <KPSU> {type="FOUR_LEVEL",	[ U2212,U2212,	minus,	U207B		]}; // −−  -⁻
//     
//     replace key <KPAD> {type="FOUR_LEVEL",	[ Tab,ISO_Left_Tab,	plus,VoidSymbol	]}; // Tab +    
//     replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 		]	};  
//     
//        
//     key  <KP7> {[ bracketleft,	7,		KP_Home,	sevensuperior	]};
//     key  <KP8> {[ bracketright,	8,		KP_Up,		eightsuperior	]};
//     key  <KP9> {[ apostrophe,	9,		KP_Prior,	ninesuperior	]}; 
//     
//     key  <KP4> {[ parenleft,	4,		KP_Left,	foursuperior	]};
//     key  <KP5> {[ parenright,	5,		KP_Begin,	fivesuperior	]};
//     key  <KP6> {[ U22C5,		6,		KP_Right,	sixsuperior		]};
//    
//     key  <KP1> {[ ampersand,	1,		KP_End,		onesuperior		]};
//     key  <KP2> {[ bar,			2,		KP_Down,	twosuperior		]};
//     key  <KP3> {[ numerosign,	3,		KP_Next,	threesuperior	]}; 
// 
//     key <KP0>  {[ space,		0,		KP_Insert,	zerosuperior	]};   
//     key <KPDL> {[ section,		period,	KP_Delete,	VoidSymbol		]}; // `. Del    
    // ↑↑↑ некрасиво, что ⋅ не на *
    
    
    
   
    
//     replace key <KPDV> {[ numerosign,	comma,				slash, 		VoidSymbol	]}; // №,  /
//     //replace key <KPMU> {[ U22C5,		plus,				asterisk,	U207A		]}; // ⋅+  *⁺
//     replace key <KPMU> {type="FOUR_LEVEL",	[ U22C5,multiply,asterisk,	U207A		]}; // ⋅×  *⁺
//     replace key <KPSU> {type="FOUR_LEVEL",	[ U2212,U2212,	minus,		U207B		]}; // −−  -⁻
//     
//     replace key <KPAD> {type="FOUR_LEVEL",	[ Tab,ISO_Left_Tab,	plus,	U207A		]}; // Tab +⁺    
//     replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 		]	};  
//     
//        
//     key  <KP7> {[ less,			7,		KP_Home,	sevensuperior	]};
//     key  <KP8> {[ greater,		8,		KP_Up,		eightsuperior	]};
//     key  <KP9> {[ apostrophe,	9,		KP_Prior,	ninesuperior	]}; 
//     
//     key  <KP4> {[ parenleft,	4,		KP_Left,	foursuperior	]};
//     key  <KP5> {[ parenright,	5,		KP_Begin,	fivesuperior	]};
//     key  <KP6> {[ ampersand,	6,		KP_Right,	sixsuperior		]};
//    
//     key  <KP1> {[ bracketleft,	1,		KP_End,		onesuperior		]};
//     key  <KP2> {[ bracketright,	2,		KP_Down,	twosuperior		]};
//     key  <KP3> {[ bar,			3,		KP_Next,	threesuperior	]}; 
// 
//     key <KP0>  {[ space,		0,		KP_Insert,	zerosuperior	]};   
//     key <KPDL> {[ section,		period,	KP_Delete,	VoidSymbol		]}; // §. Del    
   
    
   
    key.type="FOUR_LEVEL_MIXED_KEYPAD";    
    
//     replace key <KPDV> {					[ figdash,	comma,			slash, 		U207B	]}; // ‒,  /⁻
//     replace key <KPMU> {					[ U22C5,	Cyrillic_en,	asterisk,	U207A	]}; // ⋅н  *⁺
//     replace key <KPSU> {type="FOUR_LEVEL",	[ U2212,	U2212,			minus,		U207B	]}; // −−  -⁻
    
    replace key <KPDV> {type="FOUR_LEVEL",	[ figdash,	endash,			slash, 		U207B	]}; // ‒–  /⁻
    replace key <KPMU> {					[ U22C5,	Cyrillic_en,	asterisk,	U207A	]}; // ⋅н  *⁺
    replace key <KPSU> {type="FOUR_LEVEL",	[ U2212,	U2212,			minus,		U207B	]}; // −−  -⁻  12минус ширины плюса)
    
    replace key <KPAD> {type="FOUR_LEVEL",	[ Tab,		ISO_Left_Tab,	plus,		U207A	]}; // Tab +⁺    
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 										]};  
    
       
    key  <KP7> {[ less,			7,		KP_Home,	sevensuperior	]};
    key  <KP8> {[ greater,		8,		KP_Up,		eightsuperior	]};
    key  <KP9> {[ U2286,		9,		KP_Prior,	ninesuperior	]}; 
    
    key  <KP4> {[ parenleft,	4,		KP_Left,	foursuperior	]};
    key  <KP5> {[ parenright,	5,		KP_Begin,	fivesuperior	]};
    key  <KP6> {[ elementof,	6,		KP_Right,	sixsuperior		]};
   
    key  <KP1> {[ bracketleft,	1,		KP_End,		onesuperior		]};
    key  <KP2> {[ bracketright,	2,		KP_Down,	twosuperior		]};
    key  <KP3> {[ bar,			3,		KP_Next,	threesuperior	]}; 

    key <KP0>  {[ space,		0,		KP_Insert,	zerosuperior	]};   
    key <KPDL> {[ ampersand,	period,	KP_Delete,	VoidSymbol		]}; // &. Del    
   
    
};

   
partial keypad_keys xkb_symbols "common_keypad_digits_with_tab_orioks_patch" {   
    
    // Искореняем Num_Lock на одноимённой клавише
    replace key <I227>	{type="ONE_LEVEL",	[ Num_Lock 		]	}; 	// inet(evdev): key <I227> {[ XF86Finance]};
    // ↑ старый NumLock после этого не переключает режимы; символ Num_Lock на нём не имеет никакого действия   
        
    // Заново реализуем Num_Lock средствами actions на 2-4 уровнях
    replace key <NMLK> 	{type="FOUR_LEVEL",	[ percent,	VoidSymbol,		VoidSymbol,	VoidSymbol	],
        actions[Group1]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group2]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group3]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group4]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ]    
    }; 
    // ↑ символ 1 уровня НЕ включает режим, действия 2-4 — циклически переключают режимы (как оригинальный NumLock)
    
    modifier_map Mod2   { Num_Lock };
    
    key.type="FOUR_LEVEL_MIXED_KEYPAD";    
    replace key <KPMU> {[ U22C5,	Cyrillic_en,	asterisk,	VoidSymbol		]}; // ⋅н  *
    replace key <KPDL> {[ comma,	period,			KP_Delete,	VoidSymbol		]}; // ,. Del    
   
    key.type="EIGHT_LEVEL";     
    
    replace key <KPDV> {[ bar,		bar,				slash, 	VoidSymbol						]}; // ||  /
    replace key <KPSU> {[ ampersand,ampersand,			minus,	VoidSymbol,		U208B,	U207B	]}; // &&  -  ₋⁻
    //replace key <KPDV> {[ figdash,	endash,				slash, 	VoidSymbol					]}; // ‒–  /
    //replace key <KPSU> {[ U2212,	U2212,			minus,	VoidSymbol,		U208B,	U207B	]}; // −−  -  12минус ширины плюса
    
    replace key <KPAD> {[ Tab,		ISO_Left_Tab,		plus,	VoidSymbol,		U208A,	U207A	]}; // Tab +  ₊⁺
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    replace key  <KP7> {[ 7,	7,		KP_Home, 	VoidSymbol,		sevensubscript,	sevensuperior	]};
    replace key  <KP8> {[ 8,	8,		KP_Up, 		VoidSymbol,		eightsubscript,	eightsuperior	]};
    replace key  <KP9> {[ 9,	9,		KP_Prior, 	VoidSymbol,		ninesubscript,	ninesuperior	]}; 

    replace key  <KP4> {[ 4,	4,		KP_Left, 	VoidSymbol,		foursubscript,	foursuperior	]};
    replace key  <KP5> {[ 5,	5,		KP_Begin, 	VoidSymbol,		fivesubscript,	fivesuperior	]};
    replace key  <KP6> {[ 6,	6,		KP_Right, 	VoidSymbol,		sixsubscript,	sixsuperior		]};

    replace key  <KP1> {[ 1,	1,		KP_End, 	VoidSymbol,		onesubscript,	onesuperior		]};
    replace key  <KP2> {[ 2,	2,		KP_Down, 	VoidSymbol,		twosubscript,	twosuperior		]};
    replace key  <KP3> {[ 3,	3,		KP_Next, 	VoidSymbol,		threesubscript,	threesuperior	]}; 

    replace key  <KP0> {[ 0,	0,		KP_Insert, 	VoidSymbol,		zerosubscript,	zerosuperior	]};   
   
    
};









// ************************************************************************************************************************************************************
// Латиница и верхние (общие у латиницы и кириллицы) 3-8 уровни: 
// F1-F12 + линия Backspace (считая от Backspace и левее) + алфавитный блок + модификаторы (кроме RAlt) + Insert

// Расположение букв латиницы, кроме Q, V и H, соответствует оригинальной БК Электроника.
// Буквы латиницы расположены сплошным блоком, без дыр:
// — Q на Ч, ибо иметь Ctrl+Q вместо Ctrl+Z неудобно: не сам, так кто-то другой нажмёт, а подтверждение запрашивается не всегда;
// — H (аш) на Ш; 
// — V на Щ: а) это последняя дырка в середине блока; б) Ctrl+V — ой, не то — Ctrl+Z набирается одной распальцовкой
// и вообще так можно кое-как дотянуться от левого Ctrl 
// upd 22.12.2022: в) как выяснилось, в белорусской раскладке на этом месте Ў (у краткое) — и там получается почти фонетическое соответствие.
//
// На Ъ в латинице фонетически соответствующий ему апостроф:
// на 1-2 уровнях (в латинице) и 4 (сохраняется везде) — ASCII-апостроф ' (одинарная кавычка C++); на 3 уровне типографский ʼ.
//
// На ХЖЭЮЁ в латинице дублируются =()[] — для Octave.

partial alphanumeric_keys function_keys modifier_keys xkb_symbols "lat_alphanumeric_punctuation" {
    name[Group1]= "illinc — latin (jcuken)";

    // модификаторы слева
    include "illinc(common_mod_lwin_rus_CapsLk)"
    include "illinc(common_mod_caps_level3)"

    // модификаторы справа
    replace key <RWIN> {
        type="FOUR_LEVEL",
        symbols = [ VoidSymbol, VoidSymbol,		Super_R, VoidSymbol	],
        actions[Group1]=[ ],
        actions[Group2]=[ SetGroup(group=-1), SetGroup(group=-1) ],
        actions[Group3]=[ SetGroup(group=-2), SetGroup(group=-2) ],
        actions[Group4]=[ SetGroup(group=-3), SetGroup(group=-3) ]
    };    
    include "illinc(common_mod_menu_lat)"
           
    // Insert (LOCAL_EIGHT_LEVEL для Ctrl+Insert без Level3; при этом Shift+Insert без Level3 работает часто, но не везде)
    replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ backslash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  
    // ↑↑↑ В режиме CapsLock получается Insert на первом уровне! Но режим редкий...
    //
    //replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ backslash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert],  // \Лат, а не просто \ 
    //   actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  
     
   
    key <LSGT> 			{ 	type="ONE_LEVEL",	[ numbersign		]	}; // #
    replace key <BKSL> 	{	type="ONE_LEVEL",	[ ISO_Level5_Shift	]  	};
    modifier_map Mod3 { ISO_Level5_Shift };  

    
    key.type="EIGHT_LEVEL";     
    key <SPCE> {[ space,asciitilde,		asciitilde,nobreakspace,	U2241,hairspace,	digitspace,	U0303]}; //  ~ ~ nbsp  ≁ волосяная   figspace ̃x   8 ̃=комб.волна
    
    // верхняя линия: F1-F12 **************************************************
    
    key <FK01> { type="CTRL+ALT", 	[0, grave,		F1, Overlay2_Enable, 	XF86_Switch_VT_1 	],	overlay2=<AB11>	}; // 0 `
    key <FK02> { type="CTRL+ALT", 	[1, U2212,		F2, U0336, 				XF86_Switch_VT_2 	],	overlay2=<JPCM>	}; // 1 −  4 ̶=комб.зачёркивание
    key <FK03> { type="CTRL+ALT", 	[2, U27E8,		F3, F3, 				XF86_Switch_VT_3 	],	overlay2=<I120>	}; // 2 ⟨
    key <FK04> { type="CTRL+ALT", 	[3, U27E9,		F4, VoidSymbol, 		XF86_Switch_VT_4 	],	overlay2=<AE13>	}; // 3 ⟩
                                                                                             
    key <FK05> { type="CTRL+ALT", 	[4, U230A,		F5, F5, 				XF86_Switch_VT_5 	],	overlay2=<I149>	}; // 4 ⌊
    key <FK06> { type="CTRL+ALT", 	[5, U230B,		F6, F6, 				XF86_Switch_VT_6 	],	overlay2=<I154>	}; // 5 ⌋
    key <FK07> { type="CTRL+ALT", 	[6, U2308,		F7, F7, 				XF86_Switch_VT_7 	],	overlay2=<I168>	}; // 6 ⌈ 
    
    
    key <FK08> {					[7, U2309,		F8, F8, 	notsign,	VoidSymbol,				enfilledcircbullet,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬  •       
    key <FK09> {					[8, ampersand,	F9, F9, 	logicaland,	VoidSymbol,						U2023,		VoidSymbol	],	overlay2=<I183>	}; // 8&  ∧  ‣ 
    key <FK10> {					[9, bar,		F10,F10,	logicalor,	VoidSymbol,						U27A4,		VoidSymbol	],	overlay2=<I184>	}; // 9|  ∨  ➤ 
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	leftsinglequotemark,leftdoublequotemark,	VoidSymbol,	VoidSymbol	],	overlay2=<I221>	}; // «„  ‘“  
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	rightsinglequotemark,rightdoublequotemark,	VoidSymbol,	VoidSymbol	],	overlay2=<I222>	}; // »“  ’”
    
    // В консоли переключение оверлеев всё равно не работает
    key <AB11> { type="CTRL+ALT", 	[F1, F1,	F1, VoidSymbol, 		XF86_Switch_VT_1 	]};
    key <JPCM> { type="CTRL+ALT", 	[F2, F2,	F2, U0336, 				XF86_Switch_VT_2 	]};                                    
    key <I120> { type="CTRL+ALT", 	[F3, F3,	F3, F3, 				XF86_Switch_VT_3 	]};
    key <AE13> { type="CTRL+ALT", 	[F4, F4,	F4, Overlay2_Enable, 	XF86_Switch_VT_4 	]};
                                                                                   
    key <I149> { type="CTRL+ALT", 	[F5, F5,	F5, F5, 	XF86_Switch_VT_5 	]};
    key <I154> { type="CTRL+ALT", 	[F6, F6,	F6, F6, 	XF86_Switch_VT_6 	]};
    key <I168> { type="CTRL+ALT", 	[F7, F7,	F7, F7, 	XF86_Switch_VT_7 	]};         
    key <I178> { type="CTRL+ALT", 	[F8, F8,	F8, F8, 	XF86_Switch_VT_8 	]};         
    
    key <I183> { type="CTRL+ALT", 	[F9, F9,	F9, F9, 	XF86_Switch_VT_9 	]};
    key <I184> { type="CTRL+ALT", 	[F10,F10,	F10,F10, 	XF86_Switch_VT_10 	]};
    key <I221> { type="CTRL+ALT", 	[F11,F11,	F11,F11, 	XF86_Switch_VT_11 	]};         
    key <I222> { type="CTRL+ALT", 	[F12,F12,	F12,F12, 	XF86_Switch_VT_12 	]};       
    
        
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			seconds,	U2057,		zerosubscript,	zerosuperior	]}; // "§ 0`  ″⁗ ₀⁰
    key <AE01> {[ plus,			numerosign,		1,	exclam,			plusminus,	U2295,		onesubscript,	onesuperior		]}; // +№ 1!  ±⊕ ₁¹
    key <AE02> {[ minus,		at,				2,	U2011,			U2213,		U2296,		twosubscript,	twosuperior		]}; // -@ 2‑  ∓⊖ ₂²        
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		threesubscript,	threesuperior	]}; // /< 3#  ≮⊘ ₃³
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		foursubscript,	foursuperior	]}; // *> 4$  ≯⊛ ₄⁴
    key <AE05> {[ colon, 		bracketleft,	5,	percent,		U2A7D,		U205D,		fivesubscript,	fivesuperior	]}; // :[ 5%  ⩽⁝ ₅⁵
    key <AE06> {[ comma,		bracketright,	6,	asciicircum,	U2A7E,		VoidSymbol,	sixsubscript,	sixsuperior		]}; // ,] 6^  ⩾  ₆⁶ 
    key <AE07> {[ period,		braceleft,		7,	ampersand,		U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .{ 7&  ⋅⊙ ₇⁷
    key <AE08> {[ underscore,	braceright,		8,	asterisk,		U2287,		U2289,		eightsubscript,	eightsuperior	]}; // _} 8*  ⊇⊉ ₈⁸
    key <AE09> {[ question,		parenleft,		9,	bar,			U2286,		U2288,		ninesubscript,	ninesuperior	]}; // ?( 9|  ⊆⊈ ₉⁹
    key <AE10> {[ emdash,		parenright,		0,	endash,			figdash,	U22EF,		zerosubscript,	zerosuperior	]}; // —) 0–  ‒⋯ ₀⁰         
    key <AE11> {[ exclam,		equal,		minus,	underscore,		notequal, 	U229C,		U208B,			U207B			]}; // != -_  ≠⊜ ₋⁻
    key	<AE12> {[ semicolon,	backslash,	equal,	plus,			approxeq,	U2249,		U208A,			U207A			]}; // ;\ =+  ≈≉ ₊⁺
       
    key.type="EIGHT_LEVEL_ALPHABETIC";
    //					Shift	3				3+Shift			5			5+Shift			5+3			5+3+Shift
    key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	includes,	U2285,			U2C7C,		U02B2	]}; // j J θ Θ   ⊃ ⊅ ⱼ ʲ   
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		copyright,		U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ © ⤉ ᶜ
    key <AD03> {[	u,		U,	Greek_upsilon,	Greek_UPSILON,	union,		logicalor,		U1D64,		U1D58	]}; // u U υ Υ   ∪ ∨ ᵤ ᵘ
    key <AD04> {[	k,		K,	Greek_kappa,	Greek_KAPPA,	U27E1,		U207A,			U2096,		U1D4F	]}; // k K κ Κ   ⟡ ⁺ ₖ ᵏ   6 ⁺=верхний индекс (заряд)
    key <AD05> {[	e,		E,	Greek_epsilon,	Greek_EPSILON,	U2203,		U2204,			U2091,		U1D49	]}; // e E ε Ε   ∃ ∄ ₑ ᵉ
    key <AD06> {[	n,		N,	Greek_nu,		Greek_NU,		U2115,		notsign,		U2099,		U207F	]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	degree,		degree,			nabla,		U1D4D	]}; // g G γ Γ   ° ° ∇ ᵍ
    key <AD08> {[	h,		H,	guillemotleft,doublelowquotemark,	U210D,	U274C,			U2095,		U02B0	]}; // h H « „   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	guillemotright,leftdoublequotemark,	emptyset,checkmark,		U1D65,		U1D5B	]}; // v V » “   ∅ ✓ ᵥ ᵛ   4 “ рус.правая=англ.левая
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U1F12F,			VoidSymbol,	U1DBB	]}; // z Z ζ Ζ   ℤ 🄯   ᶻ   6 🄯=копилефт
    key <AD11> {[equal,	equal,	Greek_chi,		Greek_CHI,		identical,	notidentical,	U208C,		U207C	]}; // = = χ Χ   ≡ ≢ ₌ ⁼
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		minutes,	U2034,			hyphen,		U0301	]}; // ' ' ʼ '   ′ ‴ - ́a   8 ́=комб.ударение
  
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		includedin,	U2284,			VoidSymbol,	U1DA0	]}; // f F φ Φ   ⊂ ⊄   ᶠ
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U21A9,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ↩ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21AA,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↪ ʷ
    key <AC04> {[	a,		A,		Greek_alpha,Greek_ALPHA,	U2200,		U207B,			U2090,		U1D43	]}; // a A α Α   ∀ ⁻ ₐ ᵃ   6 ⁻=верхний индекс (заряд)
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		signifblank,	U209A,		U1D56	]}; // p P π Π   ∏ ␣ ₚ ᵖ   
    key <AC06> {[	r,		R,		Greek_rho,	Greek_RHO,		U211D,		U20BD,			U1D63,		U02B3	]}; // r R ρ Ρ   ℝ ₽ ᵣ ʳ
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U21A6,			U2092,		U1D52	]}; // o O ω Ω   𝕆 ↦ ₒ ᵒ   6 ↦=отображается в
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U2220,			U2097,		U02E1	]}; // l L λ Λ   ℓ ∠ ₗ ˡ
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,	partdifferential,	U1D48 	]}; // d D δ Δ   ∆ ⏨ ∂ ᵈ   5 ∆=приращение
    key <AC10> {[parenleft,	parenleft,	Greek_psi,	Greek_PSI,	elementof,	notelementof,	U208D,		U207D	]}; // ( ( ψ Ψ   ∈ ∉ ₍ ⁽  
    key <AC11> {[parenright,parenright,	Greek_eta,	Greek_ETA,	containsas,	U220C,			U208E,		U207E	]}; // ) ) η Η   ∋ ∌ ₎ ⁾  

    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U279B,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➛ x⃗
    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ ̅x   8 ̅=комб.черта
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		ssharp,			U209B,		U02E2	]}; // s S σ Σ   ∑ ß ₛ ˢ
    key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		U2212,		U2296,			U2098,		U1D50	]}; // m M μ Μ   − ⊖ ₘ ᵐ   5 −=минус ширины +     
    key <AB05> {[	i,		I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,		U1D62,		U2071	]}; // i I ι Ι   ∩ ∧ ᵢ ⁱ
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U1D40,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ᵀ ₜ ᵗ
    key <AB07> {[   x,		X,		Greek_xi,	Greek_XI,		multiply,	U2297,			U2093,		U02E3	]}; // x X ξ Ξ   × ⊗ ₓ ˣ
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,	twosubscript,	VoidSymbol,	U1D47	]};	// b B β Β   ∞ ₂   ᵇ
    key <AB09> {[braceleft,braceleft,	lessthanequal,	 U2270,		U227A,	U2280,			U227E,		U0307	]}; // { { ≤ ≰   ≺ ⊀ ≾ ẋ   8 ̇=комб.производная по t
    key <AB10> {[braceright,braceright,	greaterthanequal,U2271,		U227B,	U2281,			U227F,		U0308	]}; // } } ≥ ≰   ≻ ⊁ ≿ ẍ   8 ̈=комб.«вторая» ⟨умляут⟩
};

// +традиционно заплатка для водворения h на русскую Ха, как было в БК Электронике — в этом варианте получается красиво, со всеми верхними уровнями
partial alphanumeric_keys xkb_symbols "lat_alphanumeric_punctuation_bk_h_patch" {
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key <AD08> {[equal,	equal,	guillemotleft,doublelowquotemark,	identical,	notidentical,	U208C,	U207C	]}; // = = « „   ≡ ≢ ₌ ⁼
    key <AD11> {[	h,		H,	Greek_chi,		Greek_CHI,			U210D,	U274C,				U2095,	U02B0	]}; // h H χ Χ   ℍ ❌ ₕ ʰ
};

// Третий уровень TLDE ("§) соответствует первому us (`~ вместо 0`)
partial alphanumeric_keys xkb_symbols "lat_alphanumeric_punctuation_3_us_tilde_patch" {
    key <TLDE> {type="EIGHT_LEVEL", [ quotedbl,	section,	grave,	asciitilde	]}; // "§ `~  
};

// Умножение-точка на втором уровне линии Backspace, а не только верхней (вместо \, который есть на первом уровне — Insert в норме либо где-то ещё на ноутбуках)
partial alphanumeric_keys xkb_symbols "lat_alphanumeric_punctuation_cdot_patch" {
    key	<AE12> {[ semicolon,	U22C5			]}; // ;⋅
};

partial alphanumeric_keys xkb_symbols "common_alphanumeric_testing_patch" {   
    key.type="EIGHT_LEVEL";     
//     key <FK08> {					[7, U2309,		F8, F8, 	notsign,	VoidSymbol,						VoidSymbol,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬          
//     key <FK09> {					[8, braceleft,	F9, F9, 	U2286,		U2288,						VoidSymbol,	VoidSymbol	],	overlay2=<I183>	}; // 8{  ∧  
//     key <FK10> {					[9, braceright,	F10,F10,	logicalor,	VoidSymbol,						VoidSymbol,	VoidSymbol	],	overlay2=<I184>	}; // 9}  ∨ 
//     //key <AE07> {[ period,		ampersand,		7,	ampersand,		U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .& 7&  ⋅⊙ ₇⁷
//     key <AE07> {[ period,		ampersand,	7,enfilledcircbullet,	U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .& 7•  ⋅⊙ ₇⁷
//     key <AE08> {[ underscore,	bar,			8,	asterisk,		U2287,		U2289,		eightsubscript,	eightsuperior	]}; // _| 8*  ⊇⊉ ₈⁸
//     key <AE09> {[ question,		parenleft,		9,	U27A4,			U2286,		U2288,		ninesubscript,	ninesuperior	]}; // ?( 9➤  ⊆⊈ ₉⁹    

    //key <AD11> {[dollar,	dollar,	Greek_chi,		Greek_CHI,		VoidSymbol,	VoidSymbol,			VoidSymbol,	VoidSymbol	]}; // $ $ χ Χ   
    key <AD11> {[dollar,	dollar,	Greek_chi,		Greek_CHI,		approxeq,		U2249,			VoidSymbol,	VoidSymbol	]}; // $ $ χ Χ   ≈≉
  
    //key <FK08> {					[7, U2309,		F8, F8, 	notsign,	VoidSymbol,				VoidSymbol,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬          
    //key <FK09> {					[8, braceleft,	F9, F9, 	U2286,		U2288,						includedin,	U2284	],	overlay2=<I183>	}; // 8{  ⊆⊈ ⊂⊄ 
    //key <FK10> {					[9, braceright,	F10,F10,	U2287,		U2289,						includes,	U2285	],	overlay2=<I184>	}; // 9}  ⊇⊉ ⊃⊅

    //key <AE07> {[ period,		ampersand,	7,enfilledcircbullet,	logicaland,	U22C5,		sevensubscript,	sevensuperior	]}; // .& 7•  ∧⋅ ₇⁷
    key <AE07> {[ period,		ampersand,	7,enfilledcircbullet,	logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7•  ∧⊙ ₇⁷
    key <AE08> {[ underscore,	bar,			8,	asterisk,		logicalor,	signifblank,eightsubscript,	eightsuperior	]}; // _| 8*  ∨␣ ₈⁸
    //key <AE09> {[ question,		parenleft,	9,enfilledcircbullet,	U22C5,		U2299,		ninesubscript,	ninesuperior	]}; // ?( 9•  ⋅⊙ ₉⁹    
    
    key <AE09> {[ question,		parenleft,		9,	U27A4,			U2286,		U2288,		ninesubscript,	ninesuperior	]}; // ?( 9➤  ⊆⊈ ₉⁹
    key <AE10> {[ emdash,		parenright,		0,	endash,			U2287,		U2289,		zerosubscript,	zerosuperior	]}; // —) 0–  ⊇⊉ ₀⁰ 
         
    key <AE11> {[ exclam,		U2212,		minus,	underscore,		notsign, 	U0336,		U208B,			U207B			]}; // !− -_  ¬− ₋⁻   4=минус, 6=комб.зачёркивание
    key	<AE12> {[ semicolon,	U22C5,		equal,	plus,			VoidSymbol,	U2299,		U208A,			U207A			]}; // ;⋅ =+   ⊙ ₊⁺
    //key	<AE12> {[ semicolon,	U22C5,		equal,	plus,			VoidSymbol,	VoidSymbol,		U208A,			U207A			]}; // ;⋅ =+     ₊⁺
   
   
    key <FK02> { type="CTRL+ALT", 	[1, VoidSymbol,		F2, F2, 				XF86_Switch_VT_2 	],	overlay2=<JPCM>	}; // 1 
    key <JPCM> { type="CTRL+ALT", 	[F2, F2,	F2, F2, 				XF86_Switch_VT_2 	]};                                    
    
    key <FK08> {					[7, U2309,		F8, F8, 	VoidSymbol,		VoidSymbol,				VoidSymbol,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉            
    key <FK09> {					[8, braceleft,	F9, F9, 	bracketleft,	VoidSymbol,				VoidSymbol,	VoidSymbol	],	overlay2=<I183>	}; // 8{  [ 
    key <FK10> {					[9, braceright,	F10,F10,	parenright,		VoidSymbol,				VoidSymbol,	VoidSymbol	],	overlay2=<I184>	}; // 9}  )
    
    
    //replace key <PRSC> {[percent,	figdash,		Print,	Sys_Req,		permille,	U2031,		VoidSymbol,VoidSymbol	]	}; // %‒ PrScr SysRq  ‰‱       
    //replace key <PAUS> {[equal,		notequal,		Pause,	Break,			identical,	notidentical,U208C,		U207C		]	}; // =≠ Pause Break  ≡≢ ₌⁼      
   
    //replace key <SCLK> {[equal,		notequal,		Scroll_Lock,U229C,		identical,	notidentical,U208C,		U207C		]	}; // =≠ ScrollLock⊜  ≡≢ ₌⁼
    //replace key <PAUS> {[asciicircum,apostrophe,	Pause,	Break,			U2295,	VoidSymbol,		VoidSymbol,U0302		]	}; // ^' Pause Break  ⊕   x̂  8 ̂=комб.
    //replace key <PAUS> {[asciicircum,	U02BC,		Pause,	Break,			U2295,	VoidSymbol,		VoidSymbol,U0302		]	}; // ^ʼ Pause Break  ⊕   x̂  8 ̂=комб.  
    
    // в pc: key <PRSC> { type= "PC_ALT_LEVEL2", symbols[Group1]= [ Print, Sys_Req ] }; то есть Sys_Req набирается не с Shift, а с Alt, и на 4 уровне бесполезен
    replace key <PRSC> {[equal,		notequal,	Print,	U229C,			identical,	notidentical,	U208C,		U207C	]	}; // =≠ PrScr  ⊜       ≡≢ ₌⁼
    replace key <SCLK> {[asciicircum,figdash,	Scroll_Lock,VoidSymbol,	U2295,		endash,			VoidSymbol,	U0302	]	}; // ^‒ ScrollLock     ⊕–  x̂  8 ̂=комб.
    replace key <PAUS> {[percent,	U02BC,		Pause,	Break,			permille,	U2031,		VoidSymbol,VoidSymbol	]	}; // %ʼ Pause  Break   ‰‱        

};

partial alphanumeric_keys function_keys modifier_keys xkb_symbols "common_88hex_patch" {
    replace key <ESC>  {	type="FOUR_LEVEL",	[ F,	Escape,		Escape,	Escape		]	};
    key.type="EIGHT_LEVEL";     
    key <FK11> {	[ A,	guillemotleft	]};  
    key <FK12> {	[ B,	guillemotright	]};
    key <PRSC> {	[ C,	percent			]}; 
    key <SCLK> {	[ D,	asciicircum		]};  
    key <PAUS> {	[ E,	dollar			]};    
        
};

        
   
    key.type="EIGHT_LEVEL";     
    key <FK09> {					[8, VoidSymbol,	F9, F9, 	bracketleft,	enfilledcircbullet,		VoidSymbol,	VoidSymbol	],	overlay2=<I183>	}; // 8   [• 
    key <FK10> {					[9, VoidSymbol,	F10,F10,	parenright,		U27A4,					VoidSymbol,	VoidSymbol	],	overlay2=<I184>	}; // 9   )➤

    replace key <SCLK> {[parenleft,	approxeq,		Scroll_Lock,VoidSymbol,		U2249,		U229C,				U208D,	U207D	]	}; // (≈ ScrollLock     ≉⊜ ₍⁽
    replace key <PAUS> {[parenright,U02BC,			Pause,	Break,				VoidSymbol,	VoidSymbol,			U208E,	U207E	]	}; // )ʼ Pause  Break      ₎⁾        

    
    
    key <AE05> {[ colon, 		percent,		5,	U205D,			permille,	U2031,		fivesubscript,	fivesuperior	]}; // :% 5⁝  ‰‱ ₅⁵
    //key <AE06> {[ comma,		figdash,		6,	asciicircum,	U2295,		endash,		sixsubscript,	sixsuperior		]}; // ,‒ 6^  ⊕– ₆⁶ 
    key <AE06> {[ comma,		U2295,			6,	asciicircum,	VoidSymbol,	VoidSymbol,	sixsubscript,	sixsuperior		]}; // ,⊕ 6^     ₆⁶ 
    //key <AE07> {[ period,		ampersand,	7,enfilledcircbullet,	logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7•  ∧⊙ ₇⁷
    key <AE07> {[ period,		ampersand,		7,	ampersand,		logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7&  ∧⊙ ₇⁷
    key <AE08> {[ underscore,	bar,			8,	asterisk,		logicalor,	signifblank,eightsubscript,	eightsuperior	]}; // _| 8*  ∨␣ ₈⁸   
    key <AE09> {[ question,		bracketleft,	9,	parenleft,		VoidSymbol,	VoidSymbol,	ninesubscript,	ninesuperior	]}; // ?[ 9(     ₉⁹
    key <AE10> {[ emdash,		bracketright,	0,	parenright,		figdash,	endash,		zerosubscript,	zerosuperior	]}; // —] 0)  ‒– ₀⁰ 
    key <AE11> {[ exclam,		U2212,		minus,	underscore,		notsign, 	U0336,		U208B,			U207B			]}; // !− -_  ¬− ₋⁻ 4=минус,6=кмб.зач.
    //key<AE12> {[ semicolon,	U22C5,		equal,	plus,			VoidSymbol,	U2299,		U208A,			U207A			]}; // ;⋅ =+   ⊙ ₊⁺
    key	<AE12> {[ semicolon,	U22C5,		equal,	plus,			U2287,		U2289,		U208A,			U207A			]}; // ;⋅ =+  ⊇⊉ ₊⁺




partial alphanumeric_keys function_keys modifier_keys xkb_symbols "lat_88" {    
    include "illinc(lat_alphanumeric_punctuation)"  
    //include "illinc(lat_alphanumeric_punctuation_bk_h_patch)"  
    //include "illinc(lat_alphanumeric_punctuation_en_us_quotes_patch)"  
        
    key.type="EIGHT_LEVEL"; 
    // Включение Лат только клавишей ЛАТ, \$ — без смены языка    
    replace key <PRSC> {[percent,	multiply,		Print,	Sys_Req,		permille,	U2031,		VoidSymbol,VoidSymbol	]	}; // %× PrScr SysRq  ‰‱       
    replace key <SCLK> {[asciicircum,U22C5,			Scroll_Lock,VoidSymbol,	U2295,		U2299,		VoidSymbol,U0302		]	}; // ^⋅ ScrollLock   ⊕⊙ x̂  8 ̂=комб.
    replace key <PAUS> {[dollar,	apostrophe,		Pause,	Break,			VoidSymbol,	VoidSymbol,	VoidSymbol,VoidSymbol	]	}; // $' Pause Break          
    
};
// ↑↑↑ ralt и цифровой блок не переопределены
// Вариант РУС для lat_88
partial alphanumeric_keys function_keys modifier_keys xkb_symbols "rus_88" { include "illinc(lat_88)" include "illinc(rus_alph_patch)" };
//$ setxkbmap "illinc(lat_88),illinc(rus_88)"
// XKBVARIANT="lat_88,rus_88"
// ↑↑↑ не столько для нормальной 88-клавишной клавиатуры (если цифрового блока нет, его переопределение не помешает),
// сколько для ноутбуков, где цифровой блок представлен частично или доступен через Fn, и переопределять его нежелательно

    //key <AE06> {[ comma,		U2295,			6,	asciicircum,	VoidSymbol,	VoidSymbol,	sixsubscript,	sixsuperior		]}; // ,⊕ 6^     ₆⁶ (-)
    //key <AE06> {[ comma,		approxeq,		6,	asciicircum,	U2295,		U229C,		sixsubscript,	sixsuperior		]}; // ,≈ 6^  ⊕⊜ ₆⁶ (-)

    //key <AE07> {[ period,		ampersand,		7,	ampersand,		logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7&  ∧⊙ ₇⁷ (-)
    //key <AE07> {[ period,		U22C5,			7,	ampersand,		logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7⋅  ∧⊙ ₇⁷ (-)

    //key <AE09> {[ question,		bracketleft,	9,	parenleft,		VoidSymbol,	VoidSymbol,	ninesubscript,	ninesuperior	]}; // ?[ 9(     ₉⁹ (-)
    //key <AE10> {[ emdash,		bracketright,	0,	parenright,		figdash,	endash,		zerosubscript,	zerosuperior	]}; // —] 0)  ‒– ₀⁰  (-)
    //key <AE11> {[ exclam,		U2212,		minus,	underscore,		notsign, 	U0336,		U208B,			U207B			]}; // !− -_  ¬− ₋⁻ (-)2=минус,6=кмб.зач.



// Скобки () на втором уровне линии Backspace, [] — на втором уровне F9‒F10: если SCLK и PAUS нет на клавиатуре и нет иных вариантов для скобок
partial alphanumeric_keys function_keys xkb_symbols "common_numericparentheses_patch" {
    key.type="EIGHT_LEVEL";     
    key <AE09> {[ question,		parenleft	]}; // ?( 
    key <AE10> {[ emdash,		parenright	]}; // —)        
//     key <FK09> {					[8, bracketleft		],	overlay2=<I183>	}; // 8[   
//     key <FK10> {					[9, bracketright	],	overlay2=<I184>	}; // 9]  
    key <FK09> {					[8, bracketleft,	F9, F9, 	bracketleft],	overlay2=<I183>	}; // 8[  [ 
    key <FK10> {					[9, bracketright,	F10,F10,	parenright	],	overlay2=<I184>	}; // 9]  )
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	leftsinglequotemark,leftdoublequotemark,	VoidSymbol,	VoidSymbol	],	overlay2=<I221>	}; // «„  ‘“  
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	rightsinglequotemark,rightdoublequotemark,	VoidSymbol,	VoidSymbol	],	overlay2=<I222>	}; // »“  ’”
};
        
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			seconds,	U2057,		zerosubscript,	zerosuperior	]}; // "§ 0`  ″⁗ ₀⁰
    key <AE01> {[ plus,			numerosign,		1,	exclam,			plusminus,	U2295,		onesubscript,	onesuperior		]}; // +№ 1!  ±⊕ ₁¹
    key <AE02> {[ minus,		at,				2,	U2011,			U2213,		U2296,		twosubscript,	twosuperior		]}; // -@ 2‑  ∓⊖ ₂²        
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		threesubscript,	threesuperior	]}; // /< 3#  ≮⊘ ₃³
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		foursubscript,	foursuperior	]}; // *> 4$  ≯⊛ ₄⁴
    key <AE05> {[ colon, 		percent,		5,	U205D,			permille,	U2031,		fivesubscript,	fivesuperior	]}; // :% 5⁝  ‰‱ ₅⁵
    key <AE06> {[ comma,		U2212,			6,	asciicircum,	VoidSymbol,	VoidSymbol,	sixsubscript,	sixsuperior		]}; // ,− 6^     ₆⁶   2=минус шир.+
    key <AE07> {[ period,		ampersand,		7,	question,		logicaland,	U2299,		sevensubscript,	sevensuperior	]}; // .& 7?  ∧⊙ ₇⁷
    key <AE08> {[ underscore,	bar,			8,	asterisk,		logicalor,	signifblank,eightsubscript,	eightsuperior	]}; // _| 8*  ∨␣ ₈⁸   
    key <AE09> {[ parenleft,	bracketleft,	9,	parenleft,		bracketleft,U207D,		ninesubscript,	ninesuperior	]}; // ([ 9(  [⁽ ₉⁹
    key <AE10> {[ parenright,	bracketright,	0,	parenright,		parenright,	U207E,		zerosubscript,	zerosuperior	]}; // )] 0)  )⁾ ₀⁰ 
    key <AE11> {[ emdash,		figdash,		minus,	underscore,	U2212,		endash,		U208B,			U207B			]}; // —− -_  −– ₋⁻    
    key	<AE12> {[ semicolon,	U22C5,		equal,	plus,			U2287,		U2289,		U208A,			U207A			]}; // ;⋅ =+  ⊇⊉ ₊⁺

    key <FK08> {[7, U2309,			F8, F8, 	notsign,			U0336,					VoidSymbol,				VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬̶            
    key <FK09> {[8, guillemotleft,	F9, F9, 	doublelowquotemark,	leftsinglequotemark,	leftdoublequotemark,	VoidSymbol	],	overlay2=<I183>	}; // 8«  „‘ “
    key <FK10> {[9, guillemotright,	F10,F10,	leftdoublequotemark,rightsinglequotemark,	rightdoublequotemark,	VoidSymbol	],	overlay2=<I184>	}; // 9»  “’ ”
//     key <AD08> {[	h,		H,	guillemotleft,doublelowquotemark,	U210D,	U274C,			U2095,		U02B0	]}; // h H « „   ℍ ❌ ₕ ʰ
//     key <AD09> {[	v,		V,	guillemotright,leftdoublequotemark,	emptyset,checkmark,		U1D65,		U1D5B	]}; // v V » “   ∅ ✓ ᵥ ᵛ   4 “ рус.правая=англ.левая

    key <AD08> {[	h,		H,	leftsinglequotemark,doublelowquotemark,	U210D,	U274C,			U2095,	U02B0	]}; // h H ‘ „   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	rightsinglequotemark,leftdoublequotemark,emptyset,checkmark,	U1D65,	U1D5B	]}; // v V ’ “   ∅ ✓ ᵥ ᵛ   

    
        replace key  <KP7> {[ less,			sevensubscript,		KP_Home, 	VoidSymbol,			sevensuperior,	sevensuperior	]};
    replace key  <KP8> {[ greater,		eightsubscript,		KP_Up, 		VoidSymbol,			eightsuperior,	eightsuperior	]};
    replace key  <KP9> {[ minus,		ninesubscript,		KP_Prior, 	VoidSymbol,			ninesuperior,	ninesuperior	]}; 

    replace key  <KP4> {[ bracketleft,	foursubscript,		KP_Left, 	VoidSymbol,			foursuperior,	foursuperior	]};
    replace key  <KP5> {[ bracketright,	fivesubscript,		KP_Begin, 	VoidSymbol,			fivesuperior,	fivesuperior	]};
    replace key  <KP6> {[ parenright,	sixsubscript,		KP_Right, 	VoidSymbol,			sixsuperior,	sixsuperior		]};

    //replace key  <KP1> {[ ampersand,	onesubscript,		KP_End, 	VoidSymbol,			onesuperior,	onesuperior		]};
    replace key  <KP1> {[ U22C5,		onesubscript,		KP_End, 	VoidSymbol,			onesuperior,	onesuperior		]}; // ⋅
    replace key  <KP2> {[ bar,			twosubscript,		KP_Down, 	VoidSymbol,			twosuperior,	twosuperior		]};
    replace key  <KP3> {[ U2286,		threesubscript,		KP_Next, 	VoidSymbol,			threesuperior,	threesuperior	]}; 

    replace key  <KP0> {[ space,		zerosubscript,		KP_Insert, 	signifblank,		zerosuperior,	zerosuperior	]}; //  ₀ Ins␣  
    replace key <KPDL> {[ elementof,	VoidSymbol,			KP_Delete,	VoidSymbol,			VoidSymbol,		VoidSymbol		]}; // ∈  Del    

    //key <AE02> {[ minus,		at,				2,	U2011,			U2213,		U2296,		twosubscript,	twosuperior		]}; (-)// -@ 2‑  ∓⊖ ₂²        
    
    
    //replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ exclam, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // ! Ins
    //key <FK12> {[question,	approxeq,	F12,F12,	U2249,		U229C,				VoidSymbol,	VoidSymbol	],	overlay2=<I222>	};	// ?≈       ≉⊜ 
 //     replace key <PRSC> {[exclam,	VoidSymbol,		Print,VoidSymbol,		notsign,	U0336,			VoidSymbol,	VoidSymbol]	};	// !  PS    ¬̶(зачёрк.)
    
//     replace key <SCLK> {[percent,	VoidSymbol,		Scroll_Lock,VoidSymbol,	logicalor,	VoidSymbol,		VoidSymbol,	VoidSymbol]	};	// %  SL    ∨
//     replace key <PAUS> {[ampersand,	VoidSymbol,		Pause,	Break,			logicaland,	VoidSymbol,		VoidSymbol,	VoidSymbol]	};	// &  P BR  ∧    
    
    //replace key <PRSC> {[backslash,	VoidSymbol,		Print,VoidSymbol,		VoidSymbol,	VoidSymbol,		VoidSymbol,	VoidSymbol]	}; // \  PS    
    //replace key <SCLK> {[percent,	VoidSymbol,		Scroll_Lock,VoidSymbol,	VoidSymbol,	VoidSymbol,		VoidSymbol,	VoidSymbol]	}; // %  ScrollLock     
    
    //replace key <PRSC> { type= "PC_ALT_LEVEL2", symbols[Group1]= [ backslash, Sys_Req ] };	// \ и всё: SysRq с Alt, других уровней не предусмотрено!
   
    
    
    
   // Цифровой блок как скобки

partial keypad_keys xkb_symbols "common_keypad_to_altmodes_bracesmode" {   
    
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    //replace key <NMLK> 	{type="TWO_LEVEL",	[ percent,	VoidSymbol	]};     
    //modifier_map Mod2   { Num_Lock };
    
    key.type="EIGHT_LEVEL";     
    
    //replace key <KPDV> {[ figdash,		endash,				slash, 		VoidSymbol,			VoidSymbol,		VoidSymbol		]}; // ‒–  /
    
    replace key <NMLK> {[guillemotleft,doublelowquotemark,	VoidSymbol,VoidSymbol,	leftsinglequotemark,leftdoublequotemark,	U208D,U207D]}; // «„    ‘“ ₍⁽ 
    replace key <KPDV> {[guillemotright,leftdoublequotemark,	slash,VoidSymbol,	rightsinglequotemark,rightdoublequotemark,	U208E,U207E]}; // »“ /  ’” ₎⁾
    modifier_map Mod2   { Num_Lock };

  
    //replace key <KPMU> {[ U22C5,		VoidSymbol,			asterisk,	VoidSymbol,			VoidSymbol,		VoidSymbol		]}; (-) // ⋅   *   
    replace key <KPMU> {[ figdash,		VoidSymbol,			asterisk,	VoidSymbol,			VoidSymbol,		VoidSymbol		]}; // ‒   *    1=цифровое тире
    replace key <KPSU> {[ U2212,		plusminus,			minus,		VoidSymbol,			U208B,			U207B			]}; // −±  - ₋⁻ 1=минус ширины +
    
    replace key <KPAD> {[ Tab,		ISO_Left_Tab,			plus,		VoidSymbol,			U208A,			U207A			]}; // Tab +  ₊⁺
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    replace key  <KP7> {[ U22C5,		sevensubscript,		KP_Home, 	VoidSymbol,			sevensuperior,	sevensuperior	]}; // ⋅
    replace key  <KP8> {[ bar,			eightsubscript,		KP_Up, 		VoidSymbol,			eightsuperior,	eightsuperior	]};
    replace key  <KP9> {[ U2286,		ninesubscript,		KP_Prior, 	VoidSymbol,			ninesuperior,	ninesuperior	]}; // ⊆

    replace key  <KP4> {[ bracketleft,	foursubscript,		KP_Left, 	VoidSymbol,			foursuperior,	foursuperior	]};
    replace key  <KP5> {[ bracketright,	fivesubscript,		KP_Begin, 	VoidSymbol,			fivesuperior,	fivesuperior	]};
    replace key  <KP6> {[ parenright,	sixsubscript,		KP_Right, 	VoidSymbol,			sixsuperior,	sixsuperior		]};

    replace key  <KP1> {[ less,			onesubscript,		KP_End, 	VoidSymbol,			onesuperior,	onesuperior		]}; 
    replace key  <KP2> {[ greater,		twosubscript,		KP_Down, 	VoidSymbol,			twosuperior,	twosuperior		]};
    replace key  <KP3> {[ minus,		threesubscript,		KP_Next, 	VoidSymbol,			threesuperior,	threesuperior	]}; 

    replace key  <KP0> {[ space,		zerosubscript,		KP_Insert, 	signifblank,		zerosuperior,	zerosuperior	]}; //  ₀ Ins␣  
    replace key <KPDL> {[ elementof,	VoidSymbol,			KP_Delete,	VoidSymbol,			VoidSymbol,		VoidSymbol		]}; // ∈  Del    
     
};

partial keypad_keys xkb_symbols "common_keypad_to_altmodes_digitsmode_patch" {   
    key.type="EIGHT_LEVEL";     
    key  <KP7> {[ 7	]};
    key  <KP8> {[ 8	]};
    key  <KP9> {[ 9	]}; 

    key  <KP4> {[ 4	]};
    key  <KP5> {[ 5	]};
    key  <KP6> {[ 6	]};

    key  <KP1> {[ 1	]};
    key  <KP2> {[ 2	]};
    key  <KP3> {[ 3	]}; 

    key  <KP0> {[ 0	]};   
    key <KPDL> {[ comma]}; // ,   
     
};
   
partial keypad_keys xkb_symbols "common_keypad_to_altmodes_digitsmode_orioks_patch" {   
    include "illinc(common_keypad_to_altmodes_digitsmode_patch)"   
    key <KPMU> {[ Cyrillic_en		]}; // н 
    key <KPDL> {[ period			]}; // .        
};

// Для 104-клавишной (ANSI с цифровым блоком): # на первом уровне (вместо отсутствующей LSGT)
partial keypad_keys xkb_symbols "common_keypad_104numbersign_patch" {   
    key  <KP0>  {type="EIGHT_LEVEL",	[ numbersign ]};   
};



    key <FK08> {[7, U2309,				F8, F8, 	VoidSymbol,				VoidSymbol,				VoidSymbol,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉             
    key <FK12> {[backslash,	approxeq,	F12,F12,	U2249,		U229C,				VoidSymbol,	VoidSymbol	],	overlay2=<I222>	};	// \≈       ≉⊜ 
    replace key <SCLK> {[percent,	U22C5,			Scroll_Lock,Print,		VoidSymbol,	VoidSymbol,		VoidSymbol,	VoidSymbol]	};	// %⋅ SL PS    
    replace key <PAUS> {[ampersand,	U22C5,			Pause,	Break,			VoidSymbol,	VoidSymbol,		VoidSymbol,	VoidSymbol]	};	// &⋅ P  BR
    
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		includedin,	U2284,			U1D69,		U1DA0	]}; // f F φ Φ   ⊂ ⊄ ᵩ ᶠ

     key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		signifblank,	U209A,		U1D56	]}; // p P π Π   ∏ ␣ ₚ ᵖ   
   
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U299C,			U209A,		U1D56	]}; // p P π Π   ∏ ⦜ ₚ ᵖ   6 ⦜=прямой угол
    
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,		VoidSymbol,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠   ᶻ
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,			U2221,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠ ∡ ᶻ
   
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    //replace key <NMLK> 	{type="TWO_LEVEL",	[ percent,	VoidSymbol	]};     
    //modifier_map Mod2   { Num_Lock };
    
       replace key <KPDL> {[ comma,	VoidSymbol,			KP_Delete,	U2473,			VoidSymbol,	VoidSymbol	]}; // ,  Del   

    // 2 уровень — нижний индекс (нужен чаще), 5 — верхний, 6 — форма в круге
    replace key  <KP7> {[ 7,		sevensubscript,		KP_Home, 	U2470,			sevensuperior,	U2466	]}; // 7₇ Home ⑰ ⁷⑦
    replace key  <KP8> {[ 8,		eightsubscript,		KP_Up, 		U2471,			eightsuperior,	U2467	]}; // 8₈ ↑    ⑱ ⁸⑧
    replace key  <KP9> {[ 9,		ninesubscript,		KP_Prior, 	U2472,			ninesuperior,	U2468	]}; // 9₉ PgUp ⑲ ⁹⑨

    replace key  <KP4> {[ 4,		foursubscript,		KP_Left, 	U246D,			foursuperior,	U2463	]}; // 4₄ ←    ⑭ ⁴④
    replace key  <KP5> {[ 5,		fivesubscript,		KP_Begin, 	U246E,			fivesuperior,	U2464	]}; // 5₅ ???  ⑮ ⁵⑤
    replace key  <KP6> {[ 6,		sixsubscript,		KP_Right, 	U246F,			sixsuperior,	U2465	]}; // 6₆ →    ⑯ ⁶⑥

    replace key  <KP1> {[ 1,		onesubscript,		KP_End, 	U246A,			onesuperior,	U2460	]}; // 1₁ End  ⑪ ¹①
    replace key  <KP2> {[ 2,		twosubscript,		KP_Down, 	U246B,			twosuperior,	U2461	]}; // 2₂ ↓    ⑫ ²②
    replace key  <KP3> {[ 3,		threesubscript,		KP_Next, 	U246C,			threesuperior,	U2462	]}; // 3₃ PgDn ⑬ ³③

    replace key  <KP0> {[ 0,		zerosubscript,		KP_Insert, 	U2469,			zerosuperior,	U24EA	]}; // 0₀ Ins  ⑩ ⁰⓪ 
    replace key <KPDL> {[ comma,	VoidSymbol,			KP_Delete,	U2473,			VoidSymbol,	VoidSymbol	]}; // ,  Del     20 в круге не печатается


    replace key  <KP0> {[ 0,		zerosubscript,		KP_Insert, 	U24FF,			zerosuperior,	U24EA	]}; // 0₀ Ins  ⓿ ⁰⓪ 
    ≠0 в https://symbl.cc/ru/unicode/blocks/dingbats/

    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2766,		hyphen	]}; // $ $ η Η   ∋ ∌ ❦ -   8 =мягкий перенос (shy)
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		includedin,	U2284,			U2767,		U1DA0	]}; // f F φ Φ   ⊂ ⊄ ❧ ᶠ
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U262D,			U209A,		U1D56	]}; // p P π Π   ∏ ☭ ₚ ᵖ   
    
    
    // 2=комб.обводка с перечёркиванием, 5=горизонтальное перечёркивание; на 4 уровне не пропечатывается НИЧЕГО (Shift+Del?)
    
 
    replace key <KPMU> {[ figdash,	U20DD,	KP_Multiply,VoidSymbol,	U262D,			VoidSymbol			]}; // ‒a⃝  *  ☭  1=цифровое тире, 2=комб.обводка
    
    // 2 уровень занят Shift+Tab, 5 — верхний индекс ⁻⁺ для совместимости с цифрами, 6 — нижний индекс ₋₊ (используется реже верхнего ⁻⁺)
    replace key <KPSU> {[ U2212,plusminus,	KP_Subtract,U25B7,		U207B,			U208B				]}; // −±  -▷ ⁻₋ 1=минус ширины +
    replace key <KPAD> {[ Tab,ISO_Left_Tab,	KP_Add,	VoidSymbol,		U207A,			U208A				]}; // Tab +  ⁺₊
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    // 2 уровень — нижний индекс (нужен чаще), 5 — верхний, 6 — форма в круге, сложнонабираемый 4 — (10+x) в круге
    replace key  <KP7> {[ 7,sevensubscript,	KP_Home, 	U2470,		sevensuperior,	U2466,	U0103,	U0102		]}; // 7₇  Home ⑰   ⁷⑦ ăĂ
    replace key  <KP8> {[ 8,eightsubscript,	KP_Up, 		U2471,		eightsuperior,	U2467,	U01B0,	U01AF		]}; // 8₈  ↑    ⑱   ⁸⑧ ưƯ
    replace key  <KP9> {[ 9,ninesubscript,	KP_Prior, 	U2472,		ninesuperior,	U2468,	U2151,	U215F		]}; // 9₉  PgUp ⑲   ⁹⑨ ⅑⅟

    replace key  <KP4> {[ 4,foursubscript,	KP_Left, 	U246D,		foursuperior,	U2463,threefifths,fourfifths]}; // 4₄  ←    ⑭   ⁴④ ⅗⅘
    replace key  <KP5> {[ 5,fivesubscript,	KP_Begin, 	U246E,		fivesuperior,	U2464,	onefifth,twofifths	]}; // 5₅  ???  ⑮   ⁵⑤ ⅕⅖
    replace key  <KP6> {[ 6,sixsubscript,	KP_Right, 	U246F,		sixsuperior,	U2465,	onesixth,fivesixths	]}; // 6₆  →    ⑯   ⁶⑥ ⅙⅚

    replace key  <KP1> {[ 1,onesubscript,	KP_End, 	U246A,		onesuperior,	U2460,	U00E2,	U00C2		]}; // 1₁  End  ⑪   ¹① âÂ
    replace key  <KP2> {[ 2,twosubscript,	KP_Down, 	U246B,		twosuperior,	U2461,	U00EA,	U00CA		]}; // 2₂  ↓    ⑫   ²② êÊ
    replace key  <KP3> {[ 3,threesubscript,	KP_Next, 	U246C,		threesuperior,	U2462,	U00F4,	U00D4		]}; // 3₃  PgDn ⑬   ³③ ôÔ

    replace key  <KP0> {[ 0,zerosubscript,	KP_Insert,	U2469,		zerosuperior,	U24EA,	U01A1,	U01A0		]}; // 0₀  Ins  ⑩   ⁰⓪ ơƠ
    replace key <KPDL> {[ comma, U20E0,		KP_Delete,	VoidSymbol,	U0336,			U0336,	U0111,	U0110		]}; // ,a⃠  Del      −− đĐ  2,5 — комб.зач, 6=5
 
     key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	includes,	U2285,			U2C7C,		U02B2	]}; // j J θ Θ   ⊃ ⊅ ⱼ ʲ   
    key <AB05> {[	i,		I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,		U1D62,		U2071	]}; // i I ι Ι   ∩ ∧ ᵢ ⁱ
    
    // на 4 уровне клавиш KPDV, KPMU, KPAD, KPDL ни один символ не пропечатывается ⇒ VoidSymbol (на 4 уровне NMLK и KPSU норма)
    // на 6 уровне KPMU тоже (6 уровень NMLK/KPDV “”, KPSU/KPAD ₋₊ — норма)
    ↑↑↑ это глючит клавиатура Гарнизон
    
    replace key <KPMU> {[ figdash,	U2C7C,					KP_Multiply,U20DD,		U02B2,			U262D		]}; // ‒ⱼ    *  a⃝    ʲ☭  1=цифровое тире
    
    // 2 уровень занят Shift+Tab, 5 — верхний индекс ⁻⁺ для совместимости с цифрами, 6 — нижний индекс ₋₊ (используется реже верхнего ⁻⁺)
    replace key <KPSU> {[ U2212,	U2099,					KP_Subtract,U20E0,		U207B,			U208B		]}; // −ₙ    -  a⃠    ⁻₋  1=минус ширины плюса
    replace key <KPAD> {[ Tab,ISO_Left_Tab,					KP_Add,		U0336,		U207A,			U208A		]}; // Tab   +  a̶   ⁺₊  4=комб.зачёркивание
    key <FK11> {[equal,		notequal,	F11,F11,	identical,			notidentical,			U208C,U207C	],	overlay2=<I221>	}; // =≠  ≡≢ ₌⁼
    key <FK12> {[backslash,	approxeq,	F12,F12,	U2249,				U229C,					U2216,U29B8	],	overlay2=<I222>	}; // \≈  ≉⊜ ∖⦸

    
    replace key <SCLK> {[percent,	permille,	Scroll_Lock,Print,		onequarter,	threequarters,	onefifth,	U2152	]	};	// %‰ SL PS   ¼¾ ⅕⅒    
    
    key <FK09> {[8, doublelowquotemark,	F9, F9, 	leftsinglequotemark,leftdoublequotemark,	U20DD,U0306	],	overlay2=<I183>	}; // 8„  ‘“ a⃝ă   8 ̆=комб.кратка
    key <FK10> {[9, leftdoublequotemark,F10,F10,	rightsinglequotemark,rightdoublequotemark,	U20E0,U031B	],	overlay2=<I184>	}; // 9“  ’” a⃠a̛  8 ̛=комб.рожок
    //key <FK11> {[equal,		notequal,	F11,F11,	identical,			notidentical,			U208C,U207C	],	overlay2=<I221>	}; // =≠  ≡≢ ₌⁼
    //key <FK12> {[backslash,	approxeq,	F12,F12,	U2249,				U229C,					U2216,U29B8	],	overlay2=<I222>	}; // \≈  ≉⊜ ∖⦸
    // нечаянно набрать ≡ вместо ≠, как один раз случилось — хуже, чем вместо ≈
    //key <FK11> {[equal,		notequal,	F11,F11,	U0336,				U229C,					U208C,U207C	],	overlay2=<I221>	}; // =≠  ̶⊜ ₌⁼  5=комб.зачёркивание
    //key <FK12> {[backslash,	approxeq,	F12,F12,	identical,			notidentical,			U2216,U29B8	],	overlay2=<I222>	}; // \≈  ≡≢ ∖⦸
    
     replace key <SCLK> {[percent,	plusminus,	Scroll_Lock,Print,		onequarter,	threequarters,	onefifth,	U2152	]	};	// %± SL PS   ¼¾ ⅕⅒    
    replace key <PAUS> {[ampersand,	U02BC,		Pause,	Break,			oneeighth,threeeighths,	fiveeighths,seveneighths]	};	// &ʼ P  BR   ⅛⅜ ⅝⅞
    key <FK08> {[7, U2309,				F8, F8, 	onethird,			twothirds,				U2150,U0309	],	overlay2=<I178>	}; // 7⌉  ⅓⅔ ⅐ả   8 ̉=комб.хвостик
    replace key <PAUS> {[ampersand,	U02BC,		Pause,	Break,			oneeighth,threeeighths,	fiveeighths,seveneighths]	};	// &ʼ P  BR   ⅛⅜ ⅝⅞
   
    key <FK08> {[7, U2309,				F8, F8, 	oneeighth,threeeighths,	fiveeighths,seveneighths	],	overlay2=<I178>	}; // 7⌉  ⅛⅜ ⅝⅞
    
    key <FK09> {[8, doublelowquotemark,	F9, F9, 	leftsinglequotemark,leftdoublequotemark,	U208D,U2151	],	overlay2=<I183>	}; // 8„  ‘“ ₍⅑
    key <FK10> {[9, leftdoublequotemark,F10,F10,	rightsinglequotemark,rightdoublequotemark,	U208E,U2152	],	overlay2=<I184>	}; // 9“  ’” ₎⅒
    // 				↑↑↑ “ рус.правая=англ.левая
    key <FK11> {[equal,		approxeq,	F11,F11,	U2261,	U2262,								U208C,U207C	],	overlay2=<I221>	}; // =≈  ≡≢ ₌⁼
    key <FK12> {[backslash,	notequal,	F12,F12,	U0336,	U229C,								U20DD,U20E0	],	overlay2=<I222>	}; // \≠  ̶⊜ a⃝a⃠  5,7,8 — комбинируемые
        
    replace key <SCLK> {[percent,	plusminus,	Scroll_Lock,Print,		onequarter,	threequarters,	VoidSymbol,	U031B	]	};	// %± SL PS   ¼¾  a̛  8 ̛=комб.рожок
    replace key <SCLK> {[percent,	plusminus,	Scroll_Lock,Print,		U0336,	U0336,				U20DD,		U20E0	]	};	// %± SL PS   a̶ a⃝a⃠   5,7,8 — комб.
   
   
   key <FK12> {[backslash,	notequal,	F12,F12,	onequarter,	U229C,					threequarters,U20E0	],	overlay2=<I222>	}; // \≠  ¼⊜ ¾a⃠
    replace key <PAUS> {[ampersand,	U02BC,		Pause,	Break,			U0336,	VoidSymbol,		U2150,		U031B		]	};	// &ʼ P  BR   ̶   ⅐a̛  8=комб.рожок
   
   
   key <FK08> {[7, U2309,				F8, F8, 	oneeighth,threeeighths,				seveneighths,U0309	],	overlay2=<I178>	}; // 7⌉  ⅛⅜ ⅞ả  8 ̉=комб.хвостик (ого́нэк)
   
   
//     replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ backslash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  
//     // ↑↑↑ В режиме CapsLock получается Insert на первом уровне! Но режим редкий...
//     //
//     //replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ backslash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert],  // \Лат, а не просто \ 
//     //   actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};  
     
    key <FK08> {[7, U2309,				F8, F8, 	oneeighth,threeeighths,			fiveeighths,seveneighths],	overlay2=<I178>	}; // 7⌉  ⅛⅜ ⅝⅞

    
    replace key <PRSC> {[parenleft, U22C5,		Print,		Sys_Req,	onethird,	twothirds,		onesixth,	fivesixths	]	};	// (⋅ PS SR   ⅓⅔ ⅙⅚
    replace key <SCLK> {[percent,	plusminus,	Scroll_Lock,Print,		onefifth,	twofifths,		threefifths,fourfifths	]	};	// %± SL PS   ⅕⅖ ⅗⅘
    replace key <PAUS> {[ampersand,	U02BC,		Pause,		Break,		onequarter,	threequarters,	U2150,		U215F		]	};	// &ʼ P  BR   ¼¾ ⅐⅟
    
    
    key.type="EIGHT_LEVEL_ALPHABETIC";
    //					Shift	3				3+Shift			5			5+Shift			5+3			5+3+Shift
    key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	includes,	U2285,			U2C7C,		U02B2	]}; // j J θ Θ   ⊃ ⊅ ⱼ ʲ   
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		copyright,		U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ © ⤉ ᶜ
    key <AD03> {[	u,		U,	Greek_upsilon,	Greek_UPSILON,	union,		logicalor,		U1D64,		U1D58	]}; // u U υ Υ   ∪ ∨ ᵤ ᵘ
    key <AD04> {[	k,		K,	Greek_kappa,	Greek_KAPPA,	U27E1,		U207A,			U2096,		U1D4F	]}; // k K κ Κ   ⟡ ⁺ ₖ ᵏ   6 ⁺=верхний индекс (заряд)
    key <AD05> {[	e,		E,	Greek_epsilon,	Greek_EPSILON,	U2203,		U2204,			U2091,		U1D49	]}; // e E ε Ε   ∃ ∄ ₑ ᵉ
    key <AD06> {[	n,		N,	Greek_nu,		Greek_NU,		U2115,		notsign,		U2099,		U207F	]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	degree,		degree,			nabla,		U1D4D	]}; // g G γ Γ   ° ° ∇ ᵍ
    key <AD08> {[	h,		H,	VoidSymbol,		VoidSymbol,		U210D,		U274C,			U2095,		U02B0	]}; // h H       ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	VoidSymbol,		VoidSymbol,		emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V       ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠ ★ ᶻ
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		U2286,		U2288,			U0323,		U0300	]}; // ` ` χ Χ   ⊆ ⊈ ạ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		U2287,		U2289,			U2057,		U0301	]}; // ' ' ʼ '   ⊇ ⊉ ⁗ ́a   8 ́=комб.ударение (русское)
  
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		includedin,	U2284,			U2691,		U1DA0	]}; // f F φ Φ   ⊂ ⊄ ⚑ ᶠ
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U21A9,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ↩ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21AA,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↪ ʷ
    key <AC04> {[	a,		A,		Greek_alpha,Greek_ALPHA,	U2200,		U207B,			U2090,		U1D43	]}; // a A α Α   ∀ ⁻ ₐ ᵃ   6 ⁻=верхний индекс (заряд)
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,	partdifferential,	U209A,		U1D56	]}; // p P π Π   ∏ ∂ ₚ ᵖ   
    key <AC06> {[	r,		R,		Greek_rho,	Greek_RHO,		U211D,		U20BD,			U1D63,		U02B3	]}; // r R ρ Ρ   ℝ ₽ ᵣ ʳ
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U21A6,			U2092,		U1D52	]}; // o O ω Ω   𝕆 ↦ ₒ ᵒ   6 ↦=отображается в
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U1F12F,			U2097,		U02E1	]}; // l L λ Λ   ℓ 🄯 ₗ ˡ   6 🄯=копилефт
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,			U2300,		U1D48 	]}; // d D δ Δ   ∆ ⏨ ⌀ ᵈ   5 ∆=приращение, 7 ⌀=диаметр
    key <AC10> {[asciicircum,asciicircum,Greek_psi,	Greek_PSI,	elementof,	notelementof,	U27A4,		U0302	]}; // ^ ^ ψ Ψ   ∈ ∉ ➤ x̂   8 ̂=комб. 
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2023,		hyphen	]}; // $ $ η Η   ∋ ∌ ‣ -   8 =мягкий перенос (shy)

    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U279B,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➛ x⃗
    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ ̅x   8 ̅=комб.черта
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		ssharp,			U209B,		U02E2	]}; // s S σ Σ   ∑ ß ₛ ˢ
    key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		VoidSymbol,	VoidSymbol,		U2098,		U1D50	]}; // m M μ Μ       ₘ ᵐ        
    key <AB05> {[	i,		I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,		U1D62,		U2071	]}; // i I ι Ι   ∩ ∧ ᵢ ⁱ
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U1D40,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ᵀ ₜ ᵗ
    key <AB07> {[   x,		X,		Greek_xi,	Greek_XI,		multiply,	U2297,			U2093,		U02E3	]}; // x X ξ Ξ   × ⊗ ₓ ˣ
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,twosubscript,enfilledcircbullet,U1D47	]};	// b B β Β   ∞ ₂ • ᵇ
    key <AB09> {[braceleft,braceleft,	lessthanequal,	 U2270,		U227A,	U2280,			U227E,		U0307	]}; // { { ≤ ≰   ≺ ⊀ ≾ ẋ   8 ̇=комб.производная по t
    key <AB10> {[braceright,braceright,	greaterthanequal,U2271,		U227B,	U2281,			U227F,		U0308	]}; // } } ≥ ≰   ≻ ⊁ ≿ ẍ   8 ̈=комб.«вторая» ⟨умляут⟩
    
    
    replace key <PRSC> {[parenleft, U22C5,		U1D62,	U2071,		onethird,	twothirds,		Print,		Sys_Req	]	};	// (⋅ ᵢⁱ    ⅓⅔   PS SR
    replace key <SCLK> {[percent,	VoidSymbol,	U2C7C,	U02B2,		onequarter,threequarters,	Scroll_Lock,Print	]	};	// %  ⱼʲ    ¼¾   SL PS 
    replace key <PAUS> {[ampersand,	U02BC,		U2096,	U1D4F,		onefifth,	U2152,			Pause,		Break	]	};	// &ʼ ₖᵏ    ⅕⅒   P  BR 
   
    
// ****************************************************************************
Символы, которых нет в раскладке (хотя такая возможность рассматривалась)
+ символы, которые кочуют с клавиши на клавишу
+ символы, которые, возможно, вскоре будут удалены из раскладки
// ****************************************************************************
Поиск U-кода по символу: https://unicode-table.com/ru/blocks/
символ вводить в строку поиска наверху
// ****************************************************************************
// Имена клавиш из файла /usr/include/X11/keysymdef.h
// при этом префис XK_ опускать не просто можно (как сказано в документации), а НУЖНО (с ним не работает)!

// ****************************************************************************

Промилле permille ‰ и просантимилле U2031 ‱ — в раскладке почти с самого начала, но ни разу не понадобились и даже не нашли себе постоянного и логичного места;
частная ∂ и набла ∇ — тоже почти с самого начала (со ввода 5 уровня) и ни разу не понадобились — но и их место тоже ни подо что иное не нужно.

Отброшенные варианты ❌: ⨂✖❎🞪🞭⏹🛑🚫🛇⛔
    U26D4 ⛔ символ «Стоп», по идее красный и круглый; но он то кривой, то пробел после себя ест, то высоту строки портит
или невыразительны, или отображаются ещё кривее ❌ и часто портят следующую строку

Полукруглая шпация en space U2002 отображается шире nobreakspace; 
а тонкая шпация U2009 (thinspace) — почти такой же ширины, как nobreakspace (но чуть-чуть всё же тоньше).
Кроме того, thinspace разрывен, а U202F — нет; и U202F отображается чуть-чуть тоньше thinspace.
    
Мягкий перенос U00AD [shy, видно только в LO] #define XK_hyphen                        0x00ad  /* U+00AD SOFT HYPHEN */

---
Дефисы и минусы:
- Дефисоминус вычесть U+002D ← есть; вообще есть вся печатная ASCII
‐ Дефис U2010 [на вид ничем не отличается от дефисоминуса -‐] ← нет в раскладке 
‑ Неразрывный дефис U2011 [на вид тоже не отличается, но в LO подсвечивается и не переносится]

⊕⊕⊕
Символы в круге и в квадрате:
⊕⊖⊘⊛,⊙⊗,⊜ на us-цифрах; ⊗ продублирован на иксе. 
Чего нет, но при необходимости можно добавить заплатками:
⊚ Кольцевой оператор в круге U229A
если у арифметических операторов в круге есть смысл по умолчанию — подобнные им операторы в нестандартной алгебре, то ⊚ — это «special-defined operation»
поэтому пусть пока его не будет в раскладке (если не понадобится)
⊜ Равно в круге  U229C (логично на 6 уровне =)  → на ! (над = и ≠, на 6 уровне =)
⊝ Дефис в круге U229D
⊞ Плюс в квадрате U229E ⊟ Минус в квадрате U229F ⊠ Произведение в квадрате U22A0 ⊡ Оператор точка в квадрате U22A1

△△△
Треугольники
◁ U25C1 ▷ U25B7 ← ни на 6 уровне «», ни на 7 уровне ЖЭ не выглядят органично; ни разу не понадобились
⊲ ⋪ ⊴  U22B2,		U22EA,		U22B4
⊳ ⋫ ⊵  U22B3,		U22EB,		U22B5
U25B3 △ Белый треугольник с вершиной вверх U+25B3 ← в таблице unicode никакой семантики у этого значка нет
△ как симметрическая разность множеств — на М
но в словаре Зализняка «За этим знаком приводятся морфологически нерегулярные формы (т. е. те, которые отклоняются от общих правил склонения и спряжения, содержащихся в разделах о значении индексов, стр. 25 и 77)»

•‣➤
Маркеры списка
‣ наиболее сомнительный

⟡⟡⟡
U27E1 ⟡ Незакрашенный ромб с вогнутыми сторонами U+27E1 ← аналогично, в unicode никакой конкретной семантики нет
мною используется в смысле «пример»
— логическое «никогда», поэтому долго был над ∅ — но сейчас там мешает
— у того же Зализняка «За этим знаком приводятся словосочетания (чаще всего фразеологизмы), в которых какая-либо форма заглавного слова отклоняется от своего нормального вида (или представлена только в одном виде, тогда как в общем случае возможны варианты).
⟨...⟩ За знаком ⟡ приводятся также устойчивые сочетания, в которых сохранились ныне отсутствующие в парадигме формы»
— также встречается как маркер списка и просто как «обрати внимание»
↑↑↑ так что не только «никогда» и не только логическое  
→  на ;\  
→  на Б (бубновая масть; рядом с ≺≻), ибо над ≈ всё-таки должно быть ≉ или ничего 
(формы в круге нет ни для ;, ни для \, ни для ≈; перечёркнутой \ тоже нет; а разность множеств ∖ если есть в раскладке, то должна быть на 5 уровне; а перечёркнутое ≈ есть)
Upd. Есть форма в круге для \ — ⦸! Но ещё менее вероятно понадобится, чем ≉
Upd2. Возможно, ⟡ далее перейдёт на 6 уровень другой буквы (на Б сомнительно) — но именно буквы, аналогично ❌✓ (возможно, на зZ — рядом с ШЩ, чтобы ❌✓⟡ стояли компактно),
На 6 уровне линии Backspace математические бинарные операторы (кроме ⋯, которое тоже сомнительно), и все как-то связаны с 1-2 уровнями этой или соседней клавиши (включая ⋯).
Upd3. → на «приют изгнанников» — 5 уровень линии Esc, ибо на Б — не вспоминается, а на з — вспоминается звёздочка с 5-6-8 концами
Upd4. А ⦸ → на 6 уровень \Лат (временно)


«»„“‘’
Обязательно нужны кавычки:
— русско-французские «ёлочки» guillemotleft/guillemotright;
— русско-немецкие „лапки“ (внутренние) doublelowquotemark/leftdoublequotemark (в соответствии с английской типографикой — РУССКАЯ ПРАВАЯ лапка описана как ЛЕВАЯ);
(есть на 1-2 уровнях линии Esc и на 3-4 уровнях ШЩ);

нет/нестабильно входящие кавычки:
— американские “лапки” (английские внутренние) leftdoublequotemark/rightdoublequotemark;
— английские одинарные ‘марровские’ (американские внутренние) U2018/U2019 leftsinglequotemark/rightsinglequotemark.
Сделать заплатку с англокавычками “” на 5 уровне и ‘’ на 6? (-)
https://yalov.github.io/yarbur/quotation-mark
‘’ на 5 (закрывающая’, по мнению консорциума Unicode, — более правильный апостроф, чем апостроф-букваʼ), “” на 6;
пара „” doublelowquotemark/rightdoublequotemark, входившая ранее в раскладку — польские кавычки.

`°′″‴⁗
Есть прямые штрихи ′″‴⁗;
нет обратных (не понадобились):
‵ Обратный штрих U+2035
‶ Обратный двойной штрих U+2036
‷ Обратный тройной штрих U+2037
четверного обратного не нашлось; да и четверной прямой не такой, как три первых.
  
Есть grave — ` Апостроф U+0060 (Grave Accent).
Отображается `‵ немного отлично от обратного штриха: grave→`‵←U2035. Но почти так же.

Про апостроф и кавычки: https://yalov.github.io/yarbur/quotation-mark
На 'Ъ фигурный апостроф U02BC (а не rightsinglequotemark) на 3 уровне (на 4 — ASCII-апостроф)


∖∖∖
Разность множеств U2216 ∖: должна бы быть на 5 уровне, но отображается либо как backslash \, либо некрасиво
не хочется пока убирать совсем → на 8 уровень ;\ и \&  — или только чего-то одного?..
⇒ если ∖ должно быть на 5 уровне, но не может быть на 5 уровне, да ещё и ни разу не понадобилось — лучше вообще убрать и с ;, и с \
Upd Временно над \Лат; как раз на 5 уровне

⋮⋮⋮
Нет вертикального троеточия: 
U205D ⁝ Тройное двоеточие. Основная пунктуация.
U22EE ⋮ (Математические операторы) — отображается, но означает вертикальный пропуск в матрице, а не делимость
U2AF6 ⫶ (Дополнительные математические операторы) «кратно, делится на» — но не отображается ни в шрифтах Liberation/Deja Vu, ни в случайно натыканных
Если вертикальное троеточие ↑↑↑ всё же появится — логично на 6 уровне над двоеточием :[5%.
→ временно есть над :

➛ U279B красивая стрелка; есть только вправо

~~~
Очень хочется ставить ∾ вместо ~ как «эквивалентно», но это неправильно: inverted lazy s operator indicating logical negation (дублирует ¬)
∼ Оператор тильда U+223C выглядит как обычная ~ «Тильда примерно» U+007E
∿ Синусоидальная волна U+223F — это вообще не то
в unicode нет красивой тильды, как \sim в LaTeX

≍≍≍
(-)Нет пары: U224D ≍ Эквивалентно и U226D ≭ Не эквивалентно  ⇒  
    key <AE09> {[ question,		parenleft,		9,	bar,			U224D,		U226D,		ninesubscript,	ninesuperior	]}; // ?( 9|  ≍≭ ₉⁹
Upd. Ни одной группе ≍ не понравилось ⇒ убираем ≍≭ вовсе. ⇒ на этом месте ⊆⊈


≺≺≺≻≻≻
На 3 уровне ЮЁ ≤≥ с равенством (американское начертание), на 5 уровне ≺≻ птички ⇒ на 4 и 6 перечёркивание 3 и 5 (Математические операторы); 
на 7 помесь 3 и 5 ≾≿ (Математические операторы), но перечёркнутых ≾≿ для 8 уровня нет в этом разделе unicode (и вообще не нашлось)
русское начертание ⩽⩾ из раздела Дополнительные математические операторы — на линии Backspace.

notapproxeq — это ≇, а не ≉, хотя approxeq ≈




© U00A9		Copyright Sign[1]   	Unicode Version:	1.1 (June 1993)[2]
🄯 U1F12F	Copyleft Symbol[1]	Unicode Version:	11.0 (June 2018)[2] 🄯 
↑↑↑ копилефт не отображается ни здесь, ни в браузере, а «непарный» копирайт включать не хочется
Если вдруг — пару копилефт-копирайт можно разместить на 6 уровне JC


Комбинируемые символы:
— ударе́ние-на-си́мволе-пе́ред U0301 (не acute, тот не комбинируется) — иногда отображается над символом *после* ↓↓↓;
— предыдущий-с-тильдой x̃ — вообще не так отображается.

Комбинируемые ударение а́ и производная по t ẋ некорректно отображаются (могут переползти на следующий символ)
— для небуквенных символов (в том числе пробела и цифр): 4́́=комб.ударение, 5̇=комб. производная по t (но это ладно);
— для высоких букв: l̇, ℓ̇̇, А́стра, ё́лка (!!!).
Пусть пока будут, но — сомнительно и, вероятно, будут удалены из раскладки.

А из строки комбинируемый символ удалить можно только вместе с несколькими соседними! Особенно если он уползает с предыдущего на следующий.

Комбинируемое перечёркивание: горизонтальное x̶ съезжает почти везде, но во многих шрифтах выглядит прилично;
диагональные  U+0337 и  U+0338  тоже съезжают, но из-за их формы выглядит это хуже.

    //key <AE08> {[ underscore,	bar,			8,	asterisk,		U2423,		VoidSymbol,	eightsubscript,	eightsuperior	]}; // _| 8*  ␣  ₈⁸   

ꜝ  ﹡
❓❔
？


    key <TLDE> {[ quotedbl,		section,		0,	grave,			seconds,	U2034,		zerosubscript,	zerosuperior	]}; // "§ 0`  ″‴ ₀⁰

    key <AE06> {[ comma,		figdash,		6,	asciicircum,	minutes,	endash,		sixsubscript,	sixsuperior		]}; // ,‒ 6^  ′– ₆⁶   
    key <AE10> {[ emdash,		bracketright,	infinity,parenright,parenright,	U207E,		U23E8,			U0336			]}; // —] ∞)  )⁾ ⏨a̶  8 — комб.зач

    // ??? реализовать режим F1‒F12 как альт-РУС? Чтобы сам сбрасывался при переключении раскладки?
    // Если F1‒F12+латиница как альт-РУС, а A‒F как альт-ЛАТ — не будет ли сложностей с F+русские буквы? 
    // Если F1‒F12+русский как альт-РУС, то, скорее всего, проблемы будут... Имеет смысл, если отказаться от A-F: альт-РУС и альт-ЛАТ оба F1‒F12
    
     //key <FK11> {[equal,	approxeq,		U23E8,			Overlay2_Enable,	U2261,				U2262,		F11,F11	],	overlay2=<I221>	}; // = ≈ ⏨ РежF  ≡ ≢  F11
    //key <FK12> {[backslash,	notequal,	U2099,			U207F,				U0336,				U229C,		F12,F12	],	overlay2=<I222>	}; // \ ≠ ₙ ⁿ     ̶ ⊜  F12
   //key <AE08> {[ underscore,	bar,			8,	U2423,			VoidSymbol,	VoidSymbol,	eightsubscript,	eightsuperior	]}; // _| 8␣     ₈⁸   

    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U2216,				U2262,		F12,F12	],	overlay2=<I222>	}; // \ ≠ ₙ ⁿ     ∖ ≢  F12

    
    
    
// Включатель латиницы справа: Menu = включатель первой раскладки (ЛАТ), включатель альтернативной латиницы на 2; оригинальный Menu на 3 уровне
partial modifier_keys xkb_symbols "common_mod_menu_lat" {
     //key <MENU> {	type="FOUR_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu,	Overlay1_Enable] };
    key <MENU> {type="FOUR_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu, VoidSymbol],        
        actions[Group1]= [ LockGroup(group=1), LockGroup(group=3) ],
        actions[Group2]= [ LockGroup(group=1), LockGroup(group=3) ],
        actions[Group3]= [ LockGroup(group=1), LockGroup(group=3) ],
        actions[Group4]= [ LockGroup(group=1), LockGroup(group=3) ]            
    };    
};    
    

// РУС + hex-цифры + цифровой блок ОРИОКС
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "altrus" {
    include "illinc(altlat)"  
    include "illinc(rus_alph_patch)" 
    
    //include "illinc(lat)" // Пусть по Shift+РУС включается альтернативный ЛАТ: ЛАТ + hex-цифры + цифровой блок ОРИОКС
    //include "illinc(common_88hex_testing_patch)" 
    
};
    
    //key <TLDE> {[ quotedbl,		section,		0,	grave,			seconds,	U2057,		zerosubscript,	zerosuperior	]}; // "§ 0`  ″⁗ ₀⁰
    //key <AE06> {[ comma,		figdash,		6,	asciicircum,	minutes,	U2034,		sixsubscript,	sixsuperior		]}; // ,‒ 6^  ′‴ ₆⁶   
    //key <AE07> {[ period,		ampersand,		7,	ampersand,		U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .& 7&  ⋅⊙ ₇⁷
   

partial function_keys xkb_symbols "common_f1f12_original_patch" {
    key <FK01> { type="CTRL+ALT", 	symbols[Group1]= [ F1, F1,  	zerosubscript,	zerosuperior, 		XF86_Switch_VT_1 	] }; 
    key <FK02> { type="CTRL+ALT", 	symbols[Group1]= [ F2, F2,		onesubscript,	onesuperior,		XF86_Switch_VT_2 	] }; 
    key <FK03> { type="CTRL+ALT", 	symbols[Group1]= [ F3, F3,		twosubscript,	twosuperior,		XF86_Switch_VT_3 	] }; 
    key <FK04> { type="CTRL+ALT", 	symbols[Group1]= [ F4, F4,		threesubscript,	threesuperior,		XF86_Switch_VT_4 	] }; 
    key <FK05> { type="CTRL+ALT", 	symbols[Group1]= [ F5, F5,		foursubscript,	foursuperior,		XF86_Switch_VT_5 	] }; 
    key <FK06> { type="CTRL+ALT", 	symbols[Group1]= [ F6, F6,		fivesubscript,	fivesuperior,		XF86_Switch_VT_6 	] }; 
    key <FK07> { type="CTRL+ALT", 	symbols[Group1]= [ F7, F7,		sixsubscript,	sixsuperior,		XF86_Switch_VT_7 	] }; 
    key <FK08> { type="CTRL+ALT", 	symbols[Group1]= [ F8, F8,		sevensubscript,	sevensuperior,		VoidSymbol 			] };     
    key <FK09> { type="CTRL+ALT", 	symbols[Group1]= [ F9, F9, 		eightsubscript,	eightsuperior,		VoidSymbol 			] }; 
    key <FK10> { type="CTRL+ALT", 	symbols[Group1]= [ F10, F10,	ninesubscript,	ninesuperior,		VoidSymbol 			] }; 
    key <FK11> { type="CTRL+ALT", 	symbols[Group1]= [ F11, F11,	U208C,			U207C,				VoidSymbol 			] }; 
    key <FK12> { type="CTRL+ALT", 	symbols[Group1]= [ F12, F12, 	U2099,			U207F,				VoidSymbol 			] }; 
};

    


// ************************************************************************************************************************************************************
// Цифровой блок — по умолчанию цифры ОРИОКС: с точкой и «н» 

// Базовый вариант: цифры с запятой и минусом
partial keypad_keys xkb_symbols "common_keypad_digits" {   
    
    key.type="EIGHT_LEVEL"; 
        
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    replace key <NMLK> {[guillemotleft, doublelowquotemark, U2620,    U25C1,	VoidSymbol,	VoidSymbol,	VoidSymbol,VoidSymbol]}; // «„ ☠◁   
    replace key <KPDV> {[guillemotright,leftdoublequotemark,KP_Divide,U25B7,	VoidSymbol,	VoidSymbol,	VoidSymbol,VoidSymbol]}; // »“ /▷  
    modifier_map Mod2   { Num_Lock };

    // 2 уровень — нижний индекс (нужен чаще), 5 — верхний, 6 — форма в круге, сложнонабираемый 4 для цифр — (10+x) в круге
    replace key <KPMU> {[ figdash,	U2C7C,	KP_Multiply,VoidSymbol,	U02B2,	U24D9,			VoidSymbol,	VoidSymbol	]}; // ‒ⱼ    *     ʲⓙ    1=цифровое тире    
    replace key <KPSU> {[ U2212,	U2099,	KP_Subtract,VoidSymbol,	U207F,	U24DD,			U1D3A,		VoidSymbol	]}; // −ₙ    -     ⁿⓝ ᴺ  1=минус ширины плюса
    replace key <KPAD> {[ Tab,ISO_Left_Tab,	KP_Add,		VoidSymbol,	VoidSymbol,VoidSymbol,	VoidSymbol,	VoidSymbol	]}; // Tab   +  
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    replace key  <KP7> {[ 7,sevensubscript,	KP_Home, 	U2470,		sevensuperior,	U2466,	VoidSymbol,VoidSymbol	]}; // 7₇  Home⑰   ⁷⑦ 
    replace key  <KP8> {[ 8,eightsubscript,	KP_Up, 		U2471,		eightsuperior,	U2467,	VoidSymbol,VoidSymbol	]}; // 8₈  ↑   ⑱   ⁸⑧ 
    replace key  <KP9> {[ 9,ninesubscript,	KP_Prior, 	U2472,		ninesuperior,	U2468,	VoidSymbol,VoidSymbol	]}; // 9₉  PgUp⑲   ⁹⑨ 

    replace key  <KP4> {[ 4,foursubscript,	KP_Left, 	U246D,		foursuperior,	U2463,	VoidSymbol,VoidSymbol	]}; // 4₄  ←   ⑭   ⁴④ 
    replace key  <KP5> {[ 5,fivesubscript,	KP_Begin, 	U246E,		fivesuperior,	U2464,	VoidSymbol,VoidSymbol	]}; // 5₅  ??? ⑮   ⁵⑤
    replace key  <KP6> {[ 6,sixsubscript,	KP_Right, 	U246F,		sixsuperior,	U2465,	VoidSymbol,VoidSymbol	]}; // 6₆  →   ⑯   ⁶⑥

    replace key  <KP1> {[ 1,onesubscript,	KP_End, 	U246A,		onesuperior,	U2460,	VoidSymbol,VoidSymbol	]}; // 1₁  End ⑪   ¹①
    replace key  <KP2> {[ 2,twosubscript,	KP_Down, 	U246B,		twosuperior,	U2461,	VoidSymbol,VoidSymbol	]}; // 2₂  ↓   ⑫   ²②
    replace key  <KP3> {[ 3,threesubscript,	KP_Next, 	U246C,		threesuperior,	U2462,	VoidSymbol,VoidSymbol	]}; // 3₃  PgDn⑬   ³③

    replace key  <KP0> {[ 0,zerosubscript,	KP_Insert,	U2469,		zerosuperior,	U24EA,	VoidSymbol,VoidSymbol	]}; // 0₀  Ins ⑩   ⁰⓪
    replace key <KPDL> {[ comma, U1D62,		KP_Delete,	VoidSymbol,	U2071,			U24D8,	VoidSymbol,VoidSymbol	]}; // ,ᵢ  Del     ⁱⓘ
};

// Вариант ОРИОКС: цифры с точкой и литерой «н» 
partial keypad_keys xkb_symbols "common_keypad_digits_orioks" {   
    include "illinc(common_keypad_digits)"   
    key <KPSU> {[ Cyrillic_en		]}; // н 
    key <KPDL> {[ period			]}; // .        
};    
    
    replace key <KPMU> {type="FOUR_LEVEL",				[ figdash,	VoidSymbol,		VoidSymbol,	VoidSymbol				]}; // ‒            1=цифровое тире    
    
    
    
    
    replace key <FK01> {type="EIGHT_LEVEL",	[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,		F1, F1	]}; // 0 ⩽ ₀ ⁰  vt1⓪  F1
    replace key <FK02> {type="EIGHT_LEVEL",	[1, 	U2A7E,			onesubscript,	onesuperior,		XF86_Switch_VT_2,	U2460,		F2, F2	]}; // 1 ⩾ ₁ ¹  vt2①  F2
    replace key <FK03> {type="EIGHT_LEVEL",	[2, 	U27E8,			twosubscript,	twosuperior,		XF86_Switch_VT_3,	U2461,		F3, F3	]}; // 2 ⟨ ₂ ²  vt3②  F3
    replace key <FK04> {type="EIGHT_LEVEL",	[3, 	U27E9,			threesubscript,	threesuperior,		XF86_Switch_VT_4,	U2462,		F4, F4	]}; // 3 ⟩ ₃ ³  vt4③  F4                                                                      
    replace key <FK05> {type="EIGHT_LEVEL",	[4, 	U230A,			foursubscript,	foursuperior,		XF86_Switch_VT_5,	U2463,		F5, F5	]}; // 4 ⌊ ₄ ⁴  vt5④  F5
    replace key <FK06> {type="EIGHT_LEVEL",	[5, 	U230B,			fivesubscript,	fivesuperior,		XF86_Switch_VT_6,	U2464,		F6, F6	]}; // 5 ⌋ ₅ ⁵  vt6⑤  F6
    replace key <FK07> {type="EIGHT_LEVEL",	[6, 	U2308,			sixsubscript,	sixsuperior,		XF86_Switch_VT_7,	U2465,		F7, F7	]}; // 6 ⌈ ₆ ⁶  vt7⑥  F7  
    replace key <FK08> {type="EIGHT_LEVEL",	[7, 	U2309,			sevensubscript,	sevensuperior,		U27E1,				U2466,		F8,	F8	]}; // 7 ⌉ ₇ ⁷   ⟡ ⑦  F8  
    replace key <FK09> {type="EIGHT_LEVEL",	[8, 	plusminus,		eightsubscript,	eightsuperior,		U2295,				U2467,		F9,	F9	]}; // 8 ± ₈ ⁸   ⊕ ⑧  F9
    replace key <FK10> {type="EIGHT_LEVEL",	[9, 	U2213,			ninesubscript,	ninesuperior,		U2296,				U2468,		F10,F10	]}; // 9 ∓ ₉ ⁹   ⊖ ⑨  F10
    replace key <FK11> {type="EIGHT_LEVEL",	[equal,	approxeq,		U208C,			U207C,				U2261,				U229C,		F11,F11	]}; // = ≈ ₌ ⁼   ≡ ⊜  F11
    replace key <FK12> {type="EIGHT_LEVEL",	[backslash,	notequal,	U2099,			U207F,				U23E8,				U2262,		F12,F12	]}; // \ ≠ ₙ ⁿ   ≢ ⦸  F12

    
 
partial function_keys xkb_symbols "common_f1f12_original_patch" {
    replace key <FK01> { type="CTRL+ALT", 	symbols[Group1]= [ F1, F1,  	zerosubscript,	zerosuperior, 		XF86_Switch_VT_1 	] }; 
    replace key <FK02> { type="CTRL+ALT", 	symbols[Group1]= [ F2, F2,		onesubscript,	onesuperior,		XF86_Switch_VT_2 	] }; 
    replace key <FK03> { type="CTRL+ALT", 	symbols[Group1]= [ F3, F3,		twosubscript,	twosuperior,		XF86_Switch_VT_3 	] }; 
    replace key <FK04> { type="CTRL+ALT", 	symbols[Group1]= [ F4, F4,		threesubscript,	threesuperior,		XF86_Switch_VT_4 	] }; 
    replace key <FK05> { type="CTRL+ALT", 	symbols[Group1]= [ F5, F5,		foursubscript,	foursuperior,		XF86_Switch_VT_5 	] }; 
    replace key <FK06> { type="CTRL+ALT", 	symbols[Group1]= [ F6, F6,		fivesubscript,	fivesuperior,		XF86_Switch_VT_6 	] }; 
    replace key <FK07> { type="CTRL+ALT", 	symbols[Group1]= [ F7, F7,		sixsubscript,	sixsuperior,		XF86_Switch_VT_7 	] }; 
    replace key <FK08> { type="CTRL+ALT", 	symbols[Group1]= [ F8, F8,		sevensubscript,	sevensuperior,		VoidSymbol 			] };     
    replace key <FK09> { type="CTRL+ALT", 	symbols[Group1]= [ F9, F9, 		eightsubscript,	eightsuperior,		VoidSymbol 			] }; 
    replace key <FK10> { type="CTRL+ALT", 	symbols[Group1]= [ F10, F10,	ninesubscript,	ninesuperior,		VoidSymbol 			] }; 
    replace key <FK11> { type="CTRL+ALT", 	symbols[Group1]= [ F11, F11,	U208C,			U207C,				VoidSymbol 			] }; 
    replace key <FK12> { type="CTRL+ALT", 	symbols[Group1]= [ F12, F12, 	U2099,			U207F,				VoidSymbol 			] }; 
};   
    
// ЛАТ + hex-цифры + цифровой блок ОРИОКС
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "altlat" { 
    include "illinc(lat)" 
    //include "srvr_ctrl(fkey2vt)"
    include "illinc(common_f1f12_original_patch)" 
};

5 уровень F8-F12 не работает в основном режиме, как ни извращайся
(если не менять EIGHT_LEVEL↔CTRL+ALT — всё работает)
    
    
     // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			U2034,		U2057,		zerosubscript,	zerosuperior	]}; // "§ 0`  ‴⁗ ₀⁰
    key <AE01> {[ plus,			numerosign,		1,	U23E8,			plusminus,	U2295,		onesubscript,	onesuperior		]}; // +№ 1⏨  ±⊕ ₁¹
    key <AE02> {[ minus,		U2212,			2,	U2011,			U2213,		U2296,		twosubscript,	twosuperior		]}; // -− 2‑  ∓⊖ ₂² минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		threesubscript,	threesuperior	]}; // /< 3#  ≮⊘ ₃³
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		foursubscript,	foursuperior	]}; // *> 4$  ≯⊛ ₄⁴
    key <AE05> {[ colon, 		percent,		5,	U205D,			U2030,		U2031,		fivesubscript,	fivesuperior	]}; // :% 5⁝  ‰‱ ₅⁵
    key <AE06> {[ comma,		figdash,		6,	asciicircum,	minutes,	seconds,	sixsubscript,	sixsuperior		]}; // ,‒ 6^  ′″ ₆⁶   
    key <AE07> {[ period,		ampersand,		7,	U22C5,			U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .& 7⋅  ⋅⊙ ₇⁷
    key <AE08> {[ underscore,	bar,			8,	U2423,			figspace,	U202F,		eightsubscript,	eightsuperior	]}; // _| 8␣     ₈⁸   
    key <AE09> {[ question,		bracketleft,	9,	parenleft,		bracketleft,U207D,		ninesubscript,	ninesuperior	]}; // ?[ 9(  [⁽ ₉⁹
    key <AE10> {[ emdash,		bracketright,	endash,	parenright,	parenright,	U207E,		U23E8,			U0336			]}; // —] –)  )⁾ ⏨a̶  8 — комб.зач
    key <AE11> {[ exclam,	guillemotleft,	U26A0,doublelowquotemark,leftsinglequotemark,U207B,U208B,leftdoublequotemark	]}; // !« ⚠„  ‘⁻ ₋“
    key	<AE12> {[ semicolon,guillemotright,equal,leftdoublequotemark,rightsinglequotemark,U207A,U208A,rightdoublequotemark	]}; // ;» =“  ’⁺ ₊”
   
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		U2286,		U2288,			U208D,		U0300	]}; // ` ` χ Χ   ⊆ ⊈ ₍ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		U2287,		U2289,			U208E,		U0301	]}; // ' ' ʼ '   ⊇ ⊉ ₎ ́a   8 ́=комб.ударение (русское)
    
    
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			U2034,		U2035,		U2036,		U2037		]}; // "§ 0`  ‴‵ ‶‷ 
    key <AE01> {[ plus,			numerosign,		1,	U23E8,			plusminus,	U2295,		U208A,		U207A		]}; // +№ 1⏨  ±⊕ ₊⁺
    key <AE02> {[ minus,		U2212,			2,	U2011,			U2213,		U2296,		U208B,		U207B		]}; // -− 2‑  ∓⊖ ₋⁻ минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		onethird,	twothirds	]}; // /< 3#  ≮⊘ ⅓⅔
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		onequarter,threequarters]}; // *> 4$  ≯⊛ ¼¾
    key <AE05> {[ colon, 		percent,		5,	U205D,			U2030,		U2031,		U2026,		U22EF		]}; // :% 5⁝  ‰‱ …⋯
    key <AE06> {[ comma,		figdash,		6,	asciicircum,	minutes,	seconds,	U2034,		U2057		]}; // ,‒ 6^  ′″ ‴⁗  
    key <AE07> {[ period,		ampersand,		7,	U22C5,			U22C5,		U2299,	enfilledcircbullet,U02D9	]}; // .& 7⋅  ⋅⊙ •˙
    key <AE08> {[ underscore,	bar,			8,	U2423,			figspace,	U202F,		oneeighth,seveneighths	]}; // _| 8␣     ⅛⅞ 5=цфр.шпация, 6=тонк.неразр.
    key <AE09> {[ question,		bracketleft,	9,	parenleft,		bracketleft,U207D,		U208D,		U207D		]}; // ?[ 9(  [⁽ ₍⁽
    key <AE10> {[ emdash,		bracketright,	endash,	parenright,	parenright,	U207E,		U208E,		U207E		]}; // —] –)  )⁾ ₎⁾  
    key <AE11> {[ exclam,	guillemotleft,	U26A0,doublelowquotemark,leftsinglequotemark,U207B,	leftdoublequotemark,	UA71D	]}; // !« ⚠„  ‘⁻ “ꜝ
    key	<AE12> {[ semicolon,guillemotright,U23E8,leftdoublequotemark,rightsinglequotemark,U207A,rightdoublequotemark,VoidSymbol	]}; // ;» ⏨“  ’⁺ ”
    
    // верхняя линия: F1-F12 **************************************************
    
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,		F1, F1	]}; // 0 ⩽ ₀ ⁰  vt1⓪  F1
    key <FK02> {[1, 	U2A7E,			onesubscript,	onesuperior,		XF86_Switch_VT_2,	U2460,		F2, F2	]}; // 1 ⩾ ₁ ¹  vt2①  F2
    key <FK03> {[2, 	U27E8,			twosubscript,	twosuperior,		XF86_Switch_VT_3,	U2461,		F3, F3	]}; // 2 ⟨ ₂ ²  vt3②  F3
    key <FK04> {[3, 	U27E9,			threesubscript,	threesuperior,		XF86_Switch_VT_4,	U2462,		F4, F4	]}; // 3 ⟩ ₃ ³  vt4③  F4                                                                      
    key <FK05> {[4, 	U230A,			foursubscript,	foursuperior,		XF86_Switch_VT_5,	U2463,		F5, F5	]}; // 4 ⌊ ₄ ⁴  vt5④  F5
    key <FK06> {[5, 	U230B,			fivesubscript,	fivesuperior,		XF86_Switch_VT_6,	U2464,		F6, F6	]}; // 5 ⌋ ₅ ⁵  vt6⑤  F6
    key <FK07> {[6, 	U2308,			sixsubscript,	sixsuperior,		XF86_Switch_VT_7,	U2465,		F7, F7	]}; // 6 ⌈ ₆ ⁶  vt7⑥  F7  
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U27E1,				U2466,		F8,	F8	]}; // 7 ⌉ ₇ ⁷   ⟡ ⑦  F8  
    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U2295,				U2467,		F9,	F9	]}; // 8 ± ₈ ⁸   ⊕ ⑧  F9
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2296,				U2468,		F10,F10	]}; // 9 ∓ ₉ ⁹   ⊖ ⑨  F10
    key <FK11> {[equal,	approxeq,		U208C,			U207C,				U2261,				U229C,		F11,F11	]}; // = ≈ ₌ ⁼   ≡ ⊜  F11
    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U23E8,				U2262,		F12,F12	]}; // \ ≠ ₙ ⁿ   ≢ ⦸  F12
    
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			U2034,		U2057,		U2035,		U2036		]}; // "§ 0`  ‴⁗ ‵‶ 
    key <AE01> {[ plus,			numerosign,		1,	U23E8,			plusminus,	U2295,		U208A,		U208A		]}; // +№ 1⏨  ±⊕ ₊₊
    key <AE02> {[ minus,		U2212,			2,	U2011,			U2213,		U2296,		onehalf,	U208B		]}; // -− 2‑  ∓⊖ ½₋ минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		onethird,	twothirds	]}; // /< 3#  ≮⊘ ⅓⅔
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		onequarter,threequarters]}; // *> 4$  ≯⊛ ¼¾
    key <AE05> {[ colon, 		percent,		5,	U205D,			U2030,		U2031,		onefifth,	U2026		]}; // :% 5⁝  ‰‱ ⅕…
    key <AE06> {[ comma,		figdash,		6,	asciicircum,	minutes,	seconds,	onesixth,	fivesixths	]}; // ,‒ 6^  ′″ ⅙⅚  
    key <AE07> {[ period,		ampersand,		7,	U2219,			U22C5,		U2299,		U2150,		U02D9		]}; // .& 7⋅  ⋅⊙ ⅐˙
    key <AE08> {[ underscore,	bar,			8,	U2423,			figspace,	U202F,		oneeighth,seveneighths	]}; // _| 8␣     ⅛⅞ 5=цфр.шпация, 6=тонк.неразр.
    key <AE09> {[ question,		bracketleft,	9,	parenleft,		bracketleft,U207D,		U208D,		U207D		]}; // ?[ 9(  [⁽ ₍⁽
    key <AE10> {[ emdash,		bracketright,	endash,	parenright,	parenright,	U207E,		U208E,		U207E		]}; // —] –)  )⁾ ₎⁾  
    key <AE11> {[ exclam,	guillemotleft,	U26A0,doublelowquotemark,leftsinglequotemark,U207B,	leftdoublequotemark,UA71D	]}; // !« ⚠„  ‘⁻ “ꜝ
    key	<AE12> {[ semicolon,guillemotright,U23E8,leftdoublequotemark,rightsinglequotemark,U207A,rightdoublequotemark,U208E	]}; // ;» ⏨“  ’⁺ ”
    
    //key <FK12> {[backslash,	notequal,	U2099,			U207F,				U2262,				U29B8,		F12,F12	]}; // \ ≠ ₙ ⁿ   ≢ ⦸  F12
    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U23E8,				U2262,	U2216,		U2249		]}; // \ ≠ ₙ ⁿ   ⏨ ≢  ∖≉
    
    key <FK11> {[equal,	approxeq,		U23E8,			U207C,				U2261,				U229C,	U208C,		U207C		]}; // = ≈ ⏨ ⁼   ≡ ⊜  ₌⁼
    
    
    key <LSGT> {[ numbersign,U22C5,		U1D62,		U2071,			U2766,U2767,		U20DD,		U20E0	]};	// # ⋅ ᵢ ⁱ     ❦ ❧            a⃝       a⃠
    
    
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		U226A,		U22D8		]}; // /< 3#  ≮⊘ ≪⋘
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		U226B,		U22D9		]}; // *> 4$  ≯⊛ ≫⋙
    key	<AE12> {[ semicolon,guillemotright,U23E8,leftdoublequotemark,rightsinglequotemark,U207A,rightdoublequotemark,U2152	]}; // ;» ⏨“  ’⁺ ”⅒
    
     key <AE11> {[ exclam,	guillemotleft,	U26A0,doublelowquotemark,leftsinglequotemark,U207B,	leftdoublequotemark,UA71D	]}; // !« ⚠„  ‘⁻ “ꜝ
    key	<AE12> {[ semicolon,guillemotright,equal,leftdoublequotemark,rightsinglequotemark,U207A,rightdoublequotemark,U225F	]}; // ;» =“  ’⁺ ”≟
   
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,	partdifferential,	U2300,		U1D48 	]}; // d D δ Δ   ∆ ∂ ⌀ ᵈ   5 ∆=приращение, 7 ⌀=диаметр
     key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,	U229A, enfilledcircbullet,	U1D47	]};	// b B β Β   ∞ ⊚ • ᵇ
   
    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U2262,				U29B8,	U2216,		U2249		]}; // \ ≠ ₙ ⁿ    ≢ ⦸  ∖ ≉
    //replace key <KPDL> {type="FOUR_LEVEL_MIXED_KEYPAD",	[ comma,	period,				U216A,	U216B	]}; // ,.  
    
    replace key  <KP0> {[ 0,zerosubscript,	U25CB,	U2469,	zerosuperior,	U24EA,		U2473,		VoidSymbol	]}; // 0 ₀ ○ ⑩   ⁰ ⓪ ⑳ (20)

// Для 104-клавишной (ANSI с цифровым блоком): # на первом уровне (вместо отсутствующей LSGT)
partial keypad_keys xkb_symbols "common_keypad_104numbersign_patch" {   
    //key  <KP0>  {type="EIGHT_LEVEL",	[ numbersign ]};   
    key  <KPMU>  {type="EIGHT_LEVEL",	[ numbersign ]};   
};
    // Заново реализуем Num_Lock средствами actions на 3-4 уровнях разделителя (,↔.)
    replace key <KPDL> 	{type="FOUR_LEVEL_MIXED_KEYPAD",	[ comma,	period, 	VoidSymbol,						VoidSymbol					],
        							actions[Group1]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group2]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group3]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group4]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ]    
    };     
    modifier_map Mod2   { Num_Lock };      
   
    key.type="EIGHT_LEVEL"; 
    // 2 уровень — русские „лапки“, 6 уровень — американские “double quotes” — U+код для укорочения строки
    replace key <NMLK> {[guillemotleft, U201E,	U2169,U2798,	leftsinglequotemark,U201C,	U21C7,U2182	]}; // «„  Ⅹ➘   ‘“  ⇇ ↂ=10к        
    replace key <KPDV> {[guillemotright,U201C,	U216A,U2799,	rightsinglequotemark,U201D,	U21C4,U2187	]}; // »“  Ⅺ➙   ’”  ⇄ ↇ=50к
    replace key <KPMU> {[ figdash,		U1D62,	U216B,U279A,	U2071,				U24D8,	U21C9,U2188	]}; // ‒ᵢ  Ⅻ➚   ⁱⓘ  ⇉ ↈ=100к  1‒=цфр.тире

    replace key <KPSU> {type="FOUR_LEVEL_MIXED_KEYPAD",	[ U2212,	Cyrillic_en,		U208B,	U207B	]}; // −н  ₋⁻   1−=минус ширины плюса
    replace key <KPAD> {type="FOUR_LEVEL",				[ Tab,		ISO_Left_Tab,		U208A,	U207A	]}; // Tab ₊⁺ 
    replace key <KPEN> {type="ONE_LEVEL",				[ KP_Enter 	]};  
 // Цифровой блок: 1) по умолчанию цифры + красивый минус + разделитель-запятая; 2) в режиме NumLock цифры + «н» + разделитель-точка (режим ОРИОКС)


    // Искореняем Num_Lock на одноимённой клавише
    replace key <I227>	{type="ONE_LEVEL",	[ Num_Lock 		]	}; 	// inet(evdev): key <I227> {[ XF86Finance]};
    // ↑ старый NumLock после этого не переключает режимы; символ Num_Lock на нём не имеет никакого действия   
        
    // Заново реализуем Num_Lock средствами actions на 2-4 уровнях
//     replace key <NMLK> 	{type="FOUR_LEVEL",	[ guillemotleft, doublelowquotemark, 		VoidSymbol,	VoidSymbol	],
//         actions[Group1]= [ NoAction(), NoAction(),  LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
//         actions[Group2]= [ NoAction(), NoAction(),  LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
//         actions[Group3]= [ NoAction(), NoAction(),  LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
//         actions[Group4]= [ NoAction(), NoAction(),  LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ]    
//     }; 
    // ↑ символ 1 уровня НЕ включает режим скобок, действия 2-4 — циклически переключают режимы (как оригинальный NumLock)
    // Заново реализуем Num_Lock средствами actions на 3-4 уровнях разделителя (,↔.)
    replace key <KPDL> 	{type="FOUR_LEVEL_MIXED_KEYPAD",	[ comma,	period, 	VoidSymbol,						VoidSymbol					],
        							actions[Group1]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group2]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group3]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ],
        							actions[Group4]= [	NoAction(), NoAction(),		LockMods(modifiers=NumLock),	LockMods(modifiers=NumLock) ]    
    };     
    modifier_map Mod2   { Num_Lock };      
 
    
    
    
    key <AE11> {[ exclam,	guillemotleft,	U26A0,	doublelowquotemark,			leftsinglequotemark,U207B,	leftdoublequotemark,UA71D	]}; // !« ⚠„   ‘⁻ “ꜝ
    key	<AE12> {[ semicolon,guillemotright,	U2243,	leftdoublequotemark,		rightsinglequotemark,U207A,	rightdoublequotemark,U225F	]}; // ;» ≃“   ’⁺ ”≟

    
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2296,				U2468,	U2151,		U2619		]}; // 9 ∓ ₉ ⁹    ⊖ ⑨  ⅑ ☙
    key <FK11> {[equal,	approxeq,		U23E8,			U207C,				U2261,				U229C,	U2152,		U208C		]}; // = ≈ ⏨ ⁼    ≡ ⊜  ⅒ ₌
    key <AE01> {[ plus,			numerosign,		1,	backslash,			plusminus,	U2295,		U208A,		U207A	]}; // +№ 1\   ±⊕ ₊⁺
    
    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U2213,				U2467,	oneeighth,seveneighths	]}; // 8 ± ₈ ⁸    ∓ ⑧  ⅛ ⅞
    key <FK10> {[9, 	approxeq,		ninesubscript,	ninesuperior,		U225F,				U2468,	U2151,		U2243		]}; // 9 ≈ ₉ ⁹    ≟ ⑨  ⅑ ≃
☙ U2619 
❦ ❧  U2766,U2767

        
    replace key <PRSC> {[parenleft, U22C5,		U1D62,	U2071,		VoidSymbol,VoidSymbol,		Print,		Sys_Req	]};	// ( ⋅ ᵢ ⁱ        PS SR
    replace key <SCLK> {[percent,	U23E8,		U2C7C,	U02B2,		VoidSymbol,VoidSymbol,		Scroll_Lock,Print	]};	// % ⏨ ⱼ ʲ        SL PS 
    replace key <PAUS> {[ampersand,	U02BC,		U2096,	U1D4F,		VoidSymbol,VoidSymbol,		Pause,		Break	]};	// & ʼ ₖ ᵏ        P  BR 

    key <AE06> {[ comma,		figdash,		6,	asciicircum,		minutes,	seconds,	U2034,		U2057	]}; // ,‒ 6^   ′″ ‴⁗  

    replace key  <KP0> {[ 0,zerosubscript,	U2741,	U2469,	zerosuperior,	U24EA,		U263A,		U273A		]}; // 0 ₀ ❁ ⑩   ⁰ ⓪ ☺ ✺ 
    replace key <KPDL> {[ comma, U1D62,		U2704,	U2473,	U2071,			U24D8,		U2639,		U2728		]}; // , ᵢ ✄ ⑳   ⁱ ⓘ ☹ ✨

    //key <SPCE> {[ space,asciitilde,		asciitilde,nobreakspace,	U2241,hairspace,	digitspace,	U0303	]}; //   ~ ~ nbsp  ≁ волосяная   figspace ̃x   8 ̃=комб.волна

    key <AE03> {[ slash,		less,			3,	numbersign,			U226E,		U2298,		U226A,		U22D8	]}; // /< 3#   ≮⊘ ≪⋘
    key <AE04> {[ asterisk,		greater,		4,	dollar,				U226F,		U229B,		U226B,		U22D9	]}; // *> 4$   ≯⊛ ≫⋙
    
    
    key <AE03> {[ slash,		less,			3,	numbersign,			U226E,		U2298,		U226A,		U2A7B	]}; // /< 3#   ≮⊘ ≪⩻  8 ⩻= <?
    key <AE04> {[ asterisk,		greater,		4,	dollar,				U226F,		U229B,		U226B,		U2A7C	]}; // *> 4$   ≯⊛ ≫⩼  8 ⩼= >?
    key <FK09> {[8, 	approxeq,		eightsubscript,	eightsuperior,		U225F,				U2467,	oneeighth,seveneighths	]}; // 8 ≈ ₈ ⁸    ≟ ⑧  ⅛ ⅞
    
    key <AE08> {[ underscore,	bar,			8,	U2423,				figspace,	U202F,		U27C2,	thinspace	]}; // _| 8␣      ⟂  5=цфр, 6=тонк.нрз

(-)Пустые места: 3‒4 уровни ШЩ, 5‒6 К и М.

(-)Перечёркивание:
=/≠, ≡/≢ — вообще на разных клавишах, 2 пары
<>/≮≯                 — 2/5, на 5 уровне,          2 пары
≤≥/≰≱                 — 3/4 — «правильное» +Shift, 1 пара
←→↔~/↚↛↮≁             — 3/5, на 5 уровне,          4 пары
↑↓/⤉⤈                — 3/7,              +Level5, 2 пары
⇐⇒⇔/⇍⇏⇎               — 4/6,        −Shift+Level5, 3 пары
∃⊃⊂⊆⊇∈∋≺≻/∄⊅⊄⊈⊉∉∌⊀⊁ т — 5/6 — «правильное» +Shift, 9 пар
(-)комбинируемые a⃝ / a⃠   — 7/8 — «правильное» +Shift, 1 пара
———————————————————————————————————————————————————————
(-)11 «правильных» пар (10 без комбинируемых рамок);
(-)«неправильных» 2+2+4+2+3 = 13 пар, из них: перечёркивание на 5 уровне 2+4 = 6 пар, прочих: 2 на разных клавишах, 2 и 3 разных.



    replace key <KPMU> {[ U22C5,	U2C7C,		U216B,U279A,	U02B2,	U24D8,			U21C9,	U2188	]}; // ⋅ⱼ  Ⅻ➚   ʲⓙ  ⇉ ↈ=100к  
    
    
    key <FK09> {[8, 	approxeq,		eightsubscript,	eightsuperior,		U2733,				U2467,	oneeighth,seveneighths	]}; // 8 ≈ ₈ ⁸    ✳ ⑧  ⅛ ⅞
    key <FK10> {[9, 	plusminus,		ninesubscript,	ninesuperior,		U2213,				U2468,	U2151,		U2214		]}; // 9 ± ₉ ⁹    ∓ ⑨  ⅑ ∔
    
    key <AE06> {[ comma,		figdash,		6,	asciicircum,		minutes,	seconds,				U2034,		U2057	]}; // ,‒ 6^   ′″ ‴⁗      
    
[) на 5 уровне:    
    key <AE10> {[ emdash,		bracketright,	endash,	parenright,		parenright,	U207E,					U208E,		U207E	]}; // —] –)   )⁾ ₎⁾      
    
     key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U2213,				U2467,	oneeighth,seveneighths	]}; // 8 ± ₈ ⁸    ∓ ⑧  ⅛ ⅞   
     key <FK10> {[9, 	figdash,		ninesubscript,	ninesuperior,		U2733,				U2468,	U2151,		U2738		]}; // 9 ‒ ₉ ⁹    ✳ ⑨  ⅑ ✸
    replace key <SCLK> {[percent,	U2213,			U2C7C,	U02B2,			U2C7C,	U02B2,				Scroll_Lock,Print		]};	// % ∓ ⱼ ʲ    ⱼ ʲ SL PS 
        
    replace key <PRSC> {[parenleft, U22C5,			U1D62,	U2071,			U1D62,	U2071,				Print,		Sys_Req		]};	// ( ⋅ ᵢ ⁱ    ᵢ ⁱ PS SR
    replace key <SCLK> {[percent,	figdash,		U2C7C,	U02B2,			U2C7C,	U02B2,				Scroll_Lock,Print		]};	// % ‒ ⱼ ʲ    ⱼ ʲ SL PS 
    replace key <PAUS> {[ampersand,	U02BC,			U2096,	U1D4F,			U2096,	U1D4F,				Pause,		Break		]};	// & ʼ ₖ ᵏ    ₖ ᵏ P  BR 

    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		approxeq,		U209A,		U1D56	]}; // p P π Π   ∏ ≈ ₚ ᵖ       
  
    key <AD08> {[	h,		H,	equal,			bracketleft,	U210D,		U274C,			U2095,		U02B0	]}; // h H = [   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	backslash,		bracketright,	emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V \ ]   ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)
  
  
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,	partdifferential,	U1D48 	]}; // d D δ Δ   ∆ ⏨ ∂ ᵈ   5 ∆=приращение 
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U23E8,			U2300,		U1D48 	]}; // d D δ Δ   ∆ ⏨ ⌀ ᵈ   5 ∆=приращение, 8 ⌀=диаметр 
  
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,	partdifferential,	U209A,		U1D56	]}; // p P π Π   ∏ ∂ ₚ ᵖ   
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,twosubscript, enfilledcircbullet,U1D47	]};	// b B β Β   ∞ ₂ • ᵇ

    

    
    
Фантазийный проект на случай полного отсутствия верхней линии (F1‒F12):

 "§ 0❀   ′″ ₀⁰  
 +№ 1❀   ±⊕ ₁¹
 -− 2‑   ∓⊖ ₂²
 /< 3⩽   ≮⊘ ₃³
 *> 4⩾   ≯⊛ ₄⁴
 :% 5⁝   ‰‱ ₅⁵
 ,= 6≈   ≠⊜ ₆⁶
 .& 7\   ⋅⊙ ₇⁷
 _| 8␣      ₈⁸ ← пробелы на 5‒6, как и в 88-клавишной
 ?( 9[   ❀⁽ ₉⁹
 —) –]   ‒⁾ ⏨❀
 !« „⟨   ‘⁻ “ꜝ
 ;» “⟩   ’⁺ ”❁

 hH ⌊⌈   ℍ❌ ₕʰ
 vV ⌋⌉   ∅✓ ᵥᵛ
 
Над «» угловые скобки ⟨⟩ на 4 уровне, над ШЩ округление: ⌊⌋ на 3 уровне, ⌈⌉ на 4.
Из более-менее употребительных нет пары ≡≢ ⇒ (?) на 5‒6 уровни M (?).

Скорее всего на такой клавиатуре не будет и LSGT (#).

Если левого нуля тоже нет:
    ноль — на место us-нуля: —) 0] ‒⁾ ₀⁰, а ⏨ куда-нибудь на 7 уровень, например, вместо диаметра на D;
    ′″ — куда-нибудь на 6 или 7‒8 уровень (≡≢ используются чаще, поэтому не на М);
    "§:
        (?) " на У вместо υ, § на И вместо ι (?)   или § над ", тоже на У, тогда \ можно и на 3 уровень ι (или ещё чем-то заместить ιΙ на И)
        или (?) " вместо ↔ на Я (↔↮ на 5/7: реже нужны, чем двойные) — а куда §? Вместо ␣ над |? (?) И υ удобнее ↔   | ⇒ вряд ли
        или вообще вернуть " на tw-место:  "> 4⩾   ≯§ ₄⁴, тогда:
            (?) *⊛ вместо υΥ на У [Умножение];  и ещё чем-то заместить ιΙ на И: (?) # (?)
            или +⊕ вместо υΥ на У (под ∪∨), *⊛ вместо ιΙ на И (под ∩∧), а # на 1 уровень под № (или даже, совсем как в tw, № на 1 уровень, # на второй)
                тогда ±∓ → вместо ‘’ на !; на удобные места;    ‘’ → вместо дубля ⊂⊃ на ХЪ.   
                Неудобные места ±⊕ и ∓ справа освободятся, туда можно штрихи ′″‴.
 №# 1§   ′‴ ₁¹
 -− 2‑   ″⊖ ₂²
 /< 3⩽   ≮⊘ ₃³
 "> 4⩾   ≯§ ₄⁴
 
 —) 0]   ‒⁾ ₀⁰
 !« „⟨   ±⁻ “ꜝ
 ;» “⟩   ∓⁺ ”❁
 
 uU +⊕   ∪∨ ᵤᵘ 
 iI *⊛   ∩∧ ᵢⁱ
 
 `` χΧ   ⊆⊈ ‘à 
 '' ʼ'   ⊇⊉ ’á 
 
В общем — такое делать только под конкретную клавиатуру.



в kate от пользователя не набирается ⁶, а вместо того открывается поиск; Firefox, kate от root ⁶,  kate от пользователя ⁶ (???)
    key <FK07> {[6, 	U2308,			sixsubscript,	sixsuperior,		XF86_Switch_VT_7,	U2465,	onesixth,	fivesixths	]}; // 6 ⌈ ₆ ⁶  vt7 ⑥  ⅙ ⅚  
    key <FK07> {[6, 	U2308,			sixsubscript,	U2076,				XF86_Switch_VT_7,	U2465,	onesixth,	fivesixths	]}; // 6 ⌈ ₆ ⁶  vt7 ⑥  ⅙ ⅚  
нормально и с sixsuperior, глюки — случайны и U2076 не лечатся


    key <TLDE> {[ quotedbl,		section,		0,	grave,				U2035,		U2036,					U2037,		U2728	]}; // "§ 0`   ‵‶ ‷✨  
    key <AE01> {[ plus,			numerosign,		1,	U2295,				plusminus,	U2295,					U208A,		U207A	]}; // +№ 1⊕   ±⊕ ₊⁺

Всё время порываюсь набрать § с Level3+Shift ⇒ пусть он и там будет,    и № симметрично продублировать

    key <AE03> {[ slash,		less,			3,	numbersign,			U226E,		U2298,					U226A,		U22D8	]}; // /< 3#   ≮⊘ ≪⋘
    key <AE04> {[ asterisk,		greater,		4,	dollar,				U226F,		U229B,					U226B,		U22D9	]}; // *> 4$   ≯⊛ ≫⋙

    key <AE06> {[ comma,		approxeq,		6,	asciicircum,		minutes,	seconds,				U2034,		U2057	]}; // ,≈ 6^   ′″ ‴⁗  


    key <AE06> {[ comma,		approxeq,		6,	U26A1,				minutes,	seconds,				U2034,		U2057	]}; // ,≈ 6⚡   ′″ ‴⁗  
    key <AE07> {[ period,		ampersand,		7,	U22C5,				U22C5,		U2299,			enfilledcircbullet,	U02D9	]}; // .& 7⋅   ⋅⊙ •˙
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U225F,	rightsinglequotemark,U207A,	rightdoublequotemark,U2741	]}; // ;» “≟   ’⁺ ”❁

    
    
    key <AE03> {[ slash,		less,			3,	U226A,				U226E,		U2298,				numbersign,		U22D8	]}; // /< 3≪   ≮⊘ #⋘
    key <AE04> {[ asterisk,		greater,		4,	U226B,				U226F,		U229B,					dollar,		U22D9	]}; // *> 4≫   ≯⊛ $⋙


    replace key <PRSC> {[parenleft, multiply,		U1D62,	U2071,			U1D62,	U2071,				Print,		Sys_Req		]};	// ( × ᵢ ⁱ    ᵢ ⁱ PS SR
    replace key <SCLK> {[percent,	U22C5,			U2C7C,	U02B2,			U2C7C,	U02B2,				Scroll_Lock,Print		]};	// % ⋅ ⱼ ʲ    ⱼ ʲ SL PS 
    replace key <PAUS> {[ampersand,	figdash,		U2096,	U1D4F,			U2096,	U1D4F,				Pause,		Break		]};	// & ‒ ₖ ᵏ    ₖ ᵏ P  BR 
 
 
 
    replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ parenright, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // ) Ins
        
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	section,			U2035,		U2036,					U2037,		U2728	]}; // "§ 0§   ‵‶ ‷✨  
    key <AE01> {[ plus,			numerosign,		1,	numerosign,			plusminus,	U2295,					U208A,		U207A	]}; // +№ 1№   ±⊕ ₊⁺
    key <AE02> {[ minus,		U2212,			2,	U2011,				U2213,		U2296,					U208B,		U207B	]}; // -− 2‑   ∓⊖ ₋⁻  минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,			U226E,		U2298,					U226A,		U22D8	]}; // /< 3#   ≮⊘ ≪⋘
    key <AE04> {[ asterisk,		greater,		4,	U2740,				U226F,		U229B,					U226B,		U22D9	]}; // *> 4❀   ≯⊛ ≫⋙
    key <AE05> {[ colon, 		percent,		5,	U205D,				U2030,		U2031,					U2026,		U22EF	]}; // :% 5⁝   ‰‱ …⋯
    key <AE06> {[ comma,		approxeq,		6,	equal,				minutes,	seconds,				U2034,		U2057	]}; // ,≈ 6=   ′″ ‴⁗  
    key <AE07> {[ period,		ampersand,		7,	backslash,			U22C5,		U2299,			enfilledcircbullet,	U02D9	]}; // .& 7\   ⋅⊙ •˙
    key <AE08> {[ underscore,	bar,			8,	U2423,				figspace,	U202F,					U27C2,		U02D4	]}; // _| 8␣      ⟂˔ 5=цфр, 6=тонк.нрз
    key <AE09> {[ question,		bracketleft,	9,	parenleft,			bracketleft,U207D,					U208D,		U207D	]}; // ?[ 9(   [⁽ ₍⁽
    key <AE10> {[ emdash,		bracketright,	endash,	parenright,		figdash,	U207E,					U208E,		U207E	]}; // —] –)   ‒⁾ ₎⁾  
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	leftsinglequotemark,U207B,	leftdoublequotemark,UA71D	]}; // !« „⚠   ‘⁻ “ꜝ
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U26A1,	rightsinglequotemark,U207A,	rightdoublequotemark,U2741	]}; // ;» “⚡   ’⁺ ”❁


    
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		U2286,		U2288,			includedin,	U0300	]}; // ` ` χ Χ   ⊆ ⊈ ⊂ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		U2287,		U2289,			includes,	U0301	]}; // ' ' ʼ '   ⊇ ⊉ ⊃ ́a   8 ́=комб.ударение (русское)
 
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		U2286,		U2288,	leftsinglequotemark,U0300	]}; // ` ` χ Χ   ⊆ ⊈ ‘ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		U2287,		U2289,	rightsinglequotemark,U0301	]}; // ' ' ʼ '   ⊇ ⊉ ’ ́a   8 ́=комб.ударение (русское)
    
    
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	leftsinglequotemark,U207B,	U208B,leftdoublequotemark	]}; // !« „⚠   ‘⁻ ₋“
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U2295,	rightsinglequotemark,U207A,	U208A,rightdoublequotemark	]}; // ;» “⊕   ’⁺ ₊”
    
    
    key <AE02> {[ minus,		U2212,			2,	U2011,				U2740,		U2296,					U208B,		U207B	]}; // -− 2‑   ❀⊖ ₋⁻  минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,			U226E,		U2298,					U226A,		U22D8	]}; // /< 3#   ≮⊘ ≪⋘
    key <AE04> {[ asterisk,		greater,		4,	U2740,				U226F,		U229B,					U226B,		U22D9	]}; // *> 4❀   ≯⊛ ≫⋙
    
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		U2287,		U2289,	rightsinglequotemark,U0301	]}; // ' ' ʼ '   ⊇ ⊉ ’ ́a   8 ́=комб.ударение (русское)
    
    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U2262,				U29B8,	U2216,		U20E0		]}; // \ ≠ ₙ ⁿ    ≢ ⦸  ∖ a⃠        
    replace key <PRSC> {[quotedbl, multiply,		U1D62,	U2071,			U1D62,	U2071,				Print,		Sys_Req		]};	// " × ᵢ ⁱ    ᵢ ⁱ PS SR
    replace key <SCLK> {[percent,	U22C5,			U2C7C,	U02B2,			U2C7C,	U02B2,				Scroll_Lock,Print		]};	// % ⋅ ⱼ ʲ    ⱼ ʲ SL PS 
    replace key <PAUS> {[ampersand,	figdash,		U2096,	U1D4F,			U2096,	U1D4F,				Pause,		Break		]};	// & ‒ ₖ ᵏ    ₖ ᵏ P  BR 
   
    key <AE02> {[ slash,		less,			2,	U2740,				U226E,		U2298,					U226A,		U2740	]}; // /< 2❀   ≮⊘ ≪❀
    key <AE03> {[ asterisk,		greater,		3,	U2740,				U226F,		U229B,					U226B,		U2740	]}; // *> 3❀   ≯⊛ ≫❀
    
    replace key <PRSC> {[backslash, multiply,		U1D62,	U2071,			backslash,backslash,		Print,		Sys_Req		],        
      actions[Group1]= [NoAction(),NoAction(),NoAction(),NoAction(), LockGroup(group=3),LockGroup(group=3),NoAction(),NoAction()],
      actions[Group2]= [NoAction(),NoAction(),NoAction(),NoAction(), LockGroup(group=3),LockGroup(group=3),NoAction(),NoAction()],
      actions[Group3]= [NoAction(),NoAction(),NoAction(),NoAction(), LockGroup(group=3),LockGroup(group=3),NoAction(),NoAction()],
      actions[Group4]= [NoAction(),NoAction(),NoAction(),NoAction(), LockGroup(group=3),LockGroup(group=3),NoAction(),NoAction()]};	// \ × ᵢ ⁱ    \ \ PS SR
    
    replace key <SCLK> {[percent,	figdash,		U2C7C,	U02B2,			U2030,	U2031,				Scroll_Lock,Print		]};	// % ‒ ⱼ ʲ    ‰ ‱ SL PS 
    replace key <PAUS> {[ampersand,	infinity,		U2096,	U1D4F,			U2740,	U2740,				Pause,		Break		]};	// & ∞ ₖ ᵏ    ❀ ❀ P  BR 
    
   
    
    key <AD08> {[	h,		H,	infinity,		VoidSymbol,		U210D,		U274C,			U2095,		U02B0	]}; // h H ∞     ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	U2206,			VoidSymbol,		emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V ∆     ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)

    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ parenleft,	section,		0,	section,			U207D,		U207D,			U208D,		U207D	]}; // (§ 0§   ⁽⁽ ₍⁽  
    key <AE01> {[ parenright,	numerosign,		1,	numbersign,			U207E,		U207E,			U208E,		U207E	]}; // )№ 1#   ⁾⁾ ₎⁾
    key <AE02> {[ slash,		less,			2,	U226A,				U226E,		U2298,			U2740,		U2740	]}; // /< 2≪   ≮⊘ ❀❀
    key <AE03> {[ asterisk,		greater,		3,	U226B,				U226F,		U229B,			U2740,		U2740	]}; // *> 3≫   ≯⊛ ❀❀
    key <AE04> {[ minus,		U2212,			4,	U2011,				U207B,		U2296,			U208B,		hyphen	]}; // -− 4‑   ⁻⊖ ₋- 8 =мягкий перенос (shy)
    key <AE05> {[ colon, 		percent,		5,	U205D,				U2030,		U2031,			percent,	U2740	]}; // :% 5⁝   ‰‱ %❀
    key <AE06> {[ comma,		approxeq,		6,	U02BC,				minutes,	seconds,		U2034,		U2057	]}; // ,≈ 6ʼ   ′″ ‴⁗
    key <AE07> {[ period,		ampersand,		7,	U2026,				U22C5,		U2299,			ampersand,	U0307	]}; // .& 7…   ⋅⊙ &ẋ 8 ̇=комб.производная по t
    key <AE08> {[ underscore,	bar,			8,	U2423,				figspace,	U202F,			U27C2,		U2740	]}; // _| 8␣      ⟂❀ 5=цфр, 6=тонк.нрз
    key <AE09> {[ question,		bracketleft,	9,	U2740,				bracketleft,U2740,			U2740,		U2740	]}; // ?[ 9❀   [❀ ❀❀
    key <AE10> {[ emdash,		bracketright,	endash,	U2740,			figdash,	U22EF,			U2740,		U2740	]}; // —] –❀   ‒⋯ ❀❀ 6 ⋯=cdots
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // !« „⚠   ⁻⊖ ₋“
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U26A1,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // ;» “⚡   ⁺⊕ ₊”

//   Ð À Ô Ư Á Ộ Ủ Ế Ã Ố Ấ Ớ
partial function_keys xkb_symbols "common_f1f12_viet_patch" {
    key.type="CTRL+ALT";     
    key <FK01> { type="CTRL+ALT", [ U0111,	U0110,		F1, F1, XF86_Switch_VT_1 	] }; // đĐ
    key <FK02> { type="CTRL+ALT", [ U00E0,	U00C0,		F2, F2, XF86_Switch_VT_2 	] }; // àÀ
    key <FK03> { type="CTRL+ALT", [ U00F4,	U00D4,		F3, F3, XF86_Switch_VT_3 	] }; // ôÔ
    key <FK04> { type="CTRL+ALT", [ U01B0,	U01AF,		F4, F4, XF86_Switch_VT_4 	] }; // ưƯ
    
    key <FK05> { type="CTRL+ALT", [ U00E1,	U00C1,		F5, F5, XF86_Switch_VT_5 	] }; // áÁ
    key <FK06> { type="CTRL+ALT", [ U1ED9,	U1ED8,		F6, F6, XF86_Switch_VT_6 	] }; // ộỘ
    key <FK07> { type="CTRL+ALT", [ U1EE7,	U1EE6,		F7, F7, XF86_Switch_VT_7 	] }; // ủỦ
    key <FK08> { type="CTRL+ALT", [ U1EBF,	U1EBE,		F8, F8, XF86_Switch_VT_8 	] }; // ếẾ
    
    key <FK09> { type="CTRL+ALT", [ U00E3,	U00C3,		F9, F9, XF86_Switch_VT_9 	] }; // ãÃ
    key <FK10> { type="CTRL+ALT", [ U1ED1,	U1ED0,		F10, F10, XF86_Switch_VT_10	] }; // ốỐ
    key <FK11> { type="CTRL+ALT", [ U1EA5,	U1EA4,		F11, F11, XF86_Switch_VT_11	] }; // ấẤ
    key <FK12> { type="CTRL+ALT", [ U1EDB,	U1EDA,		F12, F12, XF86_Switch_VT_12	] }; // ớỚ
};


// Базовый вариант: цифры с запятой и минусом
partial keypad_keys xkb_symbols "common_keypad_digits" {   
    key.type="EIGHT_LEVEL"; 
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    
    // 2 уровень — русские „лапки“, 6 уровень — американские “double quotes” — U+код для укорочения строки
    replace key <NMLK> {[guillemotleft, U201E,	U2169,U2798,	leftsinglequotemark,U201C,	U21C7,U2182	]}; // «„  Ⅹ➘   ‘“  ⇇ ↂ=10к        
    replace key <KPDV> {[guillemotright,U201C,	U216A,U2799,	rightsinglequotemark,U201D,	U21C4,U2187	]}; // »“  Ⅺ➙   ’”  ⇄ ↇ=50к
    modifier_map Mod2   { Num_Lock };
    
    replace key <KPMU> {[ figdash,		U2C7C,	U216B,U279A,	U02B2,	U24D8,			U21C9,	U2188	]}; // ‒ⱼ  Ⅻ➚   ʲⓙ  ⇉ ↈ=100к  1‒=цфр.тире    
    replace key <KPSU> {[ U2212,	U2099,		U208B,U207B,	U207F,	U24DD,			U1D3A,	U2619	]}; // −н  ₋⁻   ⁿⓝ  ᴺ ☙  1−=минус ширины плюса

    replace key <KPAD> {type="FOUR_LEVEL",		[ Tab,		ISO_Left_Tab,		U208A,	U207A			]}; // Tab ₊⁺ 
    replace key <KPEN> {type="ONE_LEVEL",		[ KP_Enter 	]};  
        
    // 2 уровень — нижний индекс, 5 — верхний, 6 — форма в круге, 4 — (10+x) в круге на цифрах и 20 на запятой; 3 римские 1‒12
    //                                  	римск.	(10+x)	          	 	(x)	  	   	x/5 + рим. 	x/8 + рим. устар.
    replace key  <KP7> {[ 7,sevensubscript,	U2166, 	U2470,	sevensuperior,	U2466,		U216E,		seveneighths]}; // 7 ₇ Ⅶ ⑰   ⁷ ⑦ Ⅾ=500 ⅞
    replace key  <KP8> {[ 8,eightsubscript,	U2167, 	U2471,	eightsuperior,	U2467,		U216F,		U2180		]}; // 8 ₈ Ⅷ ⑱   ⁸ ⑧ Ⅿ=1к  ↀ=1к устар
    replace key  <KP9> {[ 9,ninesubscript,	U2168, 	U2472,	ninesuperior,	U2468,		U2618,		U2181		]}; // 9 ₉ Ⅸ ⑲   ⁹ ⑨ ☘     ↁ=5к устар.

    replace key  <KP4> {[ 4,foursubscript,	U2163, 	U246D,	foursuperior,	U2463,		fourfifths,	onehalf		]}; // 4 ₄ Ⅳ ⑭   ⁴ ④ ⅘     ½=⁴/₈
    replace key  <KP5> {[ 5,fivesubscript,	U2164, 	U246E,	fivesuperior,	U2464,		U216C,		fiveeighths	]}; // 5 ₅ Ⅴ ⑮   ⁵ ⑤ Ⅼ=50  ⅝
    replace key  <KP6> {[ 6,sixsubscript,	U2165, 	U246F,	sixsuperior,	U2465,		U216D,	threequarters	]}; // 6 ₆ Ⅵ ⑯   ⁶ ⑥ Ⅽ=100 ¾=⁶/₈

    replace key  <KP1> {[ 1,onesubscript,	U2160, 	U246A,	onesuperior,	U2460,		onefifth,	oneeighth	]}; // 1 ₁ Ⅰ ⑪   ¹ ① ⅕     ⅛
    replace key  <KP2> {[ 2,twosubscript,	U2161, 	U246B,	twosuperior,	U2461,		twofifths,	onequarter	]}; // 2 ₂ Ⅱ ⑫   ² ② ⅖     ¼=²/₈
    replace key  <KP3> {[ 3,threesubscript,	U2162, 	U246C,	threesuperior,	U2462,		threefifths,threeeighths]}; // 3 ₃ Ⅲ ⑬   ³ ③ ⅗     ⅜

    replace key  <KP0> {[ 0,zerosubscript,	U2741,	U2469,	zerosuperior,	U24EA,		U263A,		U2766		]}; // 0 ₀ ❁ ⑩   ⁰ ⓪ ☺     ❦
    replace key <KPDL> {[ comma, U1D62,		U2704,	U2473,	U2071,			U24D8,		U2639,		U2767		]}; // , ᵢ ✄ ⑳   ⁱ ⓘ ☹     ❧
};

    key <TLDE> {[ parenleft,	section,		section,	0,			U207D,		U207D,			U208D,		U207D	]}; // (§ §0   ⁽⁽ ₍⁽  



    key <FK11> {[equal,	U23E8,			U208C,			U207C,				U2261,				U229C,	U2152,		U2243		]}; // = ⏨ ₌ ⁼    ≡ ⊜  ⅒ ≃
    //key <FK12> {[quotedbl,	notequal,	U2099,			U207F,				U2262,				U2740,	U2740,		U20E0		]}; // " ≠ ₙ ⁿ    ≢ ❀  ❀ a⃠        
    //replace key <PRSC> {[backslash, multiply,		U1D62,	U2071,			U2216,	U29B8,				Print,		Sys_Req		]};	// \ × ᵢ ⁱ    ∖ ⦸ PS SR
    key <FK12> {[quotedbl,	notequal,	U2099,			U207F,				U207F,				U2262,	U2740,		U20E0		]}; // " ≠ ₙ ⁿ    ⁿ ≢  ❀ a⃠        
   
    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2216,	U29B8,				Print,		Sys_Req		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ∖ ⦸ PS SR
      
    replace key <SCLK> {[percent,	U2206,			U2C7C,	U02B2,			U2030,	U2031,				Scroll_Lock,Print		]};	// % ∆ ⱼ ʲ    ‰ ‱ SL PS 
    replace key <PAUS> {[ampersand,	figdash,		U2096,	U1D4F,			U2740,	U2740,				Pause,		Break		]};	// & ‒ ₖ ᵏ    ❀ ❀ P  BR 
        
        
    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U207D,			U208D,		U207D	]}; // (§ ∞0   ⁽⁽ ₍⁽  
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U207E,			U208E,		U207E	]}; // )№ #1   ⁾⁾ ₎⁾
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2740,		U2740	]}; // /< ≪2   ≮⊘ ❀❀
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U2740,		U2740	]}; // *> ≫3   ≯⊛ ❀❀
    key <AE04> {[ minus,		U2212,			U2011,		4,			U2212,		U2296,			U2740,		hyphen	]}; // -− ‑4   −⊖ ❀- 8 =мягкий перенос (shy)
    key <AE05> {[ colon, 		percent,		U205D,		5,			U2030,		U2031,			percent,	U2740	]}; // :% ⁝5   ‰‱ %❀
    key <AE06> {[ comma,		approxeq,		U02BC,		6,			minutes,	seconds,		U2034,		U2057	]}; // ,≈ ʼ6   ′″ ‴⁗
    key <AE07> {[ period,		ampersand,		U2026,		7,			U22C5,		U2299,			ampersand,	U0307	]}; // .& …7   ⋅⊙ &ẋ 8 ̇=комб.производная по t
    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U27C2,		U2740	]}; // _| ␣8      ⟂❀ 5=цфр, 6=тонк.нрз
    key <AE09> {[ question,		bracketleft,	U2740,		9,			bracketleft,U2740,			U2740,		U2740	]}; // ?[ ❀9   [❀ ❀❀
    key <AE10> {[ emdash,		bracketright,	endash,		0,			figdash,	U22EF,			U2740,		U2740	]}; // —] –0   ‒⋯ ❀❀ 6 ⋯=cdots
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // !« „⚠   ⁻⊖ ₋“
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U26A1,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // ;» “⚡   ⁺⊕ ₊”

      
    key.type="EIGHT_LEVEL_ALPHABETIC";
    //					Shift	3				3+Shift			5			5+Shift			5+3			5+3+Shift
    key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	includes,	U2285,			U2C7C,		U02B2	]}; // j J θ Θ   ⊃ ⊅ ⱼ ʲ   
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		copyright,		U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ © ⤉ ᶜ
    key <AD03> {[	u,		U,	Greek_upsilon,	Greek_UPSILON,	union,		logicalor,		U1D64,		U1D58	]}; // u U υ Υ   ∪ ∨ ᵤ ᵘ
    key <AD04> {[	k,		K,	Greek_kappa,	Greek_KAPPA,	U2245,		U2247,			U2096,		U1D4F	]}; // k K κ Κ   ≅ ≇ ₖ ᵏ   5 ≅=конгруэнтно
    key <AD05> {[	e,		E,	Greek_epsilon,	Greek_EPSILON,	U2203,		U2204,			U2091,		U1D49	]}; // e E ε Ε   ∃ ∄ ₑ ᵉ
    key <AD06> {[	n,		N,	Greek_nu,		Greek_NU,		U2115,		notsign,		U2099,		U207F	]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	degree,		nabla,			nabla,		U1D4D	]}; // g G γ Γ   ° ∇ ∇ ᵍ
    key <AD08> {[	h,		H,	VoidSymbol,		VoidSymbol,		U210D,		U274C,			U2095,		U02B0	]}; // h H       ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	VoidSymbol,		VoidSymbol,		emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V       ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)


    // Insert (LOCAL_EIGHT_LEVEL для Ctrl+Insert без Level3; при этом Shift+Insert без Level3 работает часто, но не везде); в реж.CapsLock Insert на первом уровне!
    // replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ parenright, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // ) Ins
    replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ plus, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // + Ins
   
    //key <LSGT> 			{ 	type="ONE_LEVEL",	[ numbersign		]	}; // #
    replace key <BKSL> 	{	type="ONE_LEVEL",	[ ISO_Level5_Shift	]  	};
    modifier_map Mod3 { ISO_Level5_Shift };  

    
    key.type="EIGHT_LEVEL";     
    key <LSGT> {[ numbersign,numbersign,U1D62,	U2071,			U2766,U2767,		U2619,	U2618	]};	// # # ᵢ ⁱ     ❦ ❧          ☙       ☘          
    //key <SPCE> {[ space,asciitilde,	asciitilde,nobreakspace,	U2241,hairspace,	emspace,U0303	]}; //   ~ ~ nbsp  ≁ волосяная  круглая ̃x   8 ̃=комб.волна
    key <SPCE> {[ space,asciitilde,	nobreakspace,asciitilde,	U2241,hairspace,	emspace,U0303	]}; //   ~ nbsp ~   ≁ волосяная  круглая ̃x   8 ̃=комб.волна


partial alphanumeric_keys function_keys modifier_keys xkb_symbols "common_88_multiply_patch" {
    key.type="EIGHT_LEVEL";     
    key <AE05> {[ colon, 		multiply	]}; // :%
    key <AE07> {[ period,		U22C5		]}; // .& 
};

partial alphanumeric_keys function_keys modifier_keys xkb_symbols "common_88_brackets_patch" {
    key.type="EIGHT_LEVEL";     
//     key <SCLK> {[bracketleft,	U2206		]};	// [ ∆ 
//     key <PAUS> {[bracketright,	figdash		]};	// ] ‒ 
//     key <AE09> {[ question,		ampersand	]}; // ?&
//     key <AE10> {[ emdash,		percent,	endash,		0,			figdash,	U22EF,			U2030,		U2031	]}; // —% –0   ‒⋯ ❀❀ 6 ⋯=cdots
    
    replace key <SCLK> {[bracketleft,	U2206,			U2C7C,	U02B2,			U2030,	U2031,				Scroll_Lock,Print		]};	// [ ∆ ⱼ ʲ    ‰ ‱ SL PS 
    replace key <PAUS> {[bracketright,	figdash,		U2096,	U1D4F,			U2740,	U2740,				Pause,		Break		]};	// ] ‒ ₖ ᵏ    ❀ ❀ P  BR 
    key <AE09> {[ question,		ampersand,	U2740,		9,			U2740,		U2740,			U2740,		U2740	]}; // ?& ❀9   ❀❀ ❀❀
    key <AE10> {[ emdash,		percent,	endash,		0,			figdash,	U22EF,			U2030,		U2031	]}; // —% –0   ‒⋯ ‰‱ 6 ⋯=cdots
};




    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U29B8,				Print,		Sys_Req		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ⦸ PS SR
      
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,Print		]};	// % ∆ ⱼ ʲ    ʲ ⱼ SL PS 
    replace key <PAUS> {[bracketright,figdash,		U2096,	U1D4F,			U1D4F,	U2096,				Pause,		Break		]};	// & ‒ ₖ ᵏ    ᵏ ₖ P  BR 

    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U1D62,				Print,		Sys_Req		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ᵢ PS SR
      
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,Print		]};	// % ∆ ⱼ ʲ    ʲ ⱼ SL PS 
    replace key <PAUS> {[bracketright,figdash,		U2096,	U1D4F,			U1D4F,	U2096,				Pause,		Break		]};	// & ‒ ₖ ᵏ    ᵏ ₖ P  BR 



partial alphanumeric_keys function_keys modifier_keys xkb_symbols "common_88_hex_patch" {
    replace key <ESC>  {	type="FOUR_LEVEL",	[ F,	Escape,		Escape,	Escape		]	};
    key.type="EIGHT_LEVEL";     
    
//     key <FK11> {	[ A,	equal		]};  
//     key <FK12> {	[ B,	quotedbl	]};
//     key <PRSC> {	[ C,	backslash	]}; 
//     key <SCLK> {	[ D,	percent		]};  
//     key <PAUS> {	[ E,	ampersand	]};    
     
    key <FK11> {[ A	]};  
    key <FK12> {[ B	]};
    key <PRSC> {[ C	]}; 
    key <SCLK> {[ D	]};  
    key <PAUS> {[ E	]};    
      
    //key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ backslash, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // + Ins
};


    key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		VoidSymbol,	VoidSymbol,		U2098,		U1D50	]}; // m M μ Μ       ₘ ᵐ        

    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U29B8,				Print,		U20E0		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ⦸ PS a⃠
      
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,U20E0		]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL a⃠ 

    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2740,		U2740	]}; // /< ≪2   ≮⊘ ❀❀

    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ ̅x   8 ̅=комб.черта

    //key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U27C2,		U0305	]}; // _| ␣8      ⟂a̅  5=цфр, 6=тонк.нрз 8 ̅=комб.черта
    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U27C2,		U0331	]}; // _| ␣8      ⟂a̱̅  5=цфр, 6=тонк.нрз 

    //key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0331	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ a̱  8 ̱=комб.нижнее подчёркивание


    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U0331,		U0305	]}; // _| ␣8      a̱a̅ 5=цфр, 6=тонк.нрз 


    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U27E1,				U2466,	U2150,		U2735		]}; // 7 ⌉ ₇ ⁷    ⟡ ⑦  ⅐ ✵ 
    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U2740,				U2467,	oneeighth,seveneighths	]}; // 8 ± ₈ ⁸    ❀ ⑧  ⅛ ⅞
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2740,				U2468,	U2151,		U2738		]}; // 9 ∓ ₉ ⁹    ❀ ⑨  ⅑ ✸

    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠ ★ ᶻ

    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U27C2,				U2467,	oneeighth,seveneighths	]}; // 8 ± ₈ ⁸    ⟂ ⑧  ⅛ ⅞
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2225,				U2468,	U2151,		U2226		]}; // 9 ∓ ₉ ⁹    ∥ ⑨  ⅑ ✸

    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U2740,			U208D,		U0306	]}; // (§ ∞0   ⁽❀ ₍ ă
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U2740,			U208E,		U20DD	]}; // )№ #1   ⁾❀ ₎ a⃝
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U29B8,		U20E0	]}; // /< ≪2   ≮⊘ ⦸ a⃠
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U27A4,		U20D7	]}; // *> ≫3   ≯⊛ ➤ a⃗
    key <AE04> {[ minus,		U2212,			U2011,		4,			U2216,		U2296,			U2023,		hyphen	]}; // -− ‑4   ∖⊖ ‣- 8 =мягкий перенос (shy)

    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2740,		U2740	]}; // _| ␣8      ❀❀ 5=цфр, 6=тонк.нрз 

    key <AE09> {[ question,		ampersand,		U2740,		9,			U2225,		U2226,			U2740,		U2740	]}; // ?& ❀9   ∥∦ ❀❀
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U27C2,				U2466,	U2150,		U2735		]}; // 7 ⌉ ₇ ⁷    ⟂ ⑦  ⅐ ✵ 
    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2740,		U2740	]}; // _| ␣8      ❀❀ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		U2740,		9,			U2740,		U2740,			U2740,		U2740	]}; // ?& ❀9   ❀❀ ❀❀

    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2225,		U2226	]}; // _| ␣8      ∥∦ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		U2740,		9,			U2220,		U27C2,			U27C2,		U2740	]}; // ?& ❀9   ∠⟂ ⟂❀

    
   //					Shift	3				3+Shift			5			5+Shift			5+3			5+3+Shift
    key <AD01> {[	j,		J,	Greek_theta,	Greek_THETA,	includes,	U2285,			U2C7C,		U02B2	]}; // j J θ Θ   ⊃ ⊅ ⱼ ʲ   
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		copyright,		U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ © ⤉ ᶜ
    key <AD03> {[	u,		U,	Greek_upsilon,	Greek_UPSILON,	union,		logicalor,		U1D64,		U1D58	]}; // u U υ Υ   ∪ ∨ ᵤ ᵘ
    key <AD04> {[	k,		K,	Greek_kappa,	Greek_KAPPA,	U2245,		U2247,			U2096,		U1D4F	]}; // k K κ Κ   ≅ ≇ ₖ ᵏ   5 ≅=конгруэнтно
    key <AD05> {[	e,		E,	Greek_epsilon,	Greek_EPSILON,	U2203,		U2204,			U2091,		U1D49	]}; // e E ε Ε   ∃ ∄ ₑ ᵉ
    key <AD06> {[	n,		N,	Greek_nu,		Greek_NU,		U2115,		notsign,		U2099,		U207F	]}; // n N ν Ν   ℕ ¬ ₙ ⁿ
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	degree,		nabla,			nabla,		U1D4D	]}; // g G γ Γ   ° ∇ ∇ ᵍ
    key <AD08> {[	h,		H,	bracketleft,	backslash,		U210D,		U274C,			U2095,		U02B0	]}; // h H [ \   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	bracketright,	backslash,		emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V ] \   ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠ ★ ᶻ
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		U2286,		U2288,	leftsinglequotemark,U0300	]}; // ` ` χ Χ   ⊆ ⊈ ‘ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,apostrophe,apostrophe,	U2287,		U2289,	rightsinglequotemark,U0301	]}; // ' ' ' '   ⊇ ⊉ ’ ́a   8 ́=комб.ударение (русское)
  
    key <AC01> {[	f,		F,		Greek_phi,	Greek_PHI,		includedin,	U2284,			U2691,		U1DA0	]}; // f F φ Φ   ⊂ ⊄ ⚑ ᶠ
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U21A9,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ↩ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21AA,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↪ ʷ
    key <AC04> {[	a,		A,		Greek_alpha,Greek_ALPHA,	U2200,		U2135,			U2090,		U1D43	]}; // a A α Α   ∀ ℵ ₐ ᵃ
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2740,			U209A,		U1D56	]}; // p P π Π   ∏ ❀ ₚ ᵖ   
    key <AC06> {[	r,		R,		Greek_rho,	Greek_RHO,		U211D,		U20BD,			U1D63,		U02B3	]}; // r R ρ Ρ   ℝ ₽ ᵣ ʳ
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U21A6,			U2092,		U1D52	]}; // o O ω Ω   𝕆 ↦ ₒ ᵒ   6 ↦=отображается в
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U1F12F,			U2097,		U02E1	]}; // l L λ Λ   ℓ 🄯 ₗ ˡ   6 🄯=копилефт
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,	partdifferential,	U2300,		U1D48 	]}; // d D δ Δ   ∆ ∂ ⌀ ᵈ   5 ∆=приращение, 8 ⌀=диаметр 
    key <AC10> {[asciicircum,asciicircum,Greek_psi,	Greek_PSI,	elementof,	notelementof,	U27A4,		U0302	]}; // ^ ^ ψ Ψ   ∈ ∉ ➤ x̂   8 ̂=комб. 
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2023,		hyphen	]}; // $ $ η Η   ∋ ∌ ‣ -   8 =мягкий перенос (shy)

    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U279B,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➛ x⃗
    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ ̅x   8 ̅=комб.черта
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		ssharp,			U209B,		U02E2	]}; // s S σ Σ   ∑ ß ₛ ˢ
    key <AB04> {[	m,		M,		Greek_mu,	Greek_MU,		U1D50,		U2098,			U2098,		U1D50	]}; // m M μ Μ   ᵐ ₘ ₘ ᵐ        
    key <AB05> {[	i,		I,		Greek_iota,	Greek_IOTA,	intersection,	logicaland,		U1D62,		U2071	]}; // i I ι Ι   ∩ ∧ ᵢ ⁱ
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U2740,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ❀ ₜ ᵗ
    key <AB07> {[   x,		X,		Greek_xi,	Greek_XI,		multiply,	U2297,			U2093,		U02E3	]}; // x X ξ Ξ   × ⊗ ₓ ˣ
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,	U229A, 	enfilledcircbullet,	U1D47	]};	// b B β Β   ∞ ⊚ • ᵇ
    key <AB09> {[braceleft,braceleft,	lessthanequal,	 U2270,		U227A,	U2280,			U227E,		U0307	]}; // { { ≤ ≰   ≺ ⊀ ≾ ẋ   8 ̇=комб.производная по t
    key <AB10> {[braceright,braceright,	greaterthanequal,U2271,		U227B,	U2281,			U227F,		U0308	]}; // } } ≥ ≰   ≻ ⊁ ≿ ẍ   8 ̈=комб.«вторая» ⟨умляут⟩
    
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U27C2,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ⟂ ₜ ᵗ
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U2740,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ❀ ₜ ᵗ
    
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2226,			U209A,		U1D56	]}; // p P π Π   ∏ ∦ ₚ ᵖ   
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2740,			U209A,		U1D56	]}; // p P π Π   ∏ ❀ ₚ ᵖ   
   
    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2225,		U2226	]}; // _| ␣8      ∥∦ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		U2740,		9,			U2220,		U27C2,			U27C2,		U02D4	]}; // ?& ❀9   ∠⟂ ⟂˔
    
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2220,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ∠ ★ ᶻ
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2740,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ❀ ★ ᶻ
   
    
    key <LSGT> {[ numbersign,numbersign,U1D62,	U2071,			U2071,U2766,		U2767,	U2619	]};	// # # ᵢ    ⁱ   ⁱ ❦          ❧       ☙
    
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2220,		U2740	]}; // /< ≪2   ≮⊘ ∠ ❀
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U27A4,		U20D7	]}; // *> ≫3   ≯⊛ ➤ a⃗
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,U2740		]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL ❀
    
    
 












    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U27E1,			U208D,		U20DD	]}; // ( § ∞ 0    ⁽ ⟡ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U29B8,			U208E,		U20E0	]}; // ) № # 1    ⁾ ⦸ ₎ a⃠
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2220,		U2619	]}; // / < ≪ 2    ≮ ⊘ ∠ ☙
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U27A4,		U2767	]}; // * > ≫ 3    ≯ ⊛ ➤ ❧
    key <AE04> {[ minus,		U2212,			U2011,		4,			U2216,		U2296,			U2023,		hyphen	]}; // - − ‑ 4    ∖ ⊖ ‣ - 8 =мягкий перенос (shy)
    key <AE05> {[ colon, 		multiply,		U205D,		5,			U205D,		U2297,			U2740,		U2740	]}; // : × ⁝ 5    ⁝ ⊗ ❀ ❀
    key <AE06> {[ comma,		approxeq,		U02BC,		6,			minutes,	seconds,		U2034,		U2057	]}; // , ≈ ʼ 6    ′ ″ ‴ ⁗
    key <AE07> {[ period,		U22C5,			U2026,		7,			U22C5,		U2299,	enfilledcircbullet,	U0307	]}; // . ⋅ … 7    ⋅ ⊙ • ẋ 8 ̇=комб.производная по t
    key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2740,		U2740	]}; // _ | ␣ 8        ❀ ❀ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		U2740,		9,			U2220,		U2740,			U2740,		U2740	]}; // ? & ❀ 9    ∠ ❀ ❀ ❀
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U22EF,			U2030,		U2031	]}; // — % – 0    ‒ ⋯ ‰ ‱ 6 ⋯=cdots
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // ! « „ ⚠    ⁻ ⊖ ₋ “
    key	<AE12> {[ semicolon,guillemotright,	leftdoublequotemark,U26A1,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // ; » “ ⚡    ⁺ ⊕ ₊ ”


    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U27E1		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ⟡
    key <FK02> {[1, 	U2A7E,			onesubscript,	onesuperior,		XF86_Switch_VT_2,	U2460,	U215F,		U262D		]}; // 1 ⩾ ₁ ¹  vt2 ①  ⅟ ☭
    key <FK03> {[2, 	U27E8,			twosubscript,	twosuperior,		XF86_Switch_VT_3,	U2461,	onehalf,	U2744		]}; // 2 ⟨ ₂ ²  vt3 ②  ½ ❄
    key <FK04> {[3, 	U27E9,			threesubscript,	threesuperior,		XF86_Switch_VT_4,	U2462,	onethird,	twothirds	]}; // 3 ⟩ ₃ ³  vt4 ③  ⅓ ⅔                                                                      
    key <FK05> {[4, 	U230A,			foursubscript,	foursuperior,		XF86_Switch_VT_5,	U2463,	onequarter,threequarters]}; // 4 ⌊ ₄ ⁴  vt5 ④  ¼ ¾
    key <FK06> {[5, 	U230B,			fivesubscript,	fivesuperior,		XF86_Switch_VT_6,	U2464,	onefifth,	fourfifths	]}; // 5 ⌋ ₅ ⁵  vt6 ⑤  ⅕ ⅘
    key <FK07> {[6, 	U2308,			sixsubscript,	sixsuperior,		XF86_Switch_VT_7,	U2465,	onesixth,	fivesixths	]}; // 6 ⌈ ₆ ⁶  vt7 ⑥  ⅙ ⅚  
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U2740,				U2466,	U2150,		U2735		]}; // 7 ⌉ ₇ ⁷    ❀ ⑦  ⅐ ✵ 
    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U22A2,				U2467,	oneeighth,seveneighths	]}; // 8 ± ₈ ⁸    ⊢ ⑧  ⅛ ⅞
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U22A3,				U2468,	U2151,		U2738		]}; // 9 ∓ ₉ ⁹    ⊣ ⑨  ⅑ ✸
    key <FK11> {[equal,	U23E8,			U208C,			U207C,				U2261,				U229C,	U2152,		U2243		]}; // = ⏨ ₌ ⁼    ≡ ⊜  ⅒ ≃
    key <FK12> {[quotedbl,	notequal,	U2099,			U207F,				U207F,				U2099,	U2262,		U2262		]}; // " ≠ ₙ ⁿ    ⁿ ₙ  ≢ ≢  
   
    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U1D62,				Print,		Sys_Req		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ᵢ PS SR
      
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,U2766		]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL ❦
    replace key <PAUS> {[bracketright,figdash,		U2096,	U1D4F,			U1D4F,	U2096,				Pause,		Break		]};	// ] ‒ ₖ ᵏ    ᵏ ₖ P  BR 

   
// Базовый вариант: цифры с запятой и минусом
partial keypad_keys xkb_symbols "common_keypad_digits" {   
    key.type="EIGHT_LEVEL"; 
    // Не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет
    
    // 2 уровень — русские „лапки“, 6 уровень — американские “double quotes” — U+код для укорочения строки
    replace key <NMLK> {[guillemotleft, U201E,	U2169,U2798,	leftsinglequotemark,U201C,	U21C7,U2182	]}; // «„  Ⅹ➘   ‘“  ⇇ ↂ=10к        
    replace key <KPDV> {[guillemotright,U201C,	U216A,U2799,	rightsinglequotemark,U201D,	U21C4,U2187	]}; // »“  Ⅺ➙   ’”  ⇄ ↇ=50к
    modifier_map Mod2   { Num_Lock };
    
    replace key <KPMU> {[ figdash,	U2096,		U216B,U279A,	U1D4F,	U24DA,			U21C9,	U2188	]}; // ‒ₖ  Ⅻ➚   ᵏⓚ  ⇉ ↈ=100к  1‒=цфр.тире    
    replace key <KPSU> {[ U2212,	U2099,		U208B,U207B,	U207F,	U24DD,			U1D3A,	U2619	]}; // −н  ₋⁻   ⁿⓝ  ᴺ ☙  1−=минус ширины плюса

    replace key <KPAD> {type="FOUR_LEVEL",		[ Tab,		ISO_Left_Tab,		U208A,	U207A			]}; // Tab ₊⁺ 
    replace key <KPEN> {type="ONE_LEVEL",		[ KP_Enter 	]};  
        
    // 2 уровень — нижний индекс, 5 — верхний, 6 — форма в круге, 4 — (10+x) в круге на цифрах и 20 на запятой; 3 римские 1‒12
    //                                  	римск.	(10+x)	          	 	(x)	  	   	x/5 + рим. 	x/8 + рим. устар.
    replace key  <KP7> {[ 7,sevensubscript,	U2166, 	U2470,	sevensuperior,	U2466,		U216E,		seveneighths]}; // 7 ₇ Ⅶ ⑰   ⁷ ⑦ Ⅾ=500 ⅞
    replace key  <KP8> {[ 8,eightsubscript,	U2167, 	U2471,	eightsuperior,	U2467,		U216F,		U2180		]}; // 8 ₈ Ⅷ ⑱   ⁸ ⑧ Ⅿ=1к  ↀ=1к устар
    replace key  <KP9> {[ 9,ninesubscript,	U2168, 	U2472,	ninesuperior,	U2468,		U2618,		U2181		]}; // 9 ₉ Ⅸ ⑲   ⁹ ⑨ ☘     ↁ=5к устар.

    replace key  <KP4> {[ 4,foursubscript,	U2163, 	U246D,	foursuperior,	U2463,		fourfifths,	onehalf		]}; // 4 ₄ Ⅳ ⑭   ⁴ ④ ⅘     ½=⁴/₈
    replace key  <KP5> {[ 5,fivesubscript,	U2164, 	U246E,	fivesuperior,	U2464,		U216C,		fiveeighths	]}; // 5 ₅ Ⅴ ⑮   ⁵ ⑤ Ⅼ=50  ⅝
    replace key  <KP6> {[ 6,sixsubscript,	U2165, 	U246F,	sixsuperior,	U2465,		U216D,	threequarters	]}; // 6 ₆ Ⅵ ⑯   ⁶ ⑥ Ⅽ=100 ¾=⁶/₈

    replace key  <KP1> {[ 1,onesubscript,	U2160, 	U246A,	onesuperior,	U2460,		onefifth,	oneeighth	]}; // 1 ₁ Ⅰ ⑪   ¹ ① ⅕     ⅛
    replace key  <KP2> {[ 2,twosubscript,	U2161, 	U246B,	twosuperior,	U2461,		twofifths,	onequarter	]}; // 2 ₂ Ⅱ ⑫   ² ② ⅖     ¼=²/₈
    replace key  <KP3> {[ 3,threesubscript,	U2162, 	U246C,	threesuperior,	U2462,		threefifths,threeeighths]}; // 3 ₃ Ⅲ ⑬   ³ ③ ⅗     ⅜

    replace key  <KP0> {[ 0,zerosubscript,	U2741,	U2469,	zerosuperior,	U24EA,		U263A,		U2766		]}; // 0 ₀ ❁ ⑩   ⁰ ⓪ ☺     ❦
    replace key <KPDL> {[ comma, U2C7C,		U2704,	U2473,	U02B2,			U24D8,		U2639,		U2767		]}; // , ⱼ ✄ ⑳   ʲ ⓙ ☹     ❧
};	


// Для 104-клавишной (ANSI с цифровым блоком): # на первом уровне (вместо отсутствующей LSGT)
partial keypad_keys xkb_symbols "common_keypad_104numbersign_patch" {   
    key  <KPMU>  {type="EIGHT_LEVEL",	[ numbersign ]};   
};


// Базовый вариант: цифры с запятой     
partial keypad_keys xkb_symbols "common_keypad_digits" {       
    key.type="EIGHT_LEVEL";     
        
    //  	 	 	 	 	нижний индекс,  	x/5, 	x/8,			верхний индекс,	нижний индекс,	римск.    
    replace key  <KP0> {[ 0,zerosubscript,		U2740,	U2740,			zerosuperior,	zerosubscript,	U2740,U2740	]}; // 0₀ ❀❀  ⁰₀ ❀❀    
    replace key <KPDL> {[ comma, U2C7C,			U2740,	U2740,			U02B2,			U2C7C,			U2740,U2740	]}; // ,ⱼ ❀❀  ʲⱼ ❀❀    
       
    replace key  <KP1> {[ 1,onesubscript,	onefifth,	oneeighth,		onesuperior,	onesubscript,	U2160,U2740	]}; // 1₁ ⅕¼  ¹₁ Ⅰ❀    
    replace key  <KP2> {[ 2,twosubscript,	twofifths,	onequarter,		twosuperior,	twosubscript,	U2161,U2740	]}; // 2₂ ⅖¼  ²₂ Ⅱ❀    
    replace key  <KP3> {[ 3,threesubscript,	threefifths,threeeighths,	threesuperior,	threesubscript,	U2162,U2740	]}; // 3₃ ⅗⅜  ³₃ Ⅲ❀    
        
    replace key  <KP4> {[ 4,foursubscript,	fourfifths,	onehalf,		foursuperior,	foursubscript,	U2163,U2740	]}; // 4₄ ⅘½  ⁴₄ Ⅳ❀    
    replace key  <KP5> {[ 5,fivesubscript,		U2740,	fiveeighths,	fivesuperior,	fivesubscript,	U2164,U2740	]}; // 5₅ ❀⅝  ⁵₅ Ⅴ❀    
    replace key  <KP6> {[ 6,sixsubscript,		U2740,	threequarters,	sixsuperior,	sixsubscript,	U2165,U2740	]}; // 6₆ ❀¾  ⁶₆ Ⅵ❀    
    
    replace key  <KP7> {[ 7,sevensubscript,		U2740,	seveneighths,	sevensuperior,	sevensubscript,	U2166,U2740	]}; // 7₇ ❀⅞  ⁷₇ Ⅶ❀    
    replace key  <KP8> {[ 8,eightsubscript,		U2740,	U2740,			eightsuperior,	eightsubscript,	U2167,U2740	]}; // 8₈ ❀❀  ⁸₈ Ⅷ❀    
    replace key  <KP9> {[ 9,ninesubscript,		U2740,	U2740,			ninesuperior,	ninesubscript,	U2168,U2740	]}; // 9₉ ❀❀  ⁹₉ Ⅸ❀    
            
    replace key <NMLK> {[ ampersand,U1D62,		U2740,	U2740,			U2071,			U1D62,			U2169,U2740	]}; // &ᵢ ❀❀  ⁱᵢ Ⅹ❀               
    replace key <KPDV> {[ less,		U2096,		U2740,	U2740,			U1D4F,			U2096,			U216A,U2740	]}; // <ₖ ❀❀  ᵏₖ Ⅺ❀    
    replace key <KPMU> {[ greater,	U2098,		U2740,	U2740,			U1D50,			U2098,			U216B,U2740	]}; // >ₘ ❀❀  ᵐₘ Ⅻ❀        
    replace key <KPSU> {[ bar,		U2099,		U2740,	U2740,			U207F,			U2099,			U2740,U2740	]}; // |ₙ ❀❀  ⁿₙ ❀❀    
        
    // не искореняем Num_Lock на одноимённой клавише, пусть сбрасывает в режим «по умолчанию». И не реализуем заново — режима Num_Lock не будет        
    modifier_map Mod2   { Num_Lock };    
    
    replace key <KPAD> {type="TWO_LEVEL",		[ Tab,		ISO_Left_Tab			]}; // Tab      
    replace key <KPEN> {type="ONE_LEVEL",		[ KP_Enter 	]};                                                                
};	    
    
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U27E1		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ⟡
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U2699		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ⚙

    //key <FK11> {[quotedbl,	U23E8,		U2098,			U1D50,				U2261,				U2262,	U2152,		U2243		]}; // " ⏨ ₘ ᵐ    ≡ ≢  ⅒ ≃
    //key <FK12> {[equal,		notequal,	U2099,			U207F,				U207F,				U229C,	U208C,		U207C		]}; // = ≠ ₙ ⁿ    ⁿ ⊜  ₌ ⁼  
    //key <FK11> {[equal,	U23E8,			U208C,			U207C,				U2261,				U229C,	U2152,		U2243		]}; // = ⏨ ₌ ⁼    ≡ ⊜  ⅒ ≃
    //key <FK12> {[quotedbl,	notequal,	U2099,			U207F,				U207F,				U2099,	U2262,		U2262		]}; // " ≠ ₙ ⁿ    ⁿ ₙ  ≢ ≢  
    //replace key <PAUS> {[bracketright,figdash,		U2096,	U1D4F,			U1D4F,	U2096,				Pause,		Break		]};	// ] ‒ ₖ ᵏ    ᵏ ₖ P  BR 
    
    //key <AE08> {[ underscore,	bar,			U2423,		8,			figspace,	U202F,			U2740,		U2740	]}; // _ | ␣ 8       ❀ ❀ 5=цфр, 6=тонк.нрз 
   //key <AE07> {[ period,		U22C5,			U2026,		7,			U22C5,		U2299,	enfilledcircbullet,	U0307	]}; // . ⋅ … 7   ⋅ ⊙ • ẋ 
    //key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U22EF,			U2030,		U2031	]}; // — % – 0   ‒ ⋯ ‰ ‱ 6 ⋯=cdots
  
//   Ð À Ô Ư Á Ộ Ủ Ế Ã Ố Ấ Ớ
partial function_keys xkb_symbols "common_f1f12_viet_patch" {
    key.type="CTRL+ALT";     
    key <FK01> {[ U0111, U0110,  F1, F1, XF86_Switch_VT_1   ]}; // đĐ
    key <FK02> {[ U00E0, U00C0,  F2, F2, XF86_Switch_VT_2   ]}; // àÀ
    key <FK03> {[ U00F4, U00D4,  F3, F3, XF86_Switch_VT_3   ]}; // ôÔ
    key <FK04> {[ U01B0, U01AF,  F4, F4, XF86_Switch_VT_4   ]}; // ưƯ
                                                            
    key <FK05> {[ U00E1, U00C1,  F5, F5, XF86_Switch_VT_5   ]}; // áÁ
    key <FK06> {[ U1ED9, U1ED8,  F6, F6, XF86_Switch_VT_6   ]}; // ộỘ
    key <FK07> {[ U1EE7, U1EE6,  F7, F7, XF86_Switch_VT_7   ]}; // ủỦ
    key <FK08> {[ U1EBF, U1EBE,  F8, F8, XF86_Switch_VT_8   ]}; // ếẾ
                                                            
    key <FK09> {[ U00E3, U00C3,  F9, F9, XF86_Switch_VT_9   ]}; // ãÃ
    key <FK10> {[ U1ED1, U1ED0,  F10, F10, XF86_Switch_VT_10]}; // ốỐ
    key <FK11> {[ U1EA5, U1EA4,  F11, F11, XF86_Switch_VT_11]}; // ấẤ
    key <FK12> {[ U1EDB, U1EDA,  F12, F12, XF86_Switch_VT_12]}; // ớỚ
};    
    

// Полная раскладка (вариант ЛАТ)
default partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "lat" {        
    include "illinc(lat_88_testing)"    // 88-клавишный блок (tenkeyless) целиком (ralt = Alt (не переопределяется)) — вариант ЛАТ
    //include "illinc(common_88_multiply_patch)"            // ×⋅ на втором уровне линии Backspace, так как %& в 88 есть на первом
    //include "illinc(common_88_brackets_patch)"            // %& на втором уровне линии Backspace; [] на первом верхней линии
    include "illinc(common_mod_ralt_tilde_patch)"         // ralt = ~  и пробел на Shift+пробел
    //include "illinc(common_keypad_digits_orioks)"   
    include "illinc(common_keypad_digits)"   
       
    //include "illinc()"   
};

default partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "viet" {        
    include "illinc(lat)"   
    include "illinc(viet_patch)"   
};


// Полная раскладка (вариант РУС)
// Включаем lat, чтобы не дублировать третий и четвёртый (и все вышележащие) уровни + собственно замена двух нижних уровней на РУС
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "rus" { include "illinc(lat)" include "illinc(rus_alph_patch)" };

// ЛАТ + hex-цифры + цифровой блок ОРИОКС
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "altlat" { 
    include "illinc(lat)" 
    //include "srvr_ctrl(fkey2vt)"
    //include "illinc(common_f1f12_original_patch)" 
    include "illinc(common_88_hex_patch)" 
    include "illinc(common_keypad_digits_alt)"   
};

// РУС + hex-цифры + цифровой блок ОРИОКС
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "altrus" {
    //include "illinc(altlat)"  
    //include "illinc(rus_alph_patch)" 
    include "illinc(rus)" 
    include "illinc(common_f1f12_original_patch)" 
    //include "illinc(common_88_hex_patch)" 
    include "illinc(common_keypad_digits_alt)"   
};


// ЛАТ и РУС для консоли: с учётом отсутствия \[] на верхней линии — надо ли?.. \[] есть на 3 уровне, но ⇑3 работает только на последней раскладке
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "latcon" { 
    include "illinc(lat)" 
    // ⇑5 и ЛатВрем в консоли бесполезны; ЛАТ дублирует РУС, циклически переключая раскладки (с учётом привычки нажимать на него перед командой скорее вредит)
    key.type="FOUR_LEVEL";     
    replace key <BKSL> {[backslash,		backslash	]};	// \ \  
    replace key <RWIN> {[bracketleft,	U2206		]};	// [ ∆ 
    replace key <MENU> {[bracketright,	figdash		]};	// ] ‒ 
};
partial alphanumeric_keys function_keys keypad_keys modifier_keys xkb_symbols "ruscon" { include "illinc(latcon)" include "illinc(rus_alph_patch)" };

 //     replace key <RWIN> {
//         type="FOUR_LEVEL",
//         [ VoidSymbol, VoidSymbol,		Super_R, VoidSymbol	],
//         actions[Group1]=[ ],
//         actions[Group2]=[ SetGroup(group=-1), SetGroup(group=-1) ],
//         actions[Group3]=[ SetGroup(group=-2), SetGroup(group=-2) ],
//         actions[Group4]=[ SetGroup(group=-3), SetGroup(group=-3) ]
//     };

//     replace key <RWIN> {
//         type="FOUR_LEVEL",
//         [ VoidSymbol, VoidSymbol,		Super_R, VoidSymbol	],
//         actions[Group1]=[ SetGroup(group=4), SetGroup(group=4) ],
//         actions[Group2]=[ SetGroup(group=1), SetGroup(group=1) ],
//         actions[Group3]=[ SetGroup(group=1), SetGroup(group=1) ],
//         actions[Group4]=[ SetGroup(group=1), SetGroup(group=1) ]
//     };    

    
// Вариант ОРИОКС: цифры с точкой и литерой «н» 
    //replace key <KPSU> {[ bar,		U2099,			U2740,	U2740,			U207F,			U2099,			U2740,U2740	]}; // |ₙ ❀❀  ⁿₙ ❀❀
    //key <KPSU> {[ Cyrillic_en		]}; // н 


// RAlt как тильда + переопределение пробела (убираем тильду со второго уровня пробела)  ⇒  _patch в имени; применяется только поверх ЛАТ
partial modifier_keys xkb_symbols "common_mod_ralt_tilde_patch" {
    //replace key <RALT> { type="FOUR_LEVEL",	[ asciitilde,	nobreakspace,		nobreakspace, U202F ] }; // ~ nbsp nbsp ← EIGHT_LEVEL всё равно не получается
    // nobreakspace на 2 уровне — чтобы набирать аббревиатуры, не отпуская Shift; а вот 3‒4 не нужно; U202F (узкий неразрывный) есть на 6 уровне _ и там логичнее
    // replace key <RALT> { type="FOUR_LEVEL",	[ asciitilde,	nobreakspace,		space, space ] }; // ~ nbsp нормпробел нормпробел 
    // и FOUR_LEVEL не всегда 3‒4 работают
    replace key <RALT> { type="TWO_LEVEL",	[ asciitilde,	nobreakspace	]}; // ~ nbsp
    
//       replace key <RALT> {
//         type="FOUR_LEVEL",
//         symbols[Group1]=[ asciitilde,	nobreakspace,		space,	space ],
//         symbols[Group2]=[ asciitilde,	nobreakspace,		space,	space ],
//         symbols[Group3]=[ asciitilde,	nobreakspace,		space,	space ],
//         symbols[Group4]=[ asciitilde,	nobreakspace,		space,	space ]
//     }; 
    
    key <SPCE> { type="EIGHT_LEVEL",		[ space,		space			]}; //  пробел и на 2 уровне пробела, ибо тильда теперь есть на 1 уровне другой клавиши
};


// Скобки () на втором уровне линии Backspace, [] — на втором уровне F9‒F10: если SysRq и Insert нет/неудобно расположены и нет иных вариантов для скобок
partial alphanumeric_keys function_keys xkb_symbols "common_numericparentheses_patch" {
    key.type="EIGHT_LEVEL";     
    key <AE09> {[ question,		parenleft	]}; // ?( 
    key <AE10> {[ emdash,		parenright	]}; // —)        
    key <FK09> {[8, 		bracketleft		]}; // 8[   
    key <FK10> {[9,	 		bracketright	]}; // 9]  
};


partial function_keys xkb_symbols "common_f1f12_original_patch" {
    key.type="EIGHT_LEVEL";     
    key <FK01> {[ F1, F1   	]}; 
    key <FK02> {[ F2, F2	]}; 
    key <FK03> {[ F3, F3	]}; 
    key <FK04> {[ F4, F4	]}; 
    key <FK05> {[ F5, F5	]}; 
    key <FK06> {[ F6, F6	]}; 
    key <FK07> {[ F7, F7	]}; 
    key <FK08> {[ F8, F8	]};     
    key <FK09> {[ F9, F9 	]}; 
    key <FK10> {[ F10, F10	]}; 
    key <FK11> {[ F11, F11,		quotedbl,	quotedbl	]}; 
    key <FK12> {[ F12, F12,		equal,		equal		]}; 
};


не работает!!!
    // модификаторы справа
    replace key <RWIN> {
        type="FOUR_LEVEL",
        [ VoidSymbol, VoidSymbol,		Super_R, VoidSymbol	],
        actions[Group1]=[ SetGroup(group=1), SetGroup(group=1) ],
        actions[Group2]=[ SetGroup(group=1), SetGroup(group=1) ],
        actions[Group3]=[ SetGroup(group=1), SetGroup(group=1) ],
        actions[Group4]=[ SetGroup(group=1), SetGroup(group=1) ]
    };  

    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U27E1,			U208D,		U20DD	]}; // ( § ∞ 0   ⁽ ⟡ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U29B8,			U208E,		U20E0	]}; // ) № # 1   ⁾ ⦸ ₎ a⃠
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2220,		U2619	]}; // / < ≪ 2   ≮ ⊘ ∠ ☙
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U27A4,		U2767	]}; // * > ≫ 3   ≯ ⊛ ➤ ❧
    key <AE04> {[ minus,		U2212,			U2011,		4,			U2216,		U2296,			U2023,		hyphen	]}; // - − ‑ 4   ∖ ⊖ ‣ - 8=мягкий перенос (shy)
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2740,		U2740	]}; // : × ; 5   ⁝ ⊗ ❀ ❀
    key <AE06> {[ comma,		approxeq,		U02BC,		6,			minutes,	seconds,		U2034,		U2057	]}; // , ≈ ʼ 6   ′ ″ ‴ ⁗
    key <AE07> {[ period,		U22C5,			U2026,		7,			U22EF,		U2299,	enfilledcircbullet,	U0307	]}; // . ⋅ … 7   ⋯ ⊙ • ẋ 5 ⋯=cdots
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U202F,			U2740,		U2740	]}; // _ | ␣ 8       ❀ ❀ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		backslash,	9,			U2740,		U2740,			U2740,		U2740	]}; // ? & \ 9   ❀ ❀ ❀ ❀
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U2030,		U2031	]}; // — % – 0   ‒ ‰ ‰ ‱ 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // ! « „ ⚠   ⁻ ⊖ ₋ “
    key	<AE12> {[ plus,		guillemotright,	leftdoublequotemark,U2699,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // + » “ ⚙   ⁺ ⊕ ₊ ”
    
    //key <AE05> {[ colon, 		multiply,		U205D,		5,			U205D,		U2297,			U2740,		U2740	]}; // : × ⁝ 5   ⁝ ⊗ ❀ ❀
    //key <AE12> {[ semicolon,guillemotright,	leftdoublequotemark,plus,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // ; » “ +   ⁺ ⊕ ₊ ”


    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U27E1,			U208D,		U20DD	]}; // ( § ∞ 0   ⁽ ⟡ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U2740,			U208E,		U20E0	]}; // ) № # 1   ⁾ ❀ ₎ a⃠
    key <AE02> {[ slash,		less,			U226A,		2,			U226E,		U2298,			U2220,		U2619	]}; // / < ≪ 2   ≮ ⊘ ∠ ☙
    key <AE03> {[ asterisk,		greater,		U226B,		3,			U226F,		U229B,			U27A4,		U2767	]}; // * > ≫ 3   ≯ ⊛ ➤ ❧
    key <AE04> {[ minus,		U2212,			U2011,		4,			U207B,		U2296,			U208B,		hyphen	]}; // - − ‑ 4   ⁻ ⊖ ₋ - 8=мягкий перенос (shy)
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2740,		U2740	]}; // : × ; 5   ⁝ ⊗ ❀ ❀
    key <AE06> {[ comma,		approxeq,		U02BC,		6,			minutes,	seconds,		U2034,		U2057	]}; // , ≈ ʼ 6   ′ ″ ‴ ⁗
    key <AE07> {[ period,		U22C5,			U2026,		7,			U22EF,		U2299,	enfilledcircbullet,	U0307	]}; // . ⋅ … 7   ⋯ ⊙ • ẋ 5 ⋯=cdots
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U202F,			U2740,		U2740	]}; // _ | ␣ 8       ❀ ❀ 5=цфр, 6=тонк.нрз 
    key <AE09> {[ question,		ampersand,		backslash,	9,			U2216,		U29B8,			U2023,		U2740	]}; // ? & \ 9   ∖ ⦸ ‣ ❀
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U2030,		U2031	]}; // — % – 0   ‒ ‰ ‰ ‱ 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // ! « „ ⚠   ⁻ ⊖ ₋ “
    key	<AE12> {[ plus,		guillemotright,	leftdoublequotemark,U2699,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // + » “ ⚙   ⁺ ⊕ ₊ ”

    key <AD08> {[	h,		H,	bracketleft,	quotedbl,		U210D,		U274C,			U2095,		U02B0	]}; // h H [ "   ℍ ❌ ₕ ʰ
    key <AD09> {[	v,		V,	bracketright,	equal,			emptyset,	checkmark,		U1D65,		U1D5B	]}; // v V ] =   ∅ ✓ ᵥ ᵛ   5 ∅=пустое мн-во (void set)
    
    //key <AE05> {[ colon, 		multiply,		U205D,		5,			U205D,		U2297,			U2740,		U2740	]}; // : × ⁝ 5   ⁝ ⊗ ❀ ❀
    //key <AE12> {[ semicolon,guillemotright,	leftdoublequotemark,plus,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // ; » “ +   ⁺ ⊕ ₊ ”
    //replace key <INS> 	{	type="LOCAL_EIGHT_LEVEL", [ plus, Insert, Insert, Insert, 		Insert, Insert, Insert, Insert]	};  // + Ins

    key <AE09> {[ question,		ampersand,		equal,		9,			U2740,		U2740,			U2740,		U2740	]}; // ? & = 9   ❀ ❀ ❀ ❀
    key <AE09> {[ question,		ampersand,		equal,		9,			notequal,	U229C,			U2740,		U2740	]}; // ? & = 9   ≠ ⊜ ❀ ❀

    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2740,		U2740	]}; // $ $ η Η   ∋ ∌ ❀ ❀
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2216,		U29B8	]}; // $ $ η Η   ∋ ∌ ∖ ⦸   7 ∖=setminus

    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U2740,			U208E,		U20E0	]}; // ) № # 1   ⁾ ❀ ₎ a⃠
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U2605,			U208E,		U20E0	]}; // ) № # 1   ⁾ ★ ₎ a⃠
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U2699,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ⚙ ★ ᶻ
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U29B8,			U2216,		U1DBB	]}; // z Z ζ Ζ   ℤ ⦸ ∖ ᶻ   7 ∖=setminus

    key <AC10> {[asciicircum,asciicircum,Greek_psi,	Greek_PSI,	elementof,	notelementof,	U2740,		U0302	]}; // ^ ^ ψ Ψ   ∈ ∉ ❀ x̂   8 ̂=комб. 
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2740,		U2740	]}; // $ $ η Η   ∋ ∌ ❀ ❀
    key <AC10> {[asciicircum,asciicircum,Greek_psi,	Greek_PSI,	elementof,	notelementof,	U2764,		U0302	]}; // ^ ^ ψ Ψ   ∈ ∉ ❤ x̂   8 ̂=комб. 
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U263A,		U2639	]}; // $ $ η Η   ∋ ∌ ☺ ☹
    
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U29B8,			U2216,		U1DBB	]}; // z Z ζ Ζ   ℤ ⦸ ∖ ᶻ   7 ∖=setminus (за исключ-м)
    key <AD10> {[	z,		Z,	Greek_zeta,		Greek_ZETA,		U2124,		U27E1,			U2605,		U1DBB	]}; // z Z ζ Ζ   ℤ ⟡ ★ ᶻ
    
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		infinity,	U229A, 	enfilledcircbullet,	U1D47	]};	// b B β Β   ∞ ⊚ • ᵇ
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 	enfilledcircbullet,	U1D47	]};	// b B β Β   ∖ ⦸ • ᵇ   5 ∖=setminus (без)
    
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U229A,			U208D,		U20DD	]}; // ( § ∞ 0   ⁽ ⊚ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U26A0,			U208E,		U20E0	]}; // ) № # 1   ⁾ ⚠ ₎ a⃠
   
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U229D,			U208E,		U20E0	]}; // ) № # 1   ⁾ ⊝ ₎ a⃠
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U29B6,			U208E,		U20E0	]}; // ) № # 1   ⁾ ⦶ ₎ a⃠
    
минус в круге ↓↓↓    
    key <AE04> {[ minus,		U2212,			U2011,		4,			U207B,		U2296,			U208B,		hyphen	]}; // - − ‑ 4   ⁻ ⊖ ₋ - 8=мягкий перенос (shy)
    key <AE04> {[ minus,		U2212,			U2011,		4,			U207B,		U229D,			U208B,		hyphen	]}; // - − ‑ 4   ⁻ ⊝ ₋ - 8=мягкий перенос (shy)
дефис в круге ↑↑↑
    
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U207B,		U2296,	U208B,leftdoublequotemark	]}; // ! « „ ⚠   ⁻ ⊖ ₋ “
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2740,		U2740,	U2740,leftdoublequotemark	]}; // ! « „ ⚠   ❀ ❀ ❀ “
    
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U202F,			U2740,		U2740	]}; // _ | ␣ 8       ❀ ❀ 5=цфр, 6=тонк.нрз 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2740,		U2740,	U2740,leftdoublequotemark	]}; // ! « „ ⚠   ❀ ❀ ❀ “
    
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U202F,			U2225,		U2226	]}; // _ | ␣ 8       ∥ ∦ 5=цфр, 6=тонк.нрз 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2220,		U2221,	U27C2,leftdoublequotemark	]}; // ! « „ ⚠   ∠ ∡ ⟂ “
    
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U22A3,				U2466,	U2150,		U2735		]}; // 7 ⌉ ₇ ⁷    ⊣ ⑦  ⅐ ✵ 
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2740,				U2468,	U2151,		U2738		]}; // 9 ∓ ₉ ⁹    ❀ ⑨  ⅑ ✸
    
    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U1D62,				Print,		Sys_Req		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ᵢ PS SR
  
    replace key <PRSC> {[backslash, backslash,		U1D62,	U2071,			U2071,	U1D62,				Print,		U2735		],        
        actions[Group1]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group2]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group3]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()],
        actions[Group4]= [NoAction(),LockGroup(group=3), NoAction(),NoAction(),		NoAction(),NoAction(), NoAction(),NoAction()]};	// \ \ ᵢ ⁱ    ⁱ ᵢ PS ✵
    
    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U2220,		U226A	]}; // / < \ 2   ≮ ⊘ ∠ ≪
    key <AE03> {[ asterisk,		greater,		quotedbl,	3,			U226F,		U229B,			U27A4,		U226B	]}; // * > " 3   ≯ ⊛ ➤ ≫    
    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U226A,		U22D8	]}; // / < \ 2   ≮ ⊘ ≪ ⋘
    key <AE03> {[ asterisk,		greater,		quotedbl,	3,			U226F,		U229B,			U226B,		U22D9	]}; // * > " 3   ≯ ⊛ ≫ ⋙
     
    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U279B,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➛ x⃗
    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U27A4,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➤ x⃗
    
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U2030,		U2031	]}; // — % – 0   ‒ ‰ ‰ ‱ 
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U27A4,		U2031	]}; // — % – 0   ‒ ‰ ➤ ‱ 
   
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2740,		U2740	]}; // : × ; 5   ⁝ ⊗ ❀ ❀
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2735,		U273A	]}; // : × ; 5   ⁝ ⊗ ✵ ✺
    
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 	enfilledcircbullet,	U1D47	]};	// b B β Β   ∖ ⦸ • ᵇ   5 ∖=setminus (без)
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 			U2740,		U1D47	]};	// b B β Β   ∖ ⦸ ❀ ᵇ   5 ∖=setminus (без)
   
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2226,			U209A,		U1D56	]}; // p P π Π   ∏ ∦ ₚ ᵖ   
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U27C2,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ⟂ ₜ ᵗ
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2740,			U209A,		U1D56	]}; // p P π Π   ∏ ❀ ₚ ᵖ   
    key <AB06> {[	t,		T,		Greek_tau,	Greek_TAU,		U1D40,		U2740,			U209C,		U1D57	]}; // t T τ Τ   ᵀ ❀ ₜ ᵗ
    
    
    
    
    
    key <FK12> {[equal,		notequal,	U208C,			U207C,				U26A1,				U229C,	U2261,		U2262		]}; // = ≠ ₌ ⁼    ⚡ ⊜  ≡ ≢  
    
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 			U2738,		U1D47	]};	// b B β Β   ∖ ⦸ ✸ ᵇ   5 ∖=setminus (без)
    
    
    
    key <AE09> {[ question,		ampersand,		equal,		9,			U2740,		U2740,			U2740,		U2740	]}; // ? & = 9   ❀ ❀ ❀ ❀
    key	<AE12> {[ plus,		guillemotright,	leftdoublequotemark,U2699,	U207A,		U2295,	U208A,rightdoublequotemark	]}; // + » “ ⚙   ⁺ ⊕ ₊ ”
    
    
    
    
    
    
    
    
    
    
    
Исключаемые символы:


    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U229A,			U208D,		U20DD	]}; // ( § ∞ 0   ⁽ ⊚ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U29B6,			U208E,		U20E0	]}; // ) № # 1   ⁾ ⦶ ₎ a⃠
⊚⦶ — ни разу не понадобились; понадобятся — вернуть    
    key <TLDE> {[ parenleft,	section,		infinity,	0,			U207D,		U2740,			U208D,		U20DD	]}; // ( § ∞ 0   ⁽ ❀ ₍ a⃝
    key <AE01> {[ parenright,	numerosign,		numbersign,	1,			U207E,		U2740,			U208E,		U20E0	]}; // ) № # 1   ⁾ ❀ ₎ a⃠

    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U226A,		U22D8	]}; // / < \ 2   ≮ ⊘ ≪ ⋘
    key <AE03> {[ asterisk,		greater,		quotedbl,	3,			U226F,		U229B,			U226B,		U22D9	]}; // * > " 3   ≯ ⊛ ≫ ⋙
в переписке надёжнее писать <<>> и <<<>>>, в LaTeX есть команды
    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U2740,		U2740	]}; // / < \ 2   ≮ ⊘ ❀ ❀
    key <AE03> {[ asterisk,		greater,		quotedbl,	3,			U226F,		U229B,			U2740,		U2740	]}; // * > " 3   ≯ ⊛ ❀ ❀
???    
    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U21A9,		U2740	]}; // / < \ 2   ≮ ⊘ ↩ ❀
    key <AE03> {[ asterisk,		greater,		quotedbl,	3,			U226F,		U229B,			U21AA,		U2740	]}; // * > " 3   ≯ ⊛ ↪ ❀
???
    key <AE03> {[ asterisk,		greater,		asterisk,	3,			U226F,		U229B,			U21AA,		U2740	]}; // * > * 3   ≯ ⊛ ↪ ❀
    key <AE03> {[ asterisk,		greater,		U27A4,		3,			U226F,		U229B,			U21AA,		U2740	]}; // * > ➤ 3   ≯ ⊛ ↪ ❀
    key <AE03> {[ asterisk,		greater,		U2740,		3,			U226F,		U229B,			U21AA,		U2740	]}; // * > ❀ 3   ≯ ⊛ ↪ ❀

    key <AE02> {[ slash,		less,			backslash,	2,			U226E,		U2298,			U21A9,		U2740	]}; // / < \ 2   ≮ ⊘ ↩ ❀
    key <AE03> {[ asterisk,		greater,		U2740,		3,			U226F,		U229B,			U21AA,		U2740	]}; // * > ❀ 3   ≯ ⊛ ↪ ❀
    
    
    
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2220,		U2221,	U27C2,leftdoublequotemark	]}; // ! « „ ⚠   ∠ ∡ ⟂ “
∡ — ни разу не понадобился и не похож на знак угла с дугой
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2220,		U2740,	U27C2,leftdoublequotemark	]}; // ! « „ ⚠   ∠ ❀ ⟂ “
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2220,		U26A0,	U27C2,leftdoublequotemark	]}; // ! « „ ⚠   ∠ ⚠ ⟂ “
    
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U21A9,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ↩ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21AA,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↪ ʷ
↩↪ — ни разу не понадобились; понадобятся — вернуть    
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U2740,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ❀ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21A6,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↦ ʷ
знак юаня и иены — один символ из Latin-1 punctuation and symbols, причём в китайских и японских текстах пишется не он, а иероглиф; различение — JP¥50 и CN¥ 50
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U00A5,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ¥ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21A6,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↦ ʷ


    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U21A6,			U2092,		U1D52	]}; // o O ω Ω   𝕆 ↦ ₒ ᵒ   6 ↦=отображается в
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U1F12F,			U2097,		U02E1	]}; // l L λ Λ   ℓ 🄯 ₗ ˡ   6 🄯=копилефт
↦ (отображается в) — не ассоциируется с oO; парная ↤ — не используется
копилефт — не отображается ни в одном употребительном шрифте
    key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U20B9,			U209A,		U1D56	]}; // p P π Π   ∏ ₹ ₚ ᵖ   
    key <AC06> {[	r,		R,		Greek_rho,	Greek_RHO,		U211D,		U20BD,			U1D63,		U02B3	]}; // r R ρ Ρ   ℝ ₽ ᵣ ʳ
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U20AC,			U2092,		U1D52	]}; // o O ω Ω   𝕆 € ₒ ᵒ   
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U2113,			U2097,		U02E1	]}; // l L λ Λ   ℓ ℓ ₗ ˡ  
???
    key <AC07> {[	o,		O,		Greek_omega,Greek_OMEGA,	U1D546,		U2740,			U2092,		U1D52	]}; // o O ω Ω   𝕆 ❀ ₒ ᵒ   
    key <AC08> {[	l,		L,		Greek_lamda,Greek_LAMDA,	U2113,		U2740,			U2097,		U02E1	]}; // l L λ Λ   ℓ ❀ ₗ ˡ  
    
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		copyright,		U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ © ⤉ ᶜ
копирайт без копилефта не хочется    

    
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,	partdifferential,	U2300,		U1D48 	]}; // d D δ Δ   ∆ ∂ ⌀ ᵈ   5 ∆=приращение, 8 ⌀=диаметр 
∂⌀ — непонятно, в каком порядке на 5—6; ∂ не ассоциируется с 6 уровнем
???⌀ больше похож на «форму в круге»???
    key <AC09> {[	d,		D,		Greek_delta,Greek_DELTA,	U2206,		U2300,	partdifferential,	U1D48 	]}; // d D δ Δ   ∆ ⌀ ∂ ᵈ   5 ∆=приращение, 6 ⌀=диаметр 


    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U279B,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ➛ x⃗
➛ — ни разу не понадобился
    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U2740,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ❀ x⃗
    key <AB01> {[	at,		at,		U2194,		ifonlyif,		U21AE,		U21CE,			U2764,		U20D7	]}; // @ @ ↔ ⇔   ↮ ⇎ ❤ x⃗


    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,U2766		]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL ❦
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,			Scroll_Lock,partdifferential]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL ∂
    replace key <SCLK> {[bracketleft, U2206,		U2C7C,	U02B2,			U02B2,	U2C7C,				Scroll_Lock,U2740		]};	// [ ∆ ⱼ ʲ    ʲ ⱼ SL ❀

    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U27E1		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ⟡
⟡ есть на зЗzZ, под ★ — возможно, проще запомнить на одном месте
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U2766		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ❦

Вообще убрать ❦❧☙ и ✵✺???
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U2766		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ❦
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U22A3,				U2466,	U2150,		U2767		]}; // 7 ⌉ ₇ ⁷    ⊣ ⑦  ⅐ ❧ 
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2740,				U2468,	U2151,		U2619		]}; // 9 ∓ ₉ ⁹    ❀ ⑨  ⅑ ☙
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2735,		U273A	]}; // : × ; 5   ⁝ ⊗ ✵ ✺
???
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,	infinity,	U2740		]}; // 0 ⩽ ₀ ⁰  vt1 ⓪ ∞ ❀
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U22A3,				U2466,	U2150,		U2740		]}; // 7 ⌉ ₇ ⁷    ⊣ ⑦  ⅐ ❀ 
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2740,				U2468,	U2151,		U2740		]}; // 9 ∓ ₉ ⁹    ❀ ⑨  ⅑ ❀
    key <AE05> {[ colon, 		multiply,		semicolon,	5,			U205D,		U2297,			U2740,		U2740	]}; // : × ; 5   ⁝ ⊗ ❀ ❀
    
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U22A3,				U2466,	U2150,		U27C2		]}; // 7 ⌉ ₇ ⁷    ⊣ ⑦  ⅐ ⟂ 

Убрать ❤ и ☺☹???
    key <AC10> {[asciicircum,asciicircum,Greek_psi,	Greek_PSI,	elementof,	notelementof,	U2764,		U0302	]}; // ^ ^ ψ Ψ   ∈ ∉ ❤ x̂   8 ̂=комб. 
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U263A,		U2639	]}; // $ $ η Η   ∋ ∌ ☺ ☹
вместо ☺☹ я использую :—)
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			U2740,		U2740	]}; // $ $ η Η   ∋ ∌ ❀ ❀
    key <AC11> {[dollar,	dollar,		Greek_eta,	Greek_ETA,	containsas,	U220C,			EuroSign,	U2740	]}; // $ $ η Η   ∋ ∌ € ❀

    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U27A4,		U2031	]}; // — % – 0   ‒ ‰ ➤ ‱ 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U26A0,	U2220,		U26A0,	U27C2,leftdoublequotemark	]}; // ! « „ ⚠   ∠ ⚠ ⟂ “
   
    
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U202F,			U2225,		U2226	]}; // _ | ␣ 8       ∥ ∦ 5=цфр, 6=тонк.нрз 
вместо ∥ я использую ||, чтобы уж точно отобразилось
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U2740,			U202F,		U2226	]}; // _ | ␣ 8     ❀   ∦ 5=цфр, 7=тонк.нрз 
    key <AE08> {[ underscore,	bar,			U2423,		8,			U2007,		U2740,			U202F,		U2740	]}; // _ | ␣ 8     ❀   ❀ 5=цфр, 7=тонк.нрз 
    
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		ssharp,			U209B,		U02E2	]}; // s S σ Σ   ∑ ß ₛ ˢ
ß — ни разу не понадобился
    key <AB03> {[	s,		S,		Greek_sigma,Greek_SIGMA,	U2211,		U2740,			U209B,		U02E2	]}; // s S σ Σ   ∑ ❀ ₛ ˢ

    key <AE09> {[ question,		ampersand,		U2699,		9,			U2740,		U2740,			U2740,		U2740	]}; // ? & ⚙ 9   ❀ ❀ ❀ ❀    
    key <AE10> {[ emdash,		percent,		endash,		0,			figdash,	U2030,			U2031,		U2031	]}; // — % – 0   ‒ ‰ ‱ ‱ 
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,U27A4,	U2220,		U26A0,	U27C2,leftdoublequotemark	]}; // ! « „ ➤   ∠ ⚠ ⟂ “
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,quotedbl,U2220,		U26A0,	U27C2,leftdoublequotemark	]}; // ! « „ "   ∠ ⚠ ⟂ “
    key <AE11> {[ exclam,	guillemotleft,	doublelowquotemark,quotedbl,U2740,		U26A0,	U2740,leftdoublequotemark	]}; // ! « „ "   ❀ ⚠ ❀ “
    
    
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 			U26A1,		U1D47	]};	// b B β Β   ∖ ⦸ ⚡ ᵇ   5 ∖=setminus (без)
⚡ — ни разу не понадобился
    key <AB08> {[	b,		B,		Greek_beta,	Greek_BETA,		U2216,		U29B8, 			U2740,		U1D47	]};	// b B β Β   ∖ ⦸ ❀ ᵇ   5 ∖=setminus (без)

    key <AB07> {[   x,		X,		Greek_xi,	Greek_XI,		multiply,	U2297,			U2093,		U02E3	]}; // x X ξ Ξ   × ⊗ ₓ ˣ
×⊗ — теперь есть над : (us-5)
