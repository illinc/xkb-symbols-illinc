Неактуальное, но оставлено для справки 
(в том числе команды и ценные комментарии)

// Клавиши на мышином боку ****************************************************

// setxkbmap -device $(xinput list | sed -n 's/.*Naga.*id=\([0-9]*\).*keyboard.*/\1/p') "illinc(naga_keypad)"

// naga_keyboard_id=$(xinput list | sed -n 's/.*Naga.*id=\([0-9]*\).*keyboard.*/\1/p')
// for i in ${naga_keyboard_id} ; do  
// # раскладка клавиатуры только для мыши (на боку)
//   setxkbmap -device ${i} "illinc(naga_keypad)"  
// done
// 
// naga_mouse_id=$(xinput list | sed -n 's/.*Naga.*id=\([0-9]*\).*pointer.*/\1/p')
// for i in ${naga_mouse_id} ; do  
// # переназначение верхней передней (9) кнопки мыши на среднюю (2)
//   xinput set-button-map "${i}" 1 2 3 4 5 6 7 8 2 ; 
// done

partial alphanumeric_keys
xkb_symbols "naga_keypad" {
    key.type[Group1]="ONE_LEVEL";
    // на мыши не keypad, а AE01-AE12

    key <AE01>   { [	XF86Launch1 	] };
    key <AE02>   { [	XF86Launch2 	] };
    key <AE03>   { [	XF86Forward	] };
    key <AE04>   { [	XF86Launch4 	] };
    key <AE05>   { [	XF86Launch5 	] };
    key <AE06>   { [	XF86Back	] };
    key <AE07>   { [	XF86Launch7	] };
    key <AE08>   { [	XF86Launch8	] };
    key <AE09>   { [	SunFront	] };
    key <AE10>   { [	XF86LaunchA	] };
    key <AE11>   { [	XF86LaunchB 	] };
    key <AE12>   { [	SunProps 	] };

};


// Можно на F1-F12 в Chromium:  _:; ? %&«» /"' + `(grave) <>{}[]| ~ ,. × 0123456789 =\
// нельзя на F1-F12 в Chromium: ^ ! —(тире) „” * @# −(минус ширины +) №§ ←→ ()
// так что текущий вариант с 0123456789«» на F1-F12 нормально работает
// в других приложениях вроде вообще всё работает, в основном блоке в Chromium — тоже
// ↑ нельзя — на ПЕРВОМ уровне, на втором — работает


Ни в Firefox, ни в Chromium, ни в Inkscape не вводится знак промилле permille ‰: ни пятым уровнем «:» (в Firefox отображается «:», хотя на соседних клавишах работает), ни вторым уровнем %. Скопировать из другого приложения можно. Знак просантимилле ‱ шестым уровнем «:» вводится нормально.

Возможно, дело в самом символе ‰: в таблице Unicode так же отображается U0089 «Установка позиций и выравнивания горизонтальной табуляции» (но в /usr/include/X11/keysymdef.h permille — это U2030)?

А вот U2030, как ни странно, пропечатывается!

С identical тоже проблемы → U2261; notidentical нормально, но для симметрии → U2262
signifblank → U2423


// ****************************************************************************
Символы, которых нет в раскладке (хотя такая возможность рассматривалась)
+ символы, которые кочуют с клавиши на клавишу
+ символы, которые, возможно, вскоре будут удалены из раскладки
// ****************************************************************************
Поиск U-кода по символу: https://symbl.cc/ru/
символ вводить в строку поиска наверху
// ****************************************************************************
// Имена клавиш из файла /usr/include/X11/keysymdef.h
// при этом префис XK_ опускать не просто можно (как сказано в документации), а НУЖНО (с ним не работает)!
// ****************************************************************************

Отброшенные варианты ❌: ⨂✖❎🞪🞭⏹🛑🚫🛇⛔
    U26D4 ⛔ символ «Стоп», по идее красный и круглый; но он то кривой, то пробел после себя ест, то высоту строки портит
или невыразительны, или отображаются ещё кривее ❌ и часто портят следующую строку

Полукруглая шпация en space U2002 отображается шире nobreakspace U00A0; 
а тонкая шпация U2009 (thinspace) — почти такой же ширины, как nobreakspace (но чуть-чуть всё же тоньше).
Кроме того, thinspace разрывен, а Узкий неразрывный пробел U202F — нет; и U202F отображается чуть-чуть тоньше thinspace.
** — без пробела
* * — U+2000 en quad
* * — U+2001 em quad
* * — U+2002 en space
* * — U+2003 em space
* * — U+2004 1/3 em
* * — U+2005 ¼ em
* * — U+2006 1/6 em
* * — U+2007 цифровая шпация
* * — U+2008 пунктуационный пробел
* * — U+2009 тонкий пробел
* * — U+200A волосяная шпация
* * — U+202F тонкий неразрывный пробел
* * — U+00A0 неразрывный пробел
См. в LibreOffice — там они разные.
Самые тонкие (по 30 пробелов, 31×*): 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * — U+200A волосяная
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * — U+2009
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * — U+202F
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * — U+00A0
Ширина U2009 и U202F совпадает.


Дефисы и минусы:
- Дефисоминус вычесть U+002D ← есть; вообще есть вся печатная ASCII
‐ Дефис U2010 [на вид ничем не отличается от дефисоминуса -‐] ← нет в раскладке 
‑ Неразрывный дефис U2011 [на вид тоже не отличается, но в LO подсвечивается и не переносится]

Точка
· Точка по центру U+00B7 Дополнение к латинице — 1; «маркер списка, разделитель слов, знак умножения».
∙ Оператор буллит U+2219. Математические операторы; «скалярное умножение, знак умножения, умножить».
⋅ Dot Operator U+22C5. Математические операторы; без пояснений.

«...a\cdot b. В некоторых шрифтах 22С5 на более высокой позиции в строке (используется math centerline вместо text baseline), также может быть кругом (тогда как начертания 00B7 могут быть разными в разных шрифтах) или отображаться квадратной точкой. Юникод отмечает 22С5 более предпочтительным в математическом использовании[4], по сравнению с 00B7. В ISO 80000-2:2009 входило нормативное приложение Annex A "Clarification of the symbols used" с таблицей A.1 в которой для умножения (2-9.5) был явно указан "dot operator 22C5". В ISO 80000-2:2019 такого приложения уже не содержалось. Действующий ГОСТ 54521 содержит такую таблицу определяющую символ точки как 22C5[1].»

Индексы:
₀₁₂₃₄₅₆₇₈₉⁰¹²³⁴⁵⁶⁷⁸⁹ — на 3‒4 уровнях F1‒F10, на 2 и 5 уровнях цифрового блока;
⏨ — на 3 уровне F11;
ₙᵢⱼₖⁿⁱʲᵏ — на 7‒8 уровнях букв, на 3‒4 уровнях F12, SysRq и #, ScrollLock и Break;
ⱼᵤₖₑₙₕᵥ₍₎ₐₚᵣₒₗₛₘᵢₜₓʲᶜᵘᵏᵉⁿᵍʰᵛᶻᶠʸʷᵃᵖʳᵒˡᵈˢᵐⁱᵗˣᵇ — на 7‒8 уровнях соответствующих букв;
⁽⁾⁻⁺  ₋₊₍₎ 
₌⁼ U208C,U207C

Маркеры списка: •➤‣ на 7 уровне БЖЭ.

Символы в круге и в квадрате: 
⊕⊖⊘⊛⊜⊗ есть; сомнительный ⊚ — оператор-бублик (Кольцевой оператор в круге U229A) то есть, то нет:
если у арифметических операторов в круге есть смысл по умолчанию — подобнные им операторы в нестандартной алгебре, то ⊚ — это «special-defined operation»
⊝ Дефис в круге U229D
⊞ Плюс в квадрате U229E ⊟ Минус в квадрате U229F ⊠ Произведение в квадрате U22A0 ⊡ Оператор точка в квадрате U22A1

Треугольники:
◁ U25C1 ▷ U25B7 ← ни на 6 уровне «», ни на 7 уровне ЖЭ не выглядят органично; ни разу не понадобились
⊲ ⋪ ⊴  U22B2,		U22EA,		U22B4
⊳ ⋫ ⊵  U22B3,		U22EB,		U22B5
U25B3 △ Белый треугольник с вершиной вверх U+25B3 ← в таблице unicode никакой семантики у этого значка нет
△ как симметрическая разность множеств — на М
но в словаре Зализняка «За этим знаком приводятся морфологически нерегулярные формы (т. е. те, которые отклоняются от общих правил склонения и спряжения, содержащихся в разделах о значении индексов, стр. 25 и 77)»

⟡ U27E1 Незакрашенный ромб с вогнутыми сторонами U+27E1 ← аналогично, в unicode никакой конкретной семантики нет
мною используется в смысле «пример»
— логическое «никогда», поэтому долго был над ∅ — но сейчас там мешает
— у того же Зализняка «За этим знаком приводятся словосочетания (чаще всего фразеологизмы), в которых какая-либо форма заглавного слова отклоняется от своего нормального вида (или представлена только в одном виде, тогда как в общем случае возможны варианты).
⟨...⟩ За знаком ⟡ приводятся также устойчивые сочетания, в которых сохранились ныне отсутствующие в парадигме формы»
— также встречается как маркер списка и просто как «обрати внимание»
↑↑↑ так что не только «никогда» и не только логическое

«»„“‘’ Кавычки, обсуждение https://yalov.github.io/yarbur/quotation-mark
Обязательно нужны (и есть) кавычки:
— русско-французские «ёлочки» guillemotleft/guillemotright;
— русско-немецкие „лапки“ (внутренние) doublelowquotemark/leftdoublequotemark (в соответствии с английской типографикой — РУССКАЯ ПРАВАЯ лапка описана как ЛЕВАЯ);
нет/нестабильно входящие кавычки:
— американские “лапки” (английские внутренние) leftdoublequotemark/rightdoublequotemark;
— английские одинарные ‘марровские’ (американские внутренние) U2018/U2019 leftsinglequotemark/rightsinglequotemark.

‘’ на 5 (закрывающая’, по мнению консорциума Unicode, — более правильный апостроф, чем апостроф-букваʼ), “” на 8 уровне над «»;
пара „” doublelowquotemark/rightdoublequotemark, входившая ранее в раскладку — польские кавычки.


Штрихи °′″‴⁗:
°′″‴ есть, четверного нет в раскладке 
нет обратных (не понадобились):
‵ Обратный штрих U+2035
‶ Обратный двойной штрих U+2036
‷ Обратный тройной штрих U+2037
четверного обратного не нашлось; да и четверной прямой не такой, как три первых.
  
Есть grave — ` Апостроф U+0060 (Grave Accent).
Отображается `‵ немного отлично от обратного штриха: grave→`‵←U2035. Но почти так же.

Про апостроф и кавычки: https://yalov.github.io/yarbur/quotation-mark
На 'Ъ фигурный апостроф U02BC (а не rightsinglequotemark) на 3 уровне (на 4 — ASCII-апостроф)


∖ Разность множеств U2216: должна бы быть на 5 уровне \, но отображается либо как backslash \, либо некрасиво → 7 уровень

⁝ Вертикальное троеточие — над двоеточием :%5; варианты:
U205D ⁝ Тройное двоеточие. Основная пунктуация — считается устаревшим, нормально отображается; ← есть
U22EE ⋮ (Математические операторы) — отображается, но означает вертикальный пропуск в матрице, а не делимость ← нет в раскладке
U2AF6 ⫶ (Дополнительные математические операторы) «кратно, делится на» — но не отображается ни в шрифтах Liberation/Deja Vu, ни в случайно натыканных

➛ U279B красивая стрелка; есть только вправо

~~~ Очень хочется ставить ∾ вместо ~ как «эквивалентно», но это неправильно: inverted lazy s operator indicating logical negation (дублирует ¬)
∼ Оператор тильда U+223C выглядит как обычная ~ «Тильда примерно» U+007E
∿ Синусоидальная волна U+223F — это вообще не то
в unicode нет красивой тильды, как \sim в LaTeX

≍≍≍ Нет пары: U224D ≍ Эквивалентно и U226D ≭ Не эквивалентно, так как ни одной группе ≍ не понравилось (понравилось ~)

≺≺≺≻≻≻ Сравнение:
≤≥ и ≰≱ американское начертание (Математические операторы) — на 3 и 4 уровнях ЮЁ;
≺≻ и ⊀⊁ птички (Математические операторы) — на 5 и 6 уровне ЮЁ ⇒ на 7 помесь 3 и 5 ≾≿ (Математические операторы);
перечёркнутых ≾≿ для 8 уровня нет в этом разделе unicode (и вообще не нашлось);

⩽⩾ (русское начертание) из раздела Дополнительные математические операторы — на 2 уровне линии Esc, как основной вариант;
перечёркнутых таких нет.

notapproxeq — это ≇, а не ≉, хотя approxeq ≈

≟ U225F	— символы <> со знаком «?» в Unicode есть, но не отображаются: ⩻ ≡ <? U2A7B, ⩼ ≡ >? U2A7C

≃ U2243 Асимптотически равно (Математические операторы).

≅ U2245 Конгруэнтность (геометрическое равенство) (Математические операторы)
перечёркивание ≇ U2247 Ни приблизительно, ни фактически равный (и соответствует notapproxeq)


Копилефт и копирайт: сомнительно; если найдётся другой символ для 6 уровня C — убрать оба;
© U00A9		Copyright Sign[1]   	Unicode Version:	1.1 (June 1993)[2]
🄯 U1F12F	Copyleft Symbol[1]	Unicode Version:	11.0 (June 2018)[2] 🄯 
↑↑↑ копилефт не отображается ни здесь, ни в браузере, а «непарный» копирайт включать не хочется

❓❔？⍰ знаки вопроса, которые не ASCII-?
ꜝ  UA71D
˙  U02D9
˂˃ U02C2 и U02C3


Комбинируемые символы:
— ударе́ние-на-си́мволе-пе́ред U0301 (не acute, тот не комбинируется) — иногда отображается над символом *после* ↓↓↓;
— предыдущий-с-тильдой x̃ — вообще не так отображается.

Комбинируемые ударение а́ и производная по t ẋ некорректно отображаются (могут переползти на следующий символ)
— для небуквенных символов (в том числе пробела и цифр): 4́́=комб.ударение, 5̇=комб. производная по t (но это ладно);
— для высоких букв: l̇, ℓ̇̇, А́стра, ё́лка (!!!).
Пусть пока будут, но — сомнительно и, вероятно, будут удалены из раскладки.

А из строки комбинируемый символ удалить можно только вместе с несколькими соседними! Особенно если он уползает с предыдущего на следующий.

Комбинируемое перечёркивание: горизонтальное x̶ съезжает почти везде, но во многих шрифтах выглядит прилично;
диагональные  U+0337 и  U+0338  тоже съезжают, но из-за их формы выглядит это хуже.
перечёркивание с кругом a⃠ U20E0 и обводка a⃝ U20DD на 8 и 7 уровнях # (сомнительно).

// Стрелки на стрелках ********************************************************
// Стрелки на 3 уровне — символы стрелок
// ни одна комбинация с Shift печатного символа не выдаёт, даже если там вписать печатный символ uparrow/leftarrow/... (просто Shift — выделение, 3+Shift и 5+Shift — просто смещение)
// поэтому пусть лучше на этих уровнях будут умолчальные смещения Up/Left/... (тогда x+Shift+стрелка = выделение)
// на уровнях 3, 5 и 3+5 — в некоторых приложениях выдаётся символ uparrow/leftarrow/... вместо смещения (но не во всех)
// названия клавиш /usr/share/X11/xkb/symbols/pc
//
// В LibreOffice не работают (точнее, срабатывают как стрелки);
// в Chromium — тоже;
// в xterm, kate, sylpheed печатаются символы
// 
// так что в алфавитном блоке на CYQW удобнее
partial alphanumeric_keys
xkb_symbols "drawable_arrows_lvl3" {
    key.type[group1]="EIGHT_LEVEL_ALPHABETIC";
    //							Shift	3			3+Shift	5			5+Shift	5+3				  3, 5	3+5
    key   <UP> {	[  Up,		Up,		uparrow,	Up,		uparrow,	Up,		U21D1	]	}; // ↑		⇑
    key <LEFT> {	[  Left,	Left,	leftarrow,	Left,	leftarrow,	Left,	U21D0	]	}; // ←		⇐
    key <DOWN> {	[  Down,	Down,	downarrow,	Down,	downarrow,	Down,	U21D3	]	}; // ↓		⇓
    key <RGHT> {	[  Right,	Right,	rightarrow,	Right,	rightarrow,	Right,	implies	]	}; // →		⇒
};

// для IceWM можно в файле ~/.icewm/keys задать сочетания клавиш (после сохранения ~/.icewm/keys нужен перезапуск IceWM):
// key "Ctrl+ISO_Last_Group"			setxkbmap "illinc(lat),illinc(rus)"
// key "Ctrl+ISO_First_Group"			setxkbmap "illinc(lat),illinc(rus)"
// key "Alt+ISO_Last_Group"			setxkbmap "illinc(lat_88_text),illinc(rus_88_text)"
// key "Alt+ISO_First_Group"			setxkbmap "illinc(lat_88_text),illinc(rus_88_text)"
// то есть Alt+РУС/ЛАТ → РУС/ЛАТ в режиме текста, Ctrl+РУС/ЛАТ → РУС/ЛАТ в режиме LaTeX 
// исходный смысл ISO_Last_Group/ISO_First_Group при этом также отрабатывается
// Без Alt/Ctrl включатели РУС/ЛАТ включают соответствующий вариант текущей раскладки, не переключаясь между режимом текста и LaTeX
// ↑ отдельный режим текста удалён, но сочетания, возможно, ещё понадобятся



//     // А ещё на кафедре есть круглая кнопка Sleep над стрелками...
// 	key <I150>   {      [ plus             ]       };
//     // В файле /usr/share/X11/xkb/symbols/inet: key <I150>   {      [ XF86Sleep             ]       };
//     // там же есть key <I16>	{	[ XF86Sleep		]	};, но это не она ← см. код 150 в xev
//     // почему-то работает только на русской раскладке...
//     
//     // При этом компьютер всё ещё уходит в спящий режим по нажатию этой кнопки → обрабатывается уровнем ниже    
//     // Отменить (источник: https://www.raspberrypi.org/forums/viewtopic.php?t=261257  Disable Keyboard Power Button):
//     // sudo nano /etc/systemd/logind.conf
//     // добавить HandleSuspendKey=ignore
// 	// sudo systemctl restart systemd-logind
// 	// После перезапуска systemd-logind компьютер не засыпает по I150  

//заглавные греческие совпадают с латинскими: 
//υΥ, κΚ, εΕ, νΝ, ζΖ, 
//αΑ,
//μΜ, ιΙ, τΤ

// Теоретически ISO-клавиатура (с LSGT между Я и правым Shift и с высоким Enter) может иметь 62 (без ничего), 88 (без цифрового блока) или 105 клавиш;
// ANSI (без LSGT и с маленьким Enter)  — 61, 87 или 104; 
// нестандартная — сколько угодно...
// Раз везде, где оригинала нет на 1-2 уровнях, он доступен на 3 — восстанавливаем и на 3 уровне включателей РУС/ЛАТ исходные символы клавиш
// /usr/share/X11/xkb/symbols/pc
//     key <LWIN> {	[ Super_L		]	};
//     key <RWIN> {	[ Super_R		]	};
//     key <MENU> {	[ Menu			]	};

// ****************************************************************************
// Ноутбук Samsung (ANSI)

// SCLK на samsung'е нет; верхний угол: PRSC Pause/Break INS DEL
// и этот блок дальше от алфавитного, чем цифровой (и клавиши меньше по размеру).
// При этом клавиша, на которой написано Pause/Break — не PAUS!!! 
// Это key <I210>   {      [ XF86Launch3           ]       }; 
// но здесь переопределить I210 не получается — переопределяется только для русской раскладки

// нужен отдельный запуск
//  xmodmap -e "keycode 210 = ampersand"

// Цифрового блока тоже нет (не верь глазам своим);
// клавиши, похожие на цифровой блок — это не KP0..KP9, KPEN, KPDL и т. п., 
// а аппаратные дубликаты HOME, RTRN, INS, DELE и т. п.;
// то есть переопределение INS влияет и на Insert «цифрового блока»!
// А вот NumLock переопределить можно




// Алфавитный блок qwerty-латиницы на всякий случай (us включает и цифры, и BKSL, так что не подходит)
partial alphanumeric_keys xkb_symbols "lat_alph_qwerty_patch" {
    name[Group1]= "illinc — latin (qwerty)";
    key.type="EIGHT_LEVEL_ALPHABETIC";
   
    key <AD01> {[	  q,		Q	]};
    key <AD02> {[	  w,		W	]};
    key <AD03> {[	  e,		E	]};
    key <AD04> {[	  r,		R	]};
    key <AD05> {[	  t,		T	]};
    key <AD06> {[	  y,		Y	]};
    key <AD07> {[	  u,		U	]};
    key <AD08> {[	  i,		I	]};
    key <AD09> {[	  o,		O	]};
    key <AD10> {[	  p,		P	]};
    key <AD11> {[ bracketleft,	braceleft	]};
    key <AD12> {[ bracketright,	braceright	]};

    key <AC01> {[	  a,		A	]};
    key <AC02> {[	  s,		S	]};
    key <AC03> {[	  d,		D	]};
    key <AC04> {[	  f,		F	]};
    key <AC05> {[	  g,		G	]};
    key <AC06> {[	  h,		H	]};
    key <AC07> {[	  j,		J	]};
    key <AC08> {[	  k,		K	]};
    key <AC09> {[	  l,		L	]};
    key <AC10> {[ semicolon,	    colon	]};
    key <AC11> {[ apostrophe,	 quotedbl	]};

    key <AB01> {[	  z,		Z	]};
    key <AB02> {[	  x,		X	]};
    key <AB03> {[	  c,		C	]};
    key <AB04> {[	  v,		V	]};
    key <AB05> {[	  b,		B	]};
    key <AB06> {[	  n,		N	]};
    key <AB07> {[	  m,		M	]};    
    key <AB08> {[     comma,	     less	]};
    key <AB09> {[    period,	  greater	]};   
    key <AB10> {[     slash,	 question	]};
};


// Алфавитный блок русской стандартной раскладки йцукен, исключая букву Ё (общий для winkeys и машинописной)
partial alphanumeric_keys xkb_symbols "rus_alph_base" {
    // Заменяем два нижних уровня
    key.type="EIGHT_LEVEL_ALPHABETIC";

    key	<AD01> {[ Cyrillic_shorti, Cyrillic_SHORTI	]};
    key	<AD02> {[    Cyrillic_tse,    Cyrillic_TSE	]};
    key	<AD03> {[      Cyrillic_u,    Cyrillic_U	]};
    key	<AD04> {[     Cyrillic_ka,     Cyrillic_KA	]};
    key	<AD05> {[     Cyrillic_ie,     Cyrillic_IE	]};
    key	<AD06> {[     Cyrillic_en,     Cyrillic_EN	]};
    key	<AD07> {[    Cyrillic_ghe,    Cyrillic_GHE	]};
    key	<AD08> {[    Cyrillic_sha,    Cyrillic_SHA	]};
    key	<AD09> {[  Cyrillic_shcha,  Cyrillic_SHCHA	]};
    key	<AD10> {[     Cyrillic_ze,     Cyrillic_ZE	]};
    key	<AD11> {[     Cyrillic_ha,     Cyrillic_HA	]};
  key <AD12> {[Cyrillic_hardsign,Cyrillic_HARDSIGN	]};

    key	<AC01> {[     Cyrillic_ef,     Cyrillic_EF	]};
    key	<AC02> {[   Cyrillic_yeru,   Cyrillic_YERU	]};
    key	<AC03> {[     Cyrillic_ve,     Cyrillic_VE	]};
    key	<AC04> {[      Cyrillic_a,      Cyrillic_A	]};
    key	<AC05> {[     Cyrillic_pe,     Cyrillic_PE	]};
    key	<AC06> {[     Cyrillic_er,     Cyrillic_ER	]};
    key	<AC07> {[      Cyrillic_o,      Cyrillic_O	]};
    key	<AC08> {[     Cyrillic_el,     Cyrillic_EL	]};
    key	<AC09> {[     Cyrillic_de,     Cyrillic_DE	]};
    key	<AC10> {[    Cyrillic_zhe,    Cyrillic_ZHE	]};
    key	<AC11> {[      Cyrillic_e,      Cyrillic_E	]};

    key	<AB01> {[     Cyrillic_ya,     Cyrillic_YA	]};
    key	<AB02> {[    Cyrillic_che,    Cyrillic_CHE	]};
    key	<AB03> {[     Cyrillic_es,     Cyrillic_ES	]};
    key	<AB04> {[     Cyrillic_em,     Cyrillic_EM	]};
    key	<AB05> {[      Cyrillic_i,      Cyrillic_I	]};
    key	<AB06> {[     Cyrillic_te,     Cyrillic_TE	]};
  key <AB07> {[Cyrillic_softsign,Cyrillic_SOFTSIGN	]};
    key	<AB08> {[     Cyrillic_be,     Cyrillic_BE	]}; 
    key	<AB09> {[     Cyrillic_yu,     Cyrillic_YU	]}; 

};


// Раскладка с Ё слева, перед единицей (ru, которая изначально winkeys)
partial alphanumeric_keys xkb_symbols "rus_alph_winkeys" {
    include "illinc(rus_alph_base)"
    name[Group1]= "illinc — cyrillic (winkeys)";
    key.type="EIGHT_LEVEL_ALPHABETIC";
    
    // Заменяем два нижних уровня
    key	<AE02> {[		2,        quotedbl	]};
    key	<AE03> {[		3,      numerosign	]};
    key	<AE04> {[		4,       semicolon	]};
    key	<AE06> {[		6,	     colon	]};
    key	<AE07> {[		7,	  question	]};
    key	<TLDE> {[     Cyrillic_io,     Cyrillic_IO	]};    
    key <AB10> {[          period,           comma	]};
    key <BKSL> {[       backslash,           slash	]};
};

// Раскладка с Ё справа (там, где в ru-winkeys запятая и точка), как в классической машинописной
// знаки препинания набираются там, где они определены в общей части (см. включаемые фрагменты ниже, а подробности — выше) и латинице (со временным включателем латиницы)
partial alphanumeric_keys xkb_symbols "rus_alph_myyo" {
    include "illinc(rus_alph_base)"
    name[Group1]= "illinc — cyrillic (yo tw)";
    key.type="EIGHT_LEVEL_ALPHABETIC";
    key	<AB10> {[     Cyrillic_io,     Cyrillic_IO	]}; // ё Ё 
};

 
// Для ANSI-клавиатур с маленьким Enter: BKSL на месте Enter и постоянно нажимается по ошибке:
// имеет смысл переопределить его как правый Level3 или (если нет RWin) как единственный Level5;
// это возможно: символы ^'\|, располагающиеся в lat_88_default на BKSL, дублируются на 2-4 уровнях прочих клавиш

// BKSL = правый Level3
partial modifier_keys xkb_symbols "common_ansi_mod_bksl_level3" {
    replace key <BKSL> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level3_Shift ]  };
    modifier_map Mod5   { ISO_Level3_Shift };
};
// Если правый Level3 = BKSL, то ScrollLock можно переопределить как Level5
// ScrollLock  = Level5
partial modifier_keys xkb_symbols "common_all_mod_scroll_level5" {
    replace key <SCLK> {    type="ONE_LEVEL",    symbols[Group1] = [ ISO_Level5_Shift ]  };
    modifier_map Mod3 { ISO_Level5_Shift };  
};


// ****************************************************************************
// Комбинации модификаторов

// Правые модификаторы для американской ANSI с маленьким Enter, длинным BKSL и вообще без RWin
// с сохранением позиции Enter (неудобно: левый и правый Level3 немного несимметричны — а это хуже, чем много)
partial modifier_keys xkb_symbols "common_ansi_mod_usmodern_patch" {
    include "illinc(common_ansi_mod_bksl_level3)"   // правый Level3 на BKSL
    include "illinc(common_all_mod_scroll_level5)"  // правый Level5 вверху, на ScrollLock (нужен реже)
};
// с переносом Enter вверх, на BKSL (сомнительно, ибо ANSI-BKSL хоть и длинный, но короче ANSI-Enter)
partial modifier_keys xkb_symbols "common_ansi_mod_usmodern_enterswap_patch" {
    replace key <BKSL> {type="ONE_LEVEL", symbols[Group1] = [ Return ]  };          // Enter вверху, на BKSL
    replace key <RTRN> {type="ONE_LEVEL", symbols[Group1] = [ ISO_Level3_Shift ] }; // правый Level3 внизу, на одной горизонтали с левым Level3=CapsLock
    modifier_map Mod5   { ISO_Level3_Shift };
    include "illinc(common_all_mod_scroll_level5)"                                  // правый Level5 вверху, на ScrollLock 
};
    
    // ANSI: на месте BKSL — длинный Enter; на месте Enter — длинный BKSL; LSGT нет вообще
    // переопределение BKSL на ANSI-клавиатурах как модификатора (можно не переопределять, тогда останется BKSL=//\| от определений выше)
    // include "illinc(common_ansi_mod_bksl_level5)"    // BKSL=Level5 (правый Level3 остаётся на ScrollLock или отсутствующим)
    // include "illinc(common_ansi_mod_usmodern_patch)" // BKSL=Level3 (правый), ScrollLock=Level5, Enter на месте
    // include "illinc(common_ansi_mod_usmodern_enterswap_patch)"    // BKSL=Enter, Enter=Level3 (правый), ScrollLock=Level5

// ↑↑↑ основные символы вынесены на 1-2 уровни или остались на 5; два больших Level3 нужны разве что для 61-клавишной
// или для сильно изменённых вариантов 
// но ни на 61 нет, ни на некоторых ноутбуках может не быть клавиш ScrollLock и RWin; и надо разбираться индивидуально, а не таким патчем

// при этом в режиме CapsLock капитализируются не только русские/латинские буквы, но и греческие; 
// и вообще вместо 3 уровня букв — 4, вместо 5 — 6, вместо 7 — 8;    вместо 4 — 3, вместо 8 — 7; 
// а вместо 6 почему-то 1
// на знаки препинания и цифры (EIGHT_LEVEL) режим CapsLock не влияет; а вот на Insert, если он LOCAL_EIGHT_LEVEL — да
// (-) так что возврат к FOUR_LEVEL для Insert — Л.Ф. хочет Ctrl+Insert, а я практически не использую режим CapsLock ⇒ LOCAL_EIGHT_LEVEL

    key <AD11> {[	h,		H,	Greek_chi,		Greek_CHI,		U2286,		U2288,			includedin,	U2284	]}; // h H χ Χ   ⊆ ⊈ ⊂ ⊄ 
    key <AD12> {[apostrophe,apostrophe,	apostrophe,apostrophe,	U2287,		U2289,			includes,	U2285	]}; // ' ' ' '   ⊇ ⊉ ⊃ ⊅
    key <AD07> {[	g,		G,	Greek_gamma,	Greek_GAMMA,	U2286,		U2288,			includedin,	U2284	]}; // g G γ Γ   ⊆ ⊈ ⊂ ⊄
    key <AD12> {[apostrophe,apostrophe,	apostrophe,apostrophe,	U2287,		U2289,			includes,	U2285	]}; // ' ' ' '   ⊇ ⊉ ⊃ ⊅
Символы строгого включения ⊂⊃ не получается разместить под ⊆⊇ или на 5 уровне соседних клавиш ⇒ временно исключаются вообще;
взамен на 6 уровне (так как есть перечёркивание и чтобы не ломать последовательность штрихов °′″‴⁗) — строгое включение в виде ⊊⊋.
Перечёркнутой формы у этих символов нет 
    key <FK11> {		[ guillemotleft,	doublelowquotemark,		F11,F11,	U2034,	U228A,		U25C1,VoidSymbol]}; // « „  ‴ ⊊  ◁   
    key <FK12> {		[ guillemotright,	leftdoublequotemark,	F12,F12,	U2057,	U228B,		U25B7,VoidSymbol]}; // » “  ⁗ ⊋  ▷
или ⊂⊃ теперь под ⊊⊋? Треугольники парой точно ни разу не пригодились, а маркер ➤ есть на другой клавише


    key <FK08> {									[7, U2309,		F8, F8, 	degree,	U27E1,enfilledcircbullet,U0300	]}; // 7 ⌉  ° ⟡  • ̀a  8 ̀=тяжёлое ударение

Верхние индексы ʸʷ на 8 уровне yw ⇒ перечёркнутые стрелки на 5-6
    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U219A,		U21CD,			U21A9,		U02B8 	]}; // y Y ← ⇐   ↚ ⇍ ↩ ʸ
    key <AC03> {[	w,		W,		rightarrow,	implies,		U219B,		U21CF,			U21AA,		U02B7	]}; // w W → ⇒   ↛ ⇏ ↪ ʷ
но тогда перечёркнутые ⤉⤈ надо убирать из раскладки (иначе они должны будут оказаться на месте ℂℚ)
     key <AD02> {[	c,		C,		uparrow,	U21D1,			U2102,		U207A,			VoidSymbol,	U1D9C	]}; // c C ↑ ⇑   ℂ ⁺   ᶜ   6 ⁺=верхний индекс (заряд)
     key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U035F,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ x͟ ̅x   8 ̅=комб.черта

    
Оригинальное перечёркивание стрелок (+L5):
    key <AD02> {[	c,		C,	uparrow,		U21D1,			U2102,		U207A,			U2909,		U1D9C	]}; // c C ↑ ⇑   ℂ ⁺ ⤉ ᶜ   6 ⁺=верхний индекс (заряд)

    key <AC02> {[	y,		Y,		leftarrow,	U21D0,			U21A4,		U21A9,			U219A,		U21CD	]}; // y Y ← ⇐   ↤ ↩ ↚ ⇍
    key <AC03> {[	w,		W,		rightarrow,	implies,		U21A6,		U21AA,			U219B,		U21CF	]}; // w W → ⇒   ↦ ↪ ↛ ⇏
    
    key <AB01> {[	at,		at,		U2194,		ifonlyif,		VoidSymbol,	VoidSymbol,		U21AE,		U21CE	]}; // @ @ ↔ ⇔       ↮ ⇎
    key <AB02> {[	q,		Q, 		downarrow,	U21D3,			U211A,		U220E,			U2908,		U0305	]}; // q Q ↓ ⇓   ℚ ∎ ⤈ ̅x   8 ̅=комб.черта
    
Переносим ↚↛↮ на 5, ⤉⤈ оставляем пока на 7 (остальные места ещё менее очевидны)
↦ (отображается в) — на oO; ↤ вроде бы не используется (как и комбинируемая ← над предыдущим — только →)

// в консоли:
// — уровни 1-4 нормальных клавиш работают (хотя часть символов не отображается из-за шрифта); 
// — модификаторы второго (Shift) и третьего уровня работают нормально;
// — стрелки на 3 уровне стрелок печатаются, а знаки препинания на 3 уровне Fx — нет;
// — включатели раскладок ISO_First_Group/ISO_Last_Group работают как циклические ПЕРЕключатели;
// (а вот XKBOPTIONS="grp:shift_toggle,grp_led:caps,grp_led:scroll" — не работает вообще: ни переключатель не переключает, ни лампочки не меняются);
// — модификатор пятого уровня ISO_Level5_Shift работает как модификатор третьего уровня ISO_Level3_Shift;
// — символ \ с переходом на латиницу выдаёт только символ \, а временный включатель латиницы вообще не работает (проблемы с actions?).
// — при переопределённых F1-F12 не работает переход между виртуальными консолями по [Control+] Alt+Fx, но работает по Alt+стрелки 
// (если не переопределять — переход работает только по чистому Alt+Fx и Control+Alt+Fx, но не по Shift+Alt+Fx, например) 
// В X работает всё (но в некоторых приложениях — не совсем).

// Установки для X:
// /etc/default/keyboard
// XKBLAYOUT="illinc,illinc"
// #XKBOPTIONS="grp:shift_toggle,grp_led:caps,grp_led:scroll"
// XKBOPTIONS="grp:shift_toggle,grp_led:scroll" # вернули CapsLock → освобождаем лампочку
// Полная клавиатура
// XKBVARIANT="lat,rus"
// для смены набора модификаторов не меняем XKBVARIANT, меняем lat или её компоненты — всё равно раскладка подгоняется под конкретную клавиатуру

// ************************************************************************************************************************************************************
// Алфавитно-цифровые клавиши, F1-F12, прочие, переопределяемые символами 
// восемь уровней:
// клавиша,	Shift + клавиша,	ISO_Level3_Shift + клавиша,	ISO_Level3_Shift + Shift + клавиша
// ISO_Level5_Shift + клавиша,	ISO_Level5_Shift + Shift + клавиша,	ISO_Level5_Shift + ISO_Level3_Shift + клавиша,	ISO_Level5_Shift + ISO_Level3_Shift + Shift + клавиша
// ----------------------------------------------------------------------------
// уровни 1 и 2 — буквы кириллицы и латиницы, цифры; знаки препинания на линии Backspace (неполная совместимость с ru, полная несовместимость с us);
// уровни 3 и 4 — некоторые кракозябры и цифры с 1 уровня qwerty (но не буквы!); стрелки и греческие буквы для формул (не полный греческий алфавит!);
// уровни 5 и 6 — математические символы (иногда +Shift — перечёркивание); также на 6 уровне формы в круге и разные дополнительные кракозябры;
// уровни 7 и 8 — нижний и верхний индексы соответственно; перечёркнутые стрелки; также 7: ≡ над =, ⊂⊃ над ⊆⊇ (+Shift — перечёркивание) и ≾≿ над ≺≻; 7/8: ∂/∇ на Д.
// ----------------------------------------------------------------------------
// перечёркивание = +Shift: уровни 3/4: ≤≥;   5/6: ≈, ∃, ⊆⊇, ∈∋, ≺≻;   7/8: ≡, ⊂⊃   — итого 13 пар символ/перечёркивание: пока «правильного» всё же больше
// перечёркивание другое:   2/5: <>, =, ~ на пробеле (если нет common_mod_ralt_tilde_patch; если есть: ~/≁ 3/5);   
//                          3/5: одинарные стрелки ↚↛↮ (три из пяти);   4/6: двойные стрелки ⇍⇏⇎ (три из пяти)   — итого 10 пар «неправильного» перечёркивания




// для IceWM можно в файле ~/.icewm/keys задать сочетания клавиш (после сохранения ~/.icewm/keys нужен перезапуск IceWM):
// # Альтернативный режим и его сброс
// 
// key "ISO_Last_Group"			setxkbmap "illinc(lat),illinc(rus)"
// key "ISO_First_Group"			setxkbmap "illinc(lat),illinc(rus)"
// key "Alt+ISO_Last_Group"			setxkbmap "illinc(altlat),illinc(altrus)"
// key "Alt+ISO_First_Group"			setxkbmap "illinc(altlat),illinc(altrus)"
// то есть Alt+РУС/ЛАТ → РУС/ЛАТ в альтернативном режиме (Hex и н), просто РУС/ЛАТ → РУС/ЛАТ в режиме «по умолчанию» (а вот на Ctrl+φ повесить не удалось)
// исходный смысл ISO_Last_Group/ISO_First_Group при этом также отрабатывается
// четыре раскладки setxkbmap "illinc(lat),illinc(rus),illinc(altlat),illinc(altrus)" — получается (lat=1, rus=2, altlat=3, altrus=4), но работает неадекватно: 
// \Лат и $Лат сохраняют своё Лат-действие и как C и E, как ни переопределяй (даже если altlat не на основе lat и не содержит определения \Лат вообще)

//      key <MENU> {	type="FOUR_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu] };
//     key <MENU> {	type="EIGHT_LEVEL",	[ ISO_First_Group,	ISO_First_Group,		Menu, VoidSymbol, VoidSymbol],        
//         actions[Group1]= [ LockGroup(group=1), LockGroup(group=1),   NoAction(), NoAction(), LockGroup(group=3) ],
//         actions[Group2]= [ LockGroup(group=1), LockGroup(group=1),   NoAction(), NoAction(), LockGroup(group=3) ],
//         actions[Group3]= [ LockGroup(group=1), LockGroup(group=1),   NoAction(), NoAction(), LockGroup(group=3) ],
//         actions[Group4]= [ LockGroup(group=1), LockGroup(group=1),   NoAction(), NoAction(), LockGroup(group=3) ]            
//     };







Попытка оверлея и на F1-F12, и на A-F:
один оверлей на одну клавишу — работает; два — нет (работает только overlay1)
Если overlay1 отключён, всё равно на перключение overlay2 никакой реакции

Кроме того, режим F1-F10 вместо цифр сильно обескураживает

    // верхняя линия: F1-F12 **************************************************
    key <FK01> { type="CTRL+ALT", 	[0, grave,		F1, F1, 	XF86_Switch_VT_1 	],	overlay2=<AB11>	}; // 0 `
    key <FK02> { type="CTRL+ALT", 	[1, U2212,		F2, F2, 	XF86_Switch_VT_2 	],	overlay2=<JPCM>	}; // 1 −                                    	
    key <FK03> { type="CTRL+ALT", 	[2, U27E8,		F3, F3, 	XF86_Switch_VT_3 	],	overlay2=<I120>	}; // 2 ⟨
    key <FK04> { type="CTRL+ALT", 	[3, U27E9,		F4, F4, 	XF86_Switch_VT_4 	],	overlay2=<AE13>	}; // 3 ⟩
                                                                                             
    key <FK05> { type="CTRL+ALT", 	[4, U230A,		F5, F5, 	XF86_Switch_VT_5 	],	overlay2=<I149>	}; // 4 ⌊
    key <FK06> { type="CTRL+ALT", 	[5, U230B,		F6, F6, 	XF86_Switch_VT_6 	],	overlay2=<I154>	}; // 5 ⌋
    key <FK07> { type="CTRL+ALT", 	[6, U2308,		F7, F7, 	XF86_Switch_VT_7 	],	overlay2=<I168>	}; // 6 ⌈ 
    
    key <FK08> {					[7, U2309,		F8, F8, 	notsign,	U0338,			enfilledcircbullet,	VoidSymbol	],	overlay2=<I178>	}; // 7⌉  ¬  •  6̸= комб    
    key <FK09> {					[8, ampersand,	F9, F9, 	logicaland,	VoidSymbol,		U2023,				VoidSymbol	],	overlay2=<I183>	}; // 8&  ∧  ‣ 
    key <FK10> {					[9, bar,		F10,F10,	logicalor,	VoidSymbol,		U27A4,				VoidSymbol	],	overlay2=<I184>	}; // 9|  ∨  ➤ 
    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	U228A,leftsinglequotemark,	includedin,	U2284],overlay2=<I221>,	overlay1=<I247>	}; // «„  ⊊‘ ⊂⊄   
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	U228B,rightsinglequotemark,	includes,	U2285],overlay2=<I222>,	overlay1=<I248>	}; // »“  ⊋’ ⊃⊅
    
    key <AB11> { type="CTRL+ALT", 	[F1, F1,	F1, F1, 	XF86_Switch_VT_1 	]};
    key <JPCM> { type="CTRL+ALT", 	[F2, F2,	F2, F2, 	XF86_Switch_VT_2 	]};                                    
    key <I120> { type="CTRL+ALT", 	[F3, F3,	F3, F3, 	XF86_Switch_VT_3 	]};
    key <AE13> { type="CTRL+ALT", 	[F4, F4,	F4, F4, 	XF86_Switch_VT_4 	]};
                                                                                   
    key <I149> { type="CTRL+ALT", 	[F5, F5,	F5, F5, 	XF86_Switch_VT_5 	]};
    key <I154> { type="CTRL+ALT", 	[F6, F6,	F6, F6, 	XF86_Switch_VT_6 	]};
    key <I168> { type="CTRL+ALT", 	[F7, F7,	F7, F7, 	XF86_Switch_VT_7 	]};         
    key <I178> { type="CTRL+ALT", 	[F8, F8,	F8, F8, 	XF86_Switch_VT_8 	]};         
    
    key <I183> { type="CTRL+ALT", 	[F9, F9,	F9, F9, 	XF86_Switch_VT_9 	]};
    key <I184> { type="CTRL+ALT", 	[F10,F10,	F10,F10, 	XF86_Switch_VT_10 	]};
    key <I221> { type="CTRL+ALT", 	[F11,F11,	F11,F11, 	XF86_Switch_VT_11 	],	overlay1=<I247>};         
    key <I222> { type="CTRL+ALT", 	[F12,F12,	F12,F12, 	XF86_Switch_VT_12 	],	overlay1=<I248>};     

Выбор клавиш — те, которые в     xmodmap -pke не определены
F13 нельзя, так как она потом переопределяется где-то снова как описана в inet(evdev)


Удручает цикличность оверлея (и даже без индикатора)
    key <KPDV> {[ less,		comma,				slash, 		slash		],	overlay1=<KPEQ>}; // <,  // 
    key <KPEQ> {[ less,	Cyrillic_en,	slash, 		slash		]}; // <н  // 


    key <FK11> {[guillemotleft,doublelowquotemark,	F11,F11,	U228A,leftsinglequotemark,	includedin,	U2284],	overlay1=<I247>	}; // «„  ⊊‘ ⊂⊄   
    key <FK12> {[guillemotright,leftdoublequotemark,F12,F12,	U228B,rightsinglequotemark,	includes,	U2285],	overlay1=<I248>	}; // »“  ⊋’ ⊃⊅
    
    replace key <ESC> 	{	type="ONE_LEVEL",	[ Escape	],	overlay1=<I252>  	};
  
    replace	key <I247> {	[ A,	guillemotleft,	F11,	F11,	U228A,leftsinglequotemark,	includedin,	U2284	]	};  
    replace	key <I248> {	[ B,	guillemotright,	F12,	F12,	U228B,rightsinglequotemark,	includes,	U2285	]	};
    replace key <I249> {	[ C,	numbersign,		Print,	Sys_Req				]}; 
    replace key <I250> {	[ D,	asciicircum,	Scroll_Lock,Scroll_Lock		]};  
    replace key <I251> {	[ E,	dollar,			Pause,	Break				]};    
    replace key <I252> {	[ F,	Escape,			Escape,	Escape				]};

  
// XKBLAYOUT="illinc,illinc"
// XKBVARIANT="lat,rus"

    // ScrollLock как правый Level3, PRSC как \Лат с Print, Sys_Req на 3-4 уровнях, PAUS как $Лат
    //replace key <SCLK> {	type="ONE_LEVEL",	symbols[Group1] = [ ISO_Level3_Shift ]	};
    //modifier_map Mod5   { ISO_Level3_Shift };    
        
    //replace key <PRSC> {	[backslash,	bar, 		Print,Sys_Req,		U2216,U29B8,		  U2225,U2226	], // \| PrScr SysRq   ∖⦸ ∥∦
    //   actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)]	};       
    
    replace key <PRSC> {[backslash,	numbersign,		Print,	Sys_Req,		U2216,U29B8,			VoidSymbol,VoidSymbol	],  // \# PrScr SysRq  ∖⦸ 
       actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)],	overlay1=<I249>	}; 
       
    replace key <SCLK> {[asciicircum,asciicircum,	Scroll_Lock,VoidSymbol,	U2295,VoidSymbol, 		VoidSymbol,U0302		],	overlay1=<I250>	}; // ^^ ScrollLock   ⊕  x̂  8 ̂=комб.

    replace key <PAUS> {[dollar,	apostrophe,		Pause,	Break,			VoidSymbol,VoidSymbol,	VoidSymbol,VoidSymbol	],  // $' Pause Break   
       actions[Group1]=[],  actions[Group2]=[LockGroup(group=-1)],  actions[Group3]=[LockGroup(group=-2)],  actions[Group4]=[LockGroup(group=-3)],	overlay1=<I251>		};  


    

// Прячем Num_Lock на 2-4 уровни (на 3 — по общему правилу, на 4 — аналогично CapsLock, но удобно пользоваться 2 уровнем)
// на 1 уровне символ «=».
// Отдельно, так как даже если цифровой блок не переопределяется — Num_Lock на 1 уровне раздражает
partial keypad_keys xkb_symbols "common_keypad_numlock_equal" { 
    //replace key <NMLK> 	{type="FOUR_LEVEL",	[ equal,	Num_Lock,		Num_Lock,	Num_Lock	]}; 
    // ↑ так 2 уровень циклически переключает режимы, 1 — включает режим скобок (в общем случае — режим по умолчанию)
    // 3-4 уровни — аналогичны 2
    
    // Искореняем Num_Lock на одноимённой клавише
    replace key <I227>	{type="ONE_LEVEL",	[ Num_Lock 		]	}; 	// inet(evdev): key <I227> {[ XF86Finance]};
    // ↑ старый NumLock после этого не переключает режимы; символ Num_Lock на нём не имеет никакого действия   
        
    // Заново реализуем Num_Lock средствами actions на 2-4 уровнях
    replace key <NMLK> 	{type="FOUR_LEVEL",	[ equal,	VoidSymbol,		VoidSymbol,	VoidSymbol	],
        actions[Group1]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group2]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group3]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ],
        actions[Group4]= [ NoAction(), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock), LockMods(modifiers=NumLock) ]    
    }; 
    // ↑ символ 1 уровня НЕ включает режим скобок, действия 2-4 — циклически переключают режимы (как оригинальный NumLock)
    
    modifier_map Mod2   { Num_Lock };      
};


// Для ввода баллов ОРИОКС: запятая не используется + нужна литера «н»  ⇒  «н» на место запятой
partial keypad_keys xkb_symbols "common_keypad_braces_digits_with_tab_orioks_N_patch" {   
    key <KPDV> {type="FOUR_LEVEL_MIXED_KEYPAD",	[ less,			Cyrillic_en		]}; // <н   
    key <KPSU> {type="ONE_LEVEL",	[ minus	]}; // -
};

// Для 104-клавишной (ANSI с цифровым блоком): # на первом уровне (вместо отсутствующей LSGT)
partial keypad_keys xkb_symbols "common_keypad_104numbersign_patch" {   
    key  <KP0>  {type="FOUR_LEVEL_MIXED_KEYPAD",	[ numbersign ]};   
};

Вьетнамские символы, включённые шутки ради:

    key <FK08> {[7, U2309,				F8, F8, 	oneeighth,threeeighths,				seveneighths,U0309	],	overlay2=<I178>	}; // 7⌉  ⅛⅜ ⅞ả  8 ̉=комб.хвостик сверху  (не тот хвостик, который ого́нэк)
    ↑↑↑ вопросительный (восходяще-нисходящий) тон
    ещё тоны:
    — восходящий как надстрочное русское ударение á;
    — нисходящий как надстрочное тяжёлое ударение à;
    — островопросительный (нисходяще-восходящий с гортанной смычкой) — надстрочная тильда (хотя в половине примеров явная черта);
    — тяжёлый (падающий, он же резко нисходящий) — подстрочная точка ạ;

    а кратка, рожок и крышка — символы буквообразующие
    
    replace key <PRSC> {[ parenleft, U22C5,		Print,	Sys_Req,		onethird,	twothirds,	onesixth,	U0306		]	};	// (⋅ PS SR   ⅓⅔ ⅙ă   8 ̆=комб.кратка
   
    replace key <PAUS> {[ampersand,	U02BC,		Pause,	Break,			onequarter,	threequarters,	U2150,	U031B		]	};	// &ʼ P  BR   ¼¾ ⅐a̛  8̛=комб.рожок
    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		VoidSymbol,	VoidSymbol,		U0323,		U0300	]}; // ` ` χ Χ       ạ à   8 ̀=гравис (тяжёлое удар.)

    // 2 уровень — нижний индекс (нужен чаще), 5 — верхний, 6 — форма в круге, сложнонабираемый 4 для цифр — (10+x) в круге
    replace key <KPMU> {[ figdash,	U2C7C,	KP_Multiply,U20DD,				U02B2,	U24D9						]}; // ‒ⱼ    * a⃝   ʲⓙ    1=цифровое тире    
    replace key <KPSU> {[ U2212,	U2099,	KP_Subtract,U20E0,				U207F,	U24DD,	U1D3A,	VoidSymbol	]}; // −ₙ    - a⃠   ⁿⓝ ᴺ  1=минус ширины плюса
    replace key <KPAD> {[ Tab,ISO_Left_Tab,	KP_Add,		U0336,				U0336,	U0336						]}; // Tab   + a̶  a̶a̶  4=5=6=комб.зачёркивание
    replace key <KPEN> {type="ONE_LEVEL",	[ KP_Enter 	]};  
        
    replace key  <KP7> {[ 7,sevensubscript,	KP_Home, 	U2470,		sevensuperior,	U2466,	U0103,	U0102		]}; // 7₇  Home⑰   ⁷⑦ ăĂ
    replace key  <KP8> {[ 8,eightsubscript,	KP_Up, 		U2471,		eightsuperior,	U2467,	U01B0,	U01AF		]}; // 8₈  ↑   ⑱   ⁸⑧ ưƯ
    replace key  <KP9> {[ 9,ninesubscript,	KP_Prior, 	U2472,		ninesuperior,	U2468,	U2151,	U215F		]}; // 9₉  PgUp⑲   ⁹⑨ ⅑⅟

    replace key  <KP4> {[ 4,foursubscript,	KP_Left, 	U246D,		foursuperior,	U2463,threefifths,fourfifths]}; // 4₄  ←   ⑭   ⁴④ ⅗⅘
    replace key  <KP5> {[ 5,fivesubscript,	KP_Begin, 	U246E,		fivesuperior,	U2464,	onefifth,twofifths	]}; // 5₅  ??? ⑮   ⁵⑤ ⅕⅖
    replace key  <KP6> {[ 6,sixsubscript,	KP_Right, 	U246F,		sixsuperior,	U2465,	onesixth,fivesixths	]}; // 6₆  →   ⑯   ⁶⑥ ⅙⅚

    replace key  <KP1> {[ 1,onesubscript,	KP_End, 	U246A,		onesuperior,	U2460,	U00E2,	U00C2		]}; // 1₁  End ⑪   ¹① âÂ
    replace key  <KP2> {[ 2,twosubscript,	KP_Down, 	U246B,		twosuperior,	U2461,	U00EA,	U00CA		]}; // 2₂  ↓   ⑫   ²② êÊ
    replace key  <KP3> {[ 3,threesubscript,	KP_Next, 	U246C,		threesuperior,	U2462,	U00F4,	U00D4		]}; // 3₃  PgDn⑬   ³③ ôÔ

    replace key  <KP0> {[ 0,zerosubscript,	KP_Insert,	U2469,		zerosuperior,	U24EA,	U01A1,	U01A0		]}; // 0₀  Ins ⑩   ⁰⓪ ơƠ
    replace key <KPDL> {[ comma, U1D62,		KP_Delete,	VoidSymbol,	U2071,			U24D8,	U0111,	U0110		]}; // ,ᵢ  Del     ⁱⓘ đĐ  
    
    
удаляются при первой оказии,
так как на 7-8 уровнях ими и пользоваться неудобно, и место занимают нелогичное.

Если понадобится вьетнамский вариант раскладки:
— буквы ăĂ (с краткой), ưƯ и ơƠ (с рогами), âÂ, êÊ, ôÔ (должны быть с крышкой, в LO так и отображаются) на 1-2 уровни цифрового блока;
— тоны á, à, ả , ã, ạ на 1 уровень туда же;
итого 6 + 5 = 11 символов, как раз на цифры 0-9 и точку или 1-9, точку и цифроминус.

Всего там 17 клавиш (включая NL и Enter), 3 крупных (0, +, Enter), хватит даже на 2*6 + 5 = 12+5 = 17 (заглавные отдельно), но нелогично.


На 3 уровне ещё F1-F12, ⊆⊇ над «»:

    // верхняя линия: F1-F12 **************************************************
    //replace key <ESC>  {	type="FOUR_LEVEL",	[ Escape,	Escape,		Escape,	Overlay2_Enable		]	};
    
    key <FK01> { type="CTRL+ALT", 	[0, U2A7D,		F1, Overlay2_Enable, 	XF86_Switch_VT_1 	],	overlay2=<AB11>	}; // 0 ⩽
    key <FK02> { type="CTRL+ALT", 	[1, U2A7E,		F2, F2, 				XF86_Switch_VT_2 	],	overlay2=<JPCM>	}; // 1 ⩾
    key <FK03> { type="CTRL+ALT", 	[2, U27E8,		F3, F3, 				XF86_Switch_VT_3 	],	overlay2=<I120>	}; // 2 ⟨
    key <FK04> { type="CTRL+ALT", 	[3, U27E9,		F4, VoidSymbol, 		XF86_Switch_VT_4 	],	overlay2=<AE13>	}; // 3 ⟩
                                                                                             
    key <FK05> { type="CTRL+ALT", 	[4, U230A,		F5, F5, 				XF86_Switch_VT_5 	],	overlay2=<I149>	}; // 4 ⌊
    key <FK06> { type="CTRL+ALT", 	[5, U230B,		F6, F6, 				XF86_Switch_VT_6 	],	overlay2=<I154>	}; // 5 ⌋
    key <FK07> { type="CTRL+ALT", 	[6, U2308,		F7, F7, 				XF86_Switch_VT_7 	],	overlay2=<I168>	}; // 6 ⌈ 
    
    key <FK08> {[7, U2309,				F8, F8, 	oneeighth,threeeighths,			fiveeighths,seveneighths],	overlay2=<I178>	}; // 7⌉  ⅛⅜ ⅝⅞
    
    key <FK09> {[8, doublelowquotemark,	F9, F9, 	leftsinglequotemark,leftdoublequotemark,	U208D,U2151	],	overlay2=<I183>	}; // 8„  ‘“ ₍⅑
    key <FK10> {[9, leftdoublequotemark,F10,F10,	rightsinglequotemark,rightdoublequotemark,	U208E,U2152	],	overlay2=<I184>	}; // 9“  ’” ₎⅒
    // 				↑↑↑ “ рус.правая=англ.левая
    key <FK11> {[equal,		approxeq,	F11,F11,	U2261,	U2262,								U208C,U207C	],	overlay2=<I221>	}; // =≈  ≡≢ ₌⁼
    key <FK12> {[backslash,	notequal,	F12,F12,	U0336,	U229C,								U20DD,U20E0	],	overlay2=<I222>	}; // \≠  ̶⊜ a⃝a⃠  5,7,8 — комбинируемые
    
    //replace key <PRSC> { type= "PC_ALT_LEVEL2", symbols[Group1]= [ parenleft, Sys_Req ] };	// ( и всё: SysRq с Alt, других уровней не предусмотрено!
    // на свежеперезагруженной системе Alt+SysRq+qwerty работают и с EIGHT_LEVEL, так что проблема не в раскладке
    replace key <PRSC> {[parenleft, U22C5,		Print,		Sys_Req,	onequarter,	onethird,		twothirds,	threequarters]	};	// (⋅ PS SR   ¼⅓ ⅔¾
    replace key <SCLK> {[percent,	plusminus,	Scroll_Lock,Print,		onefifth,	twofifths,		threefifths,fourfifths	]	};	// %± SL PS   ⅕⅖ ⅗⅘
    replace key <PAUS> {[ampersand,	U02BC,		Pause,		Break,		U2150,		onesixth,		fivesixths,	U215F		]	};	// &ʼ P  BR   ⅐⅙ ⅚⅟

    
    // режим F1‒F12 как оверлей;  в консоли переключение оверлеев не работает
    key <AB11> { type="CTRL+ALT", 	[F1, F1,	F1, VoidSymbol, 		XF86_Switch_VT_1 	]};
    key <JPCM> { type="CTRL+ALT", 	[F2, F2,	F2, F2, 				XF86_Switch_VT_2 	]};                                    
    key <I120> { type="CTRL+ALT", 	[F3, F3,	F3, F3, 				XF86_Switch_VT_3 	]};
    key <AE13> { type="CTRL+ALT", 	[F4, F4,	F4, Overlay2_Enable, 	XF86_Switch_VT_4 	]};
                                                                                   
    key <I149> { type="CTRL+ALT", 	[F5, F5,	F5, F5, 	XF86_Switch_VT_5 	]};
    key <I154> { type="CTRL+ALT", 	[F6, F6,	F6, F6, 	XF86_Switch_VT_6 	]};
    key <I168> { type="CTRL+ALT", 	[F7, F7,	F7, F7, 	XF86_Switch_VT_7 	]};         
    key <I178> { type="CTRL+ALT", 	[F8, F8,	F8, F8, 	XF86_Switch_VT_8 	]};         
    
    key <I183> { type="CTRL+ALT", 	[F9, F9,	F9, F9, 	XF86_Switch_VT_9 	]};
    key <I184> { type="CTRL+ALT", 	[F10,F10,	F10,F10, 	XF86_Switch_VT_10 	]};
    key <I221> { type="CTRL+ALT", 	[F11,F11,	F11,F11, 	XF86_Switch_VT_11 	]};         
    key <I222> { type="CTRL+ALT", 	[F12,F12,	F12,F12, 	XF86_Switch_VT_12 	]};       

    // линия Backspace (us-цифр) *********************************************
    key <TLDE> {[ quotedbl,		section,		0,	grave,			seconds,	U24B6,		zerosubscript,	zerosuperior	]}; // "§ 0`  ″Ⓐ ₀⁰
    key <AE01> {[ plus,			numerosign,		1,	exclam,			plusminus,	U2295,		onesubscript,	onesuperior		]}; // +№ 1!  ±⊕ ₁¹
    key <AE02> {[ minus,		U2212,			2,	U2011,			U2213,		U2296,		twosubscript,	twosuperior		]}; // -− 2‑  ∓⊖ ₂² минусы и дефисы     
    key <AE03> {[ slash,		less,			3,	numbersign,		U226E,		U2298,		threesubscript,	threesuperior	]}; // /< 3#  ≮⊘ ₃³
    key <AE04> {[ asterisk,		greater,		4,	dollar,			U226F,		U229B,		foursubscript,	foursuperior	]}; // *> 4$  ≯⊛ ₄⁴
    key <AE05> {[ colon, 		percent,		5,	U205D,			U2030,		U2031,		fivesubscript,	fivesuperior	]}; // :% 5⁝  ‰‱ ₅⁵
    key <AE06> {[ comma,		figdash,		6,	asciicircum,	onehalf,	endash,		sixsubscript,	sixsuperior		]}; // ,‒ 6^  ½– ₆⁶   
    key <AE07> {[ period,		ampersand,		7,	ampersand,		U22C5,		U2299,		sevensubscript,	sevensuperior	]}; // .& 7&  ⋅⊙ ₇⁷
    key <AE08> {[ underscore,	bar,			8,	asterisk,		U2423,		U26A0,		eightsubscript,	eightsuperior	]}; // _| 8*  ␣⚠ ₈⁸   
    key <AE09> {[ question,		bracketleft,	9,	parenleft,		bracketleft,U207D,		ninesubscript,	ninesuperior	]}; // ?[ 9(  [⁽ ₉⁹
    key <AE10> {[ emdash,		bracketright,	0,	parenright,		parenright,	U207E,		zerosubscript,	zerosuperior	]}; // —] 0)  )⁾ ₀⁰ 
    key <AE11> {[ exclam,		guillemotleft,	minus,	underscore,	U2286,		U2288,		U208B,			U207B			]}; // !« -_  ⊆⊈ ₋⁻
    key	<AE12> {[ semicolon,	guillemotright,	equal,	plus,		U2287,		U2289,		U208A,			U207A			]}; // ;» =+  ⊇⊉ ₊⁺

    key <AD11> {[grave,	grave,	Greek_chi,		Greek_CHI,		VoidSymbol,	VoidSymbol,		U0323,		U0300	]}; // ` ` χ Χ       ạ à   8 ̀=гравис (тяжёлое удар.)
    key <AD12> {[apostrophe,apostrophe,	U02BC,apostrophe,		minutes,	U2034,			U2057,		U0301	]}; // ' ' ʼ '   ′ ‴ ⁗ ́a   8 ́=комб.ударение (русское)


Индексы на 3-4 уровнях, „“ ещё на 89
    key <FK09> {[8, doublelowquotemark,	eightsubscript,	eightsuperior,	leftsinglequotemark,leftdoublequotemark,	F9,	F9	],	overlay2=<I183>	}; // 8„ ₈⁸ ‘“
    key <FK10> {[9, leftdoublequotemark,ninesubscript,	ninesuperior,	rightsinglequotemark,rightdoublequotemark,	F10,F10	],	overlay2=<I184>	}; // 9“ ₉⁹ ’”






До отказа от A‒F на верхней линии

    // верхняя линия: F1-F12 **************************************************
    
    key <FK01> {[0, 	U2A7D,			zerosubscript,	zerosuperior, 		XF86_Switch_VT_1,	U24EA,		F1, F1	],	overlay2=<AB11>	}; // 0 ⩽ ₀ ⁰    vt1⓪  F1
    key <FK02> {[1, 	U2A7E,			onesubscript,	onesuperior,		XF86_Switch_VT_2,	U2460,		F2, F2	],	overlay2=<JPCM>	}; // 1 ⩾ ₁ ¹    vt2①  F2
    key <FK03> {[2, 	U27E8,			twosubscript,	twosuperior,		XF86_Switch_VT_3,	U2461,		F3, F3	],	overlay2=<I120>	}; // 2 ⟨ ₂ ²    vt3②  F3
    key <FK04> {[3, 	U27E9,			threesubscript,	threesuperior,		XF86_Switch_VT_4,	U2462,		F4, F4	],	overlay2=<AE13>	}; // 3 ⟩ ₃ ³    vt4③  F4                                                                      
    key <FK05> {[4, 	U230A,			foursubscript,	foursuperior,		XF86_Switch_VT_5,	U2463,		F5, F5	],	overlay2=<I149>	}; // 4 ⌊ ₄ ⁴    vt5④  F5
    key <FK06> {[5, 	U230B,			fivesubscript,	fivesuperior,		XF86_Switch_VT_6,	U2464,		F6, F6	],	overlay2=<I154>	}; // 5 ⌋ ₅ ⁵    vt6⑤  F6
    key <FK07> {[6, 	U2308,			sixsubscript,	sixsuperior,		XF86_Switch_VT_7,	U2465,		F7, F7	],	overlay2=<I168>	}; // 6 ⌈ ₆ ⁶    vt7⑥  F7  
    key <FK08> {[7, 	U2309,			sevensubscript,	sevensuperior,		U27E1,				U2466,		F8,	F8	],	overlay2=<I178>	}; // 7 ⌉ ₇ ⁷     ⟡ ⑦  F8  
    key <FK09> {[8, 	plusminus,		eightsubscript,	eightsuperior,		U2295,				U2467,		F9,	F9	],	overlay2=<I183>	}; // 8 ± ₈ ⁸     ⊕ ⑧  F9
    key <FK10> {[9, 	U2213,			ninesubscript,	ninesuperior,		U2296,				U2468,		F10,F10	],	overlay2=<I184>	}; // 9 ∓ ₉ ⁹     ⊖ ⑨  F10
    key <FK11> {[equal,	approxeq,		U23E8,			Overlay2_Enable,	U2261,				U229C,		F11,F11	],	overlay2=<I221>	}; // = ≈ ⏨ РежF  ≡ ⊜  F11
    key <FK12> {[backslash,	notequal,	U2099,			U207F,				U2262,				U29B8,		F12,F12	],	overlay2=<I222>	}; // \ ≠ ₙ ⁿ     ≢ ⦸  F12
    
    //replace key <PRSC> { type= "PC_ALT_LEVEL2", symbols[Group1]= [ parenleft, Sys_Req ] };	// ( и всё: SysRq с Alt, других уровней не предусмотрено!
    // на свежеперезагруженной системе Alt+SysRq+qwerty работают и с EIGHT_LEVEL, так что проблема не в раскладке
    replace key <PRSC> {[parenleft, U22C5,		U1D62,	U2071,		U2299,		VoidSymbol,		Print,		Sys_Req	]	};	// (⋅ ᵢⁱ   ⊙   PS SR
    replace key <SCLK> {[percent,	VoidSymbol,	U2C7C,	U02B2,		VoidSymbol,VoidSymbol,		Scroll_Lock,Print	]	};	// %  ⱼʲ       SL PS 
    replace key <PAUS> {[ampersand,	U02BC,		U2096,	U1D4F,		VoidSymbol,VoidSymbol,		Pause,		Break	]	};	// &ʼ ₖᵏ       P  BR 

    
    // режим F1‒F12 как оверлей;  в консоли переключение оверлеев не работает, но там и F1‒F12 на 3-4 уровнях не работало
    key <AB11> { type="CTRL+ALT", 	[F1, F1,	F1, F1, 	XF86_Switch_VT_1 	]};
    key <JPCM> { type="CTRL+ALT", 	[F2, F2,	F2, F2, 	XF86_Switch_VT_2 	]};                                    
    key <I120> { type="CTRL+ALT", 	[F3, F3,	F3, F3, 	XF86_Switch_VT_3 	]};
    key <AE13> { type="CTRL+ALT", 	[F4, F4,	F4, F4, 	XF86_Switch_VT_4 	]};
                                                                                   
    key <I149> { type="CTRL+ALT", 	[F5, F5,	F5, F5, 	XF86_Switch_VT_5 	]};
    key <I154> { type="CTRL+ALT", 	[F6, F6,	F6, F6, 	XF86_Switch_VT_6 	]};
    key <I168> { type="CTRL+ALT", 	[F7, F7,	F7, F7, 	XF86_Switch_VT_7 	]};         
    key <I178> { type="CTRL+ALT", 	[F8, F8,	F8, F8, 	XF86_Switch_VT_8 	]};         
    
    key <I183> { type="CTRL+ALT", 	[F9, F9,	F9, F9, 	XF86_Switch_VT_9 	]};
    key <I184> { type="CTRL+ALT", 	[F10,F10,	F10,F10, 	XF86_Switch_VT_10 	]};
    key <I221> { type="CTRL+ALT", 	[F11,F11,	F11,VoidSymbol, 		XF86_Switch_VT_11 	]};         
    key <I222> { type="CTRL+ALT", 	[F12,F12,	F12,Overlay2_Enable, 	XF86_Switch_VT_12 	]};  // 4 = режим цифр


    //key <AC05> {[	p,		P,		Greek_pi,	Greek_PI,		U220F,		U2740,			U209A,		U1D56	]}; // p P π Π   ∏ ❀ ₚ ᵖ   













